<?php

namespace App\Services\Midtrans;

use Midtrans\Snap;
use App\Models\Keranjang;
use App\Models\pesanan;
use Illuminate\Support\Facades\Auth;

class CreateSnapTokenService extends Midtrans
{
    protected $pembayaran;

    public function __construct($pembayaran)
    {
        parent::__construct();

        $this->pembayaran = $pembayaran;
    }

    public function getSnapToken()
    {
        $pesanans = pesanan::with(['Produk'])->where('pembayaran_id', $this->pembayaran->id)->get();
        $item_details = [];
        foreach ($pesanans as $item) {
            $item_details[] = [
                'id' => $item->uuid,
                'price' => $item->Produk->harga_diskon,
                'quantity' => $item->kuantitas,
                'name' => $item->Produk->nm_produk
            ];
        }
        $item_details[] = [
            'id' => $this->pembayaran->Pengiriman->id,
            'price' => $this->pembayaran->Pengiriman->ongkir,
            'name' => 'Ongkir',
            'quantity' => 1
        ];
        $params = [
            'transaction_details' => [
                'order_id' => $this->pembayaran->uuid,
                'gross_amount' => $this->pembayaran->total_harga,
            ],
            'item_details' => $item_details,
            'customer_details' => [
                'first_name' => auth()->user()->name,
                'email' => auth()->user()->email,
                'phone' => auth()->user()->phone,
            ]
        ];

        $snapToken = Snap::getSnapToken($params);

        return $snapToken;
    }
}
