<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use App\Models\Kategori;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function index()
    {
        $kategori = Kategori::get()->take(4);
        $setting = Setting::first();
        return view('login.index', compact('setting', 'kategori'));
    }

    public function authenticate(Request $request)
    {
        $credentials =  $request->validate([
            'email' => 'required|email:dns',
            'password' => 'required',

        ]);
        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();

            if (Auth::user()->is_admin == 1) {
                return redirect('/dashboard')->with('success', 'Login Berhasil, Halo' . ' ' .  auth()->user()->name);
            } else {
                return redirect('/')->with('success', 'Login Berhasil, Halo' . ' ' .  auth()->user()->name);
            }
        }
        return back()->with('loginError', 'Login failed!');
    }
    public function logout()
    {
        auth::logout();

        request()->session()->invalidate();
        request()->session()->regenerateToken();
        return redirect('/')->with('logout', 'Berhasil Logout!');;
    }
}
