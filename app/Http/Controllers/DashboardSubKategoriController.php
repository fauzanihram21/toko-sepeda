<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use App\Models\Kategori;
use App\Models\SubKategori;
use Illuminate\Http\Request;

class DashboardSubKategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $setting = Setting::first();
        $keyword = $request->keyword;
        $subkategori = SubKategori::where(function ($q) use ($keyword) {
            $q->where('nm_sub_kategori', 'LIKE', '%' . $keyword . '%');
            $q->orWhereHas('kategori', function ($q) use ($keyword) {
                $q->where('nm_kategori', 'LIKE', '%' . $keyword . '%');
            });
        })->paginate(4);
        return view('dashboard.sub_kategori.index', compact(
            'subkategori',
            'keyword',
            'setting'
        ));
    }

    /** 
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $setting = Setting::first();
        $kategori = Kategori::all();
        $tambah = SubKategori::all();
        return view('dashboard.sub_kategori.create', compact(
            'tambah',
            'kategori',
            'setting'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nm_sub_kategori' => 'required|string',
            'kategori_id' => 'required|string',
        ]);

        SubKategori::create($validatedData);


        return redirect('dashboard/subkategori')->with('successcreate', 'Berhasil Menambahkan Data!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SubKategori  $subKategori
     * @return \Illuminate\Http\Response
     */
    public function show(SubKategori $subKategori)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SubKategori  $subKategori
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $setting = Setting::first();
        $kategori = Kategori::all();
        $subkategori = SubKategori::find($id);
        return view('dashboard.sub_kategori.edit', compact(
            'subkategori',
            'kategori',
            'setting'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SubKategori  $subKategori
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $id)
    {
        $update = SubKategori::find($id);
        $validatedData = $request->validate([
            'nm_sub_kategori' => 'required|string',
            'kategori_id' => 'required|string',
        ]);

        $update->update($validatedData);


        return redirect('dashboard/subkategori')->with('successupdate', 'Berhasil Mengupdate Data!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SubKategori  $subKategori
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $hapus = SubKategori::find($id);
        $hapus->delete();
        return redirect('dashboard/subkategori')->with('successdelete', 'Delete Successfull!');
    }
}
