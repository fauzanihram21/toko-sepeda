<?php

namespace App\Http\Controllers;

use App\Models\Fitur;
use App\Models\Setting;
use Illuminate\Http\Request;

class DashboardHalamanFiturController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $setting = Setting::first();
        $keyword = $request->keyword;
        $fitur = Fitur::where(function ($q) use ($keyword) {
            $q->where('heading', 'LIKE', '%' . $keyword . '%');
            $q->orwhere('sub_heading', 'LIKE', '%' . $keyword . '%');
        })->paginate(4);
        return view('dashboard.halaman.fitur.index', compact(
            'fitur',
            'keyword',
            'setting'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $setting = Setting::first();
        $tambah = Fitur::all();
        return view('dashboard.halaman.fitur.create', compact(
            'tambah',
            'setting'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'heading' => 'required|string',
            'sub_heading' => 'required|string',
        ]);



        $fitur = $request->all();

        if (isset($request->gambar)) {
            $file = $request->file('gambar');
            $nama_file = time() . str_replace(" ", " ", $file->getClientOriginalName());
            $file->move('storage/fitur', $nama_file);
            $fitur['gambar'] = $nama_file;
        }

        Fitur::create($fitur);

        return redirect('dashboard/halaman/fitur')->with('successcreate', 'Berhasil Menambahkan Data!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Fitur  $fitur
     * @return \Illuminate\Http\Response
     */
    public function show(Fitur $fitur)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Fitur  $fitur
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $setting = Setting::first();
        $fitur = Fitur::find($id);
        return view('dashboard.halaman.fitur.edit', compact(
            'fitur',
            'setting'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Fitur  $fitur
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tambah = Fitur::find($id);
        $validatedData = $request->validate([
            'heading' => 'required|string',
            'sub_heading' => 'required|string',
        ]);



        $fitur = $request->all();

        if (isset($request->gambar)) {
            $file = $request->file('gambar');
            $nama_file = time() . str_replace(" ", " ", $file->getClientOriginalName());
            $file->move('storage/fitur', $nama_file);
            $fitur['gambar'] = $nama_file;
        }

        $tambah->update($fitur);

        return redirect('dashboard/halaman/fitur')->with('successupdate', 'Berhasil Mengupdate Data!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Fitur  $fitur
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tambah = Fitur::find($id);
        $tambah->delete();
        $tambah->delete();
        if (file_exists(public_path('storage/fitur/' . $tambah->gambar))) {
            unlink(public_path('storage/fitur/' . $tambah->gambar));
        }
        return redirect('dashboard/halaman/fitur')->with('successdelete', 'Delete Successfull!');
    }
}
