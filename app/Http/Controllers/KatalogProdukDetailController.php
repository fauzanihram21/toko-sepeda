<?php

namespace App\Http\Controllers;

use App\Models\BalasKomentar;
use App\Models\Produk;
use App\Models\Setting;
use App\Models\Kategori;
use App\Models\SubKategori;
use Illuminate\Http\Request;
use App\Models\komentar;
use App\Models\Rating;

class KatalogProdukDetailController extends Controller
{
    public function index($uuid_produk)
    {
        $setting = Setting::first();
        $kategori = Kategori::get()->take(4);
        $produk = Produk::with(['SubKategori.Kategori', 'ProdukVariasi', 'Spesifikasi'])->where('uuid', $uuid_produk)->first();
        $komentar = Komentar::with(['BalasKomentar'])->where('produk_id', Produk::findByUuid($uuid_produk)->id)->get();
        $rating = Rating::where('produk_id', Produk::findByUuid($uuid_produk)->id)->get();
        $totalRating = Rating::where('produk_id', Produk::findByUuid($uuid_produk)->id)->sum('total_rating');
        $banyakRating = Rating::where('produk_id', Produk::findByUuid($uuid_produk)->id)->count();

        $rating1 = Rating::where('total_rating', '1')->count();
        $rating2 = Rating::where('total_rating', '2')->count();
        $rating3 = Rating::where('total_rating', '3')->count();
        $rating4 = Rating::where('total_rating', '4')->count();
        $rating5 = Rating::where('total_rating', '5')->count();


        $homekomentar = Komentar::with(['BalasKomentar' => function ($q) {
            $q->when(isset(auth()->user()->id) ?? false, function ($q) {
                $q->where('user_id', auth()->user()->id);
            });
        }])->where('produk_id', Produk::findByUuid($uuid_produk)->id)->when(isset(auth()->user()->id) ?? false, function ($q) {
            $q->where('user_id', auth()->user()->id);
        })->first();


        $sudah_beli = true;
        $cek = \App\Models\Pesanan::where('produk_id', $produk->id)->whereHas('Pembayaran', function ($q) {
            @$q->where('user_id', auth()->user()->id);
        })->first();
        if ($cek == null) {
            $sudah_beli = false;
        }

        return view('front.katalog_detail.index', compact(
            'setting',
            'homekomentar',
            'totalRating',
            'banyakRating',
            'kategori',
            'komentar',
            'rating',
            'rating1',
            'rating2',
            'rating3',
            'rating4',
            'rating5',
            'produk',
            'sudah_beli'
        ));
    }
}
