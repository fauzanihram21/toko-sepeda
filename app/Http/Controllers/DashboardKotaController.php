<?php

namespace App\Http\Controllers;

use App\Models\Kota;
use App\Models\Setting;
use App\Models\Provinsi;
use Illuminate\Http\Request;

class DashboardKotaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $setting = Setting::first();
        $keyword = $request->keyword;
        $kota = Kota::where(function ($q) use ($keyword) {
            $q->where('nm_kota', 'LIKE', '%' . $keyword . '%');
            $q->orWhereHas('provinsi', function ($q) use ($keyword) {
                $q->where('nm_provinsi', 'LIKE', '%' . $keyword . '%');
            });
        })->paginate(4);
        return view('dashboard.kota.index', compact(
            'kota',
            'keyword',
            'setting'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $setting = Setting::first();
        $provinsi = Provinsi::all();
        $tambah = Kota::all();
        return view('dashboard.kota.create', compact(
            'tambah',
            'provinsi',
            'setting'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nm_kota' => 'required|string',
            'provinsi_id' => 'required|string',

        ]);
        Kota::create($validatedData);


        return redirect('dashboard/kota')->with('successcreate', 'Berhasil Menambahkan Data!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Kota  $kota
     * @return \Illuminate\Http\Response
     */
    public function show(Kota $kota)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Kota  $kota
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $setting = Setting::first();
        $provinsi = Provinsi::all();
        $kota = Kota::find($id);
        return view('dashboard.kota.edit', compact(
            'kota',
            'provinsi',
            'setting'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Kota  $kota
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $update = Kota::find($id);
        $validatedData = $request->validate([
            'nm_kota' => 'required|string',
            'provinsi_id' => 'required|string',
        ]);

        $update->update($validatedData);


        return redirect('dashboard/kota')->with('successupdate', 'Berhasil Mengupdate Data!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Kota  $kota
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $hapus = Kota::find($id);
        $hapus->delete();
        return redirect('dashboard/kota')->with('successdelete', 'Delete Successfull!');
    }
}
