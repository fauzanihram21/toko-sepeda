<?php

namespace App\Http\Controllers;

use App\Models\Banner;
use App\Models\Brand;
use App\Models\Fitur;
use App\Models\Kategori;
use App\Models\Merk;
use App\Models\Produk;
use App\Models\Setting;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $kategori = Kategori::get()->take(4);
        $banner = Banner::get()->take(3);
        $fitur = Fitur::get()->take(4);
        $merk = Merk::get()->take(6);
        $setting = Setting::first();
        $produk = Produk::where('diskon', '>', '0')->get()->take(8);



        $sepedadiskon = Produk::where('diskon', '>', 0)->whereHas('SubKategori', function ($q) {
            $q->whereHas('Kategori', function ($q) {
                $q->where('nm_kategori', 'LIKE', '%sepeda%');
            });
        })->get()->take(3);
        $sepeda = Produk::whereHas('SubKategori', function ($q) {
            $q->whereHas('Kategori', function ($q) {
                $q->where('nm_kategori', 'LIKE', '%sepeda%');
            });
        })->get()->take(4);
        $aksesoris = Produk::whereHas('SubKategori', function ($q) {
            $q->whereHas('Kategori', function ($q) {
                $q->where('nm_kategori', 'LIKE', '%aksesoris%');
            });
        })->get()->take(4);
        $apparel = Produk::whereHas('SubKategori', function ($q) {
            $q->whereHas('Kategori', function ($q) {
                $q->where('nm_kategori', 'LIKE', '%apparel%');
            });
        })->get()->take(4);
        $parts = Produk::whereHas('SubKategori', function ($q) {
            $q->whereHas('Kategori', function ($q) {
                $q->where('nm_kategori', 'LIKE', '%parts%');
            });
        })->get()->take(4);
        $produkSepeda = $sepeda->map(function ($prd) {
            if ($prd->diskon) {
                $diskon = $prd->diskon / 100 * $prd->harga;
                $prd->harga_diskon = $prd->harga - $diskon;
            }
            return $prd;
        });
        $produkAksesoris = $aksesoris->map(function ($prd) {
            if ($prd->diskon) {
                $diskon = $prd->diskon / 100 * $prd->harga;
                $prd->harga_diskon = $prd->harga - $diskon;
            }
            return $prd;
        });
        $produkApparel = $apparel->map(function ($prd) {
            if ($prd->diskon) {
                $diskon = $prd->diskon / 100 * $prd->harga;
                $prd->harga_diskon = $prd->harga - $diskon;
            }
            return $prd;
        });
        $produkParts = $parts->map(function ($prd) {
            if ($prd->diskon) {
                $diskon = $prd->diskon / 100 * $prd->harga;
                $prd->harga_diskon = $prd->harga - $diskon;
            }
            return $prd;
        });
        return view('front.index', compact(
            'setting',
            'produkSepeda',
            'produkAksesoris',
            'produkApparel',
            'produkParts',
            'kategori',
            'banner',
            'merk',
            'fitur',
            'produk',
            'sepeda',
            'aksesoris',
            'apparel',
            'parts',
            'sepedadiskon'
        ));
    }
}
