<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Produk;
use App\Models\Setting;
use App\Models\Kategori;
use App\Models\Pembayaran;
use App\Models\Pengiriman;
use App\Models\Rating;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index(Request $request)
    {
        $setting = Setting::first();
        $keyword = $request->keyword;



        // $customer = User::where('is_admin', '0')->get();
        $user = User::where('is_admin', '0')->paginate(4);
        $kategoris = Kategori::with(['Produk'])->get();
        $rating = Rating::get();

        $pembayaran = Pembayaran::with(['Pengiriman', 'Pesanan.Produk'])->where(function ($q) use ($keyword) {
            $q->where('no_pesanan', 'LIKE', '%' . $keyword . '%');
            $q->orwhere('no_invoice', 'LIKE', '%' . $keyword . '%');
            $q->orwhere('total_harga', 'LIKE', '%' . $keyword . '%');
            $q->orWhereHas('pengiriman', function ($q) use ($keyword) {
                $q->where('alamat', 'LIKE', '%' . $keyword . '%');
                $q->orwhere('nama_penerima', 'LIKE', '%' . $keyword . '%');
            });
        })->Paginate(4);

        $prds = Produk::where(function ($q) use ($keyword) {
            $q->where('nm_produk', 'LIKE', '%' . $keyword . '%');
            $q->orWhere('deskripsi', 'LIKE', '%' . $keyword . '%');
            $q->orWhere('harga', 'LIKE', '%' . $keyword . '%');
            $q->orWhere('diskon', 'LIKE', '%' . $keyword . '%');
            $q->orWhere('berat', 'LIKE', '%' . $keyword . '%');

            $q->orWhereHas('subkategori', function ($q) use ($keyword) {
                $q->where('nm_sub_kategori', 'LIKE', '%' . $keyword . '%');
            });

            $q->orWhereHas('merk', function ($q) use ($keyword) {
                $q->where('nm_merk', 'LIKE', '%' . $keyword . '%');
            });

            $q->orWhereHas('spesifikasi', function ($q) use ($keyword) {
                $q->where('nm_spesifikasi', 'LIKE', '%' . $keyword . '%');
                $q->orWhere('deskripsi', 'LIKE', '%' . $keyword . '%');
            });

            $q->orWhereHas('produkvariasi', function ($q) use ($keyword) {
                $q->where('stock', 'LIKE', '%' . $keyword . '%');
                $q->orWhere('warna', 'LIKE', '%' . $keyword . '%');
            });
        })->Paginate(4);



        $chartPenjualan = [];
        for ($i = 1; $i <= 12; $i++) {
            $chartPenjualan[] = Pembayaran::where('payment_status', 'dibayar')->whereMonth('created_at', $i)->sum('total_harga');
            $totalOngkir = Pengiriman::whereHas('Pembayaran', function ($q) use ($i) {
                $q->where('payment_status', 'dibayar');
                $q->whereMonth('created_at', $i);
            })->sum('ongkir');
            $chartPenjualan[$i - 1] -= $totalOngkir;
        }
        $totalPenjualan = Pembayaran::where('payment_status', 'dibayar')->sum('total_harga');
        $totalOngkir = Pengiriman::whereHas('Pembayaran', function ($q) {
            $q->where('payment_status', 'dibayar');
        })->sum('ongkir');
        $totalPenjualan = $totalPenjualan - $totalOngkir;



        return view('dashboard.index', compact(
            'setting',
            'user',
            'prds',
            // 'customer',
            'kategoris',
            'pembayaran',
            'totalPenjualan',
            'keyword',
            'rating',
            'chartPenjualan'
        ));
    }
}
