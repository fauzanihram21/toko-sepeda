<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Setting;
use Illuminate\Http\Request;

class DashboardProfileController extends Controller
{
    public function index(Request $request)
    {
        $setting = Setting::first();
        $user = User::where('id', auth()->user()->id)->first();
        return view('dashboard.admin.profile', compact(
            'user',
            'setting'
        ));
    }

    public function profile(Request $request)
    {
        $tambah = User::where('id', auth()->user()->id)->first();

        $user = $request->all();
        if (isset($request->avatar)) {
            $file = $request->file('avatar');
            $nama_file = time() . str_replace(" ", " ", $file->getClientOriginalName());
            $file->move('avatar', $nama_file);
            $user['avatar'] = $nama_file;
        }
            $tambah->update($user);


        return redirect('/dashboardprofile')->with('successcreate', 'Create Successfull!');
    }
}
