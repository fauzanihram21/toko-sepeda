<?php

namespace App\Http\Controllers;

use App\Models\Produk;
use App\Models\Setting;
use App\Models\Kategori;
use App\Models\Keranjang;
use App\Models\Pembayaran;
use App\Models\DaftarAlamat;
use App\Models\pesanan;
use App\Models\ProdukVariasi;
use Illuminate\Http\Request;

class RiwayatPesananController extends Controller
{
    public function selesaipembayaran(Request $request, $uuid)
    {
        $setting = Setting::first();
        $kategori = Kategori::all();
        $pembayaran = Pembayaran::with(['Pengiriman', 'Pesanan.Produk'])->where('uuid', $uuid)->first();
        $pengiriman = $pembayaran->Pengiriman;
        $pesanan = $pembayaran->Pesanan;

        return view('front.selesaipembayaran.index', compact(
            'kategori',
            'setting',
            'pengiriman',
            'pembayaran',
            'pesanan'
        ));
    }

    public function riwayatpesanan(Request $request)
    {
        $setting = Setting::first();
        $kategori = Kategori::all();
        $pembayaran = Pembayaran::where('user_id', auth()->user()->id)->with(['Pengiriman', 'Pesanan.Produk.ProdukVariasi'])->paginate(4);

        return view('front.riwayatpesanan.index', compact(
            'kategori',
            'setting',
            'pembayaran'

        ));
    }

    public function detailriwayatpesanan(Request $request, $id)
    {
        $setting = Setting::first();
        $kategori = Kategori::all();
        $pembayaran = Pembayaran::where('id', $id)->with(['Pengiriman', 'Pesanan.Produk.ProdukVariasi'])->first();



        return view('front.riwayatpesanan.detail', compact(
            'kategori',
            'setting',
            'pembayaran'

        ));
    }

    public function updatestatusselesai(Request $request, $id)
    {
        $request->validate([
            'status' => 'required',
        ]);

        $pembayaran = Pembayaran::find($id);
        $pembayaran->update(['status' => $request->status]);
        return redirect('/riwayat/pembayaran');
    }

    public function kembalistatuscancel(Request $request, $id)
    {
        $request->validate([
            'status' => 'required',
            'payment_status' => 'required',

        ]);

        $pembayaran = Pembayaran::find($id);

        foreach ($pembayaran->Pesanan as $psn) {
            $pv = ProdukVariasi::find($psn->produk_variasi_id);
            $pv->update(['stock' => $pv->stock + $psn->kuantitas]);
        }

        $pembayaran->update(['status' => $request->status, 'payment_status' => $request->payment_status]);




        return redirect('/riwayat/pembayaran');
    }
    public function hapusriwayatpesanan($id)
    {
        $hapus = Pembayaran::where('id', $id)->where('user_id', auth()->user()->id)->get();
        $hapus->delete();
        return redirect('/riwayat/pembayaran')->with('successdelete', 'Delete Successfull!');
    }
}
