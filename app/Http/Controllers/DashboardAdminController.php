<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Setting;
use Illuminate\Http\Request;

class DashboardAdminController extends Controller
{
    public function indexprofile(Request $request)
    {
        $setting = Setting::first();
        $user = User::where('id', auth()->user()->id)->first();
        return view('dashboard.admin.profile', compact(
            'user',
            'setting'
        ));
    }

    public function profile(Request $request)
    {
        $tambah = User::where('id', auth()->user()->id)->first();

        $user = $request->all();
        if (isset($request->avatar)) {
            $file = $request->file('avatar');
            $nama_file = time() . str_replace(" ", " ", $file->getClientOriginalName());
            $file->move('storage/avatar', $nama_file);
            $user['avatar'] = $nama_file;
        }
        $tambah->update($user);


        return redirect('/dashboardprofile')->with('successcreate', 'Berhasil Menambahkan Data!');
    }

    public function indexchangepassword(Request $request)
    {
        $setting = Setting::first();
        $user = User::where('id', auth()->user()->id)->first();
        return view('dashboard.admin.changepassword', compact(
            'user',
            'setting'
        ));
    }

    public function changepassword(Request $request)
    {
        $tambah = User::where('id', auth()->user()->id)->first();
        if (password_verify($request->current_password, $tambah->password)) {
            $tambah->update([
                'password' => bcrypt($request->password)
            ]);
            return redirect('/dashboardchangepassword')->with('successcreate', 'Create Successfull!');
        } else {
            return redirect('/dashboardchangepassword')->with('errorcreate', 'Error!');
        }
    }
}
