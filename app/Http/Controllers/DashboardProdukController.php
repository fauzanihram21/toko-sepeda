<?php

namespace App\Http\Controllers;

use App\Models\Merk;
use App\Models\Produk;
use App\Models\Setting;
use App\Models\Spesifikasi;
use App\Models\SubKategori;
use Illuminate\Http\Request;
use App\Models\ProdukVariasi;
use illuminate\Support\Facades\file;

class DashboardProdukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $setting = Setting::first();
        $keyword = $request->keyword;
        $produk = Produk::where(function ($q) use ($keyword) {
            $q->where('nm_produk', 'LIKE', '%' . $keyword . '%');
            $q->orWhere('deskripsi', 'LIKE', '%' . $keyword . '%');
            $q->orWhere('harga', 'LIKE', '%' . $keyword . '%');
            $q->orWhere('diskon', 'LIKE', '%' . $keyword . '%');
            $q->orWhere('berat', 'LIKE', '%' . $keyword . '%');

            $q->orWhereHas('subkategori', function ($q) use ($keyword) {
                $q->where('nm_sub_kategori', 'LIKE', '%' . $keyword . '%');
            });

            $q->orWhereHas('merk', function ($q) use ($keyword) {
                $q->where('nm_merk', 'LIKE', '%' . $keyword . '%');
            });

            $q->orWhereHas('spesifikasi', function ($q) use ($keyword) {
                $q->where('nm_spesifikasi', 'LIKE', '%' . $keyword . '%');
                $q->orwhere('deskripsi', 'LIKE', '%' . $keyword . '%');
            });

            $q->orWhereHas('produkvariasi', function ($q) use ($keyword) {
                $q->where('stock', 'LIKE', '%' . $keyword . '%');
                $q->orwhere('warna', 'LIKE', '%' . $keyword . '%');
            });
        })->paginate(4);
        return view('dashboard.produk.index', compact(
            'produk',
            'keyword',
            'setting'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $setting = Setting::first();
        $subkategori = SubKategori::all();
        $spesifikasi = Spesifikasi::all();
        $produkvariasi = ProdukVariasi::all();
        $merk = Merk::all();


        $tambah = Produk::all();
        return view('dashboard.produk.create', compact(
            'tambah',
            'subkategori',
            'spesifikasi',
            'produkvariasi',
            'merk',
            'setting'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nm_produk' => 'required|string',
            'deskripsi' => 'required|string',
            'harga' => 'required|string',
            'diskon' => 'integer|nullable',
            'berat' => 'required|integer',
            'sub_kategori_id' => 'required|string',
            'merk_id' => 'required|string',




        ]);



        $produk = $request->all();

        if (isset($request->gambar)) {
            $file = $request->file('gambar');
            $nama_file = time() . str_replace(" ", " ", $file->getClientOriginalName());
            $file->move('storage/produk', $nama_file);
            $produk['gambar'] = $nama_file;
        }

        $produk['harga'] = str_replace('.', '', $produk['harga']);
        $produk['harga'] = str_replace(',', '.', $produk['harga']);

        $produk = Produk::create($produk);

        // if(count($request->spesifikasi) > 0)

        foreach ($request->spesifikasi as $index => $spek) {

            Spesifikasi::create([
                'nm_spesifikasi' => $spek['nm_spesifikasi'],
                'deskripsi' => $spek['deskripsi'],
                'produk_id' => $produk->id

            ]);
        }

        foreach ($request->produkvariasi as $idx => $prod) {
            if (isset($prod['gambar'])) {
                $file = $prod['gambar'];
                $nama_file = time() . "produk_variasi_" . str_replace(" ", "_", $file->getClientOriginalName());
                $file->move('storage/produk_variasi', $nama_file);
                $prod['gambar'] = $nama_file;
            }
            ProdukVariasi::create([
                'stock' => $prod['stock'],
                'warna' => $prod['warna'],
                'gambar' => $nama_file,
                'produk_id' => $produk->id
            ]);
        }

        return redirect('dashboard/produk')->with('successcreate', 'Berhasil Menambahkan Data!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Produk  $produk
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $setting = Setting::first();
        $produk = Produk::with(['SubKategori', ''])->where('id', $id)->first();
        return view('dashboard.produk.detail', compact(
            'produk',
            'spesifikasi',
            'subkategori',
            'produkvariasi',
            'merk',
            'setting'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Produk  $produk
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $setting = Setting::first();
        $spesifikasi = Spesifikasi::all();
        $subkategori = SubKategori::all();
        $produkvariasi = ProdukVariasi::all();
        $merk = Merk::all();



        $produk = Produk::with(['Spesifikasi'])->where('id', $id)->first();
        return view('dashboard.produk.edit', compact(
            'produk',
            'spesifikasi',
            'subkategori',
            'produkvariasi',
            'merk',
            'setting'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Produk  $produk
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tambah = Produk::find($id);
        $validatedData = $request->validate([
            'nm_produk' => 'required|string',
            'deskripsi' => 'required|string',
            'harga' => 'required|string',
            'diskon' => 'string|nullable',
            'berat' => 'required|string',
            'sub_kategori_id' => 'required|string',
            'merk_id' => 'required|string',




        ]);



        $produk = $request->all();

        if (isset($request->gambar)) {
            $file = $request->file('gambar');
            $nama_file = time() . str_replace(" ", " ", $file->getClientOriginalName());
            $file->move('storage/produk', $nama_file);
            $produk['gambar'] = $nama_file;
        }

        $produk['harga'] = str_replace('.', '', $produk['harga']);
        $produk['harga'] = str_replace(',', '.', $produk['harga']);

        $tambah->update($produk);

        Spesifikasi::where('produk_id', $tambah->id)->delete();


        foreach ($request->spesifikasi as $index => $spek) {

            Spesifikasi::create([
                'nm_spesifikasi' => $spek['nm_spesifikasi'],
                'deskripsi' => $spek['deskripsi'],
                'produk_id' => $tambah->id
                // 'gambar' => $spek['gambar']

            ]);
        }

        $existings = collect($request->produkvariasi)->filter(function ($i) {
            return isset($i['produk_variasi_id']);
        })->map(function ($i) {
            return $i['produk_variasi_id'];
        });
        ProdukVariasi::where('produk_id', $tambah->id)->whereNotIn('id', $existings->toArray())->delete();

        // dd($request->produkvariasi);
        foreach ($request->produkvariasi as $idx => $prod) {
            if ($prod['produk_variasi_id'] != null) {
                $var = ProdukVariasi::find($prod['produk_variasi_id']);
                $var->update($prod);

                if (isset($prod['gambar'])) {
                    $file = $prod['gambar'];
                    $nama_file = time() . "produk_variasi_" . str_replace(" ", "_", $file->getClientOriginalName());
                    $file->move('storage/produk_variasi', $nama_file);
                    $prod['gambar'] = $nama_file;

                    $var->update([
                        'gambar' => $nama_file,
                    ]);
                }
            } else {
                if (isset($prod['gambar'])) {
                    $file = $prod['gambar'];
                    $nama_file = time() . "produk_variasi_" . str_replace(" ", "_", $file->getClientOriginalName());
                    $file->move('storage/produk_variasi', $nama_file);
                    $prod['gambar'] = $nama_file;
                }
                ProdukVariasi::create([
                    'stock' => $prod['stock'],
                    'warna' => $prod['warna'],
                    'gambar' => $nama_file,

                    'produk_id' => $tambah->id
                ]);
            }
        }


        return redirect('dashboard/produk')->with('successupdate', 'Berhasil Mengupdate Data!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Produk  $produk
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $hapus = Produk::find($id);
        $delete = ProdukVariasi::find($id);
        $delete->delete();
        $hapus->delete();
        if (file_exists(public_path('storage/produk/' . $hapus->gambar))) {
            unlink(public_path('storage/produk/' . $hapus->gambar));
        }
        if (file_exists(public_path('storage/produk_variasi/' . $delete->gambar))) {
            unlink(public_path('storage/produk_variasi/' . $delete->gambar));
        }
        return redirect('dashboard/produk')->with('successdelete', 'Delete Successfull!');
    }
}
