<?php

namespace App\Http\Controllers;

use App\Models\Banner;
use App\Models\Setting;
use Illuminate\Http\Request;

class DashboardHalamanBannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $setting = Setting::first();
        $keyword = $request->keyword;
        $banner = Banner::where(function ($q) use ($keyword) {
            $q->where('heading', 'LIKE', '%' . $keyword . '%');
            $q->orwhere('sub_heading', 'LIKE', '%' . $keyword . '%');
        })->paginate(4);
        return view('dashboard.halaman.banner.index', compact(
            'banner',
            'keyword',
            'setting'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $setting = Setting::first();
        $tambah = Banner::all();
        return view('dashboard.halaman.banner.create', compact(
            'tambah',
            'setting'

        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'heading' => 'required|string',
            'sub_heading' => 'required|string',
        ]);



        $banner = $request->all();

        if (isset($request->gambar)) {
            $file = $request->file('gambar');
            $nama_file = time() . str_replace(" ", " ", $file->getClientOriginalName());
            $file->move('storage/banner', $nama_file);
            $banner['gambar'] = $nama_file;
        }

        Banner::create($banner);

        return redirect('dashboard/halaman/banner')->with('successcreate', 'Berhasil Menambahkan Data!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function show(Banner $banner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $setting = Setting::first();
        $banner = Banner::find($id);
        return view('dashboard.halaman.banner.edit', compact(
            'banner',
            'setting'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tambah = Banner::find($id);
        $validatedData = $request->validate([
            'heading' => 'required|string',
            'sub_heading' => 'required|string',
        ]);



        $banner = $request->all();

        if (isset($request->gambar)) {
            $file = $request->file('gambar');
            $nama_file = time() . str_replace(" ", " ", $file->getClientOriginalName());
            $file->move('storage/banner', $nama_file);
            $banner['gambar'] = $nama_file;
        }

        $tambah->update($banner);

        return redirect('dashboard/halaman/banner')->with('successupdate', 'Berhasil Mengupdate Data!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tambah = Banner::find($id);
        $tambah->delete();
        $tambah->delete();
        if (file_exists(public_path('storage/banner/' . $tambah->gambar))) {
            unlink(public_path('storage/banner/' . $tambah->gambar));
        }
        return redirect('dashboard/halaman/banner')->with('successdelete', 'Delete Successfull!');
    }
}
