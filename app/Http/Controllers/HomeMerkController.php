<?php

namespace App\Http\Controllers;

use App\Models\Merk;
use App\Models\Produk;
use App\Models\Setting;
use App\Models\Kategori;
use Illuminate\Http\Request;
use App\Models\ProdukVariasi;

class HomeMerkController extends Controller
{
    public function index(Request $request)
    {
        $setting = Setting::first();
        $merk = Merk::get();
        $kategori = Kategori::get()->take(4);
        return view('front.merk.index', compact(
            'merk',
            'kategori',
            'setting'
        ));
    }
    public function detail(Request $request, $uuid)
    {
        $setting = Setting::first();
        $kategori = Kategori::get()->take(4);
        $keyword = $request->keyword;
        $produkvariasi = ProdukVariasi::get();
        $merk = Merk::findByUuid($uuid);
        $produk = Produk::where('merk_id', $merk->id)->whereHas('ProdukVariasi', function ($q) use ($request) {
            $q->where('stock', '>', 0);
            $q->when(isset($request->produk_variasi_id), function ($q) use ($request) {
                $q->where('id', $request->produk_variasi_id);
            });
        })->when(isset($request->merk_id), function ($q) use ($request) {
            $q->where('merk_id', $request->merk_id);
        })->when(isset($request->urut_harga), function ($q) use ($request) {
            if ($request->urut_harga == 'tinggi') {
                $q->orderBy('harga', 'DESC');
            } else {
                $q->orderBy('harga');
            }
        })->where('nm_produk', 'LIKE', '%' . $keyword . '%')
            ->paginate(4);
        $kategori = Kategori::get()->take(4);
        return view('front.merk.detail', compact(
            'merk',
            'keyword',
            'produkvariasi',
            'kategori',
            'produk',
            'setting'
        ));
    }
}
