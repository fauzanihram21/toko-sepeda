<?php

namespace App\Http\Controllers;

use App\Models\Produk;
use App\Models\pesanan;
use App\Models\Setting;
use App\Models\Pembayaran;
use App\Models\Pengiriman;
use Illuminate\Http\Request;

class DashboardGrafikController extends Controller
{
    public function index()
    {
        $setting = Setting::first();
        $produk = Produk::get();
        $chartPendapatan = [];
        for ($i = 1; $i <= 12; $i++) {
            $chartPendapatan[] = Pembayaran::where('payment_status', 'dibayar')->whereMonth('created_at', $i)->sum('total_harga');
            $totalOngkir = Pengiriman::whereHas('Pembayaran', function ($q) use ($i) {
                $q->where('payment_status', 'dibayar');
                $q->whereMonth('created_at', $i);
            })->sum('ongkir');
            $chartPendapatan[$i - 1] -= $totalOngkir;
        }
        $totalPendapatan = Pembayaran::where('payment_status', 'dibayar')->sum('total_harga');
        $totalOngkir = Pengiriman::whereHas('Pembayaran', function ($q) {
            $q->where('payment_status', 'dibayar');
        })->sum('ongkir');
        $totalPendapatan = $totalPendapatan - $totalOngkir;

        $totalPenjualan = pesanan::sum('kuantitas');


        return view('dashboard.grafik.index', compact(
            'setting',
            'produk',
            'chartPendapatan',
            'totalPenjualan',
            'totalPendapatan'
        ));
    }
}
