<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Setting;
use App\Models\Pembayaran;
use App\Models\DaftarAlamat;
use Illuminate\Http\Request;

class DashboardCustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $setting = Setting::first();

        $keyword = $request->keyword;

        $customer = User::where('name', 'LIKE', '%' . $keyword . '%')->where('is_admin', '0')->Paginate(2);
        return view('dashboard.customer.index', compact(
            'customer',
            'keyword',
            'setting'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $setting = Setting::first();
        $customer = User::find($id);
        $alamat = DaftarAlamat::where('user_id', $id)->get();
        $pembayaran = Pembayaran::where('user_id', $id)->with(['Pengiriman', 'Pesanan.Produk.ProdukVariasi'])->paginate(4);

        return view('dashboard.customer.detail', compact(
            'customer',
            'setting',
            'alamat',
            'pembayaran'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tambah = User::find($id);
        $tambah->delete();
        return redirect('dashboard/customer')->with('successdelete', 'Delete Successfull!');
    }
}
