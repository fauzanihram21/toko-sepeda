<?php

namespace App\Http\Controllers;

use App\Models\Merk;
use App\Models\Setting;
use Illuminate\Http\Request;

class DashboardMerkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $setting = Setting::first();
        $keyword = $request->keyword;
        $merk = Merk::where('nm_merk', 'LIKE', '%' . $keyword . '%')->Paginate(4);
        return view('dashboard.merk.index', compact(
            'merk',
            'keyword',
            'setting'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $setting = Setting::first();
        $tambah = Merk::all();
        return view('dashboard.merk.create', compact(
            'tambah',
            'setting'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nm_merk' => 'required|string',
        ]);

        $merk = $request->all();

        if (isset($request->gambar)) {
            $file = $request->file('gambar');
            $nama_file = time() . str_replace(" ", " ", $file->getClientOriginalName());
            $file->move('storage/merk', $nama_file);
            $merk['gambar'] = $nama_file;
        }


        $merk =  Merk::create($merk);


        return redirect('dashboard/merk')->with('successcreate', 'Berhasil Menambahkan Data!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Merk  $merk
     * @return \Illuminate\Http\Response
     */
    public function show(Merk $merk)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Merk  $merk
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $setting = Setting::first();
        $merk = Merk::find($id);
        return view('dashboard.merk.edit', compact(
            'merk',
            'setting'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Merk  $merk
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tambah = Merk::find($id);
        $validatedData = $request->validate([
            'nm_merk' => 'required|string',
        ]);

        $merk = $request->all();

        if (isset($request->gambar)) {
            $file = $request->file('gambar');
            $nama_file = time() . str_replace(" ", " ", $file->getClientOriginalName());
            $file->move('storage/merk', $nama_file);
            $merk['gambar'] = $nama_file;
        }


        $tambah->update($merk);


        return redirect('dashboard/merk')->with('successupdate', 'Berhasil Mengupdate Data!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Merk  $merk
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $hapus = Merk::find($id);
        $hapus->delete();
        if (file_exists(public_path('storage/merk/' . $hapus->gambar))) {
            unlink(public_path('storage/merk/' . $hapus->gambar));
        }
        return redirect('dashboard/merk')->with('successdelete', 'Delete Successfull!');
    }
}
