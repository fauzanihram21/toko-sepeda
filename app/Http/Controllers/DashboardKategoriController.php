<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use App\Models\Kategori;
use Illuminate\Http\Request;

class DashboardKategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $setting = Setting::first();
        $keyword = $request->keyword;
        $kategori = Kategori::where('nm_kategori', 'LIKE', '%' . $keyword . '%')->Paginate(4);
        $kategori->withpath('/dashboard/kategori');
        $kategori->appends($request->all());
        return view('dashboard.kategori.index', compact(
            'kategori',
            'keyword',
            'setting'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $setting = Setting::first();
        $tambah = Kategori::all();
        return view('dashboard.kategori.create', compact(
            'tambah',
            'setting'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nm_kategori' => 'required|string',
        ]);
        Kategori::create($validatedData);


        return redirect('dashboard/kategori')->with('successcreate', 'Berhasil Menambahkan Data!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Kategori  $kategori
     * @return \Illuminate\Http\Response
     */
    public function show(Kategori $kategori)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Kategori  $kategori
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $setting = Setting::first();
        $kategori = Kategori::find($id);
        return view('dashboard.kategori.edit', compact(
            'kategori',
            'setting'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Kategori  $kategori
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = Kategori::find($id);
        $validatedData = $request->validate([
            'nm_kategori' => 'required|string',
        ]);
        $update->update($validatedData);


        return redirect('dashboard/kategori')->with('successupdate', 'Berhasil Mengupdate Data!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Kategori  $kategori
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $hapus = Kategori::find($id);
        $hapus->delete();
        return redirect('dashboard/kategori')->with('successdelete', 'Delete Successfull!');
    }
}
