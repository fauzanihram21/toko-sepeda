<?php

namespace App\Http\Controllers;

use App\Models\Produk;
use App\Models\Setting;
use App\Models\Pembayaran;
use App\Models\Pengiriman;
use Illuminate\Http\Request;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


class DashboardReportController extends Controller
{
    // public function tes(Request $request)
    // {
    //     dd($request->all());
    // }
    public function penjualan(Request $request)
    {
        $setting = Setting::first();
        $keyword = $request->keyword;
        $date = $request->date;
        $month = $request->month;
        $year = $request->year;


        $penjualan = Pembayaran::where('status', 'selesai')->where(function ($q) use ($keyword) {
            $q->where('no_pesanan', 'LIKE', '%' . $keyword . '%');
            $q->orWhere('no_invoice', 'LIKE', '%' . $keyword . '%');
            $q->orWhere('total_harga', 'LIKE', '%' . $keyword . '%');

            $q->orWhereHas('pengiriman', function ($q) use ($keyword) {
                $q->where('alamat', 'LIKE', '%' . $keyword . '%');
                $q->orWhere('nama_penerima', 'LIKE', '%' . $keyword . '%');
            });

            $q->orWhereHas('pesanan', function ($q) use ($keyword) {
                $q->where('kuantitas', 'LIKE', '%' . $keyword . '%');
                $q->orWhere('sub_total', 'LIKE', '%' . $keyword . '%');
            });
        })->when(isset($request->date) ?? false, function ($q) use ($date) {
            $q->whereDate('created_at', $date);
        })->Paginate(4);
        return view('dashboard.report.penjualan', compact(
            'setting',
            'keyword',
            'penjualan'
        ));
    }

    public function excelharipenjualan(Request $request)
    {
        // dd($request->all());
        $date = $request->date;

        $pembayaran = Pembayaran::where('status', 'selesai')->with(['Pengiriman', 'Pesanan.Produk.ProdukVariasi'])->whereDate('created_at', $date)->get();
        $totalPendapatan = Pembayaran::where('status', 'selesai')->whereDate('created_at', $date)->sum('total_harga');
        $totalOngkir = Pengiriman::whereHas('Pembayaran', function ($q) use ($date) {
            $q->where('status', 'selesai');
            $q->whereDate('created_at', $date);
        })->sum('ongkir');
        $totalPendapatan = $totalPendapatan - $totalOngkir;

        // $pembayaran = Pembayaran::where('')->with(['Pengiriman', 'Pesanan.Produk'])->get();
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle('Laporan Penjualan');
        $row = 6;
        // $sheet->setCellValue('A1', 'Hello World !');
        $spreadsheet->getActiveSheet()->mergeCells('A1:F3');
        $spreadsheet->getActiveSheet()->getStyle('A1:F3')->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $spreadsheet->getActiveSheet()->getStyle('A1:F3')
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('A1:F3')
            ->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('A1:F3')->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()->setARGB('0077B6');
        $spreadsheet->getActiveSheet()->getStyle('A1:F3')->getFont()->setSize(22);
        $spreadsheet->getActiveSheet()->getStyle('A1:F3')
            ->getFont()->setBold(true)->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        $spreadsheet->getActiveSheet()->getStyle('A1:F3')->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);


        $spreadsheet->getActiveSheet()->mergeCells('A4:A5');
        $spreadsheet->getActiveSheet()->getStyle('A4:A5')->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $spreadsheet->getActiveSheet()->getStyle('A4:A5')
            ->getFont()->setBold(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(180, 'px');
        $spreadsheet->getActiveSheet()->getStyle('A4:A5')
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('A4:A5')
            ->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $spreadsheet->getActiveSheet()->getRowDimension('4')->setRowHeight(40, 'px');

        $spreadsheet->getActiveSheet()->mergeCells('B4:B5');
        $spreadsheet->getActiveSheet()->getStyle('B4:B5')->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $spreadsheet->getActiveSheet()->getStyle('B4:B5')
            ->getFont()->setBold(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(140, 'px');
        $spreadsheet->getActiveSheet()->getStyle('B4:B5')
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('B4:B5')
            ->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);


        $spreadsheet->getActiveSheet()->mergeCells('C4:F4');
        $spreadsheet->getActiveSheet()->getStyle('C4:F4')->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $spreadsheet->getActiveSheet()->getStyle('C4:F4')
            ->getFont()->setBold(true);
        $spreadsheet->getActiveSheet()->getStyle('C4:F4')
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('C4:F4')
            ->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);


        $spreadsheet->getActiveSheet()->mergeCells('C5');
        $spreadsheet->getActiveSheet()->getStyle('C5')->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $spreadsheet->getActiveSheet()->getStyle('C5')
            ->getFont()->setBold(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(200, 'px');
        $spreadsheet->getActiveSheet()->getStyle('C5')
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('C5')
            ->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

        $spreadsheet->getActiveSheet()->mergeCells('D5');
        $spreadsheet->getActiveSheet()->getStyle('D5')->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $spreadsheet->getActiveSheet()->getStyle('D5')
            ->getFont()->setBold(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(160, 'px');
        $spreadsheet->getActiveSheet()->getStyle('D5')
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('D5')
            ->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

        $spreadsheet->getActiveSheet()->mergeCells('E5');
        $spreadsheet->getActiveSheet()->getStyle('E5')->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $spreadsheet->getActiveSheet()->getStyle('E5')
            ->getFont()->setBold(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(160, 'px');
        $spreadsheet->getActiveSheet()->getStyle('E5')
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('E5')
            ->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

        $spreadsheet->getActiveSheet()->mergeCells('F5');
        $spreadsheet->getActiveSheet()->getStyle('F5')->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $spreadsheet->getActiveSheet()->getStyle('F5')
            ->getFont()->setBold(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(160, 'px');
        $spreadsheet->getActiveSheet()->getStyle('F5')
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('F5')
            ->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);




        $spreadsheet->getActiveSheet()->getRowDimension('6')->setRowHeight(45, 'px');

        $sheet->setCellValue('A1', ("Laporan Penjualan"));
        $sheet->setCellValue('A4', ("Tanggal"));
        $sheet->setCellValue('B4', ("No. Pesanan"));
        $sheet->setCellValue('C4', ("Belanja"));
        $sheet->setCellValue('C5', ("Produk"));
        $sheet->setCellValue('D5', ("Harga"));
        $sheet->setCellValue('E5', ("Kuantitas"));
        $sheet->setCellValue('F5', ("Subtotal"));


        foreach ($pembayaran as $psn) {
            $spreadsheet->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
            $spreadsheet->getActiveSheet()->getStyle('A' . $row . ':B' . $row)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
            $spreadsheet->getActiveSheet()->getStyle('A' . $row . ':B' . $row)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('FFfcd5b4');

            $spreadsheet->getActiveSheet()->setCellValue('A' . $row, \Carbon\Carbon::parse($psn->created_at)->format('d-M-Y'));
            $spreadsheet->getActiveSheet()->setCellValue('B' . $row, $psn->no_pesanan);

            foreach ($psn->Pesanan as $psnn) {
                $spreadsheet->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
                $spreadsheet->getActiveSheet()->getStyle('C' . $row . ':F' . $row)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $spreadsheet->getActiveSheet()->getStyle('C' . $row . ':F' . $row)->getFill()
                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()->setARGB('FFfcd5b4');

                $spreadsheet->getActiveSheet()->setCellValue('C' . $row, $psnn->produk->nm_produk);
                $spreadsheet->getActiveSheet()->setCellValue('D' . $row, 'Rp.' . ' ' . number_format($psnn->sub_total / $psnn->kuantitas, 0, ',', '.'));
                $spreadsheet->getActiveSheet()->setCellValue('E' . $row, $psnn->kuantitas);
                $spreadsheet->getActiveSheet()->setCellValue('F' . $row, 'Rp.' . ' ' . number_format($psnn->sub_total, 0, ',', '.'));
                $row++;
            }

            $spreadsheet->getActiveSheet()->mergeCells('C' . $row . ':F' . $row);
            // $spreadsheet->getActiveSheet()->setCellValue('C' . $row, )->getStyle('E' . $row)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
            $total = 0;
            foreach ($psn->Pesanan as $psnn) {
                $total += $psnn->sub_total;
            }
            $spreadsheet->getActiveSheet()->setCellValue('C' . $row, 'Total Belanja: ' . 'Rp.' . ' ' . number_format($total, 0, ',', '.'))->getStyle('F' . $row)->applyFromArray([
                'font' => [
                    'bold' => true,
                    'size' => 12,
                ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
                ],
            ])->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
            $spreadsheet->getActiveSheet()->getRowDimension($row)->setRowHeight(30);

            $spreadsheet->getActiveSheet()->getStyle('A' . ($row - 1) . ':F' . $row)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
            $spreadsheet->getActiveSheet()->getStyle('A' . ($row - 1) . ':F' . $row)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('FFfcd5b4');

            $row += 2;
        }
        $spreadsheet->getActiveSheet()->mergeCells('A' . $row . ':F' . $row);
        $spreadsheet->getActiveSheet()->getStyle('A' . $row . ':F' . $row)
            ->getFill()->getStartColor()->setARGB('FFfcd5b4');

        $spreadsheet->getActiveSheet()->setCellValue('A' . $row, 'Total Pendapatan: ' . 'Rp.' . ' ' .  number_format($totalPendapatan, 0, ',', '.'))->getStyle('F' . $row)->applyFromArray([
            'font' => [
                'bold' => true,
                'size' => 22,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            ],

        ])->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
            ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);;
        $spreadsheet->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
        $spreadsheet->getActiveSheet()->getStyle('A' . $row  . ':F' . $row)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $spreadsheet->getActiveSheet()->getStyle('A' . $row  . ':F' . $row)
            ->getFont()->setBold(true)->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        $spreadsheet->getActiveSheet()->getStyle('A' . $row  . ':F' . $row)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('A' . $row  . ':F' . $row)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('A' . $row  . ':F' . $row)->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()->setARGB('0077B6');
        $row++;

        $writer = new Xlsx($spreadsheet);
        $fileName = 'Laporan_Penjualan.xlsx';
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="' . urlencode($fileName) . '"');
        $writer->save('php://output');
    }

    public function excelbulantahunpenjualan(Request $request)
    {
        // dd($request->all());
        $month = $request->month;
        $year = $request->year;
        $pembayaran = Pembayaran::where('status', 'selesai')->with(['Pengiriman', 'Pesanan.Produk.ProdukVariasi'])->whereMonth('created_at', $month)->whereYear('created_at', $year)->get();
        $totalPendapatan = Pembayaran::where('status', 'selesai')->whereMonth('created_at', $month)->whereYear('created_at', $year)->sum('total_harga');
        $totalOngkir = Pengiriman::whereHas('Pembayaran', function ($q) use ($month, $year) {
            $q->where('status', 'selesai');
            $q->whereMonth('created_at', $month);
            $q->whereYear('created_at', $year);
        })->sum('ongkir');
        $totalPendapatan = $totalPendapatan - $totalOngkir;
        // $total = 0;
        // foreach ($pembayaran->Pesanan as $i) {
        //     $total += $i->sub_total;
        // }


        // $pembayaran = Pembayaran::where('')->with(['Pengiriman', 'Pesanan.Produk'])->get();
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle('Laporan Penjualan');
        $row = 6;
        // $sheet->setCellValue('A1', 'Hello World !');
        $spreadsheet->getActiveSheet()->mergeCells('A1:F3');
        $spreadsheet->getActiveSheet()->getStyle('A1:F3')->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $spreadsheet->getActiveSheet()->getStyle('A1:F3')
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('A1:F3')
            ->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('A1:F3')->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()->setARGB('0077B6');
        $spreadsheet->getActiveSheet()->getStyle('A1:F3')->getFont()->setSize(22);
        $spreadsheet->getActiveSheet()->getStyle('A1:F3')
            ->getFont()->setBold(true)->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        $spreadsheet->getActiveSheet()->getStyle('A1:F3')->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);


        $spreadsheet->getActiveSheet()->mergeCells('A4:A5');
        $spreadsheet->getActiveSheet()->getStyle('A4:A5')->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $spreadsheet->getActiveSheet()->getStyle('A4:A5')
            ->getFont()->setBold(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(180, 'px');
        $spreadsheet->getActiveSheet()->getStyle('A4:A5')
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('A4:A5')
            ->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $spreadsheet->getActiveSheet()->getRowDimension('4')->setRowHeight(40, 'px');

        $spreadsheet->getActiveSheet()->mergeCells('B4:B5');
        $spreadsheet->getActiveSheet()->getStyle('B4:B5')->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $spreadsheet->getActiveSheet()->getStyle('B4:B5')
            ->getFont()->setBold(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(140, 'px');
        $spreadsheet->getActiveSheet()->getStyle('B4:B5')
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('B4:B5')
            ->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);


        $spreadsheet->getActiveSheet()->mergeCells('C4:F4');
        $spreadsheet->getActiveSheet()->getStyle('C4:F4')->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $spreadsheet->getActiveSheet()->getStyle('C4:F4')
            ->getFont()->setBold(true);
        $spreadsheet->getActiveSheet()->getStyle('C4:F4')
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('C4:F4')
            ->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);


        $spreadsheet->getActiveSheet()->mergeCells('C5');
        $spreadsheet->getActiveSheet()->getStyle('C5')->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $spreadsheet->getActiveSheet()->getStyle('C5')
            ->getFont()->setBold(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(200, 'px');
        $spreadsheet->getActiveSheet()->getStyle('C5')
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('C5')
            ->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

        $spreadsheet->getActiveSheet()->mergeCells('D5');
        $spreadsheet->getActiveSheet()->getStyle('D5')->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $spreadsheet->getActiveSheet()->getStyle('D5')
            ->getFont()->setBold(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(160, 'px');
        $spreadsheet->getActiveSheet()->getStyle('D5')
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('D5')
            ->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

        $spreadsheet->getActiveSheet()->mergeCells('E5');
        $spreadsheet->getActiveSheet()->getStyle('E5')->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $spreadsheet->getActiveSheet()->getStyle('E5')
            ->getFont()->setBold(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(160, 'px');
        $spreadsheet->getActiveSheet()->getStyle('E5')
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('E5')
            ->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

        $spreadsheet->getActiveSheet()->mergeCells('F5');
        $spreadsheet->getActiveSheet()->getStyle('F5')->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $spreadsheet->getActiveSheet()->getStyle('F5')
            ->getFont()->setBold(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(160, 'px');
        $spreadsheet->getActiveSheet()->getStyle('F5')
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('F5')
            ->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);




        $spreadsheet->getActiveSheet()->getRowDimension('6')->setRowHeight(45, 'px');

        $sheet->setCellValue('A1', ("Laporan Penjualan"));
        $sheet->setCellValue('A4', ("Tanggal"));
        $sheet->setCellValue('B4', ("No. Pesanan"));
        $sheet->setCellValue('C4', ("Belanja"));
        $sheet->setCellValue('C5', ("Produk"));
        $sheet->setCellValue('D5', ("Harga"));
        $sheet->setCellValue('E5', ("Kuantitas"));
        $sheet->setCellValue('F5', ("Subtotal"));


        foreach ($pembayaran as $psn) {
            $spreadsheet->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
            $spreadsheet->getActiveSheet()->getStyle('A' . $row . ':B' . $row)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
            $spreadsheet->getActiveSheet()->getStyle('A' . $row . ':B' . $row)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('FFfcd5b4');

            $spreadsheet->getActiveSheet()->setCellValue('A' . $row,  \Carbon\Carbon::parse($psn->created_at)->format('d-M-Y'));
            $spreadsheet->getActiveSheet()->setCellValue('B' . $row, $psn->no_pesanan);

            foreach ($psn->Pesanan as $psnn) {
                $spreadsheet->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
                $spreadsheet->getActiveSheet()->getStyle('C' . $row . ':F' . $row)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $spreadsheet->getActiveSheet()->getStyle('C' . $row . ':F' . $row)->getFill()
                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()->setARGB('FFfcd5b4');

                $spreadsheet->getActiveSheet()->setCellValue('C' . $row, $psnn->produk->nm_produk);
                $spreadsheet->getActiveSheet()->setCellValue('D' . $row, 'Rp.' . ' ' . number_format($psnn->sub_total / $psnn->kuantitas, 0, ',', '.'));
                $spreadsheet->getActiveSheet()->setCellValue('E' . $row, $psnn->kuantitas);
                $spreadsheet->getActiveSheet()->setCellValue('F' . $row, 'Rp.' . ' ' . number_format($psnn->sub_total, 0, ',', '.'));
                $row++;
            }

            $spreadsheet->getActiveSheet()->mergeCells('C' . $row . ':F' . $row);
            $total = 0;
            foreach ($psn->Pesanan as $psnn) {
                $total += $psnn->sub_total;
            }
            $spreadsheet->getActiveSheet()->setCellValue('C' . $row, 'Total Belanja: ' . 'Rp.' . ' ' . number_format($total, 0, ',', '.'))->getStyle('F' . $row)->applyFromArray([
                'font' => [
                    'bold' => true,
                    'size' => 12,
                ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
                ],
            ])->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
            $spreadsheet->getActiveSheet()->getRowDimension($row)->setRowHeight(30);

            $spreadsheet->getActiveSheet()->getStyle('A' . ($row - 1) . ':F' . $row)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
            $spreadsheet->getActiveSheet()->getStyle('A' . ($row - 1) . ':F' . $row)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('FFfcd5b4');

            $row += 2;
        }
        $spreadsheet->getActiveSheet()->mergeCells('A' . $row . ':F' . $row);
        $spreadsheet->getActiveSheet()->getStyle('A' . $row . ':F' . $row)
            ->getFill()->getStartColor()->setARGB('FFfcd5b4');

        $spreadsheet->getActiveSheet()->setCellValue('A' . $row, 'Total Pendapatan: ' . 'Rp.' . ' ' .  number_format($totalPendapatan, 0, ',', '.'))->getStyle('F' . $row)->applyFromArray([
            'font' => [
                'bold' => true,
                'size' => 22,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            ],

        ])->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
            ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);;
        $spreadsheet->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
        $spreadsheet->getActiveSheet()->getStyle('A' . $row  . ':F' . $row)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $spreadsheet->getActiveSheet()->getStyle('A' . $row  . ':F' . $row)
            ->getFont()->setBold(true)->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        $spreadsheet->getActiveSheet()->getStyle('A' . $row  . ':F' . $row)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('A' . $row  . ':F' . $row)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('A' . $row  . ':F' . $row)->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()->setARGB('0077B6');
        $row++;

        $writer = new Xlsx($spreadsheet);
        $fileName = 'Laporan_Penjualan.xlsx';
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="' . urlencode($fileName) . '"');
        $writer->save('php://output');
    }

    public function produk(Request $request)
    {
        $setting = Setting::first();
        $keyword = $request->keyword;
        $date = $request->date;
        $produk = Produk::where(function ($q) use ($keyword) {
            $q->where('nm_produk', 'LIKE', '%' . $keyword . '%');
            $q->orWhere('deskripsi', 'LIKE', '%' . $keyword . '%');
            $q->orWhere('harga', 'LIKE', '%' . $keyword . '%');
            $q->orWhere('diskon', 'LIKE', '%' . $keyword . '%');
            $q->orWhere('berat', 'LIKE', '%' . $keyword . '%');

            $q->orWhereHas('subkategori', function ($q) use ($keyword) {
                $q->where('nm_sub_kategori', 'LIKE', '%' . $keyword . '%');
            });

            $q->orWhereHas('merk', function ($q) use ($keyword) {
                $q->where('nm_merk', 'LIKE', '%' . $keyword . '%');
            });

            $q->orWhereHas('spesifikasi', function ($q) use ($keyword) {
                $q->where('nm_spesifikasi', 'LIKE', '%' . $keyword . '%');
                $q->orwhere('deskripsi', 'LIKE', '%' . $keyword . '%');
            });

            $q->orWhereHas('produkvariasi', function ($q) use ($keyword) {
                $q->where('stock', 'LIKE', '%' . $keyword . '%');
                $q->orwhere('warna', 'LIKE', '%' . $keyword . '%');
            });
        })->when(isset($request->date) ?? false, function ($q) use ($date) {
            $q->whereDate('created_at', $date);
        })->paginate(4);
        return view('dashboard.report.produk', compact(
            'setting',
            'keyword',
            'produk'
        ));
    }

    public function excelhariproduk(Request $request)
    {
        $date = $request->date;

        $produk = Produk::with(['SubKategori.Kategori', 'ProdukVariasi', 'Spesifikasi'])->whereDate('created_at', $date)->get();

        // $pembayaran = Pembayaran::where('')->with(['Pengiriman', 'Pesanan.Produk'])->get();
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle('Laporan Produk');
        $row = 6;
        // $sheet->setCellValue('A1', 'Hello World !');
        $spreadsheet->getActiveSheet()->mergeCells('A1:F3');
        $spreadsheet->getActiveSheet()->getStyle('A1:F3')->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $spreadsheet->getActiveSheet()->getStyle('A1:F3')
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('A1:F3')
            ->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('A1:F3')->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()->setARGB('0077B6');
        $spreadsheet->getActiveSheet()->getStyle('A1:F3')->getFont()->setSize(22);
        $spreadsheet->getActiveSheet()->getStyle('A1:F3')
            ->getFont()->setBold(true)->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        $spreadsheet->getActiveSheet()->getStyle('A1:F3')->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);


        $spreadsheet->getActiveSheet()->mergeCells('A4:A5');
        $spreadsheet->getActiveSheet()->getStyle('A4:A5')->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $spreadsheet->getActiveSheet()->getStyle('A4:A5')
            ->getFont()->setBold(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(180, 'px');
        $spreadsheet->getActiveSheet()->getStyle('A4:A5')
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('A4:A5')
            ->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $spreadsheet->getActiveSheet()->getRowDimension('4')->setRowHeight(40, 'px');

        $spreadsheet->getActiveSheet()->mergeCells('B4:B5');
        $spreadsheet->getActiveSheet()->getStyle('B4:B5')->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $spreadsheet->getActiveSheet()->getStyle('B4:B5')
            ->getFont()->setBold(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(200, 'px');
        $spreadsheet->getActiveSheet()->getStyle('B4:B5')
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('B4:B5')
            ->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);


        $spreadsheet->getActiveSheet()->mergeCells('C4:C5');
        $spreadsheet->getActiveSheet()->getStyle('C4:C5')->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $spreadsheet->getActiveSheet()->getStyle('C4:C5')
            ->getFont()->setBold(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(200, 'px');
        $spreadsheet->getActiveSheet()->getStyle('C4:C5')
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('C4:C5')
            ->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);


        $spreadsheet->getActiveSheet()->mergeCells('D4:D5');
        $spreadsheet->getActiveSheet()->getStyle('D4:D5')->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $spreadsheet->getActiveSheet()->getStyle('D4:D5')
            ->getFont()->setBold(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(160, 'px');
        $spreadsheet->getActiveSheet()->getStyle('D4:D5')
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('D4:D5')
            ->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

        $spreadsheet->getActiveSheet()->mergeCells('E4:E5');
        $spreadsheet->getActiveSheet()->getStyle('E4:E5')->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $spreadsheet->getActiveSheet()->getStyle('E4:E5')
            ->getFont()->setBold(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(120, 'px');
        $spreadsheet->getActiveSheet()->getStyle('E4:E5')
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('E4:E5')
            ->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

        $spreadsheet->getActiveSheet()->mergeCells('F4:F5');
        $spreadsheet->getActiveSheet()->getStyle('F4:F5')->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $spreadsheet->getActiveSheet()->getStyle('F4:F5')
            ->getFont()->setBold(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(90, 'px');
        $spreadsheet->getActiveSheet()->getStyle('F4:F5')
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('F4:F5')
            ->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);


        $spreadsheet->getActiveSheet()->getRowDimension('6')->setRowHeight(45, 'px');

        $sheet->setCellValue('A1', ("Laporan Produk"));
        $sheet->setCellValue('A4', ("Tanggal"));
        $sheet->setCellValue('B4', ("Produk"));
        $sheet->setCellValue('C4', ("Harga"));
        $sheet->setCellValue('D4', ("Merk"));
        $sheet->setCellValue('E4', ("Warna"));
        $sheet->setCellValue('F4', ("Stock"));

        foreach ($produk as $prd) {
            $spreadsheet->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
            $spreadsheet->getActiveSheet()->getStyle('A' . $row . ':D' . $row)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
            $spreadsheet->getActiveSheet()->getStyle('A' . $row . ':D' . $row)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('FFfcd5b4');

            $spreadsheet->getActiveSheet()->setCellValue('A' . $row,  \Carbon\Carbon::parse($prd->created_at)->format('d-M-Y'));
            $spreadsheet->getActiveSheet()->setCellValue('B' . $row, $prd->nm_produk);
            $spreadsheet->getActiveSheet()->setCellValue('C' . $row, $prd->harga);
            $spreadsheet->getActiveSheet()->setCellValue('D' . $row, $prd->merk->nm_merk);


            foreach ($prd->ProdukVariasi as $prds) {
                $spreadsheet->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
                $spreadsheet->getActiveSheet()->getStyle('E' . $row . ':F' . $row)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $spreadsheet->getActiveSheet()->getStyle('E' . $row . ':F' . $row)->getFill()
                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()->setARGB('FFfcd5b4');

                $spreadsheet->getActiveSheet()->setCellValue('E' . $row, $prds->warna);
                $spreadsheet->getActiveSheet()->setCellValue('F' . $row, $prds->stock);

                $row++;
            }
            $spreadsheet->getActiveSheet()->getStyle('E' . ($row - 1) . ':F' . $row)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
            $spreadsheet->getActiveSheet()->getStyle('E' . ($row - 1) . ':F' . $row)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('FFfcd5b4');

            $row += 2;
        }



        $writer = new Xlsx($spreadsheet);
        $fileName = 'Laporan_Produk.xlsx';
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="' . urlencode($fileName) . '"');
        $writer->save('php://output');
    }

    public function excelbulantahunproduk(Request $request)
    {
        $date = $request->date;
        $month = $request->month;
        $year = $request->year;
        $produk = Produk::with(['SubKategori.Kategori', 'ProdukVariasi', 'Spesifikasi'])->whereMonth('created_at', $month)->whereYear('created_at', $year)->get();

        // $pembayaran = Pembayaran::where('')->with(['Pengiriman', 'Pesanan.Produk'])->get();
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle('Laporan Produk');
        $row = 6;
        // $sheet->setCellValue('A1', 'Hello World !');
        $spreadsheet->getActiveSheet()->mergeCells('A1:F3');
        $spreadsheet->getActiveSheet()->getStyle('A1:F3')->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $spreadsheet->getActiveSheet()->getStyle('A1:F3')
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('A1:F3')
            ->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('A1:F3')->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()->setARGB('0077B6');
        $spreadsheet->getActiveSheet()->getStyle('A1:F3')->getFont()->setSize(22);
        $spreadsheet->getActiveSheet()->getStyle('A1:F3')
            ->getFont()->setBold(true)->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        $spreadsheet->getActiveSheet()->getStyle('A1:F3')->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);


        $spreadsheet->getActiveSheet()->mergeCells('A4:A5');
        $spreadsheet->getActiveSheet()->getStyle('A4:A5')->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $spreadsheet->getActiveSheet()->getStyle('A4:A5')
            ->getFont()->setBold(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(180, 'px');
        $spreadsheet->getActiveSheet()->getStyle('A4:A5')
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('A4:A5')
            ->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $spreadsheet->getActiveSheet()->getRowDimension('4')->setRowHeight(40, 'px');

        $spreadsheet->getActiveSheet()->mergeCells('B4:B5');
        $spreadsheet->getActiveSheet()->getStyle('B4:B5')->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $spreadsheet->getActiveSheet()->getStyle('B4:B5')
            ->getFont()->setBold(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(200, 'px');
        $spreadsheet->getActiveSheet()->getStyle('B4:B5')
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('B4:B5')
            ->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);


        $spreadsheet->getActiveSheet()->mergeCells('C4:C5');
        $spreadsheet->getActiveSheet()->getStyle('C4:C5')->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $spreadsheet->getActiveSheet()->getStyle('C4:C5')
            ->getFont()->setBold(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(200, 'px');
        $spreadsheet->getActiveSheet()->getStyle('C4:C5')
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('C4:C5')
            ->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);


        $spreadsheet->getActiveSheet()->mergeCells('D4:D5');
        $spreadsheet->getActiveSheet()->getStyle('D4:D5')->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $spreadsheet->getActiveSheet()->getStyle('D4:D5')
            ->getFont()->setBold(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(160, 'px');
        $spreadsheet->getActiveSheet()->getStyle('D4:D5')
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('D4:D5')
            ->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

        $spreadsheet->getActiveSheet()->mergeCells('E4:E5');
        $spreadsheet->getActiveSheet()->getStyle('E4:E5')->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $spreadsheet->getActiveSheet()->getStyle('E4:E5')
            ->getFont()->setBold(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(120, 'px');
        $spreadsheet->getActiveSheet()->getStyle('E4:E5')
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('E4:E5')
            ->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

        $spreadsheet->getActiveSheet()->mergeCells('F4:F5');
        $spreadsheet->getActiveSheet()->getStyle('F4:F5')->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $spreadsheet->getActiveSheet()->getStyle('F4:F5')
            ->getFont()->setBold(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(90, 'px');
        $spreadsheet->getActiveSheet()->getStyle('F4:F5')
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('F4:F5')
            ->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);


        $spreadsheet->getActiveSheet()->getRowDimension('6')->setRowHeight(45, 'px');

        $sheet->setCellValue('A1', ("Laporan Produk"));
        $sheet->setCellValue('A4', ("Tanggal"));
        $sheet->setCellValue('B4', ("Produk"));
        $sheet->setCellValue('C4', ("Harga"));
        $sheet->setCellValue('D4', ("Merk"));
        $sheet->setCellValue('E4', ("Warna"));
        $sheet->setCellValue('F4', ("Stock"));

        foreach ($produk as $prd) {
            $spreadsheet->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
            $spreadsheet->getActiveSheet()->getStyle('A' . $row . ':D' . $row)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
            $spreadsheet->getActiveSheet()->getStyle('A' . $row . ':D' . $row)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('FFfcd5b4');

            $spreadsheet->getActiveSheet()->setCellValue('A' . $row,  \Carbon\Carbon::parse($prd->created_at)->format('d-M-Y'));
            $spreadsheet->getActiveSheet()->setCellValue('B' . $row, $prd->nm_produk);
            $spreadsheet->getActiveSheet()->setCellValue('C' . $row, $prd->harga);
            $spreadsheet->getActiveSheet()->setCellValue('D' . $row, $prd->merk->nm_merk);


            foreach ($prd->ProdukVariasi as $prds) {
                $spreadsheet->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
                $spreadsheet->getActiveSheet()->getStyle('E' . $row . ':F' . $row)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $spreadsheet->getActiveSheet()->getStyle('E' . $row . ':F' . $row)->getFill()
                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()->setARGB('FFfcd5b4');

                $spreadsheet->getActiveSheet()->setCellValue('E' . $row, $prds->warna);
                $spreadsheet->getActiveSheet()->setCellValue('F' . $row, $prds->stock);

                $row++;
            }


            $row += 2;
        }


        $writer = new Xlsx($spreadsheet);
        $fileName = 'Laporan_Produk.xlsx';
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="' . urlencode($fileName) . '"');
        $writer->save('php://output');
    }
}
