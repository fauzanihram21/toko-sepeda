<?php

namespace App\Http\Controllers;

use App\Models\Kategori;
use App\Models\Setting;
use App\Models\Keranjang;
use App\Models\ProdukVariasi;
use Illuminate\Http\Request;

class KeranjangController extends Controller
{
    public function index(Request $request)
    {
        $setting = Setting::first();

        $keranjang = Keranjang::with(['Produk.SubKategori', 'ProdukVariasi'])->where('user_id', auth()->user()->id)->get();
        $kategori = Kategori::all();
        $total = 0;
        foreach ($keranjang as $i) {
            $total += $i->subtotal;
        }

        return view('front.keranjang.index', compact(
            'keranjang',
            'kategori',
            'setting',
            'total'
        ));
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'produk_id' => 'required|string',
            'produk_variasi_id' => 'required',
            'kuantitas' => 'required|integer|min:0'
        ]);

        if ($validatedData['kuantitas'] == 0) {
            return redirect()->back();
        }

        if ($cek = Keranjang::where('produk_id', $request->produk_id)->where('produk_variasi_id', $request->produk_variasi_id)->where('user_id', auth()->user()->id)->first()) {
            $kuantitas = $cek->kuantitas + $validatedData['kuantitas'];
            $produkVar = ProdukVariasi::find($request->produk_variasi_id);
            if ($kuantitas > $produkVar->stock) {
                $kuantitas = $produkVar->stock;
            }
            $cek->update([
                'kuantitas' => $kuantitas
            ]);
        } else {
            $validatedData['user_id'] = auth()->user()->id;
            Keranjang::create($validatedData);
        }


        return redirect()->back()->with('successkeranjang', 'Berhasil ditambahkan ke keranjang');
    }

    public function update(Request $request,  $id)
    {
        $tambah = Keranjang::find($id);
        $validatedData = $request->validate([
            'kuantitas' => 'required|integer|min:0',
        ]);

        $tambah->update($validatedData);


        return redirect('/keranjang')->with('successupdate', 'Update Successfull!');
    }

    public function delete($id)
    {
        $tambah = Keranjang::find($id);
        $tambah->delete();
        return redirect('/keranjang')->with('successdelete', 'Delete Successfull!');
    }
}
