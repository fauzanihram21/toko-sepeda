<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use Illuminate\Http\Request;

class DashboardSettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $setting = Setting::first();
        return view('dashboard.setting.index', compact(
            'setting'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function website(Request $request)
    {
        $tambah = Setting::first();
        if (isset($tambah)) {
            $tambah->update($request->all());
        } else {
            Setting::create($request->all());
        }
        return redirect('dashboard/setting')->with('successcreate', 'Create Successfull!');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function perusahaan(Request $request)
    {
        $tambah = Setting::first();
        if (isset($tambah)) {
            $tambah->update($request->all());
        } else {
            Setting::create($request->all());
        }
        return redirect('dashboard/setting')->with('successcreate', 'Create Successfull!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function media(Request $request)
    {
        $tambah = Setting::first();
        if (isset($tambah)) {
            $tambah->update($request->all());
        } else {
            Setting::create($request->all());
        }
        return redirect('dashboard/setting')->with('successcreate', 'Create Successfull!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Setting  $setting
     * @return \Illuminate\Http\Response
     */

    public function background(Request $request)
    {
        $tambah = Setting::first();

        $setting = $request->all();
        if (isset($request->background_login)) {
            $file = $request->file('background_login');
            $nama_file = time() . str_replace(" ", " ", $file->getClientOriginalName());
            $file->move('storage/background_login', $nama_file);
            $setting['background_login'] = $nama_file;
        }

        if (isset($request->background_register)) {
            $file = $request->file('background_register');
            $nama_file = time() . str_replace(" ", " ", $file->getClientOriginalName());
            $file->move('storage/background_register', $nama_file);
            $setting['background_register'] = $nama_file;
        }

        if (isset($request->logo_luar)) {
            $file = $request->file('logo_luar');
            $nama_file = time() . str_replace(" ", " ", $file->getClientOriginalName());
            $file->move('storage/logo_luar', $nama_file);
            $setting['logo_luar'] = $nama_file;
        }

        if (isset($request->logo_dalam)) {
            $file = $request->file('logo_dalam');
            $nama_file = time() . str_replace(" ", " ", $file->getClientOriginalName());
            $file->move('storage/logo_dalam', $nama_file);
            $setting['logo_dalam'] = $nama_file;
        }

        if (isset($tambah)) {
            $tambah->update($setting);
        } else {
            Setting::create($setting);
        }

        return redirect('dashboard/setting')->with('successcreate', 'Create Successfull!');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Setting  $setting
     * @return \Illuminate\Http\Response
     */

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function destroy(Setting $setting)
    {
        //
    }
}
