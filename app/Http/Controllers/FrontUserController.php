<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use App\Models\Kategori;
use App\Models\User;
use Illuminate\Http\Request;

class FrontUserController extends Controller
{
    public function index(Request $request)
    {
        $setting = Setting::first();
        $user = User::where('id', auth()->user()->id)->first();


        $kategori = Kategori::all();


        return view('front.user.profile', compact(
            'kategori',
            'user',
            'setting'
        ));
    }

    public function profilesaya(Request $request)
    {
        $tambah = User::where('id', auth()->user()->id)->first();

        if (isset($tambah)) {
            $tambah->update($request->all());
        } else {
            User::create($request->all());
        }

        $user = $request->all();
        if (isset($request->avatar)) {
            $file = $request->file('avatar');
            $nama_file = time() . str_replace(" ", "-", $file->getClientOriginalName());
            $file->move('storage/avatar', $nama_file);
            $user['avatar'] = $nama_file;
        }



        if (isset($tambah)) {
            $tambah->update($user);
        } else {
            User::create($user);
        }

        return redirect('/profile')->with('successcreate', 'Berhasil Menambahkan Data Profile!');
    }

    public function indexchangepassword(Request $request)
    {
        $kategori = Kategori::all();
        $setting = Setting::first();
        $user = User::where('id', auth()->user()->id)->first();
        return view('front.user.changepassword', compact(
            'kategori',
            'user',
            'setting'
        ));
    }

    public function changepassword(Request $request)
    {
        $tambah = User::where('id', auth()->user()->id)->first();
        if (password_verify($request->current_password, $tambah->password)) {
            $tambah->update([
                'password' => bcrypt($request->password)
            ]);
            return redirect('/profilechangepassword')->with('successcreate', 'Create Successfull!');
        } else {
            return redirect('/profilechangepassword')->with('errorcreate', 'Error!');
        }
    }
}
