<?php

namespace App\Http\Controllers;

use App\Models\BalasKomentar;
use App\Models\BalasKomentarAdmin;
use App\Models\Produk;
use App\Models\Setting;
use App\Models\komentar;
use Illuminate\Http\Request;


class KomentarController extends Controller
{
    public function index(Request $request)
    {
        $setting = Setting::first();
        $keyword = $request->keyword;
        $komentar = Komentar::where(function ($q) use ($keyword) {
            $q->where('komentar', 'LIKE', '%' . $keyword . '%');

            $q->orWhereHas('user', function ($q) use ($keyword) {
                $q->where('name', 'LIKE', '%' . $keyword . '%');
            });

            $q->orWhereHas('produk', function ($q) use ($keyword) {
                $q->where('nm_produk', 'LIKE', '%' . $keyword . '%');
            });
        })->whereHas('user', function ($q) {
            $q->where('is_admin', '0');
        })->paginate(4);

        $balaskomentar = BalasKomentar::where(function ($q) use ($keyword) {
            $q->where('komentar', 'LIKE', '%' . $keyword . '%');

            $q->orWhereHas('user', function ($q) use ($keyword) {
                $q->where('name', 'LIKE', '%' . $keyword . '%');
            });

            $q->orWhereHas('komentar', function ($q) use ($keyword) {
                $q->where('komentar', 'LIKE', '%' . $keyword . '%');
            });
        })->paginate(4);
        return view('dashboard.komentar.index', compact(
            'komentar',
            'keyword',
            'balaskomentar',
            'setting'
        ));
    }



    public function indexbalaskomentar(Request $request, $id)
    {
        $setting = Setting::first();
        $keyword = $request->keyword;

        $komentar = komentar::find($id);

        $balaskomentar = BalasKomentar::where(function ($q) use ($keyword) {
            $q->where('komentar', 'LIKE', '%' . $keyword . '%');

            $q->orWhereHas('user', function ($q) use ($keyword) {
                $q->where('name', 'LIKE', '%' . $keyword . '%');
            });

            $q->orWhereHas('komentar', function ($q) use ($keyword) {
                $q->where('komentar', 'LIKE', '%' . $keyword . '%');
            });
        })->paginate(4);

        return view('dashboard.komentar.balaskomentar.index', compact(
            'komentar',
            'keyword',
            'balaskomentar',
            'setting'
        ));
    }

    public function komentar(Request $request, $uuid)
    {
        $validatedData = $request->validate([
            'komentar' => 'required|string',
        ]);


        $validatedData['user_id'] = auth()->user()->id;
        $validatedData['produk_id'] = Produk::findByUuid($uuid)->id;
        if ($komentar = komentar::where('user_id', auth()->user()->id)->where('produk_id', $validatedData['produk_id'])->first()) {
            $komentar->update($validatedData);
        } else {

            komentar::create($validatedData);
        }



        return redirect('/katalog/detail/' . $uuid)->with('successcreate', 'Berhasil Menambahkan Komentar!');
    }

    public function balaskomentar(Request $request, $uuid)
    {
        $validatedData = $request->validate([
            'komentar' => 'required|string',
        ]);


        $validatedData['user_id'] = auth()->user()->id;
        $validatedData['komentar_id'] = komentar::findByUuid($uuid)->id;
        if ($balaskomentar = BalasKomentar::where('user_id', auth()->user()->id)->where('komentar_id', $validatedData['komentar_id'])->first()) {
            $balaskomentar->update($validatedData);
        } else {

            BalasKomentar::create($validatedData);
        }


        $produk = Produk::find(komentar::findByUuid($uuid)->produk_id);

        return redirect('/katalog/detail/' . $produk->uuid)->with('successcreate', 'Berhasil Menambahkan Komentar!');
    }

    public function indexadminkomentar(Request $request, $id)
    {
        $setting = Setting::first();
        $komentar = komentar::where('id', $id)->first();

        return view('dashboard.komentar.komentaradmin', compact(
            'komentar',
            'setting'
        ));
    }


    public function adminkomentar(Request $request, $id)
    {
        $validatedData = $request->validate([
            'komentar' => 'required|string',
            'komentar_id' => 'required|string'

        ]);


        $validatedData['user_id'] = auth()->user()->id;
        $validatedData['komentar_id'] = komentar::find($id)->id;
        if ($balaskomentar = BalasKomentarAdmin::where('komentar_id', $id)->first()) {
            $balaskomentar->update($validatedData);
        } else {

            BalasKomentarAdmin::create($validatedData);
        }


        $produk = Produk::find(komentar::find($id)->produk_id);



        return redirect('/komentar')->with('successcreate', 'Berhasil Menambahkan Komentar Admin!');
    }

    public function adminbalaskomentar(Request $request, $uuid)
    {
        $validatedData = $request->validate([
            'komentar' => 'required|string',
        ]);


        $validatedData['user_id'] = auth()->user()->id;
        $validatedData['komentar_id'] = komentar::findByUuid($uuid)->id;
        if ($balaskomentar = BalasKomentar::where('user_id', auth()->user()->id)->where('komentar_id', $validatedData['komentar_id'])->first()) {
            $balaskomentar->update($validatedData);
        } else {

            BalasKomentar::create($validatedData);
        }


        $produk = Produk::find(komentar::findByUuid($uuid)->produk_id);

        return redirect('/komentar/' . $produk->uuid)->with('successcreate', 'Create Successfull!');
    }


    public function adminhapuskomentar($uuid, $id)
    {
        $hapus = komentar::find($id);
        $hapus->delete();
        return redirect('/komentar/' . $uuid)->with('successdelete', 'Delete Successfull!');
    }

    public function adminhapusbalaskomentar($uuid, $id)
    {
        $hapus = BalasKomentar::find($id);
        $hapus->delete();
        return redirect('/komentar/' . $uuid)->with('successdelete', 'Delete Successfull!');
    }



    public function userhapuskomentar($uuid, $id)
    {
        $hapus = komentar::where('id', $id)->where('user_id', auth()->user()->id)->first();
        $hapus->delete();
        return redirect('/katalog/detail/' . $uuid)->with('successdelete', 'Delete Successfull!');
    }
    public function userhapusbalaskomentar($id)
    {
        $hapus = BalasKomentar::where('id', $id)->where('user_id', auth()->user()->id)->first();
        $hapus->delete();
        return redirect()->back()->with('successdelete', 'Delete Successfull!');
    }
}
