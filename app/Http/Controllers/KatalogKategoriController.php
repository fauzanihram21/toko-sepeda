<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use App\Models\Kategori;
use App\Models\Merk;
use App\Models\SubKategori;
use Illuminate\Http\Request;
use App\Models\Produk;
use App\Models\ProdukVariasi;

class KatalogKategoriController extends Controller
{
    public function index(Request $request, $uuid_kategori)
    {

        $setting = Setting::first();
        $kategori = Kategori::get()->take(4);
        $merk = Merk::get();
        $keyword = $request->keyword;
        $produkvariasi = ProdukVariasi::get();
        $kategorii = Kategori::where('uuid', $uuid_kategori)->first();
        $subkategori = SubKategori::whereHas('Kategori', function ($q) use ($uuid_kategori) {
            $q->where('uuid', $uuid_kategori);
        })->get();
        $produk = Produk::whereHas('SubKategori', function ($q) use ($uuid_kategori) {
            $q->whereHas('Kategori', function ($q) use ($uuid_kategori) {
                $q->where('uuid', $uuid_kategori);
            });
        })->whereHas('ProdukVariasi', function ($q) use ($request) {
            $q->where('stock', '>', 0);
            $q->when(isset($request->produk_variasi_id), function ($q) use ($request) {
                $q->where('id', $request->produk_variasi_id);
            });
        })->when(isset($request->merk_id), function ($q) use ($request) {
            $q->where('merk_id', $request->merk_id);
        })->when(isset($request->urut_harga), function ($q) use ($request) {
            if ($request->urut_harga == 'tinggi') {
                $q->orderBy('harga', 'DESC');
            } else {
                $q->orderBy('harga');
            }
        })->where('nm_produk', 'LIKE', '%' . $keyword . '%')
            ->paginate(12);
        $produkAll = $produk->map(function ($prd) {
            if ($prd->diskon) {
                $diskon = $prd->diskon / 100 * $prd->harga;
                $prd->harga_diskon = $prd->harga - $diskon;
            }
            return $prd;
        });
        return view('front.katalog.index', compact(
            'setting',
            'keyword',
            'merk',
            'produkvariasi',
            'kategori',
            'kategorii',
            'produkAll',
            'subkategori',
            'uuid_kategori',
            'produk'
            // 'diskon'
        ));
    }
}
