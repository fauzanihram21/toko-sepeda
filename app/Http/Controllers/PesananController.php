<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use App\Models\Pembayaran;
use App\Models\Pengiriman;
use Illuminate\Http\Request;

class PesananController extends Controller
{
    public function pesananbaru(Request $request)
    {
        $setting = Setting::first();
        $keyword = $request->keyword;
        $pembayaran = Pembayaran::where('status', 'pesanan_diterima')->with(['Pengiriman', 'Pesanan.Produk'])->where(function ($q) use ($keyword) {
            $q->where('no_pesanan', 'LIKE', '%' . $keyword . '%');
            $q->orWhere('no_invoice', 'LIKE', '%' . $keyword . '%');
            $q->orWhere('total_harga', 'LIKE', '%' . $keyword . '%');
            $q->orWhereHas('pengiriman', function ($q) use ($keyword) {
                $q->where('alamat', 'LIKE', '%' . $keyword . '%');
                $q->orWhere('nama_penerima', 'LIKE', '%' . $keyword . '%');
            });
        })->Paginate(4);

        return view('dashboard.pesanan.pesananbaru', compact(
            'setting',
            'pembayaran',
            'keyword'
        ));
    }

    public function showpesananbaru(Request $request)
    {
        $setting = Setting::first();
        $pembayaran = Pembayaran::with(['Pengiriman', 'Pesanan.Produk.ProdukVariasi'])->paginate(4);
        $no_resi = $request->no_resi;


        return view('dashboard.pesanan.pesananbaru', compact(
            'setting',
            'pembayaran',
            'no_resi'
        ));
    }

    public function storepesananbaru(Request $request, $id)
    {
        $pembayaran = Pembayaran::find($id);
        Pengiriman::find($pembayaran->id)->update(['no_resi' => $request->no_resi]);
        return redirect('/dashboard/pesananbaru');
    }

    public function updateStatuspesananbaru(Request $request, $id)
    {
        $request->validate([
            'status' => 'required',
        ]);

        $pembayaran = Pembayaran::find($id);
        $pembayaran->update(['status' => $request->status]);
        return redirect('/dashboard/pesananbaru')->with('diproses', 'Pesanan Di Proses!');
    }



    public function diproses(Request $request)
    {
        $setting = Setting::first();
        $keyword = $request->keyword;
        $pembayaran = Pembayaran::where('status', 'diproses')->with(['Pengiriman', 'Pesanan.Produk'])->where(function ($q) use ($keyword) {
            $q->where('no_pesanan', 'LIKE', '%' . $keyword . '%');
            $q->orwhere('no_invoice', 'LIKE', '%' . $keyword . '%');
            $q->orwhere('total_harga', 'LIKE', '%' . $keyword . '%');
            $q->orWhereHas('pengiriman', function ($q) use ($keyword) {
                $q->where('alamat', 'LIKE', '%' . $keyword . '%');
                $q->orwhere('nama_penerima', 'LIKE', '%' . $keyword . '%');
            });
        })->Paginate(4);
        return view('dashboard.pesanan.pesanandiproses', compact(
            'setting',
            'pembayaran',
            'keyword'
        ));
    }
    public function showsedangdiproses(Request $request)
    {
        $setting = Setting::first();
        $pembayaran = Pembayaran::with(['Pengiriman', 'Pesanan.Produk.ProdukVariasi'])->paginate(4);
        $no_resi = $request->no_resi;


        return view('dashboard.pesanan.pesanandiproses', compact(
            'setting',
            'pembayaran',
            'no_resi'
        ));
    }
    public function updateStatusdiproses(Request $request, $id)
    {
        $request->validate([
            'status' => 'required',
        ]);

        $pembayaran = Pembayaran::find($id);
        $pembayaran->update(['status' => $request->status]);
        return redirect('/dashboard/sedangdiproses')->with('dikirim', 'Pesanan Di Kirim!');
    }
    public function kembalistatusdiproses(Request $request, $id)
    {
        $request->validate([
            'status' => 'required',
        ]);

        $pembayaran = Pembayaran::find($id);
        $pembayaran->update(['status' => $request->status]);
        return redirect('/dashboard/sedangdiproses')->with('kembaliditerima', 'Pesanan Terjadi Masalah Pesanan sedang di terima!');
    }

    public function dalampengiriman(Request $request)
    {
        $setting = Setting::first();
        $keyword = $request->keyword;
        $pembayaran = Pembayaran::where('status', 'sedang_dikirim')->with(['Pengiriman', 'Pesanan.Produk'])->where(function ($q) use ($keyword) {
            $q->where('no_pesanan', 'LIKE', '%' . $keyword . '%');
            $q->orwhere('no_invoice', 'LIKE', '%' . $keyword . '%');
            $q->orwhere('total_harga', 'LIKE', '%' . $keyword . '%');
            $q->orWhereHas('pengiriman', function ($q) use ($keyword) {
                $q->where('alamat', 'LIKE', '%' . $keyword . '%');
                $q->orwhere('nama_penerima', 'LIKE', '%' . $keyword . '%');
            });
        })->Paginate(4);

        return view('dashboard.pesanan.pesanandalampengiriman', compact(
            'setting',
            'pembayaran',
            'keyword'
        ));
    }
    public function storedalampengiriman(Request $request, $id)
    {
        $pembayaran = Pembayaran::find($id);
        Pengiriman::find($pembayaran->id)->update(['no_resi' => $request->no_resi]);
        return redirect('/dashboard/dalampengiriman')->with('resi', 'Nomer Resi telah Ditambahkan!');
    }
    public function showpdalampengiriman(Request $request)
    {
        $setting = Setting::first();
        $pembayaran = Pembayaran::with(['Pengiriman', 'Pesanan.Produk.ProdukVariasi'])->paginate(4);
        $no_resi = $request->no_resi;


        return view('dashboard.pesanan.pesanandalampengiriman', compact(
            'setting',
            'pembayaran',
            'no_resi'
        ));
    }

    public function updatedalampengiriman(Request $request, $id)
    {
        $request->validate([
            'status' => 'required',
        ]);

        $pembayaran = Pembayaran::find($id);
        $pembayaran->update(['status' => $request->status]);
        return redirect('/dashboard/dalampengiriman')->with('selesai', 'Pesanan Selesai!');
    }

    public function kembalistatusdalampengiriman(Request $request, $id)
    {
        $request->validate([
            'status' => 'required',
        ]);

        $pembayaran = Pembayaran::find($id);
        $pembayaran->update(['status' => $request->status]);
        return redirect('/dashboard/dalampengiriman')->with('kembalidiproses', 'Pesanan Terjadi Masalah kembali diproses!');
    }

    public function telahselesai(Request $request)
    {
        $setting = Setting::first();
        $keyword = $request->keyword;
        $pembayaran = Pembayaran::where('status', 'selesai')->with(['Pengiriman', 'Pesanan.Produk'])->where(function ($q) use ($keyword) {
            $q->where('no_pesanan', 'LIKE', '%' . $keyword . '%');
            $q->orwhere('no_invoice', 'LIKE', '%' . $keyword . '%');
            $q->orwhere('total_harga', 'LIKE', '%' . $keyword . '%');
            $q->orWhereHas('pengiriman', function ($q) use ($keyword) {
                $q->where('alamat', 'LIKE', '%' . $keyword . '%');
                $q->orwhere('nama_penerima', 'LIKE', '%' . $keyword . '%');
            });
        })->Paginate(4);

        return view('dashboard.pesanan.pesanantelahselesai', compact(
            'setting',
            'pembayaran',
            'keyword'
        ));
    }

    public function pesananbatal(Request $request)
    {
        $setting = Setting::first();
        $keyword = $request->keyword;
        $pembayaran = Pembayaran::where('status', 'cancel')->with(['Pengiriman', 'Pesanan.Produk'])->where(function ($q) use ($keyword) {
            $q->where('no_pesanan', 'LIKE', '%' . $keyword . '%');
            $q->orwhere('no_invoice', 'LIKE', '%' . $keyword . '%');
            $q->orwhere('total_harga', 'LIKE', '%' . $keyword . '%');
            $q->orWhereHas('pengiriman', function ($q) use ($keyword) {
                $q->where('alamat', 'LIKE', '%' . $keyword . '%');
                $q->orwhere('nama_penerima', 'LIKE', '%' . $keyword . '%');
            });
        })->Paginate(4);

        return view('dashboard.pesanan.pesananbatal', compact(
            'setting',
            'pembayaran',
            'keyword'
        ));
    }

    public function hapuspesenanbatal($id)
    {
        $hapus = Pembayaran::find($id);
        $hapus->delete();
        return redirect('dashboard/pesananbatal')->with('successdelete', 'Delete Successfull!');
    }

    // public function excel(Request $request)
    // {
    //     // $pembayaran = Pembayaran::where('')->with(['Pengiriman', 'Pesanan.Produk'])->get();
    //     $spreadsheet = new Spreadsheet();
    //     $sheet = $spreadsheet->getActiveSheet();
    //     $sheet->setTitle('Laporan');
    //     // $row = 6;
    //     // $sheet->setCellValue('A1', 'Hello World !');
    //     $spreadsheet->getActiveSheet()->mergeCells('A1:K3');

    //     $spreadsheet->getActiveSheet()->getStyle('A1:K3')
    //         ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
    //     $spreadsheet->getActiveSheet()->getStyle('A1:K3')
    //         ->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
    //     $spreadsheet->getActiveSheet()->getStyle('A1:K3')->getFill()
    //         ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    //         ->getStartColor()->setARGB('0077B6');
    //     $spreadsheet->getActiveSheet()->getStyle('A1:K3')->getFont()->setSize(22);
    //     $spreadsheet->getActiveSheet()->getStyle('A1:K3')
    //         ->getFont()->setBold(true)->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
    //     $spreadsheet->getActiveSheet()->getStyle('A1:K3')->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);


    //     $spreadsheet->getActiveSheet()->mergeCells('A4:B4');
    //     $spreadsheet->getActiveSheet()->getStyle('A4:B4')
    //         ->getFont()->setBold(true);
    //     $spreadsheet->getActiveSheet()->getStyle('A4:B4')
    //         ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
    //     $spreadsheet->getActiveSheet()->getStyle('A4:B4')
    //         ->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
    //     $spreadsheet->getActiveSheet()->getRowDimension('4')->setRowHeight(40, 'px');

    //     $spreadsheet->getActiveSheet()->mergeCells('C4:E4');
    //     $spreadsheet->getActiveSheet()->getStyle('C4:E4')
    //         ->getFont()->setBold(true);
    //     $spreadsheet->getActiveSheet()->getStyle('C4:E4')
    //         ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
    //     $spreadsheet->getActiveSheet()->getStyle('C4:E4')
    //         ->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

    //     $spreadsheet->getActiveSheet()->mergeCells('F4');
    //     $spreadsheet->getActiveSheet()->getStyle('F4')
    //         ->getFont()->setBold(true);
    //     $spreadsheet->getActiveSheet()->getStyle('F4')
    //         ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
    //     $spreadsheet->getActiveSheet()->getStyle('F4')
    //         ->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

    //     $spreadsheet->getActiveSheet()->mergeCells('G4:G5');
    //     $spreadsheet->getActiveSheet()->getStyle('G4:G5')
    //         ->getFont()->setBold(true);
    //     $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(155, 'px');
    //     $spreadsheet->getActiveSheet()->getStyle('G4:G5')
    //         ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
    //     $spreadsheet->getActiveSheet()->getStyle('G4:G5')
    //         ->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);


    //     $spreadsheet->getActiveSheet()->mergeCells('H4:I4');
    //     $spreadsheet->getActiveSheet()->getStyle('H4:I4')
    //         ->getFont()->setBold(true);
    //     $spreadsheet->getActiveSheet()->getStyle('H4:I4')
    //         ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
    //     $spreadsheet->getActiveSheet()->getStyle('H4:I4')
    //         ->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

    //     $spreadsheet->getActiveSheet()->mergeCells('A5');
    //     $spreadsheet->getActiveSheet()->getStyle('A5')
    //         ->getFont()->setBold(true);
    //     $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(180, 'px');
    //     $spreadsheet->getActiveSheet()->getStyle('A5')
    //         ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
    //     $spreadsheet->getActiveSheet()->getStyle('A5')
    //         ->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

    //     $spreadsheet->getActiveSheet()->mergeCells('B5');
    //     $spreadsheet->getActiveSheet()->getStyle('B5')
    //         ->getFont()->setBold(true);
    //     $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(140, 'px');
    //     $spreadsheet->getActiveSheet()->getStyle('B5')
    //         ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
    //     $spreadsheet->getActiveSheet()->getStyle('B5')
    //         ->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

    //     $spreadsheet->getActiveSheet()->mergeCells('C5');
    //     $spreadsheet->getActiveSheet()->getStyle('C5')
    //         ->getFont()->setBold(true);
    //     $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(280, 'px');
    //     $spreadsheet->getActiveSheet()->getStyle('C5')
    //         ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
    //     $spreadsheet->getActiveSheet()->getStyle('C5')
    //         ->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

    //     $spreadsheet->getActiveSheet()->mergeCells('D5');
    //     $spreadsheet->getActiveSheet()->getStyle('D5')
    //         ->getFont()->setBold(true);
    //     $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(400, 'px');
    //     $spreadsheet->getActiveSheet()->getStyle('D5')
    //         ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
    //     $spreadsheet->getActiveSheet()->getStyle('D5')
    //         ->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

    //     $spreadsheet->getActiveSheet()->mergeCells('E5');
    //     $spreadsheet->getActiveSheet()->getStyle('E5')
    //         ->getFont()->setBold(true);
    //     $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(143, 'px');
    //     $spreadsheet->getActiveSheet()->getStyle('E5')
    //         ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
    //     $spreadsheet->getActiveSheet()->getStyle('E5')
    //         ->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);


    //     $spreadsheet->getActiveSheet()->mergeCells('F5');
    //     $spreadsheet->getActiveSheet()->getStyle('F5')
    //         ->getFont()->setBold(true);
    //     $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(145, 'px');
    //     $spreadsheet->getActiveSheet()->getStyle('F5')
    //         ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
    //     $spreadsheet->getActiveSheet()->getStyle('F5')
    //         ->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

    //     $spreadsheet->getActiveSheet()->mergeCells('H5');
    //     $spreadsheet->getActiveSheet()->getStyle('H5')
    //         ->getFont()->setBold(true);
    //     $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(213, 'px');
    //     $spreadsheet->getActiveSheet()->getStyle('H5')
    //         ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
    //     $spreadsheet->getActiveSheet()->getStyle('H5')
    //         ->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

    //     $spreadsheet->getActiveSheet()->mergeCells('I5');
    //     $spreadsheet->getActiveSheet()->getStyle('I5')
    //         ->getFont()->setBold(true);
    //     $spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(160, 'px');
    //     $spreadsheet->getActiveSheet()->getStyle('I5')
    //         ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
    //     $spreadsheet->getActiveSheet()->getStyle('I5')
    //         ->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

    //     $spreadsheet->getActiveSheet()->mergeCells('J5');
    //     $spreadsheet->getActiveSheet()->getStyle('J5')
    //         ->getFont()->setBold(true);
    //     $spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(78, 'px');
    //     $spreadsheet->getActiveSheet()->getStyle('J5')
    //         ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
    //     $spreadsheet->getActiveSheet()->getStyle('J5')
    //         ->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

    //     $spreadsheet->getActiveSheet()->mergeCells('K5');
    //     $spreadsheet->getActiveSheet()->getStyle('K5')
    //         ->getFont()->setBold(true);
    //     $spreadsheet->getActiveSheet()->getColumnDimension('K')->setWidth(126, 'px');
    //     $spreadsheet->getActiveSheet()->getStyle('K5')
    //         ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
    //     $spreadsheet->getActiveSheet()->getStyle('K5')
    //         ->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

    //     $spreadsheet->getActiveSheet()->getRowDimension('6')->setRowHeight(85, 'px');

    //     $sheet->setCellValue('A1', ("Laporan Penjualan"));
    //     $sheet->setCellValue('A4', ("#"));
    //     $sheet->setCellValue('C4', ("Penerima"));
    //     $sheet->setCellValue('F4', ("Pembayaran"));
    //     $sheet->setCellValue('G4', ("Status"));
    //     $sheet->setCellValue('H4', ("Belanja"));
    //     $sheet->setCellValue('A5', ("No. Invoice"));
    //     $sheet->setCellValue('B5', ("No.Pesanan"));
    //     $sheet->setCellValue('C5', ("Nama"));
    //     $sheet->setCellValue('D5', ("Alamat"));
    //     $sheet->setCellValue('E5', ("No. Telp"));
    //     $sheet->setCellValue('F5', ("Nominal"));
    //     $sheet->setCellValue('H5', ("Produk"));
    //     $sheet->setCellValue('I5', ("Harga"));
    //     $sheet->setCellValue('J5', ("Kuantitas"));
    //     $sheet->setCellValue('K5', ("Sub Total"));

















    //     $writer = new Xlsx($spreadsheet);
    //     $fileName = 'TEsting.xlsx';
    //     header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    //     header('Content-Disposition: attachment; filename="' . urlencode($fileName) . '"');
    //     $writer->save('php://output');
    // }
}
