<?php

namespace App\Http\Controllers;

use App\Models\Kota;
use App\Models\Setting;
use App\Models\Provinsi;
use Illuminate\Http\Request;

class DashboardProvinsiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $setting = Setting::first();
        $keyword = $request->keyword;
        $provinsi = Provinsi::where('nm_provinsi', 'LIKE', '%' . $keyword . '%')->Paginate(4);
        return view('dashboard.provinsi.index', compact(
            'provinsi',
            'keyword',
            'setting'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $setting = Setting::first();
        $tambah = Provinsi::all();
        return view('dashboard.provinsi.create', compact(
            'tambah',
            'setting'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nm_provinsi' => 'required|string',
        ]);
        Provinsi::create($validatedData);


        return redirect('dashboard/provinsi')->with('successcreate', 'Berhasil Menambahkan Data!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Provinsi  $provinsi
     * @return \Illuminate\Http\Response
     */
    public function show(Provinsi $provinsi)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Provinsi  $provinsi
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $setting = Setting::first();
        $provinsi = Provinsi::find($id);
        return view('dashboard.provinsi.edit', compact(
            'provinsi',
            'setting'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Provinsi  $provinsi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = Provinsi::find($id);
        $validatedData = $request->validate([
            'nm_provinsi' => 'required|string',
        ]);
        $update->update($validatedData);


        return redirect('dashboard/provinsi')->with('successupdate', 'Berhasil Mengupdate Data!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Provinsi  $provinsi
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $hapus = Provinsi::find($id);
        $hapus->delete();
        return redirect('dashboard/provinsi')->with('successdelete', 'Delete Successfull!');
    }

    public function kota($id)
    {
        $kotas = Kota::where('provinsi_id', $id)->get();
        return response()->json($kotas);
    }
}
