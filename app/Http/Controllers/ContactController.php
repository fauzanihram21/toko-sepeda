<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use App\Models\Kategori;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function index()
    {
        $setting = Setting::first();
        $kategori = Kategori::get()->take(4);

        return view('front.contact.index', compact(
            'setting',
            'kategori'
        ));
    }
}
