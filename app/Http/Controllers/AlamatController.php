<?php

namespace App\Http\Controllers;

use App\Models\DaftarAlamat;
use App\Models\Setting;
use App\Models\Kategori;
use App\Models\Kota;
use App\Models\Provinsi;
use Illuminate\Http\Request;

class AlamatController extends Controller
{
    public function index(Request $request)
    {
        $setting = Setting::first();
        $kategori = Kategori::all();
        $provinsi = Provinsi::all();
        $kota = Kota::all();
        $alamat = DaftarAlamat::where('user_id', auth()->user()->id)->get();


        return view('front.alamat.index', compact(
            'kategori',
            'alamat',
            'provinsi',
            'kota',
            'setting'
        ));
    }



    public function indextambahalamat(Request $request)
    {
        $setting = Setting::first();
        $kategori = Kategori::all();
        $provinsi = Provinsi::all();
        $kota = Kota::all();
        $alamat = DaftarAlamat::where('user_id', auth()->user()->id)->get();


        return view('front.alamat.tambahalamat.index', compact(
            'kategori',
            'alamat',
            'provinsi',
            'kota',
            'setting'
        ));
    }

    public function tambahalamat(Request $request)
    {
        $validatedData = $request->validate([
            'nama_penerima' => 'required|string',
            'kodepos' => 'required|min:5|max:6',
            'detail_alamat' => 'required|string',
            'phone' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|max:13',
            'provinsi_id' => 'required|string',
            'kota_id' => 'required|string'

        ]);


        $validatedData['user_id'] = auth()->user()->id;
        DaftarAlamat::create($validatedData);

        if (isset($request->redirect)) return redirect($request->redirect);

        return redirect('/alamatsaya')->with('successcreate', 'Berhasil Menambahkan Alamat!');
    }


    public function update(Request $request,  $id)
    {
        $tambah = DaftarAlamat::find($id);
        $validatedData = $request->validate([
            'nama_penerima' => 'required|string',
            'kodepos' => 'required|min:5|max:6',
            'detail_alamat' => 'required|string',
            'phone' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|max:13',
            'provinsi_id' => 'required|string',
            'kota_id' => 'required|string'
        ]);
        $validatedData['user_id'] = auth()->user()->id;
        $tambah->update($validatedData);


        return redirect('/alamatsaya')->with('successupdate', 'Berhasil Mengupdate Alamat!');
    }

    public function indexeditalamat(Request $request, $id)
    {
        $setting = Setting::first();
        $kategori = Kategori::all();
        $provinsi = Provinsi::all();
        $alamat = DaftarAlamat::find($id);
        $kota = Kota::where('provinsi_id', $alamat->provinsi_id)->get();


        return view('front.alamat.editalamat.index', compact(
            'kategori',
            'alamat',
            'provinsi',
            'kota',
            'setting'
        ));
    }

    // public function editalamat(Request $request)
    // {
    //     dd($request->all());
    //     $validatedData = $request->validate([
    //         'nama_penerima' => 'required|string',
    //         'kodepos' => 'required',
    //         'detail_alamat' => 'required|string',
    //         'phone' => 'required|min:5|max:6',
    //         'provinsi_id' => 'required|string',
    //         'kota_id' => 'required|string'

    //     ]);


    //     $validatedData['user_id'] = auth()->user()->id;
    //     DaftarAlamat::create($validatedData);

    //     if (isset($request->redirect)) return redirect($request->redirect);

    //     return redirect('/alamatsaya')->with('successcreate', 'Create Successfull!');
    // }




    public function delete($id)
    {
        $tambah = DaftarAlamat::find($id);
        $tambah->delete();
        return redirect('/alamatsaya')->with('successdelete', 'Berhasil Menghapus Data!');
    }
}
