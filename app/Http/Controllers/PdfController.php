<?php

namespace App\Http\Controllers;

use App\Models\Produk;
use App\Models\Setting;
use App\Models\Pembayaran;
use App\Models\Pengiriman;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade\Pdf;

class PdfController extends Controller
{
    public function pdfpesananbaru($id)
    {
        $setting = Setting::first();
        $pembayaran = Pembayaran::with(['Pengiriman', 'Pesanan.Produk.ProdukVariasi'])->find($id);
        $pdf = PDF::loadView('dashboard.pdf.pesananbarupdf', compact('pembayaran', 'setting'));
        return $pdf->download('Pesanan Baru.pdf');
    }

    public function pdfdiproses($id)
    {
        $setting = Setting::first();
        $pembayaran = Pembayaran::with(['Pengiriman', 'Pesanan.Produk.ProdukVariasi'])->find($id);
        $pdf = PDF::loadView('dashboard.pdf.pesanandiprosespdf', compact('pembayaran', 'setting'));
        return $pdf->download('Pesanan Diproses.pdf');
    }

    public function pdfdalampengiriman($id)
    {
        $setting = Setting::first();
        $pembayaran = Pembayaran::with(['Pengiriman', 'Pesanan.Produk.ProdukVariasi'])->find($id);
        $pdf = PDF::loadView('dashboard.pdf.pesanandalampengirimanpdf', compact('pembayaran', 'setting'));
        return $pdf->download('Pesanan Dalam Pengiriman.pdf');
    }

    public function pdfselesai($id)
    {
        $setting = Setting::first();
        $pembayaran = Pembayaran::with(['Pengiriman', 'Pesanan.Produk.ProdukVariasi'])->find($id);
        $pdf = PDF::loadView('dashboard.pdf.pesananselesaipdf', compact('pembayaran', 'setting'));
        return $pdf->download('Pesanan Selesai.pdf');
    }

    public function riwayatpdfselesai($id)
    {
        $setting = Setting::first();
        $pembayaran = Pembayaran::with(['Pengiriman', 'Pesanan.Produk.ProdukVariasi'])->find($id);
        $pdf = PDF::loadView('dashboard.pdf.userpesananselesaipdf', compact('pembayaran', 'setting'));
        return $pdf->download('Invoice Pesanan.pdf');
    }
    // public function pdfproduk($request)
    // {
    //     $setting = Setting::first();
    //     $keyword = $request->keyword;
    //     $produk = Produk::where(function ($q) use ($keyword) {
    //         $q->where('nm_produk', 'LIKE', '%' . $keyword . '%');
    //         $q->orWhere('deskripsi', 'LIKE', '%' . $keyword . '%');
    //         $q->orWhere('harga', 'LIKE', '%' . $keyword . '%');
    //         $q->orWhere('diskon', 'LIKE', '%' . $keyword . '%');
    //         $q->orWhere('rating', 'LIKE', '%' . $keyword . '%');
    //         $q->orWhere('berat', 'LIKE', '%' . $keyword . '%');

    //         $q->orWhereHas('subkategori', function ($q) use ($keyword) {
    //             $q->where('nm_sub_kategori', 'LIKE', '%' . $keyword . '%');
    //         });

    //         $q->orWhereHas('merk', function ($q) use ($keyword) {
    //             $q->where('nm_merk', 'LIKE', '%' . $keyword . '%');
    //         });

    //         $q->orWhereHas('spesifikasi', function ($q) use ($keyword) {
    //             $q->where('nm_spesifikasi', 'LIKE', '%' . $keyword . '%');
    //             $q->orwhere('deskripsi', 'LIKE', '%' . $keyword . '%');
    //         });

    //         $q->orWhereHas('produkvariasi', function ($q) use ($keyword) {
    //             $q->where('stock', 'LIKE', '%' . $keyword . '%');
    //             $q->orwhere('warna', 'LIKE', '%' . $keyword . '%');
    //         });
    //     })->get();
    //     $pdf = PDF::loadView('dashboard.pdf.produkpdf', compact('pembayaran', 'setting'));
    //     return $pdf->download('Laporan Produk.pdf');
    // }

    public function pdfharipenjualan(Request $request)
    {
        $date = $request->date;
        $setting = Setting::first();
        $pembayaran = Pembayaran::where('status', 'selesai')->with(['Pengiriman', 'Pesanan.Produk.ProdukVariasi'])->whereDate('created_at', $date)->get();
        $totalPendapatan = Pembayaran::where('status', 'selesai')->whereDate('created_at', $date)->sum('total_harga');
        $totalOngkir = Pengiriman::whereHas('Pembayaran', function ($q) use ($date) {
            $q->where('status', 'selesai');
            $q->whereDate('created_at', $date);
        })->sum('ongkir');
        $totalPendapatan = $totalPendapatan - $totalOngkir;

        foreach ($pembayaran as $psn) {
            $total_psn = 0;
            foreach ($psn->Pesanan as $psnnn) {
                $total_psn += $psnnn->sub_total;
            }
            $psn->total_psn = $total_psn;
        }
        $pdf = PDF::loadView('dashboard.pdf.penjualanpdf', compact('pembayaran', 'setting', 'total_psn', 'totalPendapatan'));
        return $pdf->download('Laporan Penjualan.pdf');
    }

    public function pdfbulantahunpenjualan(Request $request)
    {
        $month = $request->month;
        $year = $request->year;
        $setting = Setting::first();
        $pembayaran = Pembayaran::where('status', 'selesai')->with(['Pengiriman', 'Pesanan.Produk.ProdukVariasi'])->whereMonth('created_at', $month)->whereYear('created_at', $year)->get();
        $totalPendapatan = Pembayaran::where('status', 'selesai')->whereMonth('created_at', $month)->whereYear('created_at', $year)->sum('total_harga');
        $totalOngkir = Pengiriman::whereHas('Pembayaran', function ($q) use ($month, $year) {
            $q->where('status', 'selesai');
            $q->whereMonth('created_at', $month);
            $q->whereYear('created_at', $year);
        })->sum('ongkir');
        $totalPendapatan = $totalPendapatan - $totalOngkir;
        foreach ($pembayaran as $psn) {
            $total_psn = 0;
            foreach ($psn->Pesanan as $psnnn) {
                $total_psn += $psnnn->sub_total;
            }
            $psn->total_psn = $total_psn;
        }
        $pdf = PDF::loadView('dashboard.pdf.penjualanpdf', compact('pembayaran', 'setting', 'total_psn', 'totalPendapatan'));
        return $pdf->download('Laporan Penjualan.pdf');
    }

    public function pdfhariproduk(Request $request)
    {
        $date = $request->date;
        $setting = Setting::first();
        $produk = Produk::with(['SubKategori.Kategori', 'ProdukVariasi', 'Spesifikasi'])->whereDate('created_at', $date)->get();
        $pdf = PDF::loadView('dashboard.pdf.produkpdf', compact('produk', 'setting'));
        return $pdf->download('Laporan Penjualan.pdf');
    }

    public function pdfbulantahunproduk(Request $request)
    {
        $month = $request->month;
        $year = $request->year;
        $setting = Setting::first();
        $produk = Produk::with(['SubKategori.Kategori', 'ProdukVariasi', 'Spesifikasi'])->whereMonth('created_at', $month)->whereYear('created_at', $year)->get();
        $pdf = PDF::loadView('dashboard.pdf.produkpdf', compact('produk', 'setting'));
        return $pdf->download('Laporan Penjualan.pdf');
    }
}
