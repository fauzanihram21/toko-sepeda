<?php

namespace App\Http\Controllers;

use App\Models\pesanan;
use App\Models\Produk;
use App\Models\Rating;
use App\Models\Setting;
use Illuminate\Http\Request;

class RatingController extends Controller
{
    public function index(Request $request)
    {
        $setting = Setting::first();
        $keyword = $request->keyword;
        $rating = Rating::where(function ($q) use ($keyword) {
            $q->where('komentar', 'LIKE', '%' . $keyword . '%');

            $q->orWhereHas('user', function ($q) use ($keyword) {
                $q->where('name', 'LIKE', '%' . $keyword . '%');
            });

            $q->orWhereHas('produk', function ($q) use ($keyword) {
                $q->where('nm_produk', 'LIKE', '%' . $keyword . '%');
            });
        })->paginate(4);
        return view('dashboard.rating.index', compact(
            'setting',
            'keyword',
            'rating'
        ));
    }

    public function rating(Request $request, $uuid)
    {
        $validatedData = $request->validate([
            'komentar' => 'required|string',
            'total_rating' => 'required|integer',


        ]);

        $rating = $request->all();

        if (isset($request->gambar)) {
            $file = $request->file('gambar');
            $nama_file = time() . str_replace(" ", " ", $file->getClientOriginalName());
            $file->move('storage/produk_rating', $nama_file);
            $validatedData['gambar'] = $nama_file;
        }





        $validatedData['user_id'] = auth()->user()->id;
        $validatedData['produk_id'] = Produk::findByUuid($uuid)->id;

        if (pesanan::where('produk_id', $validatedData['produk_id'])->whereHas('Pembayaran', function ($q) {
            $q->where('user_id', auth()->user()->id);
        })->whereHas('Pembayaran', function ($q) {
            $q->where('status', 'selesai');
        })->first() == null) {
            return redirect()->back()->with('error', 'Anda Belum Membeli Produk Ini');
        }
        if ($rating = Rating::where('user_id', auth()->user()->id)->where('produk_id', $validatedData['produk_id'])->first()) {
            $rating->update($validatedData);
        } else {

            Rating::create($validatedData);
        }



        return redirect('/katalog/detail/' . $uuid)->with('successcreate', 'Berhasil Menambahkan Rating!');
    }
    public function adminhapusrating($id)
    {
        $hapus = Rating::find($id);
        $hapus->delete();
        return redirect('/rating')->with('successdelete', 'Delete Successfull!');
    }





    public function userhapusrating($uuid, $id)
    {
        $hapus = Rating::where('id', $id)->where('user_id', auth()->user()->id)->first();
        $hapus->delete();
        return redirect('/katalog/detail/' . $uuid)->with('successdelete', 'Delete Successfull!');
    }
}
