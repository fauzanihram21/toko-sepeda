<?php

namespace App\Http\Controllers;

use App\Models\Pembayaran;
use Illuminate\Http\Request;
use App\Services\Midtrans\CallbackService;

class PaymentCallbackController extends Controller

{
    public function receive()
    {
        $callback = new CallbackService;

        if ($callback->isSignatureKeyVerified()) {
            $notification = $callback->getNotification();
            $pembayaran = $callback->getOrder();

            if ($callback->isSuccess()) {
                Pembayaran::where('id', $pembayaran->id)->update([
                    'payment_status' => 'dibayar',
                    'status' => 'pesanan_diterima',
                ]);
            }

            if ($callback->isExpire()) {
                Pembayaran::where('id', $pembayaran->id)->update([
                    'payment_status' => 'kadaluarsa',
                ]);
            }

            if ($callback->isCancelled()) {
                Pembayaran::where('id', $pembayaran->id)->update([
                    'payment_status' => 'batal',
                ]);
            }

            return response()
                ->json([
                    'success' => true,
                    'message' => 'Notifikasi berhasil diproses',
                ]);
        } else {
            return response()
                ->json([
                    'error' => true,
                    'message' => 'Signature key tidak terverifikasi',
                ], 403);
        }
    }
}
