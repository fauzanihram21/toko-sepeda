<?php

namespace App\Http\Controllers;

use App\Models\Bank;
use App\Models\Setting;
use App\Models\Kategori;
use App\Models\Keranjang;
use App\Models\DaftarAlamat;
use App\Models\Pembayaran;
use App\Models\Pengiriman;
use App\Models\pesanan;
use App\Models\Produk;
use App\Models\ProdukVariasi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Services\Midtrans\CreateSnapTokenService;
use Illuminate\Support\Str;

class CheckoutController extends Controller
{
    public function index(Request $request)
    {
        $setting = Setting::first();

        $kategori = Kategori::all();
        $keranjang = Keranjang::with(['Produk.SubKategori', 'ProdukVariasi'])->where('user_id', auth()->user()->id)->get();
        $alamat = DaftarAlamat::where('user_id', auth()->user()->id)->get();
        $total = 0;
        foreach ($keranjang as $i) {
            $total += $i->subtotal;
        }



        return view('front.checkout.index', compact(
            'kategori',
            'setting',
            'total',
            'alamat',
            'keranjang'
        ));
    }



    public function pembayaran(Request $request)
    {
        $setting = Setting::first();
        $kategori = Kategori::all();
        $keranjang = Keranjang::with(['Produk.SubKategori', 'ProdukVariasi'])->where('user_id', auth()->user()->id)->get();
        $alamat = DaftarAlamat::where('id', $request->daftar_alamat_id)->first();
        $ongkir = $request->ongkir;
        $courier = $request->courier;

        $totalpembayaran = 0;
        $total = 0;
        foreach ($keranjang as $i) {
            $total += $i->subtotal;
        }
        $estimasi = $request->estimasi;
        $ongkir = $request->ongkir;
        $service = $request->service;
        $deskripsi = $request->deskripsi;


        return view('front.pembayaran.index', compact(
            'kategori',
            'setting',
            'total',
            'alamat',
            'keranjang',
            'ongkir',
            'deskripsi',
            'estimasi',
            'service'
        ));
    }

    public function charge(Request $request)
    {
        $keranjang = Keranjang::with(['Produk', 'ProdukVariasi'])->where('user_id', auth()->user()->id)->get();

        $pembayaran = Pembayaran::create([
            'user_id' => auth()->user()->id,
            'no_invoice' => 'INV' . rand(10000, 99999) . date('dmYhms') . auth()->user()->id,
            'no_pesanan' => Str::substr((string) Str::uuid(), 0, 8) . auth()->user()->id,
            'catatan' => $request->deskripsi
        ]);

        $total_harga = 0;
        foreach ($keranjang as $item) {
            $pesanan = pesanan::create([
                'pembayaran_id' => $pembayaran->id,
                'kuantitas' => $item->kuantitas,
                'sub_total' => $item->produk->harga_diskon * $item->kuantitas,
                'produk_id' => $item->produk->id,
                'produk_variasi_id' => $item->produk_variasi_id,
            ]);

            $ProdukVariasi = ProdukVariasi::where('id', $item->produk_variasi_id)->first();
            $ProdukVariasi->update([
                'stock' => $ProdukVariasi->stock - $item->kuantitas
            ]);

            $total_harga += $pesanan->sub_total;
        }

        $total_harga += $request->ongkir;

        $alamat = DaftarAlamat::find($request->daftar_alamat_id);
        $pengiriman = Pengiriman::create([
            'alamat' => $alamat->detail_alamat,
            'kode_pos' => $alamat->kodepos,
            'no_telp' => $alamat->phone,
            'nama_penerima' => $alamat->nama_penerima,
            'provinsi_id' => $alamat->provinsi_id,
            'kota_id' => $alamat->kota_id,
            'nm_ekspedisi' => $request->courier,
            'ongkir' => $request->ongkir,
            'paket_layanan' => $request->service,
            'estimasi' => $request->estimasi,

        ]);

        $pembayaran->update(['total_harga' => $total_harga, 'pengiriman_id' => $pengiriman->id]);
        $midtrans = new CreateSnapTokenService($pembayaran);
        $snapToken = $midtrans->getSnapToken();

        if ($pembayaran->update(['snap_token' => $snapToken])) {
            Keranjang::where('user_id', auth()->user()->id)->delete();
        }

        return response()->json(['snap_token' => $snapToken]);
    }


    public function getOngkir(Request $request)
    {
        $data = $request->validate([
            'daftar_alamat_id' => 'required',
            'courier' => 'required|in:jne,tiki,pos'
        ]);

        $alamat = DaftarAlamat::find($request->daftar_alamat_id);
        $keranjangs = Keranjang::with(['Produk'])->where('user_id', auth()->user()->id)->get();
        $weight = 0;
        foreach ($keranjangs as $keranjang) {
            $weight += $keranjang->Produk->berat;
        }

        $response = Http::asForm()->withHeaders([
            'key' => getenv('RAJAONGKIR_API_KEY'),
        ])->post('https://api.rajaongkir.com/starter/cost', [
            'origin' => 444,
            'destination' => $alamat->id,
            'courier' => $data['courier'],
            'weight' => $weight * 1000
        ]);

        return response()->json($response->json()['rajaongkir'], $response->status());
    }
}
