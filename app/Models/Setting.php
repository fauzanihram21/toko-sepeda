<?php

namespace App\Models;

use App\Traits\Uuid;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    use Uuid;

    protected $fillable = [
        'uuid', 'name', 'deskripsi', 'background_login', 'background_register', 'logo_luar', 'logo_dalam', 'alamat', 'no_telp', 'email', 'facebook', 'instagram', 'whatsapp'
    ];
}
