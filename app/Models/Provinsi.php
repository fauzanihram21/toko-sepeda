<?php

namespace App\Models;

use App\Traits\Uuid;

use Illuminate\Database\Eloquent\Model;

class Provinsi extends Model
{
    use Uuid;

    protected $fillable = [
        'uuid', 'nm_provinsi', 'kota_id'
    ];
    public function Kota()
    {
        return $this->hasMany('\App\Models\Kota', 'kota_id');
    }
}
