<?php

namespace App\Models;

use App\Traits\Uuid;

use Illuminate\Database\Eloquent\Model;

class Pembayaran extends Model
{
    use Uuid;

    protected $fillable = [
        'uuid', 'status', 'no_invoice', 'catatan', 'snap_token', 'bukti_pembayaran', 'user_id', 'pengiriman_id', 'total_harga', 'no_pesanan', 'payment_status'
    ];

    public function Pesanan()
    {
        return $this->hasMany('\App\Models\Pesanan');
    }
    public function User()
    {
        return $this->belongsTo('\App\Models\User', 'user_id');
    }
    public function Pengiriman()
    {
        return $this->belongsTo('\App\Models\Pengiriman', 'pengiriman_id');
    }
}
