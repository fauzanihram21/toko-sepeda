<?php

namespace App\Models;

use App\Traits\Uuid;

use Illuminate\Database\Eloquent\Model;

class DaftarAlamat extends Model
{
    use Uuid;

    protected $fillable = [
        'uuid', 'nama_penerima', 'kodepos', 'detail_alamat', 'phone', 'kota_id', 'provinsi_id', 'user_id'
    ];



    public function Provinsi()
    {
        return $this->belongsTo('\App\Models\Provinsi', 'provinsi_id');
    }
    public function Kota()
    {
        return $this->belongsTo('\App\Models\Kota', 'kota_id');
    }
    public function User()
    {
        return $this->belongsTo('\App\Models\User', 'user_id');
    }
}
