<?php

namespace App\Models;

use App\Traits\Uuid;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    use Uuid;

    protected $fillable = [
        'uuid', 'nm_kategori'
    ];

    public function SubKategori()
    {
        return $this->hasMany('\App\Models\SubKategori');
    }

    public function Produk()
    {
        return $this->hasManyThrough('\App\Models\Produk', '\App\Models\SubKategori');
    }
}
