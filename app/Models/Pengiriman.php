<?php

namespace App\Models;

use App\Traits\Uuid;

use Illuminate\Database\Eloquent\Model;

class Pengiriman extends Model
{
    use Uuid;

    protected $table = 'pengirimans';
    protected $fillable = [
        'uuid', 'alamat', 'kode_pos', 'no_telp', 'kurir', 'nm_ekspedisi', 'jenis_pengemasan', 'nama_penerima', 'ongkir', 'estimasi', 'paket_layanan', 'tanggal', 'provinsi_id', 'kota_id', 'no_resi'
    ];
    public function Provinsi()
    {
        return $this->belongsTo('\App\Models\Provinsi', 'provinsi_id');
    }
    public function Kota()
    {
        return $this->belongsTo('\App\Models\Kota', 'kota_id');
    }

    public function Pembayaran()
    {
        return $this->hasOne('\App\Models\Pembayaran');
    }
}
