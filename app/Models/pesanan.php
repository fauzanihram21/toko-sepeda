<?php

namespace App\Models;

use App\Traits\Uuid;

use Illuminate\Database\Eloquent\Model;

class pesanan extends Model
{
    use Uuid;

    protected $fillable = [
        'uuid', 'kuantitas', 'sub_total', 'pembayaran_id', 'produk_id', 'produk_variasi_id'
    ];
    public function Pembayaran()
    {
        return $this->belongsTo('\App\Models\Pembayaran', 'pembayaran_id');
    }
    public function Produk()
    {
        return $this->belongsTo('\App\Models\Produk', 'produk_id');
    }
    public function ProdukVariasi()
    {
        return $this->belongsTo('\App\Models\ProdukVariasi', 'produk_variasi_id');
    }
}
