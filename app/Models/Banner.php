<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;


class Banner extends Model
{
    use Uuid;

    protected $fillable = [
        'uuid', 'heading', 'sub_heading', 'gambar'
    ];
}
