<?php

namespace App\Models;

use App\Traits\Uuid;

use Illuminate\Database\Eloquent\Model;

class Fitur extends Model
{
    use Uuid;

    protected $fillable = [
        'uuid', 'heading', 'sub_heading', 'gambar'
    ];
}
