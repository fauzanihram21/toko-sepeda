<?php

namespace App\Models;

use App\Traits\Uuid;

use Illuminate\Database\Eloquent\Model;

class SubKategori extends Model
{
    use Uuid;

    protected $fillable = [
        'uuid', 'nm_sub_kategori', 'kategori_id'
    ];
    public function Kategori()
    {
        return $this->belongsTo('\App\Models\Kategori', 'kategori_id');
    }

    public function Produk()
    {
        return $this->hasMany('\App\Models\Produk');
    }
}
