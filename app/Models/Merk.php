<?php

namespace App\Models;

use App\Traits\Uuid;

use Illuminate\Database\Eloquent\Model;

class Merk extends Model
{
    use Uuid;

    protected $fillable = [
        'uuid', 'nm_merk','gambar'
    ];
}
