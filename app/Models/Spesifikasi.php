<?php

namespace App\Models;

use App\Traits\Uuid;

use Illuminate\Database\Eloquent\Model;

class Spesifikasi extends Model
{
    use Uuid;

    protected $fillable = [
        'uuid', 'nm_spesifikasi', 'deskripsi', 'produk_id'
    ];
    public function Produk()
    {
        return $this->belongsTo('\App\Models\Produk', 'produk_id');
    }
}
