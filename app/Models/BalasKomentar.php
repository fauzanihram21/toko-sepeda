<?php

namespace App\Models;

use App\Traits\Uuid;

use Illuminate\Database\Eloquent\Model;

class BalasKomentar extends Model
{
    use Uuid;

    protected $fillable = [
        'uuid', 'komentar', 'user_id', 'komentar_id'
    ];


    public function Komentar()
    {
        return $this->belongsTo('\App\Models\Komentar', 'komentar_id');
    }
    public function User()
    {
        return $this->belongsTo('\App\Models\User', 'user_id');
    }
}
