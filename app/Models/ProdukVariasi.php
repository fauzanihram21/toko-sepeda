<?php

namespace App\Models;

use App\Traits\Uuid;

use Illuminate\Database\Eloquent\Model;

class ProdukVariasi extends Model
{
    use Uuid;

    protected $fillable = [
        'uuid', 'warna', 'stock', 'gambar', 'produk_id'
    ];
    public function Produk()
    {
        return $this->belongsTo('\App\Models\Produk', 'produk_id');
    }
}
