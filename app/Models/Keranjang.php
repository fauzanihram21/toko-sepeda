<?php

namespace App\Models;

use App\Traits\Uuid;

use Illuminate\Database\Eloquent\Model;

class Keranjang extends Model
{
    use Uuid;

    protected $fillable = [
        'uuid', 'kuantitas', 'produk_id', 'user_id', 'produk_variasi_id'
    ];
    protected $appends = ['subtotal'];

    public function Produk()
    {
        return $this->belongsTo('\App\Models\Produk', 'produk_id');
    }

    public function ProdukVariasi()
    {
        return $this->belongsTo('\App\Models\ProdukVariasi', 'produk_variasi_id');
    }

    public function User()
    {
        return $this->belongsTo('\App\Models\User', 'user_id');
    }

    public function getSubtotalAttribute()
    {
        return $this->kuantitas * $this->Produk->harga_diskon;
    }
}
