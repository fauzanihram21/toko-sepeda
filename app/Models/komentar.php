<?php

namespace App\Models;

use App\Traits\Uuid;

use Illuminate\Database\Eloquent\Model;

class komentar extends Model
{
    use Uuid;

    protected $fillable = [
        'uuid', 'komentar', 'user_id', 'produk_id', 'balas_komentar_id', 'balas_komentar_admin_id'
    ];

    public function User()
    {
        return $this->belongsTo('\App\Models\User', 'user_id');
    }
    public function Produk()
    {
        return $this->belongsTo('\App\Models\Produk', 'produk_id');
    }
    public function BalasKomentar()
    {
        return $this->hasMany('\App\Models\BalasKomentar');
    }
    public function BalasKomentarAdmin()
    {
        return $this->hasMany('\App\Models\BalasKomentarAdmin');
    }
}
