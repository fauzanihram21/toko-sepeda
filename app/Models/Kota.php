<?php

namespace App\Models;

use App\Traits\Uuid;

use Illuminate\Database\Eloquent\Model;

class Kota extends Model
{
    use Uuid;

    protected $fillable = [
        'uuid', 'nm_kota', 'provinsi_id'
    ];

    public function Provinsi()
    {
        return $this->belongsTo('\App\Models\Provinsi', 'provinsi_id');
    }
}
