<?php

namespace App\Models;

use App\Traits\Uuid;

use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    use Uuid;

    protected $fillable = [
        'uuid', 'nm_produk', 'deskripsi', 'merk', 'harga', 'diskon', 'berat', 'gambar', 'sub_kategori_id', 'merk_id'
    ];
    protected $appends = ['harga_diskon', 'total_laku', 'total_stok'];

    public function getHargaDiskonAttribute()
    {
        $diskon = $this->diskon / 100 * $this->harga;
        return $this->harga - $diskon;
    }

    public function SubKategori()
    {
        return $this->belongsTo('\App\Models\SubKategori', 'sub_kategori_id');
    }
    public function Spesifikasi()
    {
        return $this->hasMany('\App\Models\Spesifikasi');
    }
    public function ProdukVariasi()
    {
        return $this->hasMany('\App\Models\ProdukVariasi');
    }
    public function Merk()
    {
        return $this->belongsTo('\App\Models\Merk');
    }
    public function Pesanan()
    {
        return $this->hasMany('\App\Models\Pesanan');
    }


    public function getTotalLakuAttribute()
    {
        $total_laku = 0;
        foreach ($this->pesanan as $pesanan) {
            $total_laku += $pesanan->kuantitas;
        }
        return $total_laku;
    }

    public function getTotalStokAttribute()
    {
        $total_stok = 0;
        foreach ($this->ProdukVariasi as $var) {
            $total_stok += $var->stock;
        }
        return $total_stok;
    }
}
