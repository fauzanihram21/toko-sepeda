<?php

namespace App\Models;

use App\Traits\Uuid;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    use Uuid;

    protected $fillable = [
        'uuid', 'gambar', 'komentar', 'total_rating', 'user_id', 'produk_id'
    ];
    public function User()
    {
        return $this->belongsTo('\App\Models\User', 'user_id');
    }
    public function Produk()
    {
        return $this->belongsTo('\App\Models\Produk', 'produk_id');
    }
}
