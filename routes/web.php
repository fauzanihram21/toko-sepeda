<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PdfController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\AlamatController;
use App\Http\Controllers\RatingController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\PesananController;
use App\Http\Controllers\CheckoutController;
use App\Http\Controllers\HomeMerkController;
use App\Http\Controllers\KomentarController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\FrontUserController;
use App\Http\Controllers\KeranjangController;
use App\Http\Controllers\DashboardBankController;
use App\Http\Controllers\DashboardKotaController;
use App\Http\Controllers\DashboardMerkController;
use App\Http\Controllers\DashboardAdminController;
use App\Http\Controllers\RiwayatPesananController;
use App\Http\Controllers\DashboardGrafikController;
use App\Http\Controllers\DashboardProdukController;
use App\Http\Controllers\DashboardReportController;
use App\Http\Controllers\KatalogKategoriController;
use App\Http\Controllers\PaymentCallbackController;
use App\Http\Controllers\DashboardSettingController;
use App\Http\Controllers\DashboardCustomerController;
use App\Http\Controllers\DashboardKategoriController;
use App\Http\Controllers\DashboardProvinsiController;
use App\Http\Controllers\KatalogSubKategoriController;
use App\Http\Controllers\KatalogProdukDetailController;
use App\Http\Controllers\DashboardSubKategoriController;
use App\Http\Controllers\DashboardHalamanBrandController;
use App\Http\Controllers\DashboardHalamanFiturController;
use App\Http\Controllers\DashboardHalamanBannerController;
use App\Http\Controllers\DashboardHalamanDiskonController;
use App\Http\Controllers\DashboardHalamanPosterController;
use App\Http\Controllers\DashboardHalamanLatestProdukController;
use App\Http\Controllers\DashboardHalamanRelatedProdukController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/login', [LoginController::class, 'index'])->name('login')->middleware('guest');
Route::post('/login', [LoginController::class, 'authenticate']);
Route::post('/logout', [LoginController::class, 'logout']);


Route::get('/register', [RegisterController::class, 'index'])->name('register')->middleware('guest');
Route::post('/register', [RegisterController::class, 'store']);


// Route::get('/dashboard', function () {
//     return view('dashboard.index');
// })->middleware('admin');

Route::get('/dashboard', [DashboardController::class, 'index'])->middleware('admin');
Route::get('/dashboardprofile', [DashboardAdminController::class, 'indexprofile'])->middleware('admin');
Route::post('/dashboardprofile/profileadmin', [DashboardAdminController::class, 'profile'])->middleware('admin');
Route::get('/dashboardchangepassword', [DashboardAdminController::class, 'indexchangepassword'])->middleware('admin');
Route::post('/dashboardchangepassword/changepasswordadmin', [DashboardAdminController::class, 'changepassword'])->middleware('admin');
Route::resource('/dashboard/kategori', DashboardKategoriController::class)->middleware('admin');
Route::resource('/dashboard/subkategori', DashboardSubKategoriController::class)->middleware('admin');
Route::resource('/dashboard/produk', DashboardProdukController::class)->middleware('admin');
Route::resource('/dashboard/provinsi', DashboardProvinsiController::class)->middleware('admin');
Route::resource('/dashboard/kota', DashboardKotaController::class)->middleware('admin');
Route::resource('/dashboard/merk', DashboardMerkController::class)->middleware('admin');
Route::resource('/dashboard/bank', DashboardBankController::class)->middleware('admin');
Route::resource('/dashboard/customer', DashboardCustomerController::class)->middleware('admin');
Route::get('/dashboard/customer/{id}/detail', [DashboardCustomerController::class, 'show'])->middleware('admin');


Route::get('/dashboard/setting', [DashboardSettingController::class, 'index'])->middleware('admin');
Route::post('/dashboard/setting/website', [DashboardSettingController::class, 'website'])->middleware('admin');
Route::post('/dashboard/setting/perusahaan', [DashboardSettingController::class, 'perusahaan'])->middleware('admin');
Route::post('/dashboard/setting/media', [DashboardSettingController::class, 'media'])->middleware('admin');
Route::post('/dashboard/setting/background', [DashboardSettingController::class, 'background'])->middleware('admin');

Route::resource('/dashboard/halaman/banner', DashboardHalamanBannerController::class)->middleware('admin');
Route::resource('/dashboard/halaman/brand', DashboardHalamanBrandController::class)->middleware('admin');
Route::resource('/dashboard/halaman/poster', DashboardHalamanPosterController::class)->middleware('admin');
Route::resource('/dashboard/halaman/fitur', DashboardHalamanFiturController::class)->middleware('admin');
Route::resource('/dashboard/halaman/diskon', DashboardHalamanDiskonController::class)->middleware('admin');
Route::resource('/dashboard/halaman/latestproduk', DashboardHalamanLatestProdukController::class)->middleware('admin');
Route::resource('/dashboard/halaman/relatedproduk', DashboardHalamanRelatedProdukController::class)->middleware('admin');

Route::get('/dashboard/grafik', [DashboardGrafikController::class, 'index'])->middleware('admin');
Route::get('/dashboard/report/penjualan', [DashboardReportController::class, 'penjualan'])->middleware('admin');
Route::post('/dashboard/report/penjualan/excelhari', [DashboardReportController::class, 'excelharipenjualan'])->middleware('admin');
Route::post('/dashboard/report/penjualan/excelbulantahun', [DashboardReportController::class, 'excelbulantahunpenjualan'])->middleware('admin');
Route::post('/dashboard/report/penjualan/pdfhari', [PdfController::class, 'pdfharipenjualan'])->middleware('admin');
Route::post('/dashboard/report/penjualan/pdfbulantahun', [PdfController::class, 'pdfbulantahunpenjualan'])->middleware('admin');


Route::get('/dashboard/report/produk', [DashboardReportController::class, 'produk'])->middleware('admin');
Route::post('/dashboard/report/produk/excelhari', [DashboardReportController::class, 'excelhariproduk'])->middleware('admin');
Route::post('/dashboard/report/produk/excelbulantahun', [DashboardReportController::class, 'excelbulantahunproduk'])->middleware('admin');
Route::post('/dashboard/report/produk/pdfhari', [PdfController::class, 'pdfhariproduk'])->middleware('admin');
Route::post('/dashboard/report/produk/pdfbulantahun', [PdfController::class, 'pdfbulantahunproduk'])->middleware('admin');




Route::get('/dashboard/pesananbaru', [PesananController::class, 'pesananbaru'])->middleware('admin');
Route::post('/dashboard/pesananbaru', [PesananController::class, 'pesananbaru'])->middleware('admin');
Route::get('/dashboard/pesananbaru/{id}/detail', [PesananController::class, 'showpesanbaru'])->middleware('admin');
Route::post('/dashboard/pesananbaru/{id}/updatestatus', [PesananController::class, 'updateStatuspesananbaru'])->middleware('admin');


Route::get('/dashboard/sedangdiproses', [PesananController::class, 'diproses'])->middleware('admin');
Route::get('/dashboard/sedangdiproses/{id}/detail', [PesananController::class, 'showsedangdiproses'])->middleware('admin');
Route::post('/dashboard/sedangdiproses/{id}/updatestatus', [PesananController::class, 'updatestatusdiproses'])->middleware('admin');
Route::post('/dashboard/sedangdiproses/{id}/kembalistatus', [PesananController::class, 'kembalistatusdiproses'])->middleware('admin');


Route::get('/dashboard/dalampengiriman', [PesananController::class, 'dalampengiriman'])->middleware('admin');
Route::post('/dashboard/dalampengiriman/{id}', [PesananController::class, 'storedalampengiriman'])->middleware('admin');
Route::get('/dashboard/dalampengiriman/{id}/detail', [PesananController::class, 'showpdalampengiriman'])->middleware('admin');
Route::post('/dashboard/dalampengiriman/{id}/updatestatus', [PesananController::class, 'updatedalampengiriman'])->middleware('admin');
Route::post('/dashboard/dalampengiriman/{id}/kembalistatus', [PesananController::class, 'kembalistatusdalampengiriman'])->middleware('admin');


Route::get('/dashboard/telahselesai', [PesananController::class, 'telahselesai'])->middleware('admin');
Route::get('/dashboard/telahselesai/{id}/detail', [PesananController::class, 'showtelahselesai'])->middleware('admin');

Route::get('/dashboard/pesananbatal', [PesananController::class, 'pesananbatal'])->middleware('admin');
Route::delete('/dashboard/pesananbatal/{id}', [PesananController::class, 'hapuspesenanbatal'])->middleware('admin');



Route::get('/dashboard/pdfpesananbaru/{id}', [PdfController::class, 'pdfpesananbaru'])->middleware('admin');
Route::get('/dashboard/pdfdiproses/{id}', [PdfController::class, 'pdfdiproses'])->middleware('admin');
Route::get('/dashboard/pdfdalampengiriman/{id}', [PdfController::class, 'pdfdalampengiriman'])->middleware('admin');
Route::get('/dashboard/pdfselesai/{id}', [PdfController::class, 'pdfselesai'])->middleware('admin');



Route::get('/', [HomeController::class, 'index']);
Route::get('/profile', [FrontUserController::class, 'index'])->middleware('auth');
Route::post('/profile/profilesaya', [FrontUserController::class, 'profilesaya'])->middleware('auth');
Route::get('/profilechangepassword', [FrontUserController::class, 'indexchangepassword'])->middleware('auth');
Route::post('/profilechangepassword/changepasswordadmin', [FrontUserController::class, 'changepassword'])->middleware('auth');


Route::get('/alamatsaya', [AlamatController::class, 'index'])->middleware('auth');
Route::get('/tambahalamatsaya', [AlamatController::class, 'indextambahalamat'])->middleware('auth');
Route::post('/tambahalamatsaya', [AlamatController::class, 'tambahalamat'])->middleware('auth');
Route::get('/editalamatsaya/{id}', [AlamatController::class, 'indexeditalamat'])->middleware('auth');
Route::put('/editalamatsaya/{id}', [AlamatController::class, 'update'])->middleware('auth');
Route::delete('/alamatsaya/{id}', [AlamatController::class, 'delete'])->middleware('auth');


Route::get('/merk', [HomeMerkController::class, 'index']);
Route::get('/merk/{uuid_merk}', [HomeMerkController::class, 'detail']);

Route::get('/kontak', [ContactController::class, 'index']);


Route::get('/katalog/detail/{uuid_produk}', [KatalogProdukDetailController::class, 'index']);
// Route::post('/katalog/produk/{uuid_produk}', [KatalogProdukDetailController::class, 'komentar']);
Route::get('/katalog/{uuid_kategori}', [KatalogKategoriController::class, 'index'])->name('katalog.kategori');
Route::get('/katalog/{uuid_kategori}/{uuid_sub_kategori}', [KatalogSubKategoriController::class, 'index'])->name('katalog.kategori.subkategori');

Route::get('/komentar', [KomentarController::class, 'index'])->middleware('auth');
Route::get('/komentar/{id}/balaskomentar', [KomentarController::class, 'indexbalaskomentar']);
Route::post('/komentar/{uuid_produk}', [KomentarController::class, 'komentar']);
Route::post('/balaskomentar/{uuid_komentar}', [KomentarController::class, 'balaskomentar']);
Route::get('/indexadminkomentar/{id}', [KomentarController::class, 'indexadminkomentar']);
Route::post('/adminkomentar/{id}', [KomentarController::class, 'adminkomentar']);
// Route::post('/balaskomentaradmin/{uuid_komentar}', [KomentarController::class, 'balaskomentar']);
Route::put('/komentar/{uuid_produk}/{id}', [KomentarController::class, 'ubahkomentar']);
Route::delete('/komentaradmin/{id}', [KomentarController::class, 'adminhapuskomentar']);
Route::delete('/komentaradmin/balaskomentar/{id}', [KomentarController::class, 'adminhapusbalaskomentar']);
Route::delete('/komentaruser/{uuid_produk}/{id}', [KomentarController::class, 'userhapuskomentar']);
Route::delete('/baleskomentaruser/{id}', [KomentarController::class, 'userhapusbalaskomentar']);

Route::get('/rating', [RatingController::class, 'index']);
Route::post('/rating/{uuid_produk}', [RatingController::class, 'rating']);
Route::delete('/ratingadmin/{id}', [RatingController::class, 'adminhapusrating']);
Route::delete('/ratinguser/{uuid}/{id}', [RatingController::class, 'userhapusrating']);









Route::get('/keranjang', [KeranjangController::class, 'index'])->middleware('auth');
Route::post('/keranjang', [KeranjangController::class, 'store'])->middleware('auth');
Route::post('/keranjang/{id}', [KeranjangController::class, 'update'])->middleware('auth');
Route::delete('/keranjang/{id}', [KeranjangController::class, 'delete'])->middleware('auth');

Route::get('/checkout', [CheckoutController::class, 'index'])->middleware('auth');
// Route::get('/buktipembayaran', [CheckoutController::class, 'buktipembayaran'])->middleware('auth');

Route::post('/checkout/pembayaran', [CheckoutController::class, 'pembayaran'])->middleware('auth');
Route::post('/checkout/get-ongkir', [CheckoutController::class, 'getOngkir'])->middleware('auth');
Route::post('/checkout/pembayaran/charge', [CheckoutController::class, 'charge'])->middleware('auth');

Route::get('/proses/pembayaran', [RiwayatPesananController::class, 'proses'])->middleware('auth');
Route::get('/selesai/pembayaran/{uuid}', [RiwayatPesananController::class, 'selesaipembayaran'])->middleware('auth');
Route::get('/riwayat/pembayaran', [RiwayatPesananController::class, 'riwayatpesanan'])->middleware('auth');
Route::get('/riwayat/pembayaran/{id}/detail', [RiwayatPesananController::class, 'detailriwayatpesanan'])->middleware('auth');
Route::post('/riwayat/pembayaran/{id}/updatestatusselesai', [RiwayatPesananController::class, 'updatestatusselesai'])->middleware('auth');
Route::post('/riwayat/pembayaran/{id}/kembalistatuscancel', [RiwayatPesananController::class, 'kembalistatuscancel'])->middleware('auth');
Route::delete('/hapusriwayat/{id}', [RiwayatPesananController::class, 'hapusriwayatpesanan']);

Route::get('/riwayatpembayaran/pdfselesai/{id}', [PdfController::class, 'riwayatpdfselesai'])->middleware('auth');



Route::post('payments/midtrans-notification', [PaymentCallbackController::class, 'receive']);


Route::get('provinsi/{id}/kota', [DashboardProvinsiController::class, 'kota']);

// Route::get('tes', [DashboardReportController::class, 'excel']);

// Route::post('tes', [DashboardReportController::class, 'tes']);
