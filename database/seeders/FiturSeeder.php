<?php

namespace Database\Seeders;

use App\Models\Fitur;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class FiturSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Fitur::create([
            'heading' => '  Gratis Perakitan',
            'sub_heading' => 'Gratis Perakitan bila customer ingin dirakit langsung',
            'gambar' => 'fitur1.jpg'
        ]);

        Fitur::create([
            'heading' => '  Praktis dan Efisien',
            'sub_heading' => 'Online Shop kami juga menawarkan efisiensi waktu yang sangat cepat. Hanya dengan beberapa menit pun, Sobat sudah bisa melakukan transaksi dan tinggal menunggu barangnya dikirim sampai ke rumah',
            'gambar' => 'fitur2.jpg'
        ]);

        Fitur::create([
            'heading' => 'Banyak promo/diskon dan cashback',
            'sub_heading' => 'Nikmati Diskon Hingga 50%',
            'gambar' => 'fitur3.jpg'
        ]);

        Fitur::create([
            'heading' => 'Sistem Pembayaran Lebih Mudah',
            'sub_heading' => 'Sistem Pembayaran yang mudah. Mulai dari pembayaran online, seperti transfer bank, dompet digital, atau melalui minimarket',
            'gambar' => 'fitur4.jpg'
        ]);
    }
}
