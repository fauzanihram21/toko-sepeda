<?php

namespace Database\Seeders;

use App\Models\SubKategori;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SubKategoriSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SubKategori::create([
            'nm_sub_kategori' => 'Sepeda Gunung',
            'kategori_id' => '1'
        ]);
        SubKategori::create([
            'nm_sub_kategori' => 'Sepeda Bmx',
            'kategori_id' => '1'
        ]);
        SubKategori::create([
            'nm_sub_kategori' => 'Sepeda Lipat',
            'kategori_id' => '1'
        ]);
        SubKategori::create([
            'nm_sub_kategori' => 'Sepeda Listrik',
            'kategori_id' => '1'
        ]);
        SubKategori::create([
            'nm_sub_kategori' => 'Sepeda Anak',
            'kategori_id' => '1'
        ]);


        SubKategori::create([
            'nm_sub_kategori' => 'Botol Sepeda',
            'kategori_id' => '2'
        ]);
        SubKategori::create([
            'nm_sub_kategori' => 'Lampu Sepeda',
            'kategori_id' => '2'
        ]);

        SubKategori::create([
            'nm_sub_kategori' => 'Baju',
            'kategori_id' => '3'
        ]);

        SubKategori::create([
            'nm_sub_kategori' => 'Celana',
            'kategori_id' => '3'
        ]);
        SubKategori::create([
            'nm_sub_kategori' => 'Helm',
            'kategori_id' => '3'
        ]);
        SubKategori::create([
            'nm_sub_kategori' => 'Sepatu',
            'kategori_id' => '3'
        ]);

        SubKategori::create([
            'nm_sub_kategori' => 'Pedal Sepeda',
            'kategori_id' => '4'
        ]);
        SubKategori::create([
            'nm_sub_kategori' => 'Sadel Sepeda',
            'kategori_id' => '3'
        ]);
        SubKategori::create([
            'nm_sub_kategori' => 'Grip Sepeda',
            'kategori_id' => '3'
        ]);
        SubKategori::create([
            'nm_sub_kategori' => 'Rantai Sepeda',
            'kategori_id' => '3'
        ]);
        SubKategori::create([
            'nm_sub_kategori' => 'Aki Sepeda Listrik',
            'kategori_id' => '3'
        ]);
        SubKategori::create([
            'nm_sub_kategori' => 'Sepeda Fixie',
            'kategori_id' => '1'
        ]);
    }
}
