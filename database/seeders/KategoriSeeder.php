<?php

namespace Database\Seeders;

use App\Models\Kategori;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class KategoriSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Kategori::create([
            'nm_kategori' => 'Sepeda',
        ]);

        Kategori::create([
            'nm_kategori' => 'Aksesoris',
        ]);
        Kategori::create([
            'nm_kategori' => 'Apparel',
        ]);
        Kategori::create([
            'nm_kategori' => 'Parts',
        ]);
    }
}
