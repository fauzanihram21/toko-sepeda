<?php

namespace Database\Seeders;

use App\Models\Banner;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class BannerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    { {
            Banner::create([
                'heading' => '  Nikmati Potongan Hingga 50%',
                'sub_heading' => 'Potongan ini hanya untuk bulan ini',
                'gambar' => 'banner1.png'
            ]);

            Banner::create([
                'heading' => '  Cari produk yang dijamin kualitasnya ?',
                'sub_heading' => 'Disini Tempatnya',
                'gambar' => 'banner2.png'
            ]);
        }
    }
}
