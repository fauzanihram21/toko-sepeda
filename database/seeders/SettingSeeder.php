<?php

namespace Database\Seeders;

use App\Models\Setting;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Setting::create([
            'name' => 'Bike Mercury Wow',
            'deskripsi' => 'Selamat Datang di Bike Mercury Wow
                            Pelayanan Pelanggan: Senin - Minggu  09.00 - 10.00 Wib
                            Semua produk yang dapat dipesan berarti ready stock dan siap dikirim',
            'alamat' => 'Pondok Benowo Indah Block CS-17',
            'no_telp' => '089620013761',
            'email' => 'fauzanihram21@gmail.com',
            'facebook' => 'Fauzan Tawada',
            'instagram' => 'fauzantawada',
            'background_login' => 'backgroundlogin.jpg',
            'background_register' => 'backgroundregister.jpg',
            'logo_luar' => 'logo_luar.png',
            'logo_dalam' => 'logo_dalam.png',
            'whatsapp' => '089620013761'

        ]);
    }
}
