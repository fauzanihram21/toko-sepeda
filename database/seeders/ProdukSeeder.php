<?php

namespace Database\Seeders;

use App\Models\Produk;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProdukSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Produk::create([
            'nm_produk' => 'Abus Helm Macator',
            'deskripsi' => 'Abus Helm Macator,
            Abus merupakan brand asal German yang berfokus pada aksesoris dan apparel. Salah satu produknya adalah Abus Helm Macator, merupakan helm entry-level sporty untuk pengendara sepeda yang mencari helm dengan fitur yang luar biasa. Abus Helm Macator tidak hanya memiliki bantalan yang nyaman tetapi juga menawarkan ventilasi udara yang sangat baik, dan tersedia dalam berbagai warna cerah yang sangat menarik.
            ',
            'harga' => '300000',
            'harga_diskon' => null,
            'berat' => '1',
            'gambar' => 'Abus helm-macator-hitam.jpg',
            'diskon' => '10',
            'sub_kategori_id' => '10',
            'merk_id' => '1'

        ]);

        Produk::create([
            'nm_produk' => 'Alpinestars Celana Pendek North Shore',
            'deskripsi' => 'Alpinestars Celana Pendek North Shore,
            Alpinestars Celana Pendek North Shore adalah celana pendek multi-panel yang dibuat dari taslon premium four-way stretch dan sistem penutupan pinggang baru yang inovatif untuk kinerja luar biasa terlepas dari tipe tubuh.
            ',
            'harga' => '200000',
            'harga_diskon' => null,
            'berat' => '1',
            'gambar' => 'Alpinestar celana-pendek-cream.jpg',
            'diskon' => '20',
            'sub_kategori_id' => '9',
            'merk_id' => '2'

        ]);

        Produk::create([
            'nm_produk' => 'Sepeda Avand Re-Arm X',
            'deskripsi' => 'Sepeda Avand Re-Arm X  Ukuran 16 Untuk Remaja dan Dewasa 
            dibekali dengan frame chromoly yang kuat dan ringan, dengan desain yang stylish. Memiliki 10 opsi kecepatan untuk menghadapi berbagai kondisi jalan dan memberi fleksibilitas dalam berkendara. Menggunakan rem cakram hydraulic yang sangat pakem. Handle grip dilengkapi dengan lock, yang menjamin genggaman tidak slip, berputar, atau bergeser. Penyimpanan praktis dan ringkas karena sepeda bisa dilipat di bagian: rangka, handle post dan pedal, memudahkan Anda untuk melipat dan menyimpannya di dalam ruangan yang sangat kecil. Dilengkapi dengan front block pada bagian frame depan sepeda, yang bisa digunakan sebagai bracket tas. Front block memudahkan Anda memasang, melepas dan membawa tas di bagian depan sepeda. Avand Re-Arm X sangat ideal untuk kebutuhan rider urban yang tidak memiliki banyak ruang untuk penyimpanan.
            ',
            'harga' => '3000000',
            'harga_diskon' => null,
            'berat' => '8',
            'gambar' => 'Avand sepeda-lipat-Re-arm-biru.jpg',
            'diskon' => '10',
            'sub_kategori_id' => '3',
            'merk_id' => '3'

        ]);
        Produk::create([
            'nm_produk' => 'Dahon ion Denver Y 20',
            'deskripsi' => ' Dahon ion Denver Y 20
            Untuk Remaja dan Dewasa Sepeda Dahon.ion Denver Y dibekali dengan frame berbahan alloy, yang ringan dan tahan karat. Menggunakan ukuran ban 20 inci, sepeda ini cocok untuk kalangan remaja dan dewasa. Untuk menunjang kemudahan pengendara, Dahon.ion Denver ini difasilitasi 10 pilihan kecepatan dari Shimano Tiagra untuk menghadapi berbagai kondisi jalan dan memberi fleksibilitas dalam berkendara. Pada sistem pengereman, menggunakan rem cakram hydraulic untuk pengereman yang optimal. Pegangan sepeda yang berbentuk ergonomis menyesuaikan kontur genggaman telapak tangan untuk meningkatkan kenyamanan, terutama saat berkendara lama. Dilengkapi standar samping, dan magnet yang kuat di bagian roda depan dan belakang, untuk menjaga sepeda terlipat dengan aman. Sepeda ini merupakan sepeda rakitan,selain itu sepeda lipat 20 inci ini cukup ringkas dan praktis disimpan. Anda bisa melipat bagian rangka, stang, dan pedal.            
            ',
            'harga' => '5000000',
            'harga_diskon' => null,
            'berat' => '7',
            'gambar' => 'Dahon Ion sepeda-lipat-denver-hitam.jpg',
            'diskon' => '50',
            'sub_kategori_id' => '3',
            'merk_id' => '5'
        ]);

        Produk::create([
            'nm_produk' => 'Element Nicks 10',
            'deskripsi' => ' Element Nicks 10 Speed 20
            Inci Untuk Remaja dan Dewasa
            Sepeda lipat Element Nicks 10 Speed kini hadir dalam seri tahun 2022. Dibekali dengan frame chromoly yang kuat dan ringan, dengan desain yang stylish. Berbeda dengan seri sebelumnya, kali ini Element Nicks dilengkapi dengan 10 pilihan kecepatan untuk menghadapi berbagai kondisi jalan dan memberi fleksibilitas dalam berkendara. Dilengkapi dengan standar samping dan front block pada bagian frame depan sepeda, yang bisa digunakan sebagai bracket tas. Menggunakan velg tinggi yang menambah aerodynamic untuk support kecepatan. Selain itu, sepeda lipat 20 inci ini cukup ringkas dan praktis disimpan. Anda bisa melipat bagian rangka, handle post, dan pedal.            
            ',
            'harga' => '4000000',
            'harga_diskon' => null,
            'berat' => '8',
            'gambar' => 'Element sepeda-lipat-hitam.jpg',
            'diskon' => null,
            'sub_kategori_id' => '3',
            'merk_id' => '6'
        ]);

        Produk::create([
            'nm_produk' => 'Blackburn Lampu Depan Click',
            'deskripsi' => 'Blackburn Lampu Depan Click
             Blackburn Lampu Depan Click yang sangat minimalis dapat menghasilkan jumlah cahaya 20 lumens, yang dapat membantu Anda bersepeda di saat malam hari. Tahan air IP65 sehingga tidak perlu takut saat cuaca tidak mendukung. Menggunakan tali pemasangan dari bahan silicone dapat memudahkan Anda dalam pemasangan dan melepas lampu serta dapat dipasangka pada hampir semua handlebar sepeda. Blackburn Lampu Depan Click menggunakan baterai CR2032, baterai pun sangat mudah diganti.
            ',
            'harga' => '40000',
            'harga_diskon' => null,
            'berat' => '1',
            'gambar' => 'blackburn lampu-depan.jpg',
            'diskon' => null,
            'sub_kategori_id' => '7',
            'merk_id' => '4'
        ]);


        Produk::create([
            'nm_produk' => 'Fabric Grip Semi Ergo Lock On',
            'deskripsi' => 'Fabric Grip Semi Ergo Lock On
             Fabric Grip Semi Ergo dibentuk mengikuti kontur tangan Anda saat mengenggangan dan dukungan ekstra saat berkendara dengan pegangan Semi ergo. Fabric Grip Semi Ergo pegangan berpola Hex yang ergonomis halus terbuat dari silikon kelas medis, menjadikannya sangat cengkeraman dan suportif.
            ',
            'harga' => '35000',
            'harga_diskon' => null,
            'berat' => '1',
            'gambar' => 'Fabric grip-semi.jpg',
            'diskon' => null,
            'sub_kategori_id' => '14',
            'merk_id' => '7'
        ]);

        Produk::create([
            'nm_produk' => 'Fnhon Gust V-Brake 16',
            'deskripsi' => 'Fnhon Gust V-Brake 16
            Sepeda lipat Fnhon Gust V-Brake sangat cocok untuk kebutuhan rider urban yang tidak memiliki banyak ruang untuk penyimpanan. Dibekali frame berbahan chromoly, yang ringan dan kuat, dengan desain yang stylish. Memiliki 10 opsi kecepatan untuk menghadapi berbagai kondisi jalan dan memberi fleksibilitas dalam berkendara. Dilengkapi dengan rem V-brake sistem pengereman sederhana, lebih hemat biaya perawatan. Penyimpanan praktis dan ringkas karena sepeda bisa dilipat di bagian: rangka, handle post, dan pedal memudahkan Anda untuk melipat dan menyimpannya di dalam ruangan yang sangat kecil. Harga sepeda lipat Fnhon Gust Disc Brake bisa dicek di halaman ini.
            ',
            'harga' => '8000000',
            'harga_diskon' => null,
            'berat' => '9',
            'gambar' => 'Fnhon sepeda-lipat-merah.jpg',
            'diskon' => '20',
            'sub_kategori_id' => '3',
            'merk_id' => '8'
        ]);
        Produk::create([
            'nm_produk' => 'Fox Botol Head Base Water 600ml',
            'deskripsi' => 'Fox Botol Head Base Water 600ml
            Fox Botol Head Base merupakan produk original merk FOX yang berasal dari Amerika. Botol ini berbahan material dari plastik BPA Free sehingga tidak perlu khawatir lagi air minum akan terkontaminasi dengan bau plastik dan zat berbahaya lainnya. Fox Botol Head Base mempunyai Valve yang simple sehingga memudahkan Anda minum saat bersepeda. Fox Botol Head Base 100% anti bocor serta tertutup rapat sehingga air tidak menetes ke front derailleur atau bagian crankset pada sepeda Anda. Keunggulan utama dari Fox Botol Head Base ini 100% BPA Free.
            ',
            'harga' => '150000',
            'harga_diskon' => null,
            'berat' => '1',
            'gambar' => 'Fox botol-head-black.jpg',
            'diskon' => null,
            'sub_kategori_id' => '6',
            'merk_id' => '9'
        ]);
        Produk::create([
            'nm_produk' => 'Genio Pedal G-561',
            'deskripsi' => 'Genio Pedal G-561 (9/16)
            sangat cocok di sepeda gunung dan fixie
            ',
            'harga' => '40000',
            'harga_diskon' => null,
            'berat' => '1',
            'gambar' => 'genio pedal-g.jpg',
            'diskon' => null,
            'sub_kategori_id' => '12',
            'merk_id' => '11'
        ]);
        Produk::create([
            'nm_produk' => 'Genio Hyper Z 12',
            'deskripsi' => 'Sepeda Genio Hyper Z 12 
            untuk Anak Laki-laki
            Sepeda anak laki-laki jenis BMX buatan Genio ini menggunakan rangka yang terbuat dari steel, sehingga membuat sepeda ini kuat dan tahan banting. Dengan sistem pengereman v-brake yang sederhana, cocok untuk pemakaian biasa. Sepeda yang ditujukan untuk anak usia 3-4 tahun ini, dilengkapi dengan tambahan roda bantu di kiri dan kanan sebagai pendukung dalam menjaga keseimbangan untuk buah hati Anda yang baru belajar bersepeda. Dilengkapi juga dengan pelindung rantai (chain protector), lampu depan, dan spatbor, untuk memberikan tambahan keamanan saat bersepeda. Sepeda anak Genio Hyper Z 12 inci ini dapat dijadikan salah satu pilihan sepeda untuk menemani kegiatan bersepeda buah hati Anda.            
            ',
            'harga' => '2000000',
            'harga_diskon' => null,
            'berat' => '6',
            'gambar' => 'Genio sepeda-anak-hyper-z-biru.jpg',
            'diskon' => null,
            'sub_kategori_id' => '5',
            'merk_id' => '11'
        ]);
        Produk::create([
            'nm_produk' => 'Great Cycle Helm Sepeda MTB',
            'deskripsi' => 'Sepeda Great Cycle Helm Sepeda MTB 
             Great Cycle Helm Sepeda MTB memiliki lubang ventilasi sebanyak 25 + 4 ekstra lubang ventilasi yang membuat helm ini lebih adem dan nyaman digunakan tanpa mengesampingkan keamanan. Helm ini juga memiliki standart yang aman dari Eropa, ini memastikan perlindungan yang pasti. 
            ',
            'harga' => '130000',
            'harga_diskon' => null,
            'berat' => '1',
            'gambar' => 'Great-Cycle helm-sepeda-biru.jpg',
            'diskon' => null,
            'sub_kategori_id' => '10',
            'merk_id' => '10'
        ]);
        Produk::create([
            'nm_produk' => 'ION Five 14',
            'deskripsi' => 'Sepeda Listrik ION Five Ukuran 14 Inci
             ION Five merupakan sepeda Listrik pertama buatan pabrikan sepeda Element. Sepeda bisa dikendarai untuk jarak tempuh sampai 40km (jika aki dicharge penuh) dengan kecepatan maksimal mencapai 25km/jam. Dilengkapi dengan standar untuk memudahkan Anda memarkir sepeda. Sudah dilengkapi boncengan belakang dengan bantalan dan keranjang depan yang bisa Anda gunakan untuk membawa barang belanjaan. Sepeda listrik ini cocok untuk Anda yang ingin pergi santai dengan kendaraan yang ramah lingkungan, serta cocok dipakai dalam rutinitas Anda yang menempuh jarak pendek sampai menengah, seperti ke pasar atau mengantar anak ke sekolah
            ',
            'harga' => '6200000',
            'harga_diskon' => null,
            'berat' => '7',
            'gambar' => 'Ion sepeda-listrik-five-hitam.jpg',
            'diskon' => null,
            'sub_kategori_id' => '4',
            'merk_id' => '12'
        ]);
        Produk::create([
            'nm_produk' => 'ION One 14',
            'deskripsi' => 'Sepeda Listrik ION One Ukuran 14 Inci
             ION Five merupakan sepeda Listrik pertama buatan pabrikan sepeda Element. Sepeda bisa dikendarai untuk jarak tempuh sampai 40km (jika aki dicharge penuh) dengan kecepatan maksimal mencapai 25km/jam. Dilengkapi dengan standar untuk memudahkan Anda memarkir sepeda. Sudah dilengkapi boncengan belakang dengan bantalan dan keranjang depan yang bisa Anda gunakan untuk membawa barang belanjaan. Sepeda listrik ini cocok untuk Anda yang ingin pergi santai dengan kendaraan yang ramah lingkungan, serta cocok dipakai dalam rutinitas Anda yang menempuh jarak pendek sampai menengah, seperti ke pasar atau mengantar anak ke sekolah
            ',
            'harga' => '5100000',
            'harga_diskon' => null,
            'berat' => '6',
            'gambar' => 'Ion sepeda-listrik-one-kuning.jpg',
            'diskon' => null,
            'sub_kategori_id' => '4',
            'merk_id' => '12'
        ]);
        Produk::create([
            'nm_produk' => 'Lampu Swat',
            'deskripsi' => 'Lampu Swat
            Lampu Swat menggunakan side switch atau tombol on/off  disamping senter sehingga memudahkan penggunaannya. Menggunakan bahan Alumunium berkualitas tinggi, serta 3 mode pencahayaan untuk senter ini, mode pertama 100% pencahayaan, mode kedua 50% pencahayaan, dan mode ketiga SOS. Dilengkapi fasilitas zoom in dan out dengan memutar bezel ring silver di kepala senter. Cocok untuk kegiatan outdoor hiking, camping, memancing, senter berburu, bengkel, pertambangan, dll. Fokus Adjustable untuk penggunaan yang berbeda dengan fokus putar ring silver.
            ',
            'harga' => '60000',
            'harga_diskon' => null,
            'berat' => '1',
            'gambar' => 'lampu-depan-swat.jpg',
            'diskon' => null,
            'sub_kategori_id' => '7',
            'merk_id' => '4'
        ]);
        Produk::create([
            'nm_produk' => 'Pacific 2058 16',
            'deskripsi' => 'Pacific 2058 Ukuran 16 Inci Untuk Anak Laki-laki
            Sepeda Pacific 2058 hadir untuk melengkapi line up sepeda anak dari pabrikan sepeda Pacific. Hadir dalam ukuran ban 16 inci, sepeda ini dilengkapi dengan rangka steel yang kuat dan tahan banting. Dilengkapi dengan pelindung rantai dan spakbor depan belakang untuk memberikan tambahan safety dan kenyamanan buah hati Anda dalam berkendara. Sudah termasuk roda bantu belakang untuk buah hati Anda yang baru belajar bersepeda.
            ',
            'harga' => '4000000',
            'harga_diskon' => null,
            'berat' => '5',
            'gambar' => 'Pacific sepeda-anak-2058-putih.jpg',
            'diskon' => '20',
            'sub_kategori_id' => '5',
            'merk_id' => '13'
        ]);
        Produk::create([
            'nm_produk' => 'Pacific Argos 14',
            'deskripsi' => ' Sepeda Listrik Pacific Argos Ukuran 14 Inci
            Sepeda Listrik Pacific Argos merupakan sepeda listrik keluaran terbaru dari pabrikan sepeda Pacific. Sepeda bisa dikendarai untuk jarak tempuh sampai 50km (jika aki dicharge penuh) dengan kecepatan maksimal mencapai 25km/jam. Dilengkapi dengan standar untuk memudahkan Anda memarkir sepeda. Maksimal bobot pengendara 120kg. Sudah dilengkapi boncengan belakang dengan bantalan dan keranjang depan yang bisa Anda gunakan untuk membawa barang belanjaan. Sepeda listrik ini cocok untuk Anda yang ingin pergi santai dengan kendaraan yang ramah lingkungan, serta cocok dipakai dalam rutinitas Anda yang menempuh jarak pendek sampai menengah, seperti ke pasar atau mengantar anak ke sekolah.            
            ',
            'harga' => '5600000',
            'harga_diskon' => null,
            'berat' => '7',
            'gambar' => 'Pacific sepeda-listrik-argos-biru.jpg',
            'diskon' => '30',
            'sub_kategori_id' => '4',
            'merk_id' => '13'
        ]);
        Produk::create([
            'nm_produk' => 'Police California 1.0 27.5 ',
            'deskripsi' => ' Sepeda Police California 1 Ukuran 27.5 Inci Untuk Remaja dan Dewasa
            Sepeda Police California 1 dilengkapi dengan frame alloy yang ringan dan tahan karat. Memiliki 8 x 3 opsi kecepatan dari Shimano, untuk menghadapi berbagai kondisi jalan dan memberi fleksibilitas dalam berkendara. Dilengkapi dengan rem hydraulic, untuk performa pengereman dan keamanan yang optimal. Suspensi dapat dikunci dengan model saklar untuk pemakaian di jalan rata. Menggunakan sistem internal cable routing (jalur kabel masuk ke dalam rangka) sehingga penampakan lebih rapi dan modis. Dengan ukuran ban 27.5 inci dan lebar ban 2.25, sehingga terlihat lebih kokoh dan gagah, selain untuk meningkatkan daya cengkeram ban sehingga lebih tidak licin di jalanan.            
            ',
            'harga' => '9000000',
            'harga_diskon' => null,
            'berat' => '9',
            'gambar' => 'Police sepeda-gunung-california-hitam.jpg',
            'diskon' => '10',
            'sub_kategori_id' => '1',
            'merk_id' => '14'
        ]);
        Produk::create([
            'nm_produk' => 'Polygon Celana Pendek Cargo AM Agenor',
            'deskripsi' => 'Polygon Celana Pendek Cargo AM Agenor MTB yang menggunakan bahan super stretch (4 way stretch) sehingga penggunaan lebih nyaman dan fleksibel ketika bersepeda. Terdapat kantong dibagian samping dan belakang. Terdapat additional strap di bagian kiri dan kanan serta full rubber dibagian belakang sehingga ukuran pinggang lebih leluasa dan fleksibel.
            ',
            'harga' => '170000',
            'harga_diskon' => null,
            'berat' => '1',
            'gambar' => 'Polygon celana-pendek-cargo-1.jpg',
            'diskon' => '10',
            'sub_kategori_id' => '9',
            'merk_id' => '15'
        ]);
        Produk::create([
            'nm_produk' => 'PVR Celana Pendek Wallride',
            'deskripsi' => 'PVR Celana Pendek Wallride
            PVR Celana Pendek Wallride menggunakan bahan premium terbaru Stretch. Celana Pendek mempunyai sifat seperti mudah menyerap keringat lalu menguapkan keringat tersebut karena adanya proses kapilaritas, maka tubuh akan tetap terjaga keringnya dan tetap bisa menjaga temperaturenya.
            ',
            'harga' => '400000',
            'harga_diskon' => null,
            'berat' => '1',
            'gambar' => 'Pvr celana-pendek-wallride-biru.jpg',
            'diskon' => '50',
            'sub_kategori_id' => '9',
            'merk_id' => '16'
        ]);
        Produk::create([
            'nm_produk' => 'Raze Pedal Folding 67',
            'deskripsi' => 'Raze Pedal Folding 67 QR CNC 3 Bearing
            Raze Pedal Folding 67 QR CNC 3 Bearing terbuat dari bahan alloy dengan metode pembuatan CNC yang lebih presisi dan kuat, Raze Pedal Folding 67 memiliki as berbahan steel dibalut 3 bearing yang membuat putaran lebih lancar sehingga lebih mudah mengatur pijakan pedal. Raze Pedal Folding 67 terdapat pin sebanyak 6 buah di setiap sisi pedal yang berfungsi untuk menjaga kaki kita agar tidak slip saat gowes. Raze Pedal Folding 67 mudah di lepas karena berjenis quick release.            
            ',
            'harga' => '300000',
            'harga_diskon' => null,
            'berat' => '1',
            'gambar' => 'Raze pedal-folding.jpg',
            'diskon' => null,
            'sub_kategori_id' => '12',
            'merk_id' => '17'
        ]);
        Produk::create([
            'nm_produk' => 'Santa Cruz Megantower',
            'deskripsi' => 'Santa Cruz Megantower
            Rangka sepeda ini menggunakan serat karbon alias aluminium. Tidak hanya itu, sepeda Santa Cruz dibekali dengan teknologi VPP (Virtual Pivot Point).
            ',
            'harga' => '15000000',
            'harga_diskon' => null,
            'berat' => '9',
            'gambar' => 'Santa Cruz megantower.jpg',
            'diskon' => '10',
            'sub_kategori_id' => '1',
            'merk_id' => '18'
        ]);
        Produk::create([
            'nm_produk' => 'Selle Royal Saddle Lookin Athletic Unisex',
            'deskripsi' => 'Selle Royal Saddle Lookin Athletic Unisex
            sangat nyaman & cocok untuk para atlet
            ',
            'harga' => '70000',
            'harga_diskon' => null,
            'berat' => '1',
            'gambar' => 'Selle Royal saddle-lookin.jpg',
            'diskon' => null,
            'sub_kategori_id' => '13',
            'merk_id' => '19'
        ]);

        Produk::create([
            'nm_produk' => 'Shimano Rantai Deore XT M8100 12 Speed',
            'deskripsi' => 'Shimano Rantai Deore XT M8100 12 Speed
            Shimano Rantai Deore XT M8100 12 Speed  memberikan peningkatan ketahanan rantai berkat bagian pelat dalam yang diperpanjang. Rantai baru ini memiliki perakitan yang cepat dan perawatan SIL-TEC untuk ketahanan di medan offroad yang lebih lama. Serta teknologi HYPERGLIDE+  menghasilkan ketahanan rantai yang lebih besar, efisiensi pemindahan gigi yang lebih baik, dan transmisi yang lebih halus di medan yang kasar.
            ',
            'harga' => '55000',
            'harga_diskon' => null,
            'berat' => '1',
            'gambar' => 'Shimano rantai-deore-xt-m8100.jpg',
            'diskon' => null,
            'sub_kategori_id' => '15',
            'merk_id' => '20'
        ]);
        Produk::create([
            'nm_produk' => 'Shimano Rantai HG-54 10 Speed',
            'deskripsi' => 'Shimano Rantai HG-54 10 Speed
            memberikan peningkatan ketahanan rantai berkat bagian pelat dalam yang diperpanjang
            ',
            'harga' => '40000',
            'harga_diskon' => null,
            'berat' => '1',
            'gambar' => 'Shimano rantai-hg-54.jpg',
            'diskon' => null,
            'sub_kategori_id' => '15',
            'merk_id' => '20'
        ]);
        Produk::create([
            'nm_produk' => 'Shimano Rantai HG-601 11 Speed',
            'deskripsi' => 'Shimano Rantai HG-601 11 Speed
            Rantai SHIMANO HG-601 11-Speed HYPERGLIDE
            Shimano Rantai HG-601 11 Speed rantai dengan teknologi HG-X11 menghasilkan perpindahan gigi yang lebih mulus dan kinerja keseluruhan yang lebih baik, yang dilengkapi perawatan SIL-TEC pada pelat sambungan rol dan pelat sambungan pin untuk meningkatkan daya tahan.            
            ',
            'harga' => '45000',
            'harga_diskon' => null,
            'berat' => '1',
            'gambar' => 'Shimano rantai-hg-601.jpg',
            'diskon' => null,
            'sub_kategori_id' => '15',
            'merk_id' => '20'
        ]);
        Produk::create([
            'nm_produk' => 'Shimano Rantai HG-701 11 Speed',
            'deskripsi' => 'Shimano Rantai HG-701 11 Speed
            memberikan peningkatan ketahanan rantai berkat bagian pelat dalam yang diperpanjang
            ',
            'harga' => '48000',
            'harga_diskon' => null,
            'berat' => '1',
            'gambar' => 'Shimano rantai-hg-701.jpg',
            'diskon' => null,
            'sub_kategori_id' => '15',
            'merk_id' => '20'
        ]);
        Produk::create([
            'nm_produk' => 'Shimano Sepatu SH - RC300 Women',
            'deskripsi' => 'Shimano Sepatu SH - RC300 Women
            Shimano Sepatu SH RC300 adalah sepatu sepeda perempuan dari pabrikan Shimano asal Jepang, yang di peruntukkan pengguna sepeda balap perempuan. Shimano Sepatu SH RC300 dibekali dengan teknologi Shimano Dynalast pada bagian toe-spring dari sepatu bersepeda memainkan peran penting dalam mendukung pedaling yang efisien. Jika posisi telapak kaki terlalu tinggi itu menyebabkan peningkatan ketegangan pada otot plantar, betis, dan hamstring. Terlalu rendah Anda mendapatkan bentuk mengayuh yang tidak efisien. Penelitian ekstensif oleh para ahli R&D Shimano telah menghasilkan desain sepatu terakhir yang superior dengan bagian pegas jari kaki yang dioptimalkan yang menghasilkan gerakan upstroke yang lebih halus dan lebih hemat energi. Shimano Dynalast membantu mengurangi kehilangan energi pada perjalanan panjang, memungkinkan Anda menyimpan lebih banyak energi untuk sprint diakhir perjalanan. Shimano Sepatu SH RC300 pedal yang paling cocok dengan sepatu SH-RC300 adalah PD-RS500/PD-R540
            ',
            'harga' => '100000',
            'harga_diskon' => null,
            'berat' => '1',
            'gambar' => 'Shimano sepatu-sh-rc300.jpg',
            'diskon' => null,
            'sub_kategori_id' => '11',
            'merk_id' => '20'
        ]);
        Produk::create([
            'nm_produk' => 'Shimano Sepatu SH-RC502',
            'deskripsi' => 'Shimano Sepatu SH-RC502
            Shimano Sepatu SH-RC502 adalah sepatu seris menengah dari pabrikan Shimano asal Jepang, yang di peruntukkan untuk kelas road dengan DNA S-Phyre yang merupakan turunan dari seri tertinggi yaitu RC 902. Dibekali dengan teknologi Shimano Dynalast pada bagian toe-spring dari sepatu bersepeda memainkan peran penting dalam mendukung pedaling yang efisien.
            ',
            'harga' => '150000',
            'harga_diskon' => null,
            'berat' => '1',
            'gambar' => 'Shimano sepatu-sh-rc502-3-putih.jpg',
            'diskon' => null,
            'sub_kategori_id' => '11',
            'merk_id' => '20'
        ]);
        Produk::create([
            'nm_produk' => 'Strace Saddle Thor SA5387',
            'deskripsi' => 'Strace Saddle Thor SA5387
            sangat nyaman & cocok untuk para atlet
            ',
            'harga' => '130000',
            'harga_diskon' => null,
            'berat' => '1',
            'gambar' => 'Strace saddle-thor.jpg',
            'diskon' => null,
            'sub_kategori_id' => '13',
            'merk_id' => '21'
        ]);

        Produk::create([
            'nm_produk' => 'United Tassos 20 ',
            'deskripsi' => 'United Tassos 20 Inci Untuk Free Style
            Brand United terus meluncurkan inovasi terbaik dari tahun ke tahun. Kini pabrikan United kembali mengembangkan sepeda katagori BMX, yaitu Sepeda United Tassos. Sepeda ini didesain dengan rangka steel yang kuat dan tahan banting, dengan pilihan warna yang terang, sehingga terlihat lebih fresh. Dengan sambungan las yang rapi dengan Quality Control pabrikan United yg sudah berpengalaman. Sistem pengereman sederhana dan tanpa operan gigi, lebih hemat biaya perawatan. Sepeda ini sudah dilengkapi dengan rotor di stangnya yang bisa diputar hingga 360 derajat tanpa terlilit kabel rem. United Tassos ini adalah salah satu rekomendasi BMX dari kami.            
            ',
            'harga' => '6000000',
            'harga_diskon' => null,
            'berat' => '6',
            'gambar' => 'Tassos bmx.jpg',
            'diskon' => null,
            'sub_kategori_id' => '2',
            'merk_id' => '22'
        ]);
        Produk::create([
            'nm_produk' => 'United Accu (Aki) Milez Bosch ',
            'deskripsi' => 'United Accu (Aki) Milez Bosch Inci Untuk Free Style
            United Accu (Aki) Milez Bosch Batterai yang bisa di isi ulang kembali. untuk sepeda listrik Milez Bosch. Dengan daya 12V 12 Ah.
            ',
            'harga' => '4000000',
            'harga_diskon' => null,
            'berat' => '3',
            'gambar' => 'United aki-sepeda-listrik.jpg',
            'diskon' => null,
            'sub_kategori_id' => '16',
            'merk_id' => '22'
        ]);
        Produk::create([
            'nm_produk' => 'United Dbased 20 ',
            'deskripsi' => 'United Dbased 20 Inci Untuk Anak-Anak dan Remaja
            Pabrikan sepeda United kini meremajakan kembali produk sepeda BMX nya, yaitu United Dbased. menggunakan bahan material frame yang kuat, yaitu dari steel, dengan desain yang keren. menggunakan single speed yang membuat anda lebih mudah dalam perawatannya. Dibekali dengan V-brake untuk rem depan dan rem belakang yang membuat sepeda lebih aman pada saat di kendarai. sepeda ini kami rekomendasikan untuk pengguna dengan tinggi badan mulai dari 120cm hingga 130cm. Dengan desain yang simpel dan keren memberikan anda pengalaman baru setiap harinya dalam berkendala di jalan atau beraksi di BMX park. 
            ',
            'harga' => '7000000',
            'harga_diskon' => null,
            'berat' => '6',
            'gambar' => 'United bmx-dbased-20-merah.jpg',
            'diskon' => null,
            'sub_kategori_id' => '2',
            'merk_id' => '22'
        ]);
        Produk::create([
            'nm_produk' => 'United Hanzo 20 ',
            'deskripsi' => 'United Hanzo 20
            Brand United terus menghadirkan inovasi terbaik dari tahun ke tahun. Mulai dari sepeda MTB, Citybike, Kidsbike, bahkan E-bike. Kini pabrikan United kembali mengembangkan sepeda kategori BMX, yaitu Sepeda United Hanzo. Sepeda ini didesain dengan rangka steel dan perpaduan warna yang apik dengan penggunaan cat glossy. Penggunaan cat warna glossy ini juga memberikan tampilan visual yang kuat karena warnanya yang mengkilap, juga lebih awet, serta tidak meninggalkan sidik jari walaupun sering dipegang. Menggunakan design dan bentuk rangka berkarakter yang juga menjadi daya tarik tersendiri untuk anak-anak. United Hanzo ini menjadi salah satu rekomendasi sepeda BMX dari kami untuk Anda
            ',
            'harga' => '5000000',
            'harga_diskon' => null,
            'berat' => '7',
            'gambar' => 'United bmx-hanzo.jpg',
            'diskon' => null,
            'sub_kategori_id' => '2',
            'merk_id' => '22'
        ]);
        Produk::create([
            'nm_produk' => 'United fixie-soloist-reborn ',
            'deskripsi' => 'United fixie-soloist-reborn Soloist Reborn merupakan penyempurnaan kembali dari United Bike dari seri Soloist sebelumnya. Sepeda Fixie yang ramping akan menjadi teman kamu dalam bersepeda dengan santai namun gesit di perkotaan! Dilengkapi dengan ban 700C, sepeda single gear ini sangat cocok bagi kamu yang ingin merasakan sensasi ber-fixie ria di perkotaan.
            ',
            'harga' => '10000000',
            'harga_diskon' => null,
            'berat' => '9',
            'gambar' => 'United fixie-soloist-reborn.jpg',
            'diskon' => null,
            'sub_kategori_id' => '17',
            'merk_id' => '22'
        ]);

        Produk::create([
            'nm_produk' => 'United Tank 16 ',
            'deskripsi' => 'United Tank 16
            Inci ini cocok untuk teman buah hati Anda. Menggunakan warna dan army look yang keren, membuat sepeda ini terlihat lebih garang dan gagah. Rangka besi yang kuat serta tahan banting sehingga lebih awet pemaikaiannya. Menggunakan sistem pengereman cakram mekanik, sehingga lebih pakem dan aman bagi buah hati Anda. Dilengkapi pelindung rantai dan spakbor depan belakang untuk memberikan tambahan safety dan kenyamanan dalam berkendara. Sudah termasuk bel, serta roda bantu belakang kiri-kanan khusus untuk buah hati Anda yang baru belajar bersepeda
            ',
            'harga' => '4000000',
            'harga_diskon' => null,
            'berat' => '5',
            'gambar' => 'United sepeda-anak-tank-abu.jpg',
            'diskon' => null,
            'sub_kategori_id' => '5',
            'merk_id' => '22'
        ]);
        Produk::create([
            'nm_produk' => 'United Vigour 16 ',
            'deskripsi' => 'United Vigour 16
            Inci Sepeda Anak Untuk Laki-laki
            Sepeda United Vigour 16 Inci ini cocok untuk menemani buah hati Anda bersepeda. Menggunakan warna dan karakter yang keren, membuat sepeda ini terlihat lebih garang dan gagah. Rangka besi yang kuat serta tahan banting. Dilengkapi dengan sistem pengereman sederhana, lebih hemat biaya perawatan. Dilengkapi pelindung rantai dan spakbor depan belakang untuk memberikan tambahan safety dan kenyamanan dalam berkendara. Sudah termasuk bel, serta roda bantu belakang kiri-kanan khusus untuk buah hati Anda yang baru belajar bersepeda.            
            ',
            'harga' => '4500000',
            'harga_diskon' => null,
            'berat' => '5',
            'gambar' => 'United sepeda-anak-vigour-hitam.jpg',
            'diskon' => null,
            'sub_kategori_id' => '5',
            'merk_id' => '22'
        ]);
        Produk::create([
            'nm_produk' => 'Velo Grip Attune Double Lock 1745 ',
            'deskripsi' => 'Velo Grip Attune Double Lock 1745
            sangat nyaman di genggaman tangan
            ',
            'harga' => '200000',
            'harga_diskon' => null,
            'berat' => '1',
            'gambar' => 'Velo grip-attune.jpg',
            'diskon' => null,
            'sub_kategori_id' => '14',
            'merk_id' => '23'
        ]);
        Produk::create([
            'nm_produk' => 'Wimcycle Big Foot 18 ',
            'deskripsi' => 'Wimcycle Big Foot 18
            Wimcycle tak henti memberikan inovasi terbarunya untuk seri sepeda anak. Kali ini Wimcycle mengeluarkan desain terbaru dari salah satu tokoh kartun yang disukai anak-anak, yaitu Sepeda Wimcycle Big Foot 18 inci. Dibekali dengan rangka berbahan steel yang kuat dan tahan banting. Wimcycle Big Foot 18 inci menggunakan single speed yang artinya, akan lebih mudah dalam perawatannya.
            ',
            'harga' => '2000000',
            'harga_diskon' => null,
            'berat' => '5',
            'gambar' => 'Wimcycle sepeda-anak-big-foot-abu.jpg',
            'diskon' => null,
            'sub_kategori_id' => '5',
            'merk_id' => '24'
        ]);
        Produk::create([
            'nm_produk' => 'Selle Royal saddle-vivo-reflective',
            'deskripsi' => 'Selle Royal saddle-vivo-reflective
            sangat nyaman & cocok untuk para atlet
            ',
            'harga' => '60000',
            'harga_diskon' => null,
            'berat' => '1',
            'gambar' => 'Selle Royal saddle-vivo-reflective.jpg',
            'diskon' => null,
            'sub_kategori_id' => '13',
            'merk_id' => '19'
        ]);
    }
}
