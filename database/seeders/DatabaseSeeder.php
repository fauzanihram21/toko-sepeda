<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(SettingSeeder::class);
        $this->call(FiturSeeder::class);
        $this->call(BannerSeeder::class);
        $this->call(MerkSeeder::class);
        $this->call(KategoriSeeder::class);
        $this->call(SubKategoriSeeder::class);
        $this->call(ProdukSeeder::class);
        $this->call(ProdukVariasiSeeder::class);
        $this->call(SpesifikasiSeeder::class);
        $this->call(ProvinsiSeeder::class);
        $this->call(KotaSeeder::class);
    }
}
