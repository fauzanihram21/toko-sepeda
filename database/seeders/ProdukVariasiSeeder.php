<?php

namespace Database\Seeders;

use App\Models\ProdukVariasi;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProdukVariasiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProdukVariasi::create([
            'warna' => 'Putih',
            'stock' => '2',
            'gambar' => 'Abus helm-macator-putih.jpg',
            'produk_id' => '1'

        ]);

        ProdukVariasi::create([
            'warna' => 'Biru',
            'stock' => '1',
            'gambar' => 'Abus helm-macator-biru.jpg',
            'produk_id' => '1'

        ]);

        ProdukVariasi::create([
            'warna' => 'Hitam',
            'stock' => '3',
            'gambar' => 'Abus helm-macator-hitam.jpg',
            'produk_id' => '1'

        ]);
        ProdukVariasi::create([
            'warna' => 'Navy',
            'stock' => '2',
            'gambar' => 'Alpinestar celana-pendek-navy.jpg',
            'produk_id' => '2'

        ]);
        ProdukVariasi::create([
            'warna' => 'Orange',
            'stock' => '1',
            'gambar' => 'Alpinestar celana-pendek-orange.jpg',
            'produk_id' => '2'

        ]);
        ProdukVariasi::create([
            'warna' => 'Cream',
            'stock' => '1',
            'gambar' => 'Alpinestar celana-pendek-cream.jpg',
            'produk_id' => '2'

        ]);

        ProdukVariasi::create([
            'warna' => 'Merah',
            'stock' => '2',
            'gambar' => 'Avand sepeda-lipat-Re-arm-merah.jpg',
            'produk_id' => '3'

        ]);
        ProdukVariasi::create([
            'warna' => 'Biru',
            'stock' => '1',
            'gambar' => 'Avand sepeda-lipat-Re-arm-biru.jpg',
            'produk_id' => '3'

        ]);
        ProdukVariasi::create([
            'warna' => 'Hitam',
            'stock' => '3',
            'gambar' => 'Dahon Ion sepeda-lipat-denver-hitam.jpg',
            'produk_id' => '4'

        ]);
        ProdukVariasi::create([
            'warna' => 'Kuning',
            'stock' => '2',
            'gambar' => 'Dahon Ion sepeda-lipat-denver-kuning.jpg',
            'produk_id' => '4'

        ]);

        ProdukVariasi::create([
            'warna' => 'Merah',
            'stock' => '1',
            'gambar' => 'Element sepeda-lipat-merah.jpg',
            'produk_id' => '5'

        ]);
        ProdukVariasi::create([
            'warna' => 'Hitam',
            'stock' => '1',
            'gambar' => 'Element sepeda-lipat-hitam.jpg',
            'produk_id' => '5'

        ]);
        ProdukVariasi::create([
            'warna' => 'Hitam',
            'stock' => '2',
            'gambar' => 'blackburn lampu-depan.jpg',
            'produk_id' => '6'

        ]);
        ProdukVariasi::create([
            'warna' => 'Hitam',
            'stock' => '1',
            'gambar' => 'Fabric grip-semi.jpg',
            'produk_id' => '7'

        ]);
        ProdukVariasi::create([
            'warna' => 'Biru',
            'stock' => '1',
            'gambar' => 'Fnhon sepeda-lipat-biru.jpg',
            'produk_id' => '8'

        ]);
        ProdukVariasi::create([
            'warna' => 'Merah',
            'stock' => '2',
            'gambar' => 'Fnhon sepeda-lipat-merah.jpg',
            'produk_id' => '8'

        ]);
        ProdukVariasi::create([
            'warna' => 'Merah',
            'stock' => '3',
            'gambar' => 'Fox botol-head-merah.jpg',
            'produk_id' => '9'

        ]);
        ProdukVariasi::create([
            'warna' => 'Hitam',
            'stock' => '3',
            'gambar' => 'Fox botol-head-hitam.jpg',
            'produk_id' => '9'

        ]);
        ProdukVariasi::create([
            'warna' => 'Hitam',
            'stock' => '3',
            'gambar' => 'genio pedal-g.jpg',
            'produk_id' => '10'

        ]);
        ProdukVariasi::create([
            'warna' => 'Orange',
            'stock' => '5',
            'gambar' => 'Genio sepeda-anak-hyper-z-orange.jpg',
            'produk_id' => '11'

        ]);
        ProdukVariasi::create([
            'warna' => 'Biru',
            'stock' => '5',
            'gambar' => 'Genio sepeda-anak-hyper-z-biru.jpg',
            'produk_id' => '11'

        ]);
        ProdukVariasi::create([
            'warna' => 'Orange',
            'stock' => '5',
            'gambar' => 'Great-Cycle helm-sepeda-orange.jpg',
            'produk_id' => '12'

        ]);
        ProdukVariasi::create([
            'warna' => 'Merah',
            'stock' => '5',
            'gambar' => 'Great-Cycle helm-sepeda-merah.jpg',
            'produk_id' => '12'

        ]);
        ProdukVariasi::create([
            'warna' => 'Biru',
            'stock' => '5',
            'gambar' => 'Great-Cycle helm-sepeda-biru.jpg',
            'produk_id' => '12'

        ]);

        ProdukVariasi::create([
            'warna' => 'Merah',
            'stock' => '2',
            'gambar' => 'Ion sepeda-listrik-five-merah.jpg',
            'produk_id' => '13'

        ]);
        ProdukVariasi::create([
            'warna' => 'Hitam',
            'stock' => '2',
            'gambar' => 'Ion sepeda-listrik-five-hitam.jpg',
            'produk_id' => '13'

        ]);
        ProdukVariasi::create([
            'warna' => 'Biru',
            'stock' => '1',
            'gambar' => 'Ion sepeda-listrik-one-biru.jpg',
            'produk_id' => '14'

        ]);
        ProdukVariasi::create([
            'warna' => 'Merah',
            'stock' => '1',
            'gambar' => 'Ion sepeda-listrik-one-merah.jpg',
            'produk_id' => '14'

        ]);
        ProdukVariasi::create([
            'warna' => 'Kuning',
            'stock' => '1',
            'gambar' => 'Ion sepeda-listrik-one-kuning.jpg',
            'produk_id' => '14'

        ]);
        ProdukVariasi::create([
            'warna' => 'Hitam',
            'stock' => '2',
            'gambar' => 'lampu-depan-swat.jpg',
            'produk_id' => '15'

        ]);
        ProdukVariasi::create([
            'warna' => 'Merah',
            'stock' => '3',
            'gambar' => 'Pacific sepeda-anak-2058-merah.jpg',
            'produk_id' => '16'

        ]);
        ProdukVariasi::create([
            'warna' => 'Kuning',
            'stock' => '3',
            'gambar' => 'Pacific sepeda-anak-2058-kuning.jpg',
            'produk_id' => '16'

        ]);
        ProdukVariasi::create([
            'warna' => 'Putih',
            'stock' => '3',
            'gambar' => 'Pacific sepeda-anak-2058-putih.jpg',
            'produk_id' => '16'

        ]);
        ProdukVariasi::create([
            'warna' => 'Biru',
            'stock' => '3',
            'gambar' => 'Pacific sepeda-anak-2058-biru.jpg',
            'produk_id' => '17'

        ]);
        ProdukVariasi::create([
            'warna' => 'Biru',
            'stock' => '3',
            'gambar' => 'Pacific sepeda-anak-2058-biru.jpg',
            'produk_id' => '17'

        ]);
        ProdukVariasi::create([
            'warna' => 'Hitam',
            'stock' => '3',
            'gambar' => 'Police sepeda-gunung-california-hitam.jpg',
            'produk_id' => '18'

        ]);
        ProdukVariasi::create([
            'warna' => 'Hitam',
            'stock' => '2',
            'gambar' => 'Polygon celana-pendek-cargo-3.jpg',
            'produk_id' => '19'

        ]);
        ProdukVariasi::create([
            'warna' => 'Biru',
            'stock' => '2',
            'gambar' => 'Polygon celana-pendek-cargo-1.jpg',
            'produk_id' => '19'

        ]);
        ProdukVariasi::create([
            'warna' => 'Merah',
            'stock' => '2',
            'gambar' => 'Pvr celana-pendek-wallride-merah.jpg',
            'produk_id' => '20'

        ]);
        ProdukVariasi::create([
            'warna' => 'Hijau',
            'stock' => '2',
            'gambar' => 'Pvr celana-pendek-wallride-hijau.jpg',
            'produk_id' => '20'

        ]);
        ProdukVariasi::create([
            'warna' => 'Biru',
            'stock' => '2',
            'gambar' => 'Pvr celana-pendek-wallride-biru.jpg',
            'produk_id' => '20'

        ]);
        ProdukVariasi::create([
            'warna' => 'Hitam',
            'stock' => '2',
            'gambar' => 'Raze pedal-folding.jpg',
            'produk_id' => '21'

        ]);
        ProdukVariasi::create([
            'warna' => 'Hijau Mint',
            'stock' => '5',
            'gambar' => 'Santa Cruz megantower.jpg',
            'produk_id' => '22'

        ]);
        ProdukVariasi::create([
            'warna' => 'Hitam',
            'stock' => '2',
            'gambar' => 'Selle Royal saddle-lookin.jpg',
            'produk_id' => '23'

        ]);
        ProdukVariasi::create([
            'warna' => 'Silver',
            'stock' => '4',
            'gambar' => 'Shimano rantai-deore-xt-m8100.jpg',
            'produk_id' => '24'

        ]);
        ProdukVariasi::create([
            'warna' => 'Silver',
            'stock' => '4',
            'gambar' => 'Shimano rantai-hg-54.jpg',
            'produk_id' => '25'

        ]);
        ProdukVariasi::create([
            'warna' => 'Silver',
            'stock' => '4',
            'gambar' => 'Shimano rantai-hg-601.jpg',
            'produk_id' => '26'

        ]);
        ProdukVariasi::create([
            'warna' => 'Silver',
            'stock' => '4',
            'gambar' => 'Shimano rantai-hg-701.jpg',
            'produk_id' => '27'

        ]);
        ProdukVariasi::create([
            'warna' => 'Abu',
            'stock' => '2',
            'gambar' => 'Shimano sepatu-sh-rc300.jpg',
            'produk_id' => '28'

        ]);
        ProdukVariasi::create([
            'warna' => 'Hitam',
            'stock' => '4',
            'gambar' => 'Shimano sepatu-sh-rc502-1-hitam.jpg',
            'produk_id' => '29'

        ]);
        ProdukVariasi::create([
            'warna' => 'Biru',
            'stock' => '4',
            'gambar' => 'Shimano sepatu-sh-rc502-2-biru.jpg',
            'produk_id' => '29'

        ]);
        ProdukVariasi::create([
            'warna' => 'Putih',
            'stock' => '4',
            'gambar' => 'Shimano sepatu-sh-rc502-3-putih.jpg',
            'produk_id' => '29'

        ]);
        ProdukVariasi::create([
            'warna' => 'Merah',
            'stock' => '2',
            'gambar' => 'Strace saddle-thor.jpg',
            'produk_id' => '30'

        ]);
        ProdukVariasi::create([
            'warna' => 'Hitam',
            'stock' => '3',
            'gambar' => 'Tassos bmx.jpg',
            'produk_id' => '31'

        ]);
        ProdukVariasi::create([
            'warna' => 'Hitam',
            'stock' => '3',
            'gambar' => 'United aki-sepeda-listrik.jpg',
            'produk_id' => '32'

        ]);
        ProdukVariasi::create([
            'warna' => 'Putih',
            'stock' => '3',
            'gambar' => 'United bmx-dbased-20-putih.jpg',
            'produk_id' => '33'

        ]);
        ProdukVariasi::create([
            'warna' => 'Merah',
            'stock' => '3',
            'gambar' => 'United bmx-dbased-20-merah.jpg',
            'produk_id' => '33'

        ]);
        ProdukVariasi::create([
            'warna' => 'Hijau',
            'stock' => '3',
            'gambar' => 'United bmx-hanzo.jpg',
            'produk_id' => '34'

        ]);
        ProdukVariasi::create([
            'warna' => 'Hitam',
            'stock' => '3',
            'gambar' => 'United fixie-soloist-reborn.jpg',
            'produk_id' => '35'

        ]);
        ProdukVariasi::create([
            'warna' => 'Cream',
            'stock' => '3',
            'gambar' => 'United sepeda-anak-tank-cream.jpg',
            'produk_id' => '36'

        ]);
        ProdukVariasi::create([
            'warna' => 'Abu',
            'stock' => '3',
            'gambar' => 'United sepeda-anak-tank-abu.jpg',
            'produk_id' => '36'

        ]);
        ProdukVariasi::create([
            'warna' => 'Merah',
            'stock' => '3',
            'gambar' => 'United sepeda-anak-vigour-merah.jpg',
            'produk_id' => '37'

        ]);
        ProdukVariasi::create([
            'warna' => 'Hitam',
            'stock' => '3',
            'gambar' => 'United sepeda-anak-vigour-hitam.jpg',
            'produk_id' => '37'

        ]);
        ProdukVariasi::create([
            'warna' => 'Hitam',
            'stock' => '3',
            'gambar' => 'Velo grip-attune.jpg',
            'produk_id' => '38'

        ]);
        ProdukVariasi::create([
            'warna' => 'Hitam',
            'stock' => '3',
            'gambar' => 'Wimcycle sepeda-anak-big-foot-merah.jpg',
            'produk_id' => '39'

        ]);
        ProdukVariasi::create([
            'warna' => 'Abu',
            'stock' => '3',
            'gambar' => 'Wimcycle sepeda-anak-big-foot-abu.jpg',
            'produk_id' => '39'

        ]);
        ProdukVariasi::create([
            'warna' => 'Hitam',
            'stock' => '4',
            'gambar' => 'Selle Royal saddle-vivo-reflective.jpg',
            'produk_id' => '40'

        ]);
    }
}
