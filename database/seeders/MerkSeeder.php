<?php

namespace Database\Seeders;

use App\Models\Merk;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class MerkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Merk::create([
            'nm_merk' => 'Abus',
            'gambar' => 'Abus.jpg'
        ]);
        Merk::create([
            'nm_merk' => 'Alpinestars',
            'gambar' => 'Alpinestars.jpg'
        ]);
        Merk::create([
            'nm_merk' => 'Avand',
            'gambar' => 'Avand.jpg'
        ]);
        Merk::create([
            'nm_merk' => 'Blackburn',
            'gambar' => 'Blackburn.jpg'
        ]);
        Merk::create([
            'nm_merk' => 'Dahon Ion',
            'gambar' => 'DahonIon.jpg'
        ]);
        Merk::create([
            'nm_merk' => 'Element',
            'gambar' => 'Element.jpg'
        ]);
        Merk::create([
            'nm_merk' => 'Fabric',
            'gambar' => 'Fabric.jpg'
        ]);
        Merk::create([
            'nm_merk' => 'Fnhon',
            'gambar' => 'Fnhon.jpg'
        ]);
        Merk::create([
            'nm_merk' => 'Fox',
            'gambar' => 'Fox.jpg'
        ]);
        Merk::create([
            'nm_merk' => 'GreatCycle',
            'gambar' => 'GreatCycle.jpg'
        ]);
        Merk::create([
            'nm_merk' => 'Genio',
            'gambar' => 'Genio.jpg'
        ]);
        Merk::create([
            'nm_merk' => 'Ion',
            'gambar' => 'Ion.jpg'
        ]);
        Merk::create([
            'nm_merk' => 'Pacific',
            'gambar' => 'Pacific.jpg'
        ]);
        Merk::create([
            'nm_merk' => 'Police',
            'gambar' => 'Police.jpg'
        ]);
        Merk::create([
            'nm_merk' => 'Polygon',
            'gambar' => 'Polygon.jpg'
        ]);
        Merk::create([
            'nm_merk' => 'Pvr',
            'gambar' => 'Pvr.jpg'
        ]);
        Merk::create([
            'nm_merk' => 'Raze',
            'gambar' => 'Raze.jpg'
        ]);
        Merk::create([
            'nm_merk' => 'SantaCruz',
            'gambar' => 'SantaCruz.jpg'
        ]);
        Merk::create([
            'nm_merk' => 'SelleRoyal',
            'gambar' => 'SelleRoyal.jpg'
        ]);
        Merk::create([
            'nm_merk' => 'Shimano',
            'gambar' => 'Shimano.jpg'
        ]);
        Merk::create([
            'nm_merk' => 'Str',
            'gambar' => 'Str.jpg'
        ]);
        Merk::create([
            'nm_merk' => 'United',
            'gambar' => 'United.jpg'
        ]);
        Merk::create([
            'nm_merk' => 'Velo',
            'gambar' => 'Velo.jpg'
        ]);
        Merk::create([
            'nm_merk' => 'Wimcycle',
            'gambar' => 'Wimcycle.jpg'
        ]);
    }
}
