<?php

namespace Database\Seeders;

use App\Models\Spesifikasi;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SpesifikasiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Spesifikasi::create([
            'nm_spesifikasi' => 'Pilihan Warna : ',
            'deskripsi' => 'Biru, Hitam, Putih',
            'produk_id' => '1'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Bahan :',
            'deskripsi' => 'EPS dan Polycarbonate shell',
            'produk_id' => '1'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Lingkar Kepala : ',
            'deskripsi' => 'M (52-58 cm) dan L (58-62 cm)',
            'produk_id' => '1'
        ]);

        Spesifikasi::create([
            'nm_spesifikasi' => 'Pilihan Warna :',
            'deskripsi' => 'Hitam, Orange, Navy',
            'produk_id' => '2'
        ]);

        Spesifikasi::create([
            'nm_spesifikasi' => 'Pilihan Warna :',
            'deskripsi' => 'Biru, Merah',
            'produk_id' => '3'
        ]);

        Spesifikasi::create([
            'nm_spesifikasi' => 'Pengguna :',
            'deskripsi' => 'Remaja dan Dewasa',
            'produk_id' => '3'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Berat Total :',
            'deskripsi' => '8 kg',
            'produk_id' => '3'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Pilihan Warna :',
            'deskripsi' => 'Hitam, Kuning',
            'produk_id' => '4'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Pengguna :',
            'deskripsi' => 'Remaja dan Dewasa',
            'produk_id' => '4'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Berat Total :',
            'deskripsi' => '7 kg',
            'produk_id' => '4'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Pilihan Warna :',
            'deskripsi' => 'Hitam dan Merah',
            'produk_id' => '5'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Pengguna :',
            'deskripsi' => 'Remaja dan Dewasa',
            'produk_id' => '5'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Berat Total :',
            'deskripsi' => '8 kg',
            'produk_id' => '5'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Bahan :',
            'deskripsi' => 'PVC dan Silicone',
            'produk_id' => '6'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Baterai :',
            'deskripsi' => '2 x CR2032',
            'produk_id' => '6'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Brightness :',
            'deskripsi' => 'High dan Flash',
            'produk_id' => '6'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Material :',
            'deskripsi' => 'Silicone',
            'produk_id' => '7'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Panjang :',
            'deskripsi' => '135mm',
            'produk_id' => '7'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Diameter :',
            'deskripsi' => '32mm',
            'produk_id' => '7'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Pilihan Warna :',
            'deskripsi' => 'Merah, Biru',
            'produk_id' => '8'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Pengguna :',
            'deskripsi' => 'Remaja dan Dewasa',
            'produk_id' => '8'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Berat Total :',
            'deskripsi' => '9 kg',
            'produk_id' => '8'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Bahan :',
            'deskripsi' => 'Plastik BPA free',
            'produk_id' => '9'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Ukuran :',
            'deskripsi' => '600ml',
            'produk_id' => '9'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Bahan :',
            'deskripsi' => 'Alloy',
            'produk_id' => '10'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Pilihan Warna :',
            'deskripsi' => 'Biru, Orange',
            'produk_id' => '11'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Pengguna :',
            'deskripsi' => 'Anak Laki Laki',
            'produk_id' => '11'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Berat Total :',
            'deskripsi' => '6 kg',
            'produk_id' => '11'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Bahan :',
            'deskripsi' => 'EPS (Expanded Polystyrene)',
            'produk_id' => '12'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Lingkar Kepala :',
            'deskripsi' => '56-61cm',
            'produk_id' => '12'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Pilihan Warna :',
            'deskripsi' => 'Hitam, Merah',
            'produk_id' => '13'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Aki :',
            'deskripsi' => '48v 12AH',
            'produk_id' => '13'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Kecepatan Maksimal :',
            'deskripsi' => '25 km/jam',
            'produk_id' => '13'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Pilihan Warna :',
            'deskripsi' => 'Kuning, Merah, Biru',
            'produk_id' => '14'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Aki :',
            'deskripsi' => '30v 10AH',
            'produk_id' => '14'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Bahan :',
            'deskripsi' => 'Aluminium',
            'produk_id' => '15'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Pilihan Warna :',
            'deskripsi' => 'Merah, Kuning, Putih',
            'produk_id' => '16'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Pengguna :',
            'deskripsi' => 'Anak Laki Laki',
            'produk_id' => '16'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Berat Total :',
            'deskripsi' => '5 kg',
            'produk_id' => '16'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Aki :',
            'deskripsi' => '48V 12.2AH',
            'produk_id' => '17'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Kecepatan Maksimal :',
            'deskripsi' => '25 km/jam',
            'produk_id' => '17'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Pilihan Warna :',
            'deskripsi' => 'Hitam',
            'produk_id' => '18'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Pengguna :',
            'deskripsi' => 'Remaja dan Dewasa',
            'produk_id' => '18'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Berat Total :',
            'deskripsi' => '9 kg',
            'produk_id' => '18'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Pilihan Warna :',
            'deskripsi' => 'Hitam, Biru',
            'produk_id' => '19'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Bahan :',
            'deskripsi' => 'Super Stretch (4 Way Stretch)',
            'produk_id' => '19'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Pilihan Warna :',
            'deskripsi' => 'Merah, Kuning, Hijau',
            'produk_id' => '20'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Bahan :',
            'deskripsi' => 'Stretch',
            'produk_id' => '20'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Ukuran Kemasan (Dalam Cm) :',
            'deskripsi' => '21 x 4 x 14 cm',
            'produk_id' => '21'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Bahan :',
            'deskripsi' => 'Alloy',
            'produk_id' => '21'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Pilihan Warna :',
            'deskripsi' => 'Hijau Mint',
            'produk_id' => '22'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Pengguna :',
            'deskripsi' => 'Remaja dan Dewasa',
            'produk_id' => '22'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Berat Total :',
            'deskripsi' => '9 kg',
            'produk_id' => '22'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Material :',
            'deskripsi' => 'Duoble Cover dan 3D Skin Gel',
            'produk_id' => '23'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Lebar :',
            'deskripsi' => '155mm',
            'produk_id' => '23'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Pilihan Warna :',
            'deskripsi' => 'Silver',
            'produk_id' => '24'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Tipe Rantai :',
            'deskripsi' => '12 Speed',
            'produk_id' => '24'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Model :',
            'deskripsi' => 'M8100',
            'produk_id' => '24'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Pilihan Warna :',
            'deskripsi' => 'Silver',
            'produk_id' => '25'
        ]);

        Spesifikasi::create([
            'nm_spesifikasi' => 'Tipe Rantai :',
            'deskripsi' => '10 Speed',
            'produk_id' => '25'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Model :',
            'deskripsi' => 'HG-54',
            'produk_id' => '25'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Pilihan Warna :',
            'deskripsi' => 'Silver',
            'produk_id' => '26'
        ]);

        Spesifikasi::create([
            'nm_spesifikasi' => 'Tipe Rantai :',
            'deskripsi' => '11 Speed',
            'produk_id' => '26'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Model :',
            'deskripsi' => 'HG-601',
            'produk_id' => '26'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Pilihan Warna :',
            'deskripsi' => 'Silver',
            'produk_id' => '27'
        ]);

        Spesifikasi::create([
            'nm_spesifikasi' => 'Tipe Rantai :',
            'deskripsi' => '11 Speed',
            'produk_id' => '27'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Model :',
            'deskripsi' => 'HG-701',
            'produk_id' => '27'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Pilihan Warna :',
            'deskripsi' => 'Biru',
            'produk_id' => '28'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Bahan :',
            'deskripsi' => 'mesh/TPU',
            'produk_id' => '28'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Size :',
            'deskripsi' => '37',
            'produk_id' => '28'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Pilihan Warna :',
            'deskripsi' => 'Biru, Hitam, Putih',
            'produk_id' => '29'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Size :',
            'deskripsi' => '40',
            'produk_id' => '29'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Bahan :',
            'deskripsi' => 'Silica Gel',
            'produk_id' => '30'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Lebar :',
            'deskripsi' => '13.5cm',
            'produk_id' => '30'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Pilihan Warna :',
            'deskripsi' => 'Hitam',
            'produk_id' => '31'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Pengguna :',
            'deskripsi' => 'Remaja dan Dewasa',
            'produk_id' => '31'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Berat Total :',
            'deskripsi' => '6 kg',
            'produk_id' => '31'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Ukuran Kemasan (Dalam Cm) :',
            'deskripsi' => '16 x 10 x 10 cm',
            'produk_id' => '32'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Voltage :',
            'deskripsi' => '12V 12 Ah',
            'produk_id' => '32'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Pilihan Warna :',
            'deskripsi' => 'Merah, Putih',
            'produk_id' => '33'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Pengguna :',
            'deskripsi' => 'Anak-Anak dan Remaja',
            'produk_id' => '33'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Berat Total :',
            'deskripsi' => '6 kg',
            'produk_id' => '33'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Pilihan Warna :',
            'deskripsi' => 'Hijau',
            'produk_id' => '34'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Pengguna :',
            'deskripsi' => 'Anak-Anak dan Remaja',
            'produk_id' => '34'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Berat Total :',
            'deskripsi' => '7 kg',
            'produk_id' => '34'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Pilihan Warna :',
            'deskripsi' => 'Hitam',
            'produk_id' => '35'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Pengguna :',
            'deskripsi' => 'Anak-Anak dan Remaja',
            'produk_id' => '35'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Berat Total :',
            'deskripsi' => '9 kg',
            'produk_id' => '35'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Pilihan Warna :',
            'deskripsi' => 'Abu, Cream',
            'produk_id' => '36'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Pengguna :',
            'deskripsi' => 'Anak-Anak ',
            'produk_id' => '36'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Berat Total :',
            'deskripsi' => '5 kg',
            'produk_id' => '36'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Pilihan Warna :',
            'deskripsi' => 'Hitam, Merah',
            'produk_id' => '37'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Pengguna :',
            'deskripsi' => 'Anak-Anak ',
            'produk_id' => '37'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Berat Total :',
            'deskripsi' => '5 kg',
            'produk_id' => '37'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Berat Total :',
            'deskripsi' => '5 kg',
            'produk_id' => '37'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Panjang :',
            'deskripsi' => '13cm',
            'produk_id' => '38'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Pilihan Warna :',
            'deskripsi' => 'Abu, Merah',
            'produk_id' => '39'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Pengguna :',
            'deskripsi' => 'Anak-Anak ',
            'produk_id' => '39'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Berat Total :',
            'deskripsi' => '5 kg',
            'produk_id' => '39'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Material :',
            'deskripsi' => 'Foam Matrix',
            'produk_id' => '40'
        ]);
        Spesifikasi::create([
            'nm_spesifikasi' => 'Lebar :',
            'deskripsi' => '155mm',
            'produk_id' => '40'
        ]);
    }
}
