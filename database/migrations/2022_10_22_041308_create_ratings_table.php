<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ratings', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid')->unique();
            $table->string('komentar');
            $table->string('gambar');
            $table->integer('total_rating');
            $table->foreignId('produk_id')->references('id')->on('produks')
                ->onDelete('restrict');
            $table->foreignId('user_id')->references('id')->on('users')
                ->onDelete('restrict');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ratings');
    }
};
