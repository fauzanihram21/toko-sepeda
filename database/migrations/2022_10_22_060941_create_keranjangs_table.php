<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('keranjangs', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid')->unique();
            $table->integer('kuantitas');

            $table->foreignId('produk_id')->references('id')->on('produks')
                ->onDelete('restrict');

            $table->foreignId('user_id')->references('id')->on('users')
                ->onDelete('restrict');

            $table->foreignId('produk_variasi_id')->references('id')->on('produk_variasis')
                ->onDelete('cascade');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('keranjangs');
    }
};
