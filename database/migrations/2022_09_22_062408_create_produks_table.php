<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produks', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid')->unique();
            $table->string('nm_produk');
            $table->text('deskripsi');
            $table->double('harga');
            $table->double('harga_diskon')->nullable();
            $table->integer('berat');
            $table->string('gambar');
            $table->integer('diskon')->nullable();



            $table->foreignId('sub_kategori_id')->references('id')->on('sub_kategoris')
                ->onDelete('restrict');

            $table->foreignId('merk_id')->references('id')->on('merks')
                ->onDelete('restrict');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produks');
    }
};
