<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pesanans', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid')->unique();
            $table->integer('kuantitas');
            $table->double('sub_total');

            $table->foreignId('produk_id')->references('id')->on('produks')
                ->onDelete('cascade');

            $table->foreignId('produk_variasi_id')->references('id')->on('produk_variasis')
                ->onDelete('cascade');

            $table->foreignId('pembayaran_id')->references('id')->on('pembayarans')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pesanans');
    }
};
