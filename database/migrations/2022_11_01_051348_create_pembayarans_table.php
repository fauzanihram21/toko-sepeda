<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pembayarans', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid')->unique();
            $table->enum('status', ['menunggu_pembayaran', 'pesanan_diterima', 'diproses', 'sedang_dikirim', 'selesai', 'cancel'])->default('menunggu_pembayaran');
            $table->enum('payment_status', ['belum_bayar', 'dibayar', 'batal', 'kadaluarsa'])->default('belum_bayar');

            $table->string('no_invoice');
            $table->string('no_pesanan')->nullable();
            $table->string('bukti_pembayaran')->nullable();
            $table->string('catatan')->nullable();
            $table->string('snap_token', 36)->nullable();
            $table->double('total_harga')->default(0);



            $table->foreignId('user_id')->references('id')->on('users')
                ->onDelete('restrict');

            $table->foreignId('pengiriman_id')->nullable()->references('id')->on('pengirimans')
                ->onDelete('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pembayarans');
    }
};
