<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daftar_alamats', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid')->unique();


            $table->foreignId('user_id')->references('id')->on('users')
                ->onDelete('restrict');

            $table->string('nama_penerima');
            $table->string('kodepos');
            $table->string('detail_alamat');
            $table->string('phone');



            $table->foreignId('provinsi_id')->references('id')->on('provinsis')
                ->onDelete('restrict');

            $table->foreignId('kota_id')->references('id')->on('kotas')
                ->onDelete('restrict');






            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('daftar_alamats');
    }
};
