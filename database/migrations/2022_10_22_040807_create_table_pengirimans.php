<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengirimans', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid')->unique();
            $table->string('alamat');
            $table->string('kode_pos');
            $table->string('no_telp');
            $table->string('nm_ekspedisi');
            $table->double('ongkir');
            $table->string('paket_layanan');
            $table->string('no_resi')->nullable();
            $table->string('nama_penerima');
            $table->string('estimasi');



            $table->foreignId('provinsi_id')->references('id')->on('provinsis')
                ->onDelete('restrict');

            $table->foreignId('kota_id')->references('id')->on('kotas')
                ->onDelete('restrict');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pengirimans', function (Blueprint $table) {
            //
        });
    }
};
