@extends('front.layouts.main')


@section('container')
    <!-- End Header Area -->

    <!-- Start Banner Area -->
    <section class="banner-area organic-breadcrumb">
        <div class="container">
            <div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
                <div class="col-first">
                    <h1>Merk</h1>
                    <nav class="d-flex align-items-center">
                        <a href="/">Home<span class="lnr lnr-arrow-right"></span></a>
                        <div class="text-white">Merk</div>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    <!-- End Banner Area -->

    <!--================Checkout Area =================-->

    <!-- End Banner Area -->


    <div class="container" style="margin-top: 60px; margin-bottom: 80px">
        <div class="row">
            @foreach ($merk as $value)
                <a class="col-lg-3 single-img" href="{{ url('/merk/' . $value->uuid) }}">
                    <div class="mb-4">
                        <div class="card" style="width: 15rem;">
                            <img class="card-img-top" src="{{ asset('storage/merk/' . $value->gambar) }}"
                                alt="Card image cap">
                            <div class="card-body">
                                <p class="card-text text-center" style="color: black">{{ $value->nm_merk }}</p>
                            </div>
                        </div>
                    </div>

                </a>
            @endforeach
        </div>
    </div>


    <!--================Checkout Area =================-->

    <!--================End Checkout Area =================-->
    <!--================End Checkout Area =================-->
@endsection
