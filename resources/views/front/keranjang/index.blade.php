@extends('front.layouts.main')


@section('container')
    <section class="banner-area organic-breadcrumb">
        <div class="container">
            <div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
                <div class="col-first">
                    <h1>keranjang</h1>
                    <nav class="d-flex align-items-center">
                        <a href="index.html">Home<span class="lnr lnr-arrow-right"></span></a>
                        <a href="category.html">keranjang</a>
                    </nav>
                </div>
            </div>
        </div>
    </section>

    <section class="cart_area">
        <div class="container">
            <div class="cart_inner">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">Produk</th>
                                <th scope="col">Warna</th>
                                <th scope="col">Harga</th>
                                <th scope="col">Total</th>
                                <th scope="col">Kuantitas</th>

                            </tr>
                        </thead>
                        <tbody>
                            @if (count($keranjang))
                                @foreach ($keranjang as $value)
                                    <tr>
                                        <td>
                                            <div class="media">
                                                <div class="d-flex">
                                                    <img width="100px"
                                                        src="{{ asset('storage/produk_variasi/' . $value->ProdukVariasi->gambar) }}">
                                                </div>
                                                <div class="media-body">
                                                    <p>{{ $value->produk->nm_produk }}</p>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <h5>{{ $value->ProdukVariasi->warna }}</h5>
                                        </td>
                                        <td>
                                            @if ($value->produk->diskon)
                                                <h6>{{ $value->diskon }}</h6>
                                                <h6>Rp.
                                                    {{ number_format($value->produk->harga_diskon, 0, ',', '.') }}

                                                </h6>
                                            @else
                                                <h6>Rp. {{ number_format($value->produk->harga, 0, ',', '.') }}
                                                </h6>
                                            @endif
                                        </td>

                                        <td>
                                            <h5> Rp. {{ number_format($value->sub_total, 0, ',', '.') }}
                                            </h5>
                                        </td>

                                        <td class="d-flex gap-4" style="padding:4.5rem 0;">
                                            <form action="{{ url('/keranjang/' . $value->id) }}" method="POST"
                                                id="form-kuantitas-{{ $value->id }}" class="d-flex gap-4">
                                                @csrf
                                                <input type="hidden" name="_method" value="PUT">
                                                <div class="product_count">
                                                    <input type="number" min="1"
                                                        max="{{ $value->ProdukVariasi->stock }}" min="0"
                                                        name="kuantitas" id="sst-{{ $value->id }}" title="Quantity:"
                                                        data-id="{{ $value->id }}" class="input-text kuantitas"
                                                        value="{{ $value->kuantitas }}">

                                                </div>


                                            </form>
                                            <div class="d-flex">
                                                <button type="button" class="btn btn-danger hapus"
                                                    data-id="{{ $value->id }}"><i class="fa-solid fa-trash"></i>
                                                    Hapus</button>
                                            </div>
                                        </td>

                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="5" class="text-center">
                                        <h5>Belum ada produk di keranjang</h5>
                                    </td>
                                </tr>
                            @endif

                            <tr>
                                <td>

                                </td>
                                <td>

                                </td>
                                <td>
                                    <h5>Total</h5>
                                </td>
                                <td>
                                    <h5>Rp. {{ number_format($total, 0, ',', '.') }}
                                    </h5>
                                </td>
                            </tr>

                            <tr class="out_button_area">
                                <td>

                                </td>
                                <td>

                                </td>
                                <td>

                                </td>
                                <td>
                                    <div class="checkout_btn_inner d-flex align-items-center">
                                        <a class="gray_btn" href="/">Lanjut Belanja</a>
                                        @if ($keranjang->count() > 0)
                                            <a class="primary-btn" href="/checkout">Checkout Belanja</a>
                                        @endif
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script>
        $('.table').on('click', '.hapus', function() {
            var id = $(this).data('id')
            Swal.fire({
                title: 'Apakah Anda yakin ingin menghapus?',
                showCancelButton: true,
                confirmButtonText: 'Ya, Hapus',
                showLoaderOnConfirm: true,
                preConfirm: () => {
                    return $.ajax({
                        url: '{{ url('/keranjang') }}/' + id,
                        method: 'POST',
                        headers: {
                            'X-CSRF-TOKEN': '{{ @csrf_token() }}'
                        },
                        data: {
                            _method: 'DELETE'
                        },
                        error: function() {
                            Swal.showValidationMessage('Gagal menghapus data')
                        }
                    });
                }
            }).then(result => {
                if (result.isConfirmed) {
                    Swal.fire('Berhasil menghapus data', '', 'success')
                    window.location.reload()
                }
            })
        });

        function debounce(func, timeout = 1000) {
            let timer;
            return (...args) => {
                clearTimeout(timer);
                timer = setTimeout(() => {
                    func.apply(this, args);
                }, timeout);
            };
        }

        document.querySelectorAll('.kuantitas').forEach(item => {
            item.addEventListener('input', debounce(function() {
                const id = item.dataset.id;
                const kuantitas = item.value;
                const url = `/keranjang/${id}`;
                $.ajax({
                    url: url,
                    method: 'post',
                    headers: {
                        'X-CSRF-TOKEN': '{{ @csrf_token() }}'
                    },
                    data: {
                        kuantitas: kuantitas,
                    },
                    success: function(response) {
                        window.location.reload()
                    }
                });
            }, 1000));
        });
    </script>
@endsection
