{{-- @dd($produkAll) --}}
@extends('front.layouts.main')


@section('container')
    <!-- End Header Area -->

    <!-- Start Banner Area -->
    <section class="banner-area organic-breadcrumb">
        <div class="container">
            <div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
                <div class="col-first">
                    <h1>Profile</h1>
                    <nav class="d-flex align-items-center">
                        <a href="/">Home<span class="lnr lnr-arrow-right"></span></a>
                        <div class="text-white">Profile</div>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    <!-- End Banner Area -->

    <!--================Checkout Area =================-->

    <!-- End Banner Area -->

    <!--================Checkout Area =================-->
    <section class="checkout_area section_gap">
        <div class="container">
            <div class="profile">
                <div class="row">
                    <div class="col-lg-8">
                        <h3>Profile Saya</h3>
                        <form action="{{ url('/profile/profilesaya') }}" method="POST" enctype="multipart/form-data">
                            @csrf

                            <div class="row">
                                <div class="col-12 text-center">
                                    <label for="avatar" class="mb-4" style="cursor: pointer; position: relative">

                                        <input type="file" accept="image/*" class="d-none" id="avatar" name="avatar"
                                            accept=".png, .jpg, .jpeg" onchange="previewImage()">
                                        <span class="btn bg-white rounded-circle shadow"
                                            style="position: absolute; top: -10px; right: -10px">
                                            <i class="fa fa-pencil"></i>
                                        </span>
                                        <img width="100px"
                                            src="{{ isset($user->avatar) ? asset('storage/avatar/' . $user->avatar) : asset('/assets/media/svg/avatars/blank.svg') }}"
                                            class="rounded" id="avatar-preview" alt="image">
                                    </label>
                                </div>


                                <div class="row mb-6">
                                    <label class="col-lg-4 required form-label">Nama :</label>
                                    <div class="col-lg-8">
                                        <input type="text" placeholder="Name" name="name" required="required"
                                            class="form-control mb-2" value="{{ $user->name }}">
                                    </div>
                                </div>

                                <div class="row mb-6">
                                    <label class="col-lg-4 required form-label">Username :</label>
                                    <div class="col-lg-8">
                                        <input type="text" placeholder="Username" name="username" required="required"
                                            class="form-control mb-2" value="{{ $user->username }}">
                                    </div>
                                </div>

                                <div class="row mb-6">
                                    <label class="col-lg-4 required form-label">Email :</label>
                                    <div class="col-lg-8">
                                        <input type="text" placeholder="Email" name="email" required="required"
                                            readonly class="form-control mb-2" value="{{ $user->email }}">
                                    </div>
                                </div>

                                <div class="row mb-6">
                                    <label class="col-lg-4 required form-label">Nomer Telepon :</label>
                                    <div class="col-lg-8">
                                        <input type="text" placeholder="No Telp" name="phone" class="form-control mb-2"
                                            value="{{ $user->phone }}">
                                    </div>
                                </div>

                            </div>
                            <div class="form-action">
                                <button type="submit" class="genric-btn primary circle mt-4"
                                    style="background-color:orange; outline: none; border: none; margin-right: 10px">
                                    Save
                                </button>

                            </div>
                        </form>

                    </div>

                </div>

            </div>

    </section>


    <!--================End Checkout Area =================-->
    <!--================End Checkout Area =================-->
@endsection

@section('script')
    <script>
        function previewImage() {
            const imgInput = document.querySelector("#avatar");
            const imgPreview = document.querySelector("#avatar-preview");

            const reader = new FileReader();
            reader.readAsDataURL(imgInput.files[0]);

            reader.onload = function(ev) {
                imgPreview.src = ev.target.result;
            }
        }
    </script>
    @if (session('successcreate'))
        <script>
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 2000,
                timerProgressBar: true,
                didOpen: (toast) => {
                    toast.addEventListener('mouseenter', Swal.stopTimer)
                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
            })

            Toast.fire({
                icon: 'success',
                title: '{{ session('successcreate') }}'
            }).then((result) => {
                window.location.reload();
            })
        </script>
    @endif
@endsection
