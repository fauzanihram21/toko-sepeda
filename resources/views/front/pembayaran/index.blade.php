@extends('front.layouts.main')


@section('container')
    <section class="banner-area organic-breadcrumb">
        <div class="container">
            <div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
                <div class="col-first">
                    <h1>Pembayaran</h1>
                    <nav class="d-flex align-items-center">
                        <a href="/">Home<span class="lnr lnr-arrow-right"></span></a>
                        <div class="text-white">Pembayaran</div>
                    </nav>
                </div>
            </div>
        </div>
    </section>

    <section class="cart_area">
        <div class="container">
            <div class="cart_inner">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">Produk</th>
                                <th scope="col">Warna</th>
                                <th scope="col">Harga</th>
                                <th scope="col">Total</th>
                                <th scope="col">Kuantitas</th>

                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($keranjang as $value)
                                <tr>
                                    <td>
                                        <div class="media">
                                            <div class="d-flex">
                                                <img width="100px"
                                                    src="{{ asset('storage/produk_variasi/' . $value->ProdukVariasi->gambar) }}">
                                            </div>
                                            <div class="media-body">
                                                <p>{{ $value->produk->nm_produk }}</p>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <h5>{{ $value->ProdukVariasi->warna }}</h5>
                                    </td>
                                    <td>
                                        @if ($value->produk->diskon)
                                            <h6>
                                                Rp. {{ number_format($value->produk->harga_diskon, 0, ',', '.') }}
                                            </h6>
                                        @else
                                            <h6>Rp. {{ number_format($value->produk->harga, 0, ',', '.') }}</h6>
                                        @endif
                                    </td>

                                    <td>
                                        <h5> Rp. {{ number_format($value->sub_total, 0, ',', '.') }}</h5>
                                    </td>

                                    <td class="d-flex gap-4" style="padding:4.5rem 0;">
                                        <form action="{{ url('/keranjang/' . $value->id) }}" method="POST"
                                            id="form-kuantitas-{{ $value->id }}" class="d-flex gap-4">
                                            @csrf
                                            <span>{{ $value->kuantitas }}</span>


                                        </form>

                                    </td>

                                </tr>
                            @endforeach


                            <tr>
                                <td>

                                </td>
                                <td>

                                </td>
                                <td>
                                    <h5>Total</h5>
                                </td>
                                <td>
                                    <h5>
                                        Rp. {{ number_format($total, 0, ',', '.') }}
                                    </h5>
                                </td>
                            </tr>

                            <tr class="out_button_area">
                                <td>

                                </td>
                                <td>

                                </td>
                                <td>

                                </td>
                                <td>

                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <form action="/chekout/pembayaran/charge" method="POST" class="billing_details">
                @csrf
                <div class="billing_details">
                    <div class="row" style="margin-top:70px">
                        <div class="col-lg-8">

                            <div class="order_box">
                                <h2>Alamat Pengiriman</h2>
                                <ul class="list list_2">

                                    <li class="mb-3">
                                        <div>Nama Penerima: {{ $alamat->nama_penerima }}</div>
                                        <div>Nomer Telpon{{ $alamat->phone }}</div>
                                        <div>Provinsi: {{ $alamat->provinsi->nm_provinsi }}</div>
                                        <div>Kota: {{ $alamat->kota->nm_kota }}</div>
                                        <div>Kode Pos: {{ $alamat->kodepos }}</div>
                                        <div>Alamat: {{ $alamat->detail_alamat }}</div>




                                    </li>
                                    <li class="mb-3">
                                        <h6>Metode Pengiriman</h6>
                                        <div>{{ request()->courier }} {{ $service }}</div>
                                    </li>
                                    <li class="mb-3">
                                        <h6>Catatan</h6>
                                        <div>{{ $deskripsi }}</div>
                                    </li>
                                </ul>

                            </div>

                            {{-- <div class="row">
                            @foreach ($alamat as $alamats)
                                <div class="col-lg-4">
                                    <label for="alamat-{{ $alamats->id }}" class="mb-4">
                                        <input type="radio" id="alamat-{{ $alamats->id }}" name="daftar_alamat_id"
                                            value="{{ $alamats->id }}" class="pilih-alamat d-none">
                                        <div class="card">
                                            <div class="card-header">
                                                <h4>Nama Penerima : {{ $alamats->nama_penerima }}</h4>

                                                <h6> Nomer Telpon : {{ $alamats->phone }}</h6>
                                            </div>

                                            <div class="card-body">
                                                <div>Provinsi : {{ $alamats->provinsi->nm_provinsi }}</div>
                                                <div>Kota : {{ $alamats->kota->nm_kota }}</div>
                                                <div>Kode Pos : {{ $alamats->kodepos }}</div>

                                                <div> Detai Alamat :
                                                    {{ $alamats->detail_alamat }}
                                                </div>
                                            </div>
                                        </div>
                                    </label>
                                </div>
                            @endforeach
                        </div> --}}

                        </div>

                        <div class="col-lg-4">
                            <div class="order_box">
                                <h2>Pesanan Anda</h2>
                                <ul class="list list_2">

                                    <li><span>Subtotal
                                            <span>Rp. {{ number_format($total, 0, ',', '.') }}</span></span>
                                    </li>
                                    <li><span>Ongkir
                                            <span>Rp. {{ number_format($ongkir, 0, ',', '.') }}</span></span>
                                    </li>
                                    <li><span>Total
                                            <span>Rp. {{ number_format($total + $ongkir, 0, ',', '.') }}</span></span>
                                    </li>
                                </ul>
                                <a class="btn btn-primary w-100" href="/">Continue Shopping</a>

                                @if ($keranjang->count() > 0)
                                    <button class="text-white btn btn-success w-100 mt-4"
                                        style="outline: none; border: none" id="pay-button">
                                        Lanjut Bayar
                                    </button>
                                @endif
                            </div>
                        </div>
                    </div>

                </div>

            </form>
        </div>

    </section>
@endsection

@section('script')
    <script src="https://app.sandbox.midtrans.com/snap/snap.js" data-client-key="{{ config('midtrans.client_key') }}">
    </script>
    <script>
        const payButton = document.querySelector('#pay-button');
        payButton.addEventListener('click', function(e) {
            e.preventDefault();

            $.ajax({
                url: '/checkout/pembayaran/charge',
                method: 'POST',
                data: {
                    ongkir: {{ $ongkir }},
                    courier: '{{ request()->courier }}',
                    estimasi: '{{ request()->estimasi }}',
                    service: '{{ $service }}',
                    daftar_alamat_id: {{ $alamat->id }},
                    deskripsi: '{{ $deskripsi }}',

                },
                headers: {
                    'X-CSRF-TOKEN': '{{ @csrf_token() }}'
                },
                success: function(data) {
                    snap.pay(data.snap_token, {
                        // Optional
                        onSuccess: function(result) {
                            /* You may add your own js here, this is just example */
                            // document.getElementById('result-json').innerHTML += JSON.stringify(result, null, 2);
                            console.log(result);
                            window.location.href = '/selesai/pembayaran/' + result.order_id
                        },
                        onPending: function(result) {
                            console.log('pending');
                            window.location.href = '/riwayat/pembayaran'

                        },
                        onError: function(result) {
                            console.log('error');
                            window.location.href = '/riwayat/pembayaran'

                        },
                        onClose: function() {
                            console.log(
                                window.location.href = '/riwayat/pembayaran'
                            );
                        }
                    });
                }
            })
        });
    </script>
@endsection
