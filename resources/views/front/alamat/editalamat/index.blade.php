@extends('front.layouts.main')


@section('container')
    <!-- End Header Area -->

    <!-- Start Banner Area -->
    <section class="banner-area organic-breadcrumb">
        <div class="container">
            <div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
                <div class="col-first">
                    <h1>Edit Alamat</h1>
                    <nav class="d-flex align-items-center">
                        <a href="/">Home<span class="lnr lnr-arrow-right"></span></a>
                        <a class="text-white">Alamat</a>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    <!-- End Banner Area -->

    <!--================Checkout Area =================-->

    <!-- End Banner Area -->
    <section class="checkout_area section_gap">
        <div class="container">
            <div class="profile">
                <div class="row">
                    <div class="col-lg-12">
                        <h3>Edit Alamat</h3>
                        <form action="{{ url('/editalamatsaya/' . $alamat->id) }}" method="POST">
                            @csrf
                            <input type="hidden" name="_method" value="PUT">
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="mb-4">
                                            <label class="required form-label">Nama Penerima
                                                :</label>
                                            <input type="text" placeholder="Nama Penerima" name="nama_penerima"
                                                required="required" class="form-control"
                                                value="{{ $alamat->nama_penerima }}">

                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="mb-4">
                                            <label class="required form-label">Nomer Telpon
                                                :</label>
                                            <input type="text" placeholder="Nomer Telpon" name="phone"
                                                required="required" class="form-control" value="{{ $alamat->phone }}">

                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="mb-4">
                                            <label class="required form-label">Kode Pos :</label>
                                            <input type="text" placeholder="Kode Pos" name="kodepos" required="required"
                                                class="form-control" value="{{ $alamat->kodepos }}">

                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="mb-4 form-group">
                                            <label for="exampleFormControlSelect1">Provinsi
                                                :</label>
                                            <select class="form-control" name="provinsi_id" id="provinsi_id">
                                                <option></option>
                                                @foreach ($provinsi as $value)
                                                    @if ($value->id == $alamat->provinsi_id)
                                                        <option value="{{ $value->id }}" selected>
                                                            {{ $value->nm_provinsi }}
                                                        </option>
                                                    @else
                                                        <option value="{{ $value->id }}">
                                                            {{ $value->nm_provinsi }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mb-4  form-group">
                                            <label for="exampleFormControlSelect1">Kota :</label>
                                            <select class="form-control" name="kota_id" id="kota_id">
                                                <option></option>
                                                @foreach ($kota as $value)
                                                    @if ($value->id == $alamat->kota_id)
                                                        <option value="{{ $value->id }}" selected>{{ $value->nm_kota }}
                                                        </option>
                                                    @else
                                                        <option value="{{ $value->id }}">
                                                            {{ $value->nm_kota }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="mb-4">
                                            <label class="required form-label">Detail Alamat
                                                :</label>
                                            <textarea name="detail_alamat" placeholder="Detail Alamat" class="form-control">
                                                {{ $alamat->detail_alamat }}
                                        </textarea>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="modal-footer">
                                <a href="{{ url('/alamatsaya') }}" class="genric-btn default circle">Kembali</a>
                                <button type="submit" class="genric-btn primary circle"
                                    style="background-color:orange; outline: none; border: none; margin-right: 10px">
                                    Simpan
                                </button>
                            </div>
                        </form>


                    </div>

                </div>

            </div>

    </section>



    <!--================Checkout Area =================-->

    <!--================End Checkout Area =================-->
    <!--================End Checkout Area =================-->
@endsection

@section('script')
    <script>
        $('#provinsi_id').select2().on('change', function(ev) {
            const provinsi_id = ev.target.value;
            $.ajax({
                url: `/provinsi/${provinsi_id}/kota`,
                method: 'GET',
                success: function(result) {
                    let html = "";
                    result.forEach(kota => {
                        html += `
                            <option value="${kota.id}">${kota.nm_kota}</option>
                        `;
                    })
                    $('#kota_id').html(html);
                    $('#kota_id').select2();
                }
            });
        });
        $('#kota_id').select2();
    </script>
@endsection
