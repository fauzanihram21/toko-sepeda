{{-- @dd($produkAll) --}}
@extends('front.layouts.main')


@section('container')
    <!-- End Header Area -->

    <!-- Start Banner Area -->
    <section class="banner-area organic-breadcrumb">
        <div class="container">
            <div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
                <div class="col-first">
                    <h1>Alamat</h1>
                    <nav class="d-flex align-items-center">
                        <a href="/">Home<span class="lnr lnr-arrow-right"></span></a>
                        <a class="text-white">Alamat</a>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    <!-- End Banner Area -->

    <!--================Checkout Area =================-->

    <!-- End Banner Area -->
    <div class="container" style="margin-top: 60px; margin-bottom: 80px">
        <div class="d-flex">
            <!-- Button trigger modal -->
            <a href="{{ url('tambahalamatsaya' . (isset(request()->redirect) ? '?redirect=' . request()->redirect : '')) }}"
                class="ms-auto btn text-white" style="background-color: #ffba00">
                Tambah
            </a>

            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content ">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <form
                            action="{{ url('/alamatsaya' . (isset(request()->redirect) ? '?redirect=' . request()->redirect : '')) }}"
                            method="POST">
                            @csrf
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="mb-4">
                                            <label class="required form-label">Nama Penerima :</label>
                                            <input type="text" placeholder="Nama Penerima" name="nama_penerima"
                                                required="required" @error('nama_penerima')is-invalid @enderror
                                                class="form-control">
                                            @error('nama_penerima')
                                                <div class="invalid-feedback d-block">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="mb-4">
                                            <label class="required form-label">Nomer Telpon :</label>
                                            <input type="text" placeholder="Nomer Telpon" name="phone"
                                                required="required" @error('phone')is-invalid @enderror
                                                class="form-control">
                                            @error('phone')
                                                <div class="invalid-feedback d-block">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="mb-4">
                                            <label class="required form-label">Kode Pos :</label>
                                            <input type="text" placeholder="Kode Pos" name="kodepos" required="required"
                                                @error('kodepos')is-invalid @enderror class="form-control">
                                            @error('kodepos')
                                                <div class="invalid-feedback d-block">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="mb-4 form-group">
                                            <label class="d-block">Provinsi :</label>
                                            <select class="form-control" style="width: 100%" name="provinsi_id"
                                                id="provinsi_id">
                                                <option></option>
                                                @foreach ($provinsi as $value)
                                                    <option value="{{ $value->id }}">{{ $value->nm_provinsi }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mb-4  form-group">
                                            <label class="d-block">Kota :</label>
                                            <select class="form-control" name="kota_id" style="width: 100%" id="kota_id">
                                                <option></option>
                                                {{-- @foreach ($kota as $value)
                                                    <option value="{{ $value->id }}">{{ $value->nm_kota }}</option>
                                                @endforeach --}}
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="mb-4">
                                            <label class="required form-label">Detail Alamat :</label>
                                            <textarea name="detail_alamat" placeholder="Detail Alamat" class="form-control">
                                            </textarea>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save changes</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            @foreach ($alamat as $alamats)
                <div class="col-lg-4">

                    <div class="card">

                        <div class="card-header">
                            <div class="row mb-4">
                                <div class="col-6">
                                    <a href="{{ url('/editalamatsaya/' . $alamats->id) }}"
                                        class="ms-auto btn btn-primary"><i class="fa-solid fa-pencil"></i>
                                        Edit
                                    </a>

                                    <!-- Modal -->

                                </div>
                                <div class="col-6">
                                    <button type="submit" class="ms-auto btn btn-danger hapus"
                                        data-id="{{ $alamats->id }}"><i class="fa-solid fa-trash"></i>
                                        Hapus</button>
                                </div>
                            </div>


                            <h4>Nama Penerima : {{ $alamats->nama_penerima }}</h4>

                            <h6> Nomer Telpon : {{ $alamats->phone }}</h6>
                        </div>

                        <div class="card-body">
                            <div>Provinsi : {{ $alamats->provinsi->nm_provinsi }}</div>
                            <div>Kota : {{ $alamats->kota->nm_kota }}</div>
                            <div>Kode Pos : {{ $alamats->kodepos }}</div>

                            <div> Detai Alamat :
                                {{ $alamats->detail_alamat }}
                            </div>
                        </div>
                    </div>

                </div>
            @endforeach

        </div>
    </div>




    <!--================Checkout Area =================-->

    <!--================End Checkout Area =================-->
    <!--================End Checkout Area =================-->
@endsection

@section('script')
    <script>
        $('#provinsi_id').select2().on('change', function(ev) {
            const provinsi_id = ev.target.value;
            $.ajax({
                url: `/provinsi/${provinsi_id}/kota`,
                method: 'GET',
                success: function(result) {
                    let html = "";
                    result.forEach(kota => {
                        html += `
                            <option value="${kota.id}">${kota.nm_kota}</option>
                        `;
                    })
                    $('#kota_id').html(html);
                    $('#kota_id').select2();
                }
            });
        });

        $('.hapus').on('click', function() {
            var id = $(this).data('id')
            Swal.fire({
                title: 'Apakah Anda yakin ingin menghapus?',
                showCancelButton: true,
                confirmButtonText: 'Ya, Hapus',
                showLoaderOnConfirm: true,
                preConfirm: () => {
                    return $.ajax({
                        url: '{{ url('/alamatsaya/') }}/' + id,
                        method: 'POST',
                        headers: {
                            'X-CSRF-TOKEN': '{{ @csrf_token() }}'
                        },
                        data: {
                            _method: 'DELETE'
                        },
                        error: function() {
                            Swal.showValidationMessage('Gagal Menghapus Data')
                        }
                    });
                }
            }).then(result => {
                if (result.isConfirmed) {
                    const Toast = Swal.mixin({
                        toast: true,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 1000,
                        timerProgressBar: true,
                        didOpen: (toast) => {
                            toast.addEventListener('mouseenter', Swal.stopTimer)
                            toast.addEventListener('mouseleave', Swal.resumeTimer)
                        }
                    })

                    Toast.fire({
                        icon: 'success',
                        title: 'Berhsil Menghapus Data'
                    }).then(() => {
                        window.location.href = "/alamatsaya"
                    })
                    // Swal.fire('Berhasil Menghapus Data', '', 'success')
                }
            })
        });
    </script>
    @if (session('successcreate'))
        <script>
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 2000,
                timerProgressBar: true,
                didOpen: (toast) => {
                    toast.addEventListener('mouseenter', Swal.stopTimer)
                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
            })

            Toast.fire({
                icon: 'success',
                title: '{{ session('successcreate') }}'
            }).then((result) => {
                window.location.reload();
            })
        </script>
    @endif
    @if (session('successupdate'))
        <script>
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 2000,
                timerProgressBar: true,
                didOpen: (toast) => {
                    toast.addEventListener('mouseenter', Swal.stopTimer)
                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
            })

            Toast.fire({
                icon: 'success',
                title: '{{ session('successupdate') }}'
            }).then((result) => {
                window.location.reload();
            })
        </script>
    @endif
@endsection
