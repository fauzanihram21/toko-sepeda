@extends('front.layouts.main')


@section('container')
    <!-- End Header Area -->

    <!-- Start Banner Area -->
    <section class="banner-area organic-breadcrumb">
        <div class="container">
            <div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
                <div class="col-first">
                    <h1>Tambah Alamat</h1>
                    <nav class="d-flex align-items-center">
                        <a href="/">Home<span class="lnr lnr-arrow-right"></span></a>
                        <a class="text-white">Alamat</a>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    <!-- End Banner Area -->

    <!--================Checkout Area =================-->

    <!-- End Banner Area -->
    <section class="checkout_area section_gap">
        <div class="container">
            <div class="profile">
                <div class="row">
                    <div class="col-lg-12">
                        <h3>Tambah Alamat</h3>
                        <form
                            action="{{ url('/tambahalamatsaya' . (isset(request()->redirect) ? '?redirect=' . request()->redirect : '')) }}"
                            method="POST">
                            @csrf
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="mb-4">
                                            <label class="required form-label">Nama Penerima :</label>
                                            <input type="text" placeholder="Nama Penerima" name="nama_penerima"
                                                required="required" @error('nama_penerima')is-invalid @enderror
                                                class="form-control">
                                            @error('nama_penerima')
                                                <div class="invalid-feedback d-block">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="mb-4">
                                            <label class="required form-label">Nomer Telpon :</label>
                                            <input type="text" placeholder="Nomer Telpon" name="phone"
                                                required="required" @error('phone')is-invalid @enderror
                                                class="form-control">
                                            @error('phone')
                                                <div class="invalid-feedback d-block">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="mb-4">
                                            <label class="required form-label">Kode Pos :</label>
                                            <input type="text" placeholder="Kode Pos" name="kodepos" required="required"
                                                @error('kodepos')is-invalid @enderror class="form-control">
                                            @error('kodepos')
                                                <div class="invalid-feedback d-block">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="mb-4 form-group">
                                            <label class="d-block">Provinsi :</label>
                                            <select class="form-control" style="width: 100%" name="provinsi_id"
                                                id="provinsi_id">
                                                <option></option>
                                                @foreach ($provinsi as $value)
                                                    <option value="{{ $value->id }}">{{ $value->nm_provinsi }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mb-4  form-group">
                                            <label class="d-block">Kota :</label>
                                            <select class="form-control" name="kota_id" style="width: 100%" id="kota_id">
                                                <option></option>
                                                {{-- @foreach ($kota as $value)
                                                <option value="{{ $value->id }}">{{ $value->nm_kota }}</option>
                                            @endforeach --}}
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="mb-4">
                                            <label class="required form-label">Detail Alamat :</label>
                                            <textarea name="detail_alamat" placeholder="Detail Alamat" class="form-control">
                                        </textarea>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="modal-footer">
                                <a href="{{ url('/alamatsaya') }}" class="genric-btn default circle">Kembali</a>
                                <button type="submit" class="genric-btn primary circle"
                                    style="background-color:orange; outline: none; border: none; margin-right: 10px">
                                    Simpan
                                </button>
                            </div>
                        </form>


                    </div>

                </div>

            </div>

    </section>



    <!--================Checkout Area =================-->

    <!--================End Checkout Area =================-->
    <!--================End Checkout Area =================-->
@endsection

@section('script')
    <script>
        $('#provinsi_id').select2().on('change', function(ev) {
            const provinsi_id = ev.target.value;
            $.ajax({
                url: `/provinsi/${provinsi_id}/kota`,
                method: 'GET',
                success: function(result) {
                    let html = "";
                    result.forEach(kota => {
                        html += `
                            <option value="${kota.id}">${kota.nm_kota}</option>
                        `;
                    })
                    $('#kota_id').html(html);
                    $('#kota_id').select2();
                }
            });
        });
    </script>
@endsection
