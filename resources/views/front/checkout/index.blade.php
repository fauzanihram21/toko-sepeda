@extends('front.layouts.main')


@section('container')
    <section class="banner-area organic-breadcrumb">
        <div class="container">
            <div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
                <div class="col-first">
                    <h1>Checkout</h1>
                    <nav class="d-flex align-items-center">
                        <a href="/">Home<span class="lnr lnr-arrow-right"></span></a>
                        <div class="text-white">Checkout</div>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    <!-- End Banner Area -->

    <!--================Checkout Area =================-->
    <section class="checkout_area">
        <div class="container">

            <section class="cart_area">
                <div class="container">
                    <div class="cart_inner">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">Produk</th>
                                        <th scope="col">Warna</th>
                                        <th scope="col">Harga</th>
                                        <th scope="col">Total</th>
                                        <th scope="col">Kuantitas</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($keranjang as $value)
                                        <tr>
                                            <td>
                                                <div class="media">
                                                    <div class="d-flex">
                                                        <img width="100px"
                                                            src="{{ asset('storage/produk_variasi/' . $value->ProdukVariasi->gambar) }}">
                                                    </div>
                                                    <div class="media-body">
                                                        <p>{{ $value->produk->nm_produk }}</p>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <h5>{{ $value->ProdukVariasi->warna }}</h5>
                                            </td>
                                            <td>
                                                @if ($value->produk->diskon)
                                                    <h6>{{ $value->diskon }}</h6>
                                                    <h6>Rp. {{ number_format($value->produk->harga_diskon, 0, ',', '.') }}
                                                    </h6>
                                                @else
                                                    <h6>Rp. {{ number_format($value->produk->harga, 0, ',', '.') }}</h6>
                                                @endif
                                            </td>

                                            <td>
                                                <h5> Rp. {{ number_format($value->sub_total, 0, ',', '.') }}</h5>
                                            </td>

                                            <td class="d-flex gap-4 justify-content-center" style="padding:4.5rem 0;">
                                                <form action="{{ url('/keranjang/' . $value->id) }}" method="POST"
                                                    id="form-kuantitas-{{ $value->id }}" class="d-flex gap-4">
                                                    @csrf
                                                    <span>{{ $value->kuantitas }}</span>


                                                </form>

                                            </td>

                                        </tr>
                                    @endforeach


                                    <tr>
                                        <td>

                                        </td>
                                        <td>

                                        </td>
                                        <td>
                                            <h5>Total</h5>
                                        </td>
                                        <td>
                                            <h5> Rp. {{ number_format($total, 0, ',', '.') }}</h5>
                                        </td>
                                    </tr>

                                    <tr class="out_button_area">
                                        <td>

                                        </td>
                                        <td>

                                        </td>
                                        <td>

                                        </td>
                                        <td>

                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>

            <form action="/checkout/pembayaran" method="POST" class="billing_details">
                @csrf
                <div class="row">
                    <div class="col-lg-8">
                        <h3>Ringkasan Pesanan</h3>
                        <a href="/alamatsaya?redirect=/checkout" class="link js-popup-link">+Tambah Alamat
                            Baru</a>
                        <div>
                            <div class="row">
                                @foreach ($alamat as $alamats)
                                    <div class="col-lg-4">
                                        <label for="alamat-{{ $alamats->id }}" class="mb-4">
                                            <input type="radio" id="alamat-{{ $alamats->id }}" name="daftar_alamat_id"
                                                value="{{ $alamats->id }}" class="pilih-alamat d-none">
                                            <div class="card">
                                                <div class="card-header">
                                                    <h4>Nama Penerima : {{ $alamats->nama_penerima }}</h4>

                                                    <h6> Nomer Telpon : {{ $alamats->phone }}</h6>
                                                </div>

                                                <div class="card-body">
                                                    <div>Provinsi : {{ $alamats->provinsi->nm_provinsi }}</div>
                                                    <div>Kota : {{ $alamats->kota->nm_kota }}</div>
                                                    <div>Kode Pos : {{ $alamats->kodepos }}</div>

                                                    <div> Detai Alamat :
                                                        {{ $alamats->detail_alamat }}
                                                    </div>
                                                </div>
                                            </div>
                                        </label>
                                    </div>
                                @endforeach
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="mb-4">

                                        <label class="required form-label">Pesan :</label>
                                        <textarea name="deskripsi" placeholder="Pesan" class="form-control">
                                         </textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="card" style="width: 46rem; margin-bottom:100px">
                                <div class="card-header">
                                    Pilih Opsi Pengiriman
                                </div>
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item">
                                        <label for="courier-jne">
                                            <input type="radio" id="courier-jne" name="courier" value="jne"
                                                class="pilih-pengiriman">
                                            JNE
                                        </label>
                                    </li>
                                    <li class="list-group-item">
                                        <label for="courier-pos">
                                            <input type="radio" id="courier-pos" name="courier" value="pos"
                                                class="pilih-pengiriman">
                                            POS INDONESIA
                                        </label>
                                    </li>
                                    <li class="list-group-item">
                                        <label for="courier-tiki">
                                            <input type="radio" id="courier-tiki" name="courier" value="tiki"
                                                class="pilih-pengiriman">
                                            TIKI
                                        </label>
                                    </li>
                                </ul>
                            </div>
                            <a type="button" id="cek-ongkir" style="width: 200px; border-radius: 0"
                                class="primary-btn text-white ml-auto text-center mt-4">Cek Ongkir</a>
                            <div class="card" style="width: 46rem;">
                                <div class="card-header">
                                    Pilih Opsi Layanan
                                </div>
                                <ul class="list-group list-group-flush pilih-layanan d-none">

                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="order_box">
                            <h2>Pesanan Anda</h2>
                            <ul class="list list_2">
                                <li><span>Total <span>Rp. {{ number_format($total, 0, ',', '.') }}</span></span></li>
                                <li><span>Ongkir <span class="ongkir-text">Rp. 0</span></span></li>
                                <li><span>Total Tagihan <span class="total-text">Rp.
                                            {{ number_format($total, 0, ',', '.') }}</span></span></li>
                            </ul>
                            <input type="hidden" name="service">
                            <button type="submit" class="primary-btn w-100 mt-4" id="submit" disabled
                                style="outline: none; border: none">
                                Lanjut ke Pembayaran
                            </button>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="estimasi">
            </form>

        </div>
    </section>
    <!--================End Checkout Area =================-->
@endsection

@section('style')
    <style>
        .pilih-alamat:checked~.card {
            box-shadow: 0 0 0 2px orange;
        }

        .pilih-pengiriman:checked~.li {
            box-shadow: 0 0 0 2px orange;
        }
    </style>
@endsection

@section('script')
    <script>
        document.getElementById('cek-ongkir').addEventListener('click', function() {
            document.querySelector('.pilih-layanan').classList.add('d-none');
            $.ajax({
                url: '/checkout/get-ongkir',
                method: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    courier: document.querySelector('[name="courier"]:checked').value,
                    daftar_alamat_id: document.querySelector('[name="daftar_alamat_id"]:checked').value,
                },
                success: function({
                    results
                }) {
                    const data = results[0];
                    let html = "";
                    data.costs.forEach(cost => {
                        html += `
                            <li class="list-group-item">
                                <label for="layanan-${cost.service}" class="d-flex justify-content-between" onclick='changeOngkirText(${JSON.stringify(cost)})'>
                                    <div>
                                        <input type="radio" id="layanan-${cost.service}" name="ongkir" value="${cost.cost[0].value}" class="pilih-pengiriman">
                                        <div>
                                            <span>${cost.service} (${cost.description})</span> <strong>${cost.cost[0].etd.replace(' HARI', '')} Hari</strong>
                                        </div>
                                    </div>
                                    <strong style="font-size: 1.5rem">Rp ${cost.cost[0].value}</strong>
                                </label>
                            </li>
                        `;
                    });
                    document.querySelector('.pilih-layanan').classList.remove('d-none');
                    document.querySelector('.pilih-layanan').innerHTML = html;
                }
            })
        });

        function changeOngkirText(cost) {
            document.querySelector('.ongkir-text').innerText = `Rp. ${new Intl.NumberFormat('id-ID').format(cost.cost[0]
                .value)}`;
            var ongkir = parseInt({{ $total }}) +
                cost.cost[0].value;
            document.querySelector('.total-text').innerText = `Rp. ${new Intl.NumberFormat('id-ID').format(ongkir)}`;
            document.querySelector('#submit').disabled = false;

            document.querySelector('input[name="service"]').value = `${cost.service} (${cost.description})`;
            document.querySelector('input[name="estimasi"]').value = cost.cost[0].etd;
        }
    </script>
@endsection
