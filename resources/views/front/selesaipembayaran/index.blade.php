@extends('front.layouts.main')


@section('container')
    <section class="banner-area organic-breadcrumb">
        <div class="container">
            <div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
                <div class="col-first">
                    <h1>Konfirmasi</h1>
                    <nav class="d-flex align-items-center">
                        <a href="index.html">Home<span class="lnr lnr-arrow-right"></span></a>
                        <a href="category.html">Konfirmasi</a>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    <!-- End Banner Area -->

    <!--================Order Details Area =================-->
    <section class="order_details section_gap">
        <div class="container">
            <h3 class="title_confirmation">Terima Kasih telah membeli Produk kami. Pesanan Anda telah diterima.
            </h3>
            <div class="row order_d_inner">
                <div class="col-lg-4">
                    <div class="details_item">
                        <h4>Informasi Pesanan</h4>
                        <ul class="list">
                            <li><a href="#"><span>No Invoice</span> : {{ $pembayaran->no_invoice }}</a></li>
                            <li><a href="#"><span>No Pesanan</span> : {{ $pembayaran->no_pesanan }}</a></li>
                            <li><a href="#"><span>Date</span> : {{ $pembayaran->created_at }}</a></li>
                            <li><a href="#"><span>Total</span> : Rp. {{ $pembayaran->total_harga }}</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="details_item">
                        <h4>Alamat Pengiriman</h4>

                        <ul class="list">
                            <li><a href="#"><span>Street</span> : {{ $pengiriman->alamat }}</a></li>
                            <li><a href="#"><span>City</span> : {{ $pengiriman->kota->nm_kota }}</a></li>
                            <li><a href="#"><span>Country</span> : {{ $pengiriman->provinsi->nm_provinsi }}</a></li>
                            <li><a href="#"><span>Postcode </span> : {{ $pengiriman->kode_pos }}</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="details_item">
                        <h4>Informasi Pengiriman</h4>
                        <ul class="list">
                            <li><a href="#"><span>Paket Layanan</span> :
                                    {{ $pembayaran->pengiriman->paket_layanan }}</a></li>
                            <li><a href="#"><span>Nama Ekspedisi</span> :
                                    {{ $pembayaran->pengiriman->nm_ekspedisi }}</a></li>
                            <li><a href="#"><span>Estimasi</span> :
                                    {{ $pembayaran->pengiriman->estimasi }} Hari</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="order_details_table">
                <h2>Detail Pesanan</h2>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">Produk</th>
                                <th scope="col">Kuantitas</th>
                                <th scope="col">Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($pembayaran->Pesanan as $item)
                                <tr>
                                    <td>
                                        <p>{{ $item->Produk->nm_produk }}</p>
                                    </td>
                                    <td>
                                        <h5>x {{ $item->kuantitas }}</h5>
                                    </td>
                                    <td>
                                        <h5> Rp. {{ number_format($item->sub_total, 0, ',', '.') }}</h5>
                                    </td>
                                </tr>
                            @endforeach

                            <tr>
                                <td>
                                    <h4>Ongkir</h4>
                                </td>
                                <td>
                                    <h5></h5>
                                </td>
                                <td>
                                    <p> Rp. {{ number_format($pengiriman->ongkir, 0, ',', '.') }}</p>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <h4>Total</h4>
                                </td>
                                <td>
                                    <h5></h5>
                                </td>
                                <td>
                                    <p> Rp. {{ number_format($pembayaran->total_harga, 0, ',', '.') }}</p>
                                </td>
                            </tr>
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </section>
@endsection
