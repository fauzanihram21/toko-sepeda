@extends('front.layouts.main')


@section('container')
    <!-- End Header Area -->

    <!-- Start Banner Area -->
    <section class="banner-area organic-breadcrumb">
        <div class="container">
            <div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
                <div class="col-first">
                    <h1>Riwayat Pesanan</h1>
                    <nav class="d-flex align-items-center">
                        <a href="/">Home<span class="lnr lnr-arrow-right"></span></a>
                        <div class="text-white">Riwayat Pesanan</div>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    <!-- End Banner Area -->

    <!--================Checkout Area =================-->

    <!-- End Banner Area -->


    <div class="container" style="margin-top: 60px; margin-bottom: 80px">
        <h3 style="margin-bottom: 40px">Pesanan Anda</h3>
        <div class="container">
            <div class="cart_inner">
                <h5>Pesanan Terkini</h5>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="text-center" :>No. Pesanan</th>
                                <th class="text-center">Tanggal</th>
                                <th class="text-center">Dikirim Ke</th>
                                <th class="text-center">Total Belanja</th>
                                <th class="text-center">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($pembayaran))
                                @foreach ($pembayaran as $value)
                                    <tr>
                                        <td class="text-center">
                                            <div class="media">
                                                <div class="d-flex">
                                                    {{-- <img src="img/cart.jpg" alt=""> --}}
                                                </div>
                                                <div class="media-body">
                                                    <p>{{ $value->no_pesanan }}</p>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <h5> {{ Carbon\Carbon::parse($value->created_at)->format('d-M-Y') }}</h5>
                                        </td>
                                        <td class="text-center">
                                            <h5>{{ $value->Pengiriman->nama_penerima }}</h5>
                                        </td>
                                        <td class="text-center">
                                            <h5>Rp. {{ number_format($value->total_harga, 0, ',', '.') }}</h5>
                                        </td>
                                        <td class="text-center">
                                            @if ($value->status == 'menunggu_pembayaran')
                                                <span class="badge badge-dark">Menunggu
                                                    Pembayaran</span>
                                            @elseif ($value->status == 'pesanan_diterima')
                                                <span class="badge badge-primary">Pesanan
                                                    Diterima</span>
                                            @elseif($value->status == 'diproses')
                                                <span class="badge badge-info">Pesanan
                                                    Diproses</span>
                                            @elseif($value->status == 'sedang_dikirim')
                                                <span class="badge badge-warning">Pesanan
                                                    DiKirim</span>
                                            @elseif($value->status == 'selesai')
                                                <span class="badge badge-success">
                                                    Selesai</span>
                                            @elseif($value->status == 'cancel')
                                                <span class="badge badge-danger">
                                                    Cancel</span>
                                            @endif
                                        </td>
                                        <td class="text-center"><a
                                                href="{{ url('riwayat/pembayaran/' . $value->id . '/detail') }}">Lihat
                                                order</a></td>
                                        @if ($value->status == 'sedang_dikirim')
                                            <td>
                                                <form
                                                    action="{{ url('/riwayat/pembayaran/' . $value->id . '/updatestatusselesai') }}"
                                                    method="POST">
                                                    @csrf
                                                    <input type="hidden" name="status" value="selesai">
                                                    <button class="btn btn-success" type="submit">Pesanan Diterima</button>
                                                </form>
                                            </td>
                                        @endif

                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="5" class="text-center">
                                        <h5>Belum ada Pesanan</h5>
                                    </td>
                                </tr>
                            @endif

                        </tbody>
                    </table>
                </div>
                <p class="small text-muted">
                    {!! __('Showing') !!}
                    <span class="fw-semibold">{{ $pembayaran->firstItem() }}</span>
                    {!! __('to') !!}
                    <span class="fw-semibold">{{ $pembayaran->lastItem() }}</span>
                    {!! __('of') !!}
                    <span class="fw-semibold">{{ $pembayaran->total() }}</span>
                    {!! __('results') !!}
                </p>
                <div class="pull-right">

                    {{ $pembayaran->links() }}
                </div>
            </div>
        </div>

    </div>


    <!--================Checkout Area =================-->

    <!--================End Checkout Area =================-->
    <!--================End Checkout Area =================-->
@endsection
