@extends('front.layouts.main')


@section('container')
    <!-- End Header Area -->
    <!-- Start Banner Area -->
    <section class="banner-area organic-breadcrumb">
        <div class="container">
            <div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
                <div class="col-first">
                    <h1>Detail Pesanan</h1>
                    <nav class="d-flex align-items-center">
                        <a href="/">Home<span class="lnr lnr-arrow-right"></span></a>
                        <div class="text-white">Detail Pesanan</div>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    <!-- End Banner Area -->

    <!--================Checkout Area =================-->

    <!-- End Banner Area -->


    <div class="container" style="margin-top: 60px; margin-bottom: 80px">
        <div class="billing_details">
            <div class="row">
                <div class="col-lg-8">
                    <h3>Pesanan #{{ $pembayaran->no_pesanan }}</h3>
                    <div>Order Status:
                        @if ($pembayaran->payment_status == 'belum_bayar')
                            <span class="badge badge-primary">Belum DiBayar</span>
                        @elseif ($pembayaran->payment_status == 'dibayar')
                            <span class="badge badge-success">Sudah DiBayar</span>
                        @elseif($pembayaran->payment_status == 'batal')
                            <span class="badge badge-danger">cancel</span>
                        @elseif($pembayaran->payment_status == 'kadaluarsa')
                            <span class="badge badge-warning">Kadaluarsa</span>
                        @endif
                    </div>
                    <br>
                    <div>Order Date: {{ Carbon\Carbon::parse($pembayaran->created_at)->format('d-M-Y') }}</div>
                    <br>
                    @if ($pembayaran->status == 'sedang_dikirim')
                        <div>No Resi : {{ $pembayaran->Pengiriman->no_resi }}</div>
                    @endif
                    @if ($pembayaran->status == 'selesai')
                        <button class="btn-sm btn btn-warning w-100 menu-link text-white justify-content-center cetak "
                            data-id="{{ $pembayaran->id }}">
                            <i class="fas fa-print"></i>
                            Cetak
                            Invoice</button>
                    @endif
                    <br>
                    <div>
                        @if ($pembayaran->status == 'menunggu_pembayaran')
                            <div class="d-flex">
                                {{-- <input type="hidden" name="payment_status" value="batal">
                                <input type="hidden" name="status" value="cancel"> --}}
                                <button class="btn btn-danger me-5 cancel" type="button">Cancel</button>
                                <div>
                                    <button type="button" class="btn btn-success" id="bayar">Bayar</button>

                                </div>
                            </div>
                        @endif

                    </div>

                    <br>
                    <br>
                    <h3>Detail Pesanan</h3>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th colspan="4">Produk</th>

                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($pembayaran->Pesanan as $value)
                                    <tr>
                                        <td colspan="4">
                                            <div class="media">
                                                <div class="d-flex">
                                                    <img width="200px"
                                                        src="{{ asset('storage/produk/' . $value->Produk->gambar) }}">
                                                </div>
                                                <div class="media-body">
                                                    <div>
                                                        <div class="ms-5">{{ $value->Produk->nm_produk }}</div>
                                                    </div>
                                                    @foreach ($value->Produk->ProdukVariasi as $item)
                                                        <div class="ms-5"> Warna: {{ $item->warna }} /
                                                            {{ $value->kuantitas }}
                                                        </div>
                                                    @endforeach
                                                    <div class="ms-5"> Berat: {{ $value->Produk->berat }} kg</div>


                                                </div>
                                                <div class="text-right">
                                                    <div></div>
                                                </div> Rp. {{ number_format($value->sub_total, 0, ',', '.') }}

                                            </div>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>

                                        </td>
                                        <td>

                                        </td>
                                        <td class="w-100">
                                            <h5>Subtotal</h5>
                                        </td>
                                        <td style="white-space: nowrap">
                                            <h5>Rp. {{ number_format($value->sub_total, 0, ',', '.') }}</h5>
                                        </td>
                                    </tr>
                                @endforeach

                                <tr>
                                    <td>

                                    </td>
                                    <td>

                                    </td>
                                    <td>
                                        <h5>Biaya Kirim</h5>
                                    </td>
                                    <td style="white-space: nowrap">
                                        <h5>Rp.
                                            {{ number_format($pembayaran->Pengiriman->ongkir, 0, ',', '.') }}
                                        </h5>
                                    </td>
                                </tr>

                                <tr>
                                    <td>

                                    </td>
                                    <td>

                                    </td>
                                    <td>
                                        <h5>Total</h5>
                                    </td>
                                    <td style="white-space: nowrap">
                                        <h5> Rp. {{ number_format($pembayaran->total_harga, 0, ',', '.') }}</h5>
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="order_box">
                        <h2>Alamat Pengiriman</h2>
                        <ul class="list list_2">

                            <li class="mb-3">
                                <div>Nama Penerima: {{ $pembayaran->Pengiriman->nama_penerima }} </div>
                                <div>Nomer Telpon: {{ $pembayaran->Pengiriman->no_telp }}</div>
                                <div>Provinsi: {{ $pembayaran->Pengiriman->Provinsi->nm_provinsi }}</div>
                                <div>Kota: {{ $pembayaran->Pengiriman->Kota->nm_kota }}</div>
                                <div>Kode Pos: {{ $pembayaran->Pengiriman->kode_pos }}</div>
                                <div>Alamat: {{ $pembayaran->Pengiriman->alamat }}</div>




                            </li>
                            <li class="mb-3">
                                <h6>Metode Pengiriman</h6>
                                <div>{{ $pembayaran->Pengiriman->nm_ekspedisi }} -
                                    {{ $pembayaran->Pengiriman->paket_layanan }}</div>
                                <div>Estimasi {{ $pembayaran->Pengiriman->estimasi }} Hari</div>
                            </li>
                            <li class="mb-3">
                                <h6>Catatan</h6>
                                <div>{{ $pembayaran->catatan }}</div>
                            </li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>
    </div>


    <!--================Checkout Area =================-->

    <!--================End Checkout Area =================-->
    <!--================End Checkout Area =================-->
@endsection

@section('script')
    <script src="https://app.sandbox.midtrans.com/snap/snap.js" data-client-key="{{ config('midtrans.client_key') }}">
    </script>
    @if ($pembayaran->status == 'menunggu_pembayaran')
        <script>
            $('#bayar').on('click', function(e) {
                e.preventDefault();
                snap.pay('{{ $pembayaran->snap_token }}', {
                    onSuccess: function(result) {
                        console.log(result);
                        window.location.href = '/selesai/pembayaran/' + result.order_id
                    }
                });
            })
        </script>
    @endif

    <script>
        $('.cancel').on('click', function() {
            var id = $(this).data('id')
            Swal.fire({
                title: 'Apakah Anda yakin ingin cancel?',
                showCancelButton: true,
                confirmButtonText: 'Ya, Cancel',
                showLoaderOnConfirm: true,
                preConfirm: () => {
                    return $.ajax({
                        url: "{{ url('/riwayat/pembayaran/' . $pembayaran->id . '/kembalistatuscancel') }}",
                        method: 'POST',
                        headers: {
                            'X-CSRF-TOKEN': '{{ @csrf_token() }}'
                        },
                        data: {
                            status: 'cancel',
                            payment_status: 'batal',


                        },
                        error: function() {
                            Swal.showValidationMessage('Gagal Cancel Data')
                        }
                    });
                }
            }).then(result => {
                if (result.isConfirmed) {
                    Swal.fire('Berhasil Cancel Data', '', 'success')
                    window.location.reload()
                }
            })
        });
    </script>
    <script>
        $('.cetak').on('click', function() {
            var id = $(this).data('id')
            Swal
                .fire({
                    title: "Apakah Anda Yakin?",
                    text: "Anda Akan Mendownlaod Report Berformat PDF, Mungkin Membutuhkan Waktu Beberapa Detik!",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#3085d6",
                    cancelButtonColor: "#d33",
                    confirmButtonText: "Download Sekarang!",
                    showLoaderOnConfirm: true,
                    preConfirm: function(login) {
                        return $.ajax({
                            url: "{{ url('/riwayatpembayaran/pdfselesai/') }}/" + id,
                            xhrFields: {
                                responseType: 'arraybuffer'
                            },
                            success: function(data, _, request) {
                                var blob = new Blob([data], {
                                    type: "application/pdf",
                                });

                                var link = document.createElement("a");
                                link.href = window.URL.createObjectURL(blob);
                                link.download = request.getResponseHeader('Content-Disposition')
                                    .split('filename="')[1]
                                    .split('"')[0];
                                link.click();

                                window.respon = {
                                    status: true,
                                    message: "Berhasil Download!"
                                };
                            },
                            error: function(error) {
                                window.respon = JSON.parse(
                                    String.fromCharCode.apply(
                                        null,
                                        new Uint8Array(error)
                                    )
                                );
                            }
                        })
                    },
                })
                .then(function(result) {
                    if (result.isConfirmed) {
                        if (window.respon.status) {
                            Swal.fire("Berhasil!", "File Berhasil Di Download.", "success");
                        } else {
                            Swal.fire("Error!", window.respon.message, "error");
                        }
                    }
                });
        })
    </script>
@endsection
