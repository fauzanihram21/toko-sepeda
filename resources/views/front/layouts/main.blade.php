<!DOCTYPE html>
<html lang="zxx" class="no-js">

<head>

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="{{ asset('storage/sd/LogoBmx.ico') }}" />


    <meta name="author" content="CodePixar">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>BMW | TOKO SEPEDA</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <link href="{{ asset('assets/fontawesome/css/all.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/cssbootstrap/linearicons.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/cssbootstrap/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/cssbootstrap/themify-icons.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/cssbootstrap/bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/cssbootstrap/owl.carousel.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/cssbootstrap/nice-select.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/cssbootstrap/nouislider.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/cssbootstrap/ion.rangeSlider.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/cssbootstrap/ion.rangeSlider.skinFlat.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/cssbootstrap/magnific-popup.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/cssbootstrap/main.css') }}" rel="stylesheet" type="text/css" />
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />

    @yield('style')

</head>

<body>

    @include('front.partials.navbar')

    @yield('container')

    @include('front.partials.footer')
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous">
    </script>
    <script src="{{ asset('assets/jsbootstrap/vendor/jquery-2.2.4.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"
        integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous">
    </script>
    <script src="{{ asset('assets/jsbootstrap/vendor/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/jsbootstrap/jquery.ajaxchimp.min.js') }}"></script>
    <script src="{{ asset('assets/jsbootstrap/jquery.nice-select.min.js') }}"></script>
    <script src="{{ asset('assets/jsbootstrap/jquery.sticky.js') }}"></script>
    <script src="{{ asset('assets/jsbootstrap/nouislider.min.js') }}"></script>
    <script src="{{ asset('assets/jsbootstrap/countdown.js') }}"></script>
    <script src="{{ asset('assets/jsbootstrap/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('assets/jsbootstrap/owl.carousel.min.js') }}"></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <!--gmaps Js-->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjCGmQ0Uq4exrzdcL6rvxywDDOvfAu6eE"></script>
    <script src="{{ asset('assets/jsbootstrap/gmaps.min.js') }}"></script>
    <script src="{{ asset('assets/jsbootstrap/main.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script>
        AOS.init();
    </script>

    <script>
        $('.logout').on('click', function() {
            var id = $(this).data('id')
            Swal.fire({
                title: 'Apakah Anda yakin ingin Logout?',
                showCancelButton: true,
                confirmButtonText: 'Ya, Logout',
                showLoaderOnConfirm: true,
                preConfirm: () => {
                    return $.ajax({
                        url: "{{ url('/logout') }}",
                        method: 'POST',
                        headers: {
                            'X-CSRF-TOKEN': '{{ @csrf_token() }}'
                        },
                        data: {},
                        error: function() {
                            Swal.showValidationMessage('Gagal Cancel Data')
                        }
                    });
                }
            }).then(result => {
                if (result.isConfirmed) {
                    Swal.fire('Berhasil Logout Data', '', 'success')
                    window.location.reload()
                }
            })
        });
    </script>
    @yield('script')
</body>

</html>
