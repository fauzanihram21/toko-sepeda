{{-- @dd($produkAll) --}}
@extends('front.layouts.main')


@section('container')
    <!-- End Header Area -->

    <!-- Start Banner Area -->
    <section class="banner-area organic-breadcrumb">
        <div class="container">
            <div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
                <div class="col-first">
                    <h1>{{ $kategorii->nm_kategori }}</h1>
                    <nav class="d-flex align-items-center">
                        <a href="/">Home<span class="lnr lnr-arrow-right"></span></a>
                        <div class="text-white">{{ $kategorii->nm_kategori }} <span class="lnr lnr-arrow-right"></span></div>
                        <div class="text-white">Kategori {{ $kategorii->nm_kategori }}</div>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    <!-- End Banner Area -->
    <div class="container">
        <marquee style="color: red" width="100%" direction="right" height="100px">
            "Ayo beli produk kami karena yang lain belum tentu berkualitas."
        </marquee>
        <div class="row">
            <div class="col-xl-3 col-lg-4 col-md-5">
                <div class="sidebar-categories">
                    <div class="head" style="background-color:orange">Kategori</div>
                    <ul class="main-categories">
                        @foreach ($subkategori as $sub)
                            <li
                                class="main-nav-list {{ Route::currentRouteName() == 'katalog.kategori.subkategori' && $uuid_sub_kategori == $sub->uuid ? 'active' : '' }}">
                                <a href="{{ url("/katalog/$kategorii->uuid/$sub->uuid") }}"
                                    class="{{ Route::currentRouteName() == 'katalog.kategori.subkategori' && $uuid_sub_kategori == $sub->uuid ? 'text-warning' : '' }}"
                                    aria-expanded="false" aria-controls="fruitsVegetable"><span
                                        class="lnr lnr-arrow-right"></span>{{ $sub->nm_sub_kategori }}<span
                                        class="number"></span></a>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <div class="sidebar-filter mt-50">
                    <div class="top-filter-head">Filter Produk</div>

                    <div class="common-filter">
                        <div class="head">Merk</div>

                        <ul>
                            @foreach ($merk as $value)
                                <li class="filter-list"><input class="pixel-radio select-merk" type="radio" id="merk"
                                        name="merk_id" value="{{ $value->id }}"
                                        {{ request()->merk_id == $value->id ? 'checked' : '' }}><label
                                        for="merk">{{ $value->nm_merk }}<span></span></label></li>
                            @endforeach

                        </ul>
                    </div>

                    <div class="common-filter">
                        <div class="head">Harga</div>
                        <form action="#">
                            <ul>
                                <li class="filter-list"><input class="pixel-radio select-harga" type="radio"
                                        id="harga_tertinggi" name="produk_id" value="tinggi"
                                        {{ request()->urut_harga == 'tinggi' ? 'checked' : '' }}>
                                    <label for="harga_tertinggi">Harga
                                        Tertinggi<span></span></label>

                                </li>
                                <li class="filter-list"><input class="pixel-radio select-harga" type="radio"
                                        id="harga_terendah" name="produk_id" value="rendah"
                                        {{ request()->urut_harga == 'rendah' ? 'checked' : '' }}><label
                                        for="harga_terendah">Harga
                                        Terendah<span></span></label></li>
                            </ul>
                        </form>
                    </div>
                </div>

            </div>
            <div class="col-xl-9 col-lg-8 col-md-7">
                <!-- Start Filter Bar -->
                <div class="filter-bar d-flex flex-wrap align-items-center">
                    <form class="form-inline" method="GET">
                        <input class="form-control mr-sm-2" type="text" name="keyword" placeholder="Cari"
                            aria-label="Cari" style="margin-top: 10px" value="{{ $keyword }}">
                    </form>
                    {{ $produk->links() }}
                </div>
                <!-- End Filter Bar -->
                <!-- Start Best Seller -->
                <section class="lattest-product-area pb-40 category-list">

                    <div class="row">
                        @foreach ($produkAll as $value)
                            <div class="col-lg-4 col-md-6">

                                <div class="single-product" style="position: relative">
                                    @if ($value->diskon)
                                        <div class="bg-warning fw-boldesr text-white"
                                            style="position: absolute; top: 0; right: 0; width: 48px; height: 48px; display: flex; justify-content: center; align-items: center; background-image: url(); background-size: contain; background-position: center">
                                            {{ $value->diskon }}%
                                        </div>
                                    @endif
                                    <a href="{{ url('/katalog/detail/' . $value->uuid) }}">
                                        <img alt="Foto" width="100"
                                            src="{{ asset('storage/produk/' . $value->gambar) }}" />
                                        <div class="product-details">
                                            <a class="text-black" style="font-size: 18px"
                                                href="{{ url('/katalog/detail/' . $value->uuid) }}">{{ $value->nm_produk }}</a>
                                            <div class="price">
                                                @if ($value->diskon)
                                                    <h6 class="l-through">Rp.
                                                        {{ number_format($value->harga, 0, ',', '.') }}</h6>
                                                    <h6 class="s_product_text" style="color: #ffc107">Rp.
                                                        {{ number_format($value->harga_diskon, 0, ',', '.') }}</h6>
                                                @else
                                                    <h6 class="s_product_text" style="color:#ffc107">Rp.
                                                        {{ number_format($value->harga, 0, ',', '.') }}</h6>
                                                @endif
                                            </div>
                                            <div class="prd-bottom">


                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        @endforeach

                    </div>
                </section>
                <!-- End Best Seller -->
                <!-- Start Filter Bar -->
                <div class="filter-bar d-flex flex-wrap align-items-center" style="margin-bottom: 60px">
                    {{ $produk->links() }}
                </div>
                <!-- End Filter Bar -->
            </div>
        </div>
    </div>

    <!-- Start related-product Area -->
@endsection

@section('script')
    <script>
        document.querySelectorAll('.select-warna').forEach(item => {
            item.addEventListener('change', event => {
                var warna = item.value;
                window.location.href = "?produk_variasi_id=" + warna;

            });
        })

        document.querySelectorAll('.select-merk').forEach(item => {
            item.addEventListener('change', event => {
                var merk = item.value;
                window.location.href = "?merk_id=" + merk;

            });
        })

        document.querySelectorAll('.select-harga').forEach(item => {
            item.addEventListener('change', event => {
                var produk = item.value;
                window.location.href = "?urut_harga=" + produk;

            });
        })
    </script>
@endsection
