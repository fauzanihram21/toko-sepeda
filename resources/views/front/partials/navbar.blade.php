<header class="header_area sticky-header">
    <div class="main_menu">
        <nav class="navbar navbar-expand-lg navbar-light main_box">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <a href="/"><img alt="Logo" width="100" height="80"
                        src="{{ asset('storage/logo_luar/' . $setting->logo_luar) }}" class="h-30px" /></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse offset" id="navbarSupportedContent">
                    <ul class="nav navbar-nav menu_nav ml-auto">
                        @foreach ($kategori as $value)
                            <li
                                class="nav-item {{ Route::currentRouteName() == 'katalog.kategori' && $uuid_kategori == $value->uuid ? 'active' : '' }}">
                                <a class="nav-link"
                                    href="{{ url("/katalog/$value->uuid") }}">{{ $value->nm_kategori }}</a>
                            </li>
                        @endforeach

                        <li class="nav-item {{ Request::is('kontak') ? 'active' : '' }}">
                            <a class="nav-link" href="/kontak">Kontak</a>
                        </li>

                        @auth
                            <li class="nav-item submenu dropdown">
                                <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button"
                                    aria-haspopup="true" aria-expanded="false">Welcome Back,
                                    {{ auth()->user()->name }}</a>
                                <ul class="dropdown-menu">
                                    @if (auth()->user()->is_admin == 0)
                                        <li class="nav-item"><a class="nav-link" href="/profile">My Profile</a>
                                        </li>
                                        <li class="nav-item"><a class="nav-link" href="/alamatsaya">Alamat</a>
                                        </li>
                                        <li class="nav-item"><a class="nav-link" href="/riwayat/pembayaran">Riwayat
                                                Pesanan</a>
                                        </li>
                                    @endif
                                    @can('admin')
                                        <li class="nav-item"><a class="nav-link" href="/dashboard">My Dashboard</a>
                                        </li>
                                    @endcan
                                    <li class="nav-item">
                                        <button type="submit"
                                            class="border-0 d-block nav-link text-left w-100 logout">Logout
                                        </button>
                                    </li>
                                </ul>
                            </li>
                        @endauth
                    </ul>
                    <ul class="nav navbar-nav navbar-right">

                        @guest
                            <li class="nav-item"><a href="/login"><i class="fa-solid fa-user"
                                        style="color: orange;"></i></a> </li>
                        @endguest

                        <li class="nav-item "><a href="/keranjang" class="position-relative">
                                <i class="fa-solid fa-cart-shopping " style="color: orange;"></i>

                                @auth
                                    <span
                                        class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger">
                                        {{ $keranjang->count() }}
                                        <span class="visually-hidden">unread messages</span>
                                    </span>
                                @endauth
                            </a>

                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
    <div class="search_input" id="search_input_box">
        <div class="container">
            <form class="d-flex justify-content-between">
                <input type="text" class="form-control" id="search_input" placeholder="Search Here">
                <button type="submit" class="btn"></button>
                <span class="lnr lnr-cross" id="close_search" title="Close Search"></span>
            </form>
        </div>
    </div>
</header>
