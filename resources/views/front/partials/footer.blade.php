<footer class="footer-area section_gap">
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <div class="single-footer-widget">
                    <img alt="Logo" width="200" height="150px"
                        src="{{ asset('storage/logo_luar/' . $setting->logo_luar) }}" class="h-30px" />
                    <ul>
                        <li>
                            <h4 style="color: white; margin-top:20px;">{{ $setting->name }}</h4>


                        <li>
                            <h4 style="color: white; margin-top:20px;">Alamat</h4>
                            <div>{{ $setting->alamat }}</div>
                        </li>
                        <li>
                            <h4 style="color: white; margin-top:20px;">Email</h4>
                            <div>{{ $setting->email }}</div>
                        </li>
                        <li>
                            <h4 style="color: white; margin-top:20px;">Telepon</h4>
                            <div>{{ $setting->no_telp }}</div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="single-footer-widget">
                    <h6>Kategori</h6>
                    <ul>
                        @foreach ($kategori as $value)
                            <li>
                                <a href="" style="color: #777777">{{ $value->nm_kategori }}</a>
                            </li>
                        @endforeach
                    </ul>

                </div>
            </div>
            <div class="col-lg-3">
                <div class="single-footer-widget">
                    <h6>Ikuti Kami</h6>
                    <div class="footer-social d-flex align-items-center">
                        <a href="http://www.facebook.com/fauzan.tawada.1"><i class="fa fa-facebook"></i></a>
                        <a href="http://www.twitter.com/fauzanihram21"><i class="fa fa-twitter"></i></a>
                        <a href="https://api.whatsapp.com/send?phone=6289620013761"><i class="fa fa-whatsapp"></i></a>

                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="single-footer-widget mail-chimp">
                    <h6 class="mb-20">Metode Pembayaran</h6>
                    <ul class="instafeed d-flex flex-wrap">
                        <li><img width="50px" height="40px" src="{{ asset('assets/media/bank/bca.jpg') }}"
                                alt=""></li>
                        <li><img width="50px" height="40px" src="{{ asset('assets/media/bank/bri.jpg') }}"
                                alt=""></li>
                        <li><img width="50px" height="40px" src="{{ asset('assets/media/bank/mandiri.jpg') }}"
                                alt=""></li>
                    </ul>
                </div>
            </div>

        </div>
        <div class="footer-bottom d-flex justify-content-center align-items-center flex-wrap"
            style="border-top:1px solid; margin-top:50px;">
            <p class="footer-text m-0">
                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                Copyright &copy;
                BMW 2023
                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            </p>
        </div>
    </div>
</footer>
