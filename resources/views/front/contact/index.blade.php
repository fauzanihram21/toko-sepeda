@extends('front.layouts.main')


@section('container')
    <!-- End Header Area -->

    <!-- Start Banner Area -->
    <section class="banner-area organic-breadcrumb">
        <div class="container">
            <div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
                <div class="col-first">
                    <h1>Kontak</h1>
                    <nav class="d-flex align-items-center">
                        <a href="/">Home<span class="lnr lnr-arrow-right"></span></a>
                        <div class="text-white">Kontak</div>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    <!-- End Banner Area -->

    <!--================Checkout Area =================-->

    <!-- End Banner Area -->

    <section class="contact_area section_gap_bottom">
        <div class="container">
            <div class="mapouter" style="margin-top: 100px">
                <div class="mapouter">
                    <div class="gmap_canvas"><iframe width="1200" height="400" id="gmap_canvas"
                            src="https://maps.google.com/maps?q=Fatikha%20Laundry%20Dry%20Clean%20Service&t=&z=17&ie=UTF8&iwloc=&output=embed"
                            frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a
                            href="https://123movies-to.org"></a><br>
                        <style>
                            .mapouter {
                                position: relative;
                                text-align: right;
                                height: 400px;
                                width: 1200px;
                            }
                        </style><a href="https://www.embedgooglemap.net">map widgets for websites</a>
                        <style>
                            .gmap_canvas {
                                overflow: hidden;
                                background: none !important;
                                height: 1000px;
                                width: 1100px;
                            }
                        </style>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top:60px">
                <div class="col-lg-3">
                    <div class="contact_info">
                        <div class="info_item">
                            <i class="lnr lnr-home"></i>
                            <h6>{{ $setting->alamat }}</h6>
                            <p></p>
                        </div>
                        <div class="info_item">
                            <i class="lnr lnr-phone-handset"></i>
                            <h6><a href="https://api.whatsapp.com/send?phone=6289620013761">{{ $setting->no_telp }}</a></h6>
                            <p></p>
                        </div>
                        <div class="info_item">
                            <i class="lnr lnr-envelope"></i>
                            <h6>{{ $setting->email }}</h6>
                            <p></p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9">
                </div>
            </div>
        </div>
    </section>

    <!--================Checkout Area =================-->

    <!--================End Checkout Area =================-->
    <!--================End Checkout Area =================-->
@endsection
