@extends('front.layouts.main')


@section('container')
    <section class="banner-area">
        <div class="container">
            <div class="row fullscreen align-items-center justify-content-start">
                <div class="col-lg-12">
                    <div class="active-banner-slider owl-carousel">
                        @foreach ($banner as $value)
                            <div class="row single-slide align-items-center d-flex">
                                <div class="col-lg-5 col-md-6">
                                    <div class="banner-content">
                                        <h1>{{ $value->heading }}</h1>
                                        <h4>{{ $value->sub_heading }}</h4>
                                        {{-- <div class="add-bag d-flex align-items-center">
                                            <a class="add-btn" href=""><span class="lnr lnr-cross"></span></a>
                                            <span class="add-text text-uppercase">Add to Bag</span>
                                        </div> --}}
                                    </div>
                                </div>
                                <div class="col-lg-7">
                                    <div class="banner-img">
                                        <img class="img-fluid" src="{{ asset('storage/banner/' . $value->gambar) }}">
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End banner Area -->

    <!-- start features Area -->
    <section class="features-area section_gap" data-aos="fade-up" data-aos-duration="3000">
        <div class="container">
            <div class="row features-inner">
                @foreach ($fitur as $value)
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="single-features">
                            <div class="f-icon">
                                <img class="img-fluid" src="{{ asset('storage/fitur/' . $value->gambar) }}">
                            </div>
                            <h6>{{ $value->heading }}</h6>
                            <p> {{ $value->sub_heading }}</p>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    <!-- end features Area -->

    <!-- Start category Area -->
    <section class="category-area">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8 col-md-12">
                    <div class="row">
                        <div class="col-lg-8 col-md-8">
                            <div class="single-deal">
                                <div class="overlay"></div>
                                <img class="img-fluid" style="height: 355px" src="{{ asset('storage/sd/fixie.jpg') }}"
                                    alt="">
                                <div href="{{ asset('storage/sd/fixie.jpg') }}" class="img-pop-up" target="_blank">
                                    <div class="deal-details">
                                        <h6 class="deal-title">Bike for Sports</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <div class="single-deal">
                                <div class="overlay"></div>
                                <img class="img-fluid " style="height: 355px"
                                    src="{{ asset('storage/sd/sepeda gunung.jpg') }}" alt="">
                                <div href="{{ asset('storage/sd/sepeda gunung.jpg') }}" class="img-pop-up" target="_blank">
                                    <div class="deal-details">
                                        <h6 class="deal-title">Bike for Sports</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <div class="single-deal">
                                <div class="overlay"></div>
                                <img class="img-fluid " style="height: 355px"
                                    src="{{ asset('storage/sd/sepedagunungg.jpg') }}" alt="">
                                <div href="{{ asset('storage/sd/sepedagunungg.jpg') }}" class="img-pop-up" target="_blank">
                                    <div class="deal-details">
                                        <h6 class="deal-title">Bike for Sports</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8 col-md-8">
                            <div class="single-deal">
                                <div class="overlay"></div>
                                <img class="img-fluid " style="height: 355px; width:500px"
                                    src="{{ asset('storage/sd/vintage bike.jpg') }}" alt="">
                                <div href="{{ asset('storage/sd/vintage bike.jpg') }}" class="img-pop-up" target="_blank">
                                    <div class="deal-details">
                                        <h6 class="deal-title">Bike for Sports</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="single-deal">
                        <div class="overlay"></div>
                        <img class="img-fluid " style="height: 740px; width:355px" src="{{ asset('storage/sd/bike.png') }}"
                            alt="">
                        <div href="{{ asset('storage/sd/bike.png') }}" class="img-pop-up" target="_blank">
                            <div class="deal-details">
                                <h6 class="deal-title">Bike for Sports</h6>
                            </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
    </section>
    <!-- End category Area -->

    <!-- start product Area -->
    <section class="owl-carousel active-product-area section_gap">
        <!-- single product slide -->
        <div class="single-product-slider">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-6 text-center">
                        <div class="section-title">
                            <h1>Produk Terbaru</h1>
                            <p>Kami hanya menyediakan produk berkualitas untuk kepuasan pelanggan</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!-- single product -->
                    @foreach ($produkSepeda as $value)
                        <div class="col-lg-3 col-md-6">
                            <div class="single-product" style="position: relative">



                                <a href="{{ url('/katalog/detail/' . $value->uuid) }}">
                                    <img alt="Foto" src="{{ asset('storage/produk/' . $value->gambar) }}" />
                                    @if ($value->diskon)
                                        <div class="bg-warning fw-boldesr text-white"
                                            style="position: absolute; top: 0; right: 0; width: 48px; height: 48px; display: flex; justify-content: center; align-items: center; background-image: url(); background-size: contain; background-position: center">
                                            {{ $value->diskon }}%
                                        </div>
                                    @endif
                                    <div class="product-details">
                                        <a class="text-black"
                                            href="{{ url('/katalog/detail/' . $value->uuid) }}">{{ $value->nm_produk }}
                                            </h6>
                                            <div class="price">
                                                @if ($value->diskon)
                                                    <h6 class="l-through">
                                                        Rp. {{ number_format($value->harga, 0, ',', '.') }} </h6>

                                                    <h6 style="color:#ffc107">Rp.
                                                        {{ number_format($value->harga_diskon, 0, ',', '.') }} </h6>
                                                @else
                                                    <h6>Rp. {{ number_format($value->harga, 0, ',', '.') }} </h6>
                                                @endif

                                            </div>
                                        </a>
                                        <div class="prd-bottom">
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="single-product-slider">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-6 text-center">
                        <div class="section-title">
                            <h1>Produk Terbaru</h1>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                                labore et
                                dolore
                                magna aliqua.</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!-- single product -->
                    @foreach ($produkAksesoris as $value)
                        <div class="col-lg-3 col-md-6">
                            <div class="single-product">


                                <a href="{{ url('/katalog/detail/' . $value->uuid) }}">
                                    <img alt="Foto" src="{{ asset('storage/produk/' . $value->gambar) }}" />
                                    @if ($value->diskon)
                                        <div class="bg-warning fw-boldesr text-white"
                                            style="position: absolute; top: 0; right: 0; width: 48px; height: 48px; display: flex; justify-content: center; align-items: center; background-image: url(); background-size: contain; background-position: center">
                                            {{ $value->diskon }}%
                                        </div>
                                    @endif
                                    <div class="product-details">
                                        <a class="text-black"
                                            href="{{ url('/katalog/detail/' . $value->uuid) }}">{{ $value->nm_produk }}
                                            </h6>
                                            <div class="price">
                                                @if ($value->diskon)
                                                    <h6 class="l-through">
                                                        Rp. {{ number_format($value->harga, 0, ',', '.') }} </h6>

                                                    <h6 style="color:#ffc107">Rp.
                                                        {{ number_format($value->harga_diskon, 0, ',', '.') }} </h6>
                                                @else
                                                    <h6>Rp. {{ number_format($value->harga, 0, ',', '.') }} </h6>
                                                @endif

                                            </div>
                                        </a>
                                        <div class="prd-bottom">
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="single-product-slider">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-6 text-center">
                        <div class="section-title">
                            <h1>Produk Terbaru</h1>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                                labore et
                                dolore
                                magna aliqua.</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!-- single product -->
                    @foreach ($produkApparel as $value)
                        <div class="col-lg-3 col-md-6">
                            <div class="single-product">

                                <a href="{{ url('/katalog/detail/' . $value->uuid) }}">
                                    <img alt="Foto" src="{{ asset('storage/produk/' . $value->gambar) }}" />
                                    @if ($value->diskon)
                                        <div class="bg-warning fw-boldesr text-white"
                                            style="position: absolute; top: 0; right: 0; width: 48px; height: 48px; display: flex; justify-content: center; align-items: center; background-image: url(); background-size: contain; background-position: center">
                                            {{ $value->diskon }}%
                                        </div>
                                    @endif
                                    <div class="product-details">
                                        <a class="text-black"
                                            href="{{ url('/katalog/detail/' . $value->uuid) }}">{{ $value->nm_produk }}
                                            </h6>
                                            <div class="price">
                                                @if ($value->diskon)
                                                    <h6 class="l-through">
                                                        Rp. {{ number_format($value->harga, 0, ',', '.') }} </h6>

                                                    <h6 style="color:#ffc107">Rp.
                                                        {{ number_format($value->harga_diskon, 0, ',', '.') }} </h6>
                                                @else
                                                    <h6>Rp. {{ number_format($value->harga, 0, ',', '.') }} </h6>
                                                @endif

                                            </div>
                                        </a>
                                        <div class="prd-bottom">
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="single-product-slider">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-6 text-center">
                        <div class="section-title">
                            <h1>Produk Terbaru</h1>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                                labore et
                                dolore
                                magna aliqua.</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!-- single product -->
                    @foreach ($produkParts as $value)
                        <div class="col-lg-3 col-md-6">
                            <div class="single-product">

                                <a href="{{ url('/katalog/detail/' . $value->uuid) }}">
                                    <img alt="Foto" src="{{ asset('storage/produk/' . $value->gambar) }}" />
                                    @if ($value->diskon)
                                        <div class="bg-warning fw-boldesr text-white"
                                            style="position: absolute; top: 0; right: 0; width: 48px; height: 48px; display: flex; justify-content: center; align-items: center; background-image: url(); background-size: contain; background-position: center">
                                            {{ $value->diskon }}%
                                        </div>
                                    @endif
                                    <div class="product-details">
                                        <a class="text-black"
                                            href="{{ url('/katalog/detail/' . $value->uuid) }}">{{ $value->nm_produk }}
                                            </h6>
                                            <div class="price">
                                                @if ($value->diskon)
                                                    <h6 class="l-through">
                                                        Rp. {{ number_format($value->harga, 0, ',', '.') }} </h6>

                                                    <h6 style="color:#ffc107">Rp.
                                                        {{ number_format($value->harga_diskon, 0, ',', '.') }} </h6>
                                                @else
                                                    <h6>Rp. {{ number_format($value->harga, 0, ',', '.') }} </h6>
                                                @endif

                                            </div>
                                        </a>
                                        <div class="prd-bottom">
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
    <!-- end product Area -->

    <!-- Start exclusive deal Area -->
    <section class="exclusive-deal-area">
        <div class="container-fluid">
            <div class="row justify-content-center align-items-center">
                <div class="col-lg-6 no-padding exclusive-left">
                    <div class="row clock_sec clockdiv" id="clockdiv">
                        <div class="col-lg-12">
                            <h1>Promo Akan Segera Berakhir!</h1>
                        </div>
                        <div class="col-lg-12">
                            <div class="row clock-wrap">
                                <div class="col clockinner1 clockinner">
                                    <h1 class="days">150</h1>
                                    <span class="smalltext">Days</span>
                                </div>
                                <div class="col clockinner clockinner1">
                                    <h1 class="hours">23</h1>
                                    <span class="smalltext">Hours</span>
                                </div>
                                <div class="col clockinner clockinner1">
                                    <h1 class="minutes">47</h1>
                                    <span class="smalltext">Mins</span>
                                </div>
                                <div class="col clockinner clockinner1">
                                    <h1 class="seconds">59</h1>
                                    <span class="smalltext">Secs</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 no-padding exclusive-right">
                    <div class="active-exclusive-product-slider">
                        <!-- single exclusive carousel -->
                        @foreach ($sepedadiskon as $value)
                            <div class="single-exclusive-slider">

                                <a href="{{ url('/katalog/detail/' . $value->uuid) }}">
                                    <img alt="Foto" src="{{ asset('storage/produk/' . $value->gambar) }}" />
                                    @if ($value->diskon)
                                        <div class="bg-warning fw-boldesr text-white"
                                            style="position: absolute; top: 0; right: 0; width: 48px; height: 48px; display: flex; justify-content: center; align-items: center; background-image: url(); background-size: contain; background-position: center">
                                            {{ $value->diskon }}%
                                        </div>
                                    @endif
                                    <div class="product-details">
                                        <a class="text-black"
                                            href="{{ url('/katalog/detail/' . $value->uuid) }}">{{ $value->nm_produk }}
                                            </h6>
                                            <h6>{{ $value->merk->nm_merk }}</h6>
                                            <div class="price">
                                                @if ($value->diskon)
                                                    <h6 class="l-through">
                                                        Rp. {{ number_format($value->harga, 0, ',', '.') }} </h6>

                                                    <h6 style="color:#ffc107">Rp.
                                                        {{ number_format($value->harga_diskon, 0, ',', '.') }} </h6>
                                                @else
                                                    <h6>Rp. {{ number_format($value->harga, 0, ',', '.') }} </h6>
                                                @endif

                                            </div>
                                        </a>
                                        <div class="prd-bottom">
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End exclusive deal Area -->

    <!-- Start brand Area -->
    <section class="brand-area section_gap">

        <div class="container">
            <div class="text-right" style="margin-bottom: 40px">
                <a href="/merk" class="">Lihat Semua Merk</a>
            </div>
            <div class="row">
                @foreach ($merk as $value)
                    <a class="col-lg-2 single-img" href="{{ url('/merk/' . $value->uuid) }}">
                        <img class="img-fluid  mx-auto" width="130px" height="230px"
                            src="{{ asset('storage/merk/' . $value->gambar) }}">
                    </a>
                @endforeach
            </div>
        </div>
    </section>
    <!-- End brand Area -->
@endsection
@section('script')
    @if (session('success'))
        <script>
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 2000,
                timerProgressBar: true,
                didOpen: (toast) => {
                    toast.addEventListener('mouseenter', Swal.stopTimer)
                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
            })

            Toast.fire({
                icon: 'success',
                title: '{{ session('success') }}'
            }).then((result) => {
                window.location.reload();
            })
        </script>
    @endif
    @if (session('logout'))
        <script>
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 2000,
                timerProgressBar: true,
                didOpen: (toast) => {
                    toast.addEventListener('mouseenter', Swal.stopTimer)
                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
            })

            Toast.fire({
                icon: 'success',
                title: '{{ session('logout') }}'
            }).then((result) => {
                window.location.reload();
            })
        </script>
    @endif
@endsection
