@extends('front.layouts.main')


@section('container')
    <style>
        .rating {
            margin-top: 40px;
            border: none;
            float: left;
        }

        .rating>label {
            color: #90A0A3;
            float: right;
        }

        .rating>label:before {
            margin: 5px;
            font-size: 2em;
            font-family: FontAwesome;
            content: "\f005";
            display: inline-block;
        }

        .rating>input {
            display: none;
        }

        .rating>input:checked~label,
        .rating:not(:checked)>label:hover,
        .rating:not(:checked)>label:hover~label {
            color: #F79426;
        }

        .rating>input:checked+label:hover,
        .rating>input:checked~label:hover,
        .rating>label:hover~input:checked~label,
        .rating>input:checked~label:hover~label {
            color: #FECE31;
        }
    </style>
    <section class="banner-area organic-breadcrumb">
        <div class="container-fluid">
            <div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
                <div class="col-first">
                    <h1>{{ $produk->nm_produk }}</h1>
                    <nav class="d-flex align-items-center">
                        <a href="/">Home<span class="lnr lnr-arrow-right"></span></a>
                        <a href="#">{{ $produk->SubKategori->Kategori->nm_kategori }}<span
                                class="lnr lnr-arrow-right"></span></a>
                        <a href="single-product.html">{{ $produk->SubKategori->nm_sub_kategori }}
                            <span class="lnr lnr-arrow-right"></span></a>
                        <a href="single-product.html">Detail {{ $produk->nm_produk }} </a>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    {{-- @if (session()->has('successkeranjang'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ session('successkeranjang') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif --}}

    </div>
    <!-- End Banner Area -->

    <!--================Single Product Area =================-->
    <div class="product_image_area">
        <div class="container">
            <div class="row s_product_inner">
                <div class="col-lg-6">
                    <div class="s_Product_carousel">
                        <div class="single-prd-item">
                            <img class="img-fluid" src="{{ asset('storage/produk/' . $produk->gambar) }}" alt="">
                        </div>
                        @foreach ($produk->ProdukVariasi as $var)
                            <div class="single-prd-item">
                                <img class="img-fluid" src="{{ asset('storage/produk_variasi/' . $var->gambar) }}"
                                    alt="">
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-lg-5 offset-lg-1">
                    <div class="s_product_text">
                        <h3>{{ $produk->nm_produk }}</h3>
                        @if ($produk->diskon > 0)
                            <h2>Rp.
                                {{ number_format($produk->harga_diskon, 0, ',', '.') }}
                            </h2>
                        @else
                            <h2>Rp.
                                {{ number_format($produk->harga, 0, ',', '.') }}

                            </h2>
                        @endif
                        <ul class="list">
                            <li><a class="active"
                                    href="#"><span>Kategori</span>{{ $produk->SubKategori->Kategori->nm_kategori }}</a>
                            </li>
                            <li><a class="active" href="#"><span>Merk</span>{{ $produk->merk->nm_merk }}</a>
                            </li>
                            <li><a class="active" href="#"><span>Berat</span>{{ $produk->berat }} kg</a>
                            </li>

                            <li><a class="active" href="#"><span>Terjual</span>{{ $produk->pesanan->count() }} </a>
                            </li>
                            <li>
                                <h6>Rating : {{ $banyakRating > 0 ? $totalRating / $banyakRating : 0 }} <svg
                                        style="color: yellow" width="24" height="24" viewBox="0 0 24 24"
                                        fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="M11.1359 4.48359C11.5216 3.82132 12.4784 3.82132 12.8641 4.48359L15.011 8.16962C15.1523 8.41222 15.3891 8.58425 15.6635 8.64367L19.8326 9.54646C20.5816 9.70867 20.8773 10.6186 20.3666 11.1901L17.5244 14.371C17.3374 14.5803 17.2469 14.8587 17.2752 15.138L17.7049 19.382C17.7821 20.1445 17.0081 20.7069 16.3067 20.3978L12.4032 18.6777C12.1463 18.5645 11.8537 18.5645 11.5968 18.6777L7.69326 20.3978C6.99192 20.7069 6.21789 20.1445 6.2951 19.382L6.7248 15.138C6.75308 14.8587 6.66264 14.5803 6.47558 14.371L3.63339 11.1901C3.12273 10.6186 3.41838 9.70867 4.16744 9.54646L8.3365 8.64367C8.61089 8.58425 8.84767 8.41222 8.98897 8.16962L11.1359 4.48359Z"
                                            fill="currentColor"></path>
                                    </svg></h6>



                            </li>
                        </ul>



                        <div class="card_area d-flex align-items-center mt-3">
                            <!-- Button trigger modal -->

                            <!-- Modal -->

                            <button type="button" class="primary-btn w-20 mt-4"
                                style="outline: none; border: none; margin-right: 10px" data-bs-toggle="modal"
                                data-bs-target="#exampleModal">
                                Pilih Varian
                            </button>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--================End Single Product Area =================-->
    <div class="modal fade " id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg ">
            <div class="modal-content ">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-6">
                            <div class="img-side">
                                <div class="img-side-content text-center">
                                    <img alt="Foto" width="220" class="img-variasi"
                                        src="{{ asset('storage/produk/' . $produk->gambar) }}" />
                                </div>
                            </div>
                        </div>

                        <div class="col-6">
                            <div class="img-text">
                                <h4 class="head">{{ $produk->nm_produk }}</h4>
                                <p class="sub-head">{{ $produk->SubKategori->nm_sub_kategori }}</p>
                                <p class="price">
                                    @if ($produk->diskon > 0)
                                        <h2 style="color: #ffc107">Rp.
                                            {{ number_format($produk->harga_diskon, 0, ',', '.') }}
                                        </h2>
                                    @else
                                        <h2 style="color: #ffc107">Rp.
                                            {{ number_format($produk->harga, 0, ',', '.') }}

                                        </h2>
                                    @endif
                                </p>

                                <p></p>

                                <p class="stok-variasi">
                                    Stock :
                                    <br>
                                </p>
                                <span class="badge-tersedia badge text-bg-success d-none">Stok Tersedia</span>
                                <span class="badge-habis badge text-bg-danger d-none">Stok Habis</span>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">

                            <div class="troli-varian-color change-variant-parent">
                                <div class="contentt">
                                    <h5 class="heading">Warna:</h5>
                                    <div class="contentt-color">
                                        @foreach ($produk->ProdukVariasi as $variasi)
                                            <div class="checkbox-content">
                                                <input type="radio" id="variasi-{{ $variasi->id }}" data-attr="4"
                                                    onchange="changeVariasi({{ $variasi->id }}, {{ $variasi->stock }})"
                                                    value="170" name="variasi_id"
                                                    class="input-check is-hidden change-variant">
                                                <label for="variasi-{{ $variasi->id }}"
                                                    class="label-checkbox-color variasi-{{ $variasi->id }}">
                                                    <span title="variasi-{{ $variasi->id }}"
                                                        class="custom-checkbox variasi-{{ $variasi->id }}">
                                                        <p title="variasi-{{ $variasi->id }}"
                                                            class="variasi-{{ $variasi->id }}"></p>
                                                        <span class="spam">{{ $variasi->warna }}</span>
                                                    </span>
                                                </label>
                                            </div>
                                        @endforeach

                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>


                </div>

                <hr>

                <form action="/keranjang" method="post" class="modal-body">
                    <div class="row">
                        <div class="col-6">
                            <h5>Jumlah</h5>
                        </div>

                        <div class="col-6">
                            <div class="product_count">
                                <input type="number" min="0" max="{{ $variasi->stock }}" name="kuantitas"
                                    id="sst" value="1" title="Quantity:" class="input-text kuantitas">

                                {{-- <button
                                        onclick="var result = document.getElementById('sst'); var sst = result.value; if( !isNaN( sst )) result.value++;return false;"
                                        class="increase items-count" type="button"><i
                                            class="lnr lnr-chevron-up"></i></button>
                                    <button
                                        onclick="var result = document.getElementById('sst'); var sst = result.value; if( !isNaN( sst ) &amp;&amp; sst > 0 ) result.value--;return false;"
                                        class="reduced items-count" type="button"><i
                                            class="lnr lnr-chevron-down"></i></button> --}}
                            </div>


                        </div>



                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Kembali</button>
                        <div>
                            @csrf
                            <input type="hidden" name="produk_id" value={{ $produk->id }}>
                            <input type="hidden" name="produk_variasi_id" id="produk_variasi_id">
                            <button type="submit" class="primary-btn w-20 "
                                style="outline: none; border: none; margin-right: 10px">Masukkan ke Keranjang</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!--================Product Description Area =================-->
    <section class="product_description_area">
        <div class="container">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link" id="deskripsi-tab" data-toggle="tab" href="#deskripsi" role="tab"
                        aria-controls="deskripsi" aria-selected="true">Deskripsi</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="spesifikasi-tab" data-toggle="tab" href="#spesifikasi" role="tab"
                        aria-controls="spesifikasi" aria-selected="false">Spesifikasi</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="komentar-tab" data-toggle="tab" href="#komentar" role="tab"
                        aria-controls="komentar" aria-selected="false">Komentar</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" id="ulasan-tab" data-toggle="tab" href="#ulasan" role="tab"
                        aria-controls="ulasan" aria-selected="false">Ulasan</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade" id="deskripsi" role="tabpanel" aria-labelledby="home-tab">
                    <p>{!! $produk->deskripsi !!}</p>
                </div>
                <div class="tab-pane fade" id="spesifikasi" role="tabpanel" aria-labelledby="profile-tab">
                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                @foreach ($produk->Spesifikasi as $spesifikasi)
                                    <tr>
                                        <td>
                                            <h5>{{ $spesifikasi->nm_spesifikasi }}</h5>
                                        </td>
                                        <td>
                                            <h5>{{ $spesifikasi->deskripsi }}</h5>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane fade" id="komentar" role="tabpanel" aria-labelledby="contact-tab">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="comment_list">
                                @foreach ($komentar as $i => $value)
                                    <div class="review_item">
                                        <div class="media">
                                            <div class="d-flex">
                                                <img width="100px"
                                                    src="{{ isset($value->user->avatar) ? asset('storage/avatar/' . $value->user->avatar) : asset('/assets/media/svg/avatars/blank.svg') }}"
                                                    class="rounded" id="avatar-preview" alt="image">
                                            </div>
                                            <div class="media-body">
                                                <h4>{{ $value->User->name }}</h4>
                                                <h5>{{ $value->created_at }}</h5>
                                                @auth
                                                    @if (auth()->user()->id == $value->user_id)
                                                        <button type="button" class="reply_btn hapus"
                                                            data-id="{{ $value->id }}">
                                                            Hapus</button>
                                                        <button type="button" data-bs-toggle="modal" data-bs-target="#reply"
                                                            class="reply_btn" data-id="{{ $value->id }}">
                                                            Balas</button>
                                                    @endif
                                                @endauth

                                            </div>
                                        </div>
                                        <p>{{ $value->komentar }}</p>
                                    </div>
                                    @foreach ($value->balaskomentar as $item)
                                        <div class="review_item reply">
                                            <div class="media">
                                                <div class="d-flex">
                                                    <img width="100px"
                                                        src="{{ isset($item->user->avatar) ? asset('storage/avatar/' . $item->user->avatar) : asset('/assets/media/svg/avatars/blank.svg') }}"
                                                        class="rounded" id="avatar-preview" alt="image">
                                                </div>
                                                <div class="media-body">
                                                    <h4>{{ $item->User->name }}</h4>
                                                    <h5>{{ $item->created_at }}</h5>
                                                    @auth
                                                        @if (auth()->user()->id == $item->user_id)
                                                            <button type="button" class="reply_btn delete"
                                                                data-id="{{ $item->id }}">
                                                                Hapus</button>
                                                        @endif
                                                    @endauth
                                                </div>
                                            </div>
                                            <p>{{ $item->komentar }}</p>
                                        </div>
                                    @endforeach
                                    @foreach ($value->balaskomentaradmin as $item)
                                        <div class="review_item reply">
                                            <div class="media">
                                                <div class="d-flex">
                                                    <img width="100px"
                                                        src="{{ isset($item->user->avatar) ? asset('storage/avatar/' . $item->user->avatar) : asset('/assets/media/svg/avatars/blank.svg') }}"
                                                        class="rounded" id="avatar-preview" alt="image">
                                                </div>
                                                <div class="media-body">
                                                    <h4>{{ $item->User->name }}</h4>
                                                    <h5>{{ $item->created_at }}</h5>
                                                    @auth
                                                        @if (auth()->user()->id == $item->user_id)
                                                            <button type="button" class="reply_btn delete"
                                                                data-id="{{ $item->id }}">
                                                                Hapus</button>
                                                        @endif
                                                    @endauth
                                                </div>
                                            </div>
                                            <p>{!! $item->komentar !!}</p>
                                        </div>
                                    @endforeach
                                    <div class="modal fade" id="reply" tabindex="-1"
                                        aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <form action="{{ url('balaskomentar/' . $value->uuid) }}"
                                                id="form-balaskomentar-{{ $value->id }}" method="POST">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Balas Komentar
                                                        </h5>
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                            aria-label="Close"></button>
                                                    </div>
                                                    <div class="modal-body">
                                                        @csrf
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <textarea class="form-control" readonly rows="1" placeholder="Komentar">{{ @$homekomentar->komentar }}</textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <textarea class="form-control" name="komentar" rows="1" placeholder="Komentar">{{ @$homekomentar->BalasKomentar[0]->komentar }}</textarea>
                                                            </div>
                                                        </div>


                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn secondary-btn"
                                                            data-bs-dismiss="modal">Kembali</button>
                                                        <button type="submit" class="btn primary-btn rounded-0"
                                                            form="form-balaskomentar-{{ $value->id }}">Submit
                                                            Now</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                @endforeach

                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="review_box">
                                <h4>Tulis Komentar Anda</h4>
                                <form class="row contact_form" action="{{ url('komentar/' . $produk->uuid) }}"
                                    method="post">
                                    @csrf
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <textarea class="form-control" name="komentar" rows="1" placeholder="Komentar">{{ isset($homekomentar) ? @$homekomentar->komentar : '' }}</textarea>
                                        </div>
                                    </div>

                                    <div class="col-md-12 text-right">
                                        <button type="submit" class="primary-btn w-20 mt-4" style="background: orange"
                                            style="outline: none; border: none; margin-right: 10px">
                                            Kirim
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade show active" id="ulasan" role="tabpanel" aria-labelledby="ulasan-tab">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="row total_rate">
                                <div class="col-6">
                                    <div class="box_total">
                                        <h5>Keseluruhan</h5>

                                        <h4>{{ $banyakRating > 0 ? $totalRating / $banyakRating : 0 }}</h4>
                                        <h6>({{ $banyakRating }})</h6>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="rating_list">
                                        <h3>Berdasarkan Ulasan</h3>
                                        <ul class="list">
                                            <li><a href="#">5 Star <i class="fa fa-star"></i><i
                                                        class="fa fa-star"></i><i class="fa fa-star"></i><i
                                                        class="fa fa-star"></i><i class="fa fa-star"></i></a>
                                                ({{ $rating5 }})</li>

                                            <li><a href="#">4 Star <i class="fa fa-star"></i><i
                                                        class="fa fa-star"></i><i class="fa fa-star"></i><i
                                                        class="fa fa-star"></i></a>
                                                ({{ $rating4 }})
                                            </li>
                                            <li>
                                                <a href="#">3 Star <i class="fa fa-star"></i><i
                                                        class="fa fa-star"></i><i class="fa fa-star"></i></a>
                                                ({{ $rating3 }})
                                            </li>
                                            <li>
                                                <a href="#">2 Star <i class="fa fa-star"></i><i
                                                        class="fa fa-star"></i></a>
                                                ({{ $rating2 }})
                                            </li>
                                            <li>
                                                <a href="#"> 1 Star <i class="fa fa-star"></i></a>
                                                ({{ $rating1 }})
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="review_list">
                                @foreach ($rating as $rtng)
                                    <div class="review_item">
                                        <div class="media">
                                            <div class="d-flex">
                                                <img width="100px"
                                                    src="{{ isset($rtng->user->avatar) ? asset('storage/avatar/' . $rtng->user->avatar) : asset('/assets/media/svg/avatars/blank.svg') }}"
                                                    class="rounded" id="avatar-preview" alt="image">
                                            </div>

                                            <div class="media-body">
                                                <h4>{{ $rtng->user->name }}</h4>
                                                <div class="row">
                                                    <div class="col-12">
                                                        <img width="100px" height="100px"
                                                            src="{{ asset('storage/produk_rating/' . $rtng->gambar) }}"
                                                            class="symbol-label">
                                                    </div>
                                                </div>
                                                @for ($i = 0; $i < $rtng->total_rating; $i++)
                                                    <svg style="color: yellow" width="24" height="24"
                                                        viewBox="0 0 24 24" fill="none"
                                                        xmlns="http://www.w3.org/2000/svg">
                                                        <path
                                                            d="M11.1359 4.48359C11.5216 3.82132 12.4784 3.82132 12.8641 4.48359L15.011 8.16962C15.1523 8.41222 15.3891 8.58425 15.6635 8.64367L19.8326 9.54646C20.5816 9.70867 20.8773 10.6186 20.3666 11.1901L17.5244 14.371C17.3374 14.5803 17.2469 14.8587 17.2752 15.138L17.7049 19.382C17.7821 20.1445 17.0081 20.7069 16.3067 20.3978L12.4032 18.6777C12.1463 18.5645 11.8537 18.5645 11.5968 18.6777L7.69326 20.3978C6.99192 20.7069 6.21789 20.1445 6.2951 19.382L6.7248 15.138C6.75308 14.8587 6.66264 14.5803 6.47558 14.371L3.63339 11.1901C3.12273 10.6186 3.41838 9.70867 4.16744 9.54646L8.3365 8.64367C8.61089 8.58425 8.84767 8.41222 8.98897 8.16962L11.1359 4.48359Z"
                                                            fill="currentColor"></path>
                                                    </svg>
                                                @endfor
                                                <br><br>
                                                @auth
                                                    @if (auth()->user()->id == $rtng->user_id)
                                                        <button type="button" class="reply_btn hps"
                                                            data-id="{{ $rtng->id }}">
                                                            Hapus</button>
                                                    @endauth
                                                @endif
                                            </div>
                                        </div>
                                        <p>{{ $rtng->komentar }}</p>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        @auth
                            @if ($sudah_beli)
                                <div class="col-lg-6">
                                    <div class="review_box">
                                        <h4>Tambah Ulasan</h4>
                                        <form class="row contact_form" action="{{ url('rating/' . $produk->uuid) }}"
                                            method="post" enctype="multipart/form-data">
                                            @csrf
                                            <div class="col-12 text-center">
                                                <label for="gambar" class="mb-4"
                                                    style="cursor: pointer; position: relative">

                                                    <input type="file" accept="image/*" class="d-none" id="gambar"
                                                        name="gambar" accept=".png, .jpg, .jpeg" onchange="previewImage()">
                                                    <span class="btn bg-white rounded-circle shadow"
                                                        style="position: absolute; top: -10px; right: -10px">
                                                        <i class="fa fa-pencil"></i>
                                                    </span>
                                                    <img width="100px"
                                                        src="{{ isset($rating->gambar) ? asset('storage/produk_rating/' . $rating->gambar) : asset('/assets/media/svg/avatars/blank.svg') }}"
                                                        class="rounded" id="gambar-preview">
                                                </label>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <div class="rating">
                                                        <input type="radio" id="star5" name="total_rating"
                                                            value="5" />
                                                        <label class="star" for="star5" title="Awesome"
                                                            aria-hidden="true"></label>
                                                        <input type="radio" id="star4" name="total_rating"
                                                            value="4" />
                                                        <label class="star" for="star4" title="Great"
                                                            aria-hidden="true"></label>
                                                        <input type="radio" id="star3" name="total_rating"
                                                            value="3" />
                                                        <label class="star" for="star3" title="Very good"
                                                            aria-hidden="true"></label>
                                                        <input type="radio" id="star2" name="total_rating"
                                                            value="2" />
                                                        <label class="star" for="star2" title="Good"
                                                            aria-hidden="true"></label>
                                                        <input type="radio" id="star1" name="total_rating"
                                                            value="1" />
                                                        <label class="star" for="star1" title="Bad"
                                                            aria-hidden="true"></label>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <textarea class="form-control" name="komentar" id="message" rows="1" placeholder="Komentar"></textarea></textarea>
                                                </div>
                                            </div>
                                            @auth
                                                <div class="col-md-12 text-right">
                                                    <button type="submit" style="background: orange" value="submit"
                                                        class="primary-btn">Kirim</button>
                                                </div>
                                            @endauth

                                        </form>
                                    </div>
                                </div>
                            @endif
                        @endauth
                    </div>
                </div>
            </div>
        </div>
    </section>

    {{-- <div class="modal fade" id="reply" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Reply Komentar</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="{{ url('balaskomentar/' . $komentar->uuid) }}">
                        <div class="col-md-12">
                            <div class="form-group">
                                <textarea class="form-control" name="komentar" rows="1" placeholder="Message"></textarea>
                            </div>
                        </div>


                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn secondary-btn" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn primary-btn rounded-0">Submit
                        Now</button>
                </div>
            </div>
        </div>
    </div> --}}
    <!--================End Product Description Area =================-->
    <!-- Start related-product Area -->
@endsection

@section('script')
    <script>
        function previewImage() {
            const imgInput = document.querySelector("#gambar");
            const imgPreview = document.querySelector("#gambar-preview");

            const reader = new FileReader();
            reader.readAsDataURL(imgInput.files[0]);

            reader.onload = function(ev) {
                imgPreview.src = ev.target.result;
            }
        }

        const produkVariasi = {!! json_encode($produk->ProdukVariasi->toArray()) !!};

        function changeVariasi(id, stock) {
            const imageVariasi = document.querySelector('img.img-variasi');
            const stokVariasi = document.querySelector('.stok-variasi');
            const idVariasi = document.querySelector('#produk_variasi_id');
            const selected = produkVariasi.find(variasi => variasi.id == id);
            imageVariasi.src = `/storage/produk_variasi/${selected.gambar}`;
            stokVariasi.innerText = `Stok : ${selected.stock}`;
            idVariasi.value = selected.id;

            document.querySelector('#sst').value = 0;
            document.querySelector('#sst').setAttribute('max', stock);

            if (selected.stock == 0) {
                document.querySelector('.badge-tersedia').classList.add('d-none');
                document.querySelector('.badge-habis').classList.remove('d-none');
            } else {
                document.querySelector('.badge-tersedia').classList.remove('d-none');
                document.querySelector('.badge-habis').classList.add('d-none');
            }
        }

        $('.hapus').on('click', function() {
            var id = $(this).data('id')
            Swal.fire({
                title: 'Apakah Anda yakin ingin menghapus?',
                showCancelButton: true,
                confirmButtonText: 'Ya, Hapus',
                showLoaderOnConfirm: true,
                preConfirm: () => {
                    return $.ajax({
                        url: '{{ url('/komentaruser/' . $produk->uuid) }}/' + id,
                        method: 'POST',
                        headers: {
                            'X-CSRF-TOKEN': '{{ @csrf_token() }}'
                        },
                        data: {
                            _method: 'DELETE'
                        },
                        error: function() {
                            Swal.showValidationMessage('Gagal Menghapus Data')
                        }
                    });
                }
            }).then(result => {
                if (result.isConfirmed) {
                    Swal.fire('Berhasil Menghapus Data', '', 'success')
                    window.location.reload()
                }
            })
        });

        $('.delete').on('click', function() {
            var id = $(this).data('id')
            Swal.fire({
                title: 'Apakah Anda yakin ingin menghapus?',
                showCancelButton: true,
                confirmButtonText: 'Ya, Hapus',
                showLoaderOnConfirm: true,
                preConfirm: () => {
                    return $.ajax({
                        url: '{{ url('/baleskomentaruser/') }}/' + id,
                        method: 'POST',
                        headers: {
                            'X-CSRF-TOKEN': '{{ @csrf_token() }}'
                        },
                        data: {
                            _method: 'DELETE'
                        },
                        error: function() {
                            Swal.showValidationMessage('Gagal Menghapus Data')
                        }
                    });
                }
            }).then(result => {
                if (result.isConfirmed) {
                    Swal.fire('Berhasil Menghapus Data', '', 'success')
                    window.location.reload()
                }
            })
        });

        $('.hps').on('click', function() {
            var id = $(this).data('id')
            Swal.fire({
                title: 'Apakah Anda yakin ingin menghapus?',
                showCancelButton: true,
                confirmButtonText: 'Ya, Hapus',
                showLoaderOnConfirm: true,
                preConfirm: () => {
                    return $.ajax({
                        url: '{{ url('/ratinguser/' . $produk->uuid) }}/' + id,
                        method: 'POST',
                        headers: {
                            'X-CSRF-TOKEN': '{{ @csrf_token() }}'
                        },
                        data: {
                            _method: 'DELETE'
                        },
                        error: function() {
                            Swal.showValidationMessage('Gagal Menghapus Data')
                        }
                    });
                }
            }).then(result => {
                if (result.isConfirmed) {
                    Swal.fire('Berhasil Menghapus Data', '', 'success')
                    window.location.reload()
                }
            })
        });
    </script>
    @if (session('successkeranjang'))
        <script>
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 2000,
                timerProgressBar: true,
                didOpen: (toast) => {
                    toast.addEventListener('mouseenter', Swal.stopTimer)
                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
            })

            Toast.fire({
                icon: 'success',
                title: '{{ session('successkeranjang') }}'
            }).then((result) => {
                window.location.reload();
            })
        </script>
    @endif
    @if (session('successcreate'))
        <script>
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 2000,
                timerProgressBar: true,
                didOpen: (toast) => {
                    toast.addEventListener('mouseenter', Swal.stopTimer)
                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
            })

            Toast.fire({
                icon: 'success',
                title: '{{ session('successcreate') }}'
            }).then((result) => {
                window.location.reload();
            })
        </script>
    @endif
@endsection
