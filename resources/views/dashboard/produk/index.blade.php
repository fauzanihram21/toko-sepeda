@extends('dashboard.layouts.main')



@section('container')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="toolbar" id="kt_toolbar">
            <div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
                <div data-kt-swapper="true" data-kt-swapper-mode="prepend"
                    data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}"
                    class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
                    <h1 class="d-flex align-items-center text-dark fw-bolder fs-3 my-1">Produk</h1>
                    <span class="h-20px border-gray-300 border-start mx-4"></span>

                </div>

                <div class="d-flex align-items-center gap-2 gap-lg-3">


                </div>
            </div>
        </div>

        <div id="kt_app_content" class="app-content flex-column-fluid" data-select2-id="select2-data-kt_app_content">
            <!--begin::Content container-->
            <div id="kt_app_content_container" class="app-container container-xxl"
                data-select2-id="select2-data-kt_app_content_container">
                {{-- @if (session()->has('successcreate'))
                    <div class="alert alert-dismissible bg-light-success d-flex flex-column flex-sm-row p-5 mb-10">
                        <div class="d-flex flex-column pe-0 pe-sm-10">
                            <h4 class="fw-semibold"> {{ session('successcreate') }} </h4>
                            <span>Berhasil Menambahkan Data!!</span>
                        </div>

                        <button type="button"
                            class="position-absolute position-sm-relative m-2 m-sm-0 top-0 end-0 btn btn-icon ms-sm-auto"
                            data-bs-dismiss="alert">
                            <span class="svg-icon svg-icon-1 svg-icon-primary">x</span>
                        </button>
                    </div>
                @endif

                @if (session()->has('successupdate'))
                    <div class="alert alert-dismissible bg-light-success d-flex flex-column flex-sm-row p-5 mb-10">
                        <div class="d-flex flex-column pe-0 pe-sm-10">
                            <h4 class="fw-semibold"> {{ session('successupdate') }} </h4>
                            <span>Berhasil Mengubah Data!!</span>
                        </div>

                        <button type="button"
                            class="position-absolute position-sm-relative m-2 m-sm-0 top-0 end-0 btn btn-icon ms-sm-auto"
                            data-bs-dismiss="alert">
                            <span class="svg-icon svg-icon-1 svg-icon-primary">x</span>
                        </button>
                    </div>
                @endif --}}
                <div class="card card-flush" data-select2-id="select2-data-125-r2m2">
                    <!--begin::Card header-->
                    <div class="card-header align-items-center py-5 gap-2 gap-md-5" data-select2-id="select2-data-124-ks67">
                        <!--begin::Card title-->
                        <div class="card-title">
                            <!--begin::Search-->
                            <form method="GET" action="{{ url('dashboard/produk/') }}">
                                <div class="d-flex align-items-center position-relative my-1">
                                    <span class="svg-icon svg-icon-1 position-absolute ms-4">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                            viewBox="0 0 24 24" fill="none">
                                            <rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546"
                                                height="2" rx="1" transform="rotate(45 17.0365 15.1223)"
                                                fill="black"></rect>
                                            <path
                                                d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                                                fill="black"></path>
                                        </svg>
                                    </span>
                                    <!--end::Svg Icon-->
                                    <input type="text" name="keyword" data-kt-ecommerce-category-filter="search"
                                        class="form-control form-control-solid w-250px ps-14" placeholder="Search Produk"
                                        value="{{ $keyword }}">
                                </div>
                            </form>
                            <!--end::Search-->
                        </div>
                        <!--end::Card title-->
                        <!--begin::Card toolbar-->
                        <div class="card-toolbar flex-row-fluid justify-content-end gap-5"
                            data-select2-id="select2-data-123-up6e">

                            <!--begin::Add product-->
                            <a href="{{ url('dashboard/produk/create') }}" class="btn btn-primary"><i
                                    class="fa fa-plus"></i> Add Produk</a>
                            <!--end::Add product-->
                        </div>
                        <!--end::Card toolbar-->
                    </div>
                    <!--end::Card header-->
                    <!--begin::Card body-->
                    <div class="card-body pt-0">
                        <!--begin::Table-->
                        <div id="kt_ecommerce_products_table_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                            <div class="table-responsive">
                                <table class="table align-middle table-row-dashed fs-6 gy-5 dataTable no-footer"
                                    id="kt_ecommerce_products_table">
                                    <!--begin::Table head-->
                                    <thead>
                                        <!--begin::Table row-->
                                        <tr class="text-start text-gray-400 fw-bold fs-7 text-uppercase gs-0">
                                            <th class="w-10px pe-2 sorting_disabled" rowspan="1" colspan="1"
                                                aria-label="
                                    
                                        
                                    
                                "
                                                style="width: 29.8906px;">
                                                <div
                                                    class="form-check form-check-sm form-check-custom form-check-solid me-3">
                                                    NO
                                                </div>
                                            </th>
                                            <th class="min-w-200px sorting" tabindex="0"
                                                aria-controls="kt_ecommerce_products_table" rowspan="1" colspan="1"
                                                aria-label="Product: activate to sort column ascending"
                                                style="width: 218.312px;">Product</th>
                                            <th class="text-end min-w-70px sorting text-center" tabindex="0"
                                                aria-controls="kt_ecommerce_products_table" rowspan="1" colspan="1"
                                                aria-label="HARGA: activate to sort column ascending"
                                                style="width: 109.578px;">MERK</th>
                                            <th class="text-end min-w-100px sorting text-center" tabindex="0"
                                                aria-controls="kt_ecommerce_products_table" rowspan="1" colspan="1"
                                                aria-label="Rating: activate to sort column ascending"
                                                style="width: 130.578px;">HARGA</th>
                                            <th class="text-end min-w-70px sorting_disabled text-center" rowspan="1"
                                                colspan="1" aria-label="Actions" style="width: 105.359px;">Actions
                                            </th>
                                        </tr>
                                        <!--end::Table row-->
                                    </thead>
                                    <!--end::Table head-->
                                    <!--begin::Table body-->
                                    <tbody class="fw-semibold text-gray-600">
                                        @foreach ($produk as $key => $value)
                                            <!--end::Table row-->
                                            <tr class="odd">
                                                <!--begin::Checkbox-->
                                                <td>
                                                    <div
                                                        class="form-check form-check-sm form-check-custom form-check-solid">
                                                        {{ $produk->firstItem() + $key }}

                                                    </div>
                                                </td>
                                                <!--end::Checkbox-->
                                                <!--begin::Category=-->
                                                <td>
                                                    <div class="d-flex align-items-center">
                                                        <!--begin::Thumbnail-->
                                                        <a href="/metronic8/demo1/../demo1/apps/ecommerce/catalog/edit-product.html"
                                                            class="symbol symbol-50px">

                                                            <img src="{{ asset('storage/produk/' . $value->gambar) }}"
                                                                class="symbol-label">
                                                        </a>
                                                        <!--end::Thumbnail-->
                                                        <div class="ms-5">
                                                            <!--begin::Title-->
                                                            <div class="text-gray-800 text-hover-primary fs-5 fw-bold"
                                                                data-kt-ecommerce-product-filter="product_name">
                                                                {{ $value->nm_produk }}</div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <!--end::Category=-->
                                                <!--begin::SKU=-->
                                                <td class="text-end pe-0 text-center">
                                                    <span class="fw-bold">{{ $value->merk->nm_merk }}</span>
                                                </td>
                                                <!--end::SKU=-->
                                                <!--begin::Qty=-->
                                                <td class="text-end pe-0 text-center">
                                                    <span class="fw-bold">
                                                        Rp. {{ number_format($value->harga, 0, ',', '.') }}
                                                    </span>
                                                </td>

                                                <!--end::Qty=-->
                                                <!--begin::Price=-->
                                                {{-- <td class="text-end pe-0" data-order="7">
                                    @if ($value->stock > 50)
                                    <span class="badge badge-light-warning">high stock stock</span>
                                    @elseif($value->stock > 30)
                                    <span class="badge badge-light-warning">middle stock stock</span>
                                    @else
                                    <span class="badge badge-light-danger">Low stock</span>
                                    @endif
                                    <span class="fw-bold text-warning ms-3">{{ $value->stock }}</span>
                                </td> --}}
                                                <!--end::Price=-->
                                                <!--begin::Rating-->
                                                {{-- <td class="text-end pe-0">
                                    <span class="fw-bold text-dark">{{ $value->harga }}</span>
                                </td> --}}

                                                <!--end::Rating-->
                                                <!--begin::Status=-->
                                                {{-- <td class="text-end pe-0" data-order="rating-4">
                                                    <div class="rating justify-content-end">

                                                        <div class="rating-label checked">
                                                            <!--begin::Svg Icon | path: icons/duotune/general/gen029.svg-->
                                                            <span class="svg-icon svg-icon-2" style="white-space:nowrap;">
                                                                @for ($i = 0; $i < $value->rating; $i++)
                                                                    <svg width="24" height="24"
                                                                        viewBox="0 0 24 24" fill="none"
                                                                        xmlns="http://www.w3.org/2000/svg">
                                                                        <path
                                                                            d="M11.1359 4.48359C11.5216 3.82132 12.4784 3.82132 12.8641 4.48359L15.011 8.16962C15.1523 8.41222 15.3891 8.58425 15.6635 8.64367L19.8326 9.54646C20.5816 9.70867 20.8773 10.6186 20.3666 11.1901L17.5244 14.371C17.3374 14.5803 17.2469 14.8587 17.2752 15.138L17.7049 19.382C17.7821 20.1445 17.0081 20.7069 16.3067 20.3978L12.4032 18.6777C12.1463 18.5645 11.8537 18.5645 11.5968 18.6777L7.69326 20.3978C6.99192 20.7069 6.21789 20.1445 6.2951 19.382L6.7248 15.138C6.75308 14.8587 6.66264 14.5803 6.47558 14.371L3.63339 11.1901C3.12273 10.6186 3.41838 9.70867 4.16744 9.54646L8.3365 8.64367C8.61089 8.58425 8.84767 8.41222 8.98897 8.16962L11.1359 4.48359Z"
                                                                            fill="currentColor"></path>
                                                                    </svg>
                                                                @endfor
                                                            </span>
                                                            <!--end::Svg Icon-->
                                                        </div>
                                                    </div>
                                                </td> --}}

                                                <!--end::Status=-->
                                                <!--begin::Action=-->
                                                <td class="text-end text-center">
                                                    <a href="#"
                                                        class="btn btn-sm btn-light btn-active-light-primary"
                                                        data-kt-menu-trigger="click"
                                                        data-kt-menu-placement="bottom-end">Actions
                                                        <!--begin::Svg Icon | path: icons/duotune/arrows/arr072.svg-->
                                                        <span class="svg-icon svg-icon-5 m-0">
                                                            <svg width="24" height="24" viewBox="0 0 24 24"
                                                                fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                    d="M11.4343 12.7344L7.25 8.55005C6.83579 8.13583 6.16421 8.13584 5.75 8.55005C5.33579 8.96426 5.33579 9.63583 5.75 10.05L11.2929 15.5929C11.6834 15.9835 12.3166 15.9835 12.7071 15.5929L18.25 10.05C18.6642 9.63584 18.6642 8.96426 18.25 8.55005C17.8358 8.13584 17.1642 8.13584 16.75 8.55005L12.5657 12.7344C12.2533 13.0468 11.7467 13.0468 11.4343 12.7344Z"
                                                                    fill="currentColor"></path>
                                                            </svg>
                                                        </span>
                                                        <!--end::Svg Icon-->
                                                    </a>
                                                    <!--begin::Menu-->
                                                    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-semibold fs-7 w-125px py-4"
                                                        data-kt-menu="true">
                                                        <!--begin::Menu item-->
                                                        <div class="menu-item px-3">
                                                            <a href="{{ url('dashboard/produk/' . $value->id . '/edit') }}"
                                                                class="btn btn-primary menu-link px-3 text-white w-100 justify-content-center"><i
                                                                    class="fas fa-pencil-alt"></i> Edit</a>

                                                        </div>
                                                        <!--end::Menu item-->
                                                        <!--begin::Menu item-->
                                                        <div class="menu-item px-3">
                                                            <button type="submit"
                                                                class="btn btn-danger menu-link px-3 text-white hapus w-100 justify-content-center"
                                                                data-id="{{ $value->id }}"><i class="fas fa-trash"></i>
                                                                Delete</button>
                                                        </div>
                                                        <!--end::Menu item-->
                                                    </div>
                                                    <!--end::Menu-->
                                                </td>
                                                <!--end::Action=-->
                                            </tr>
                                        @endforeach

                                    </tbody>
                                    <!--end::Table body-->
                                </table>
                            </div>
                            <p class="small text-muted">
                                {!! __('Showing') !!}
                                <span class="fw-semibold">{{ $produk->firstItem() }}</span>
                                {!! __('to') !!}
                                <span class="fw-semibold">{{ $produk->lastItem() }}</span>
                                {!! __('of') !!}
                                <span class="fw-semibold">{{ $produk->total() }}</span>
                                {!! __('results') !!}
                            </p>
                            <div class="pull-right">

                                {{ $produk->links() }}
                            </div>
                        </div>
                        <!--end::Card body-->
                    </div>
                    <!--end::Products-->
                </div>
                <!--end::Content container-->
            </div>
        @endsection

        @section('script')
            <script>
                $('table').on('click', '.hapus', function() {
                    var id = $(this).data('id')
                    Swal.fire({
                        title: 'Apakah Anda yakin ingin menghapus?',
                        showCancelButton: true,
                        confirmButtonText: 'Ya, Hapus',
                        showLoaderOnConfirm: true,
                        preConfirm: () => {
                            return $.ajax({
                                url: '{{ url('dashboard/produk') }}/' + id,
                                method: 'POST',
                                headers: {
                                    'X-CSRF-TOKEN': '{{ @csrf_token() }}'
                                },
                                data: {
                                    _method: 'DELETE'
                                },
                                error: function() {
                                    Swal.showValidationMessage('Gagal menghapus data')
                                }
                            });
                        }
                    }).then(result => {
                        if (result.isConfirmed) {
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 1000,
                                timerProgressBar: true,
                                didOpen: (toast) => {
                                    toast.addEventListener('mouseenter', Swal.stopTimer)
                                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                                }
                            })

                            Toast.fire({
                                icon: 'success',
                                title: 'Berhasil Menghapus Data'
                            }).then(() => {
                                window.location.href = "/dashboard/produk"
                            })
                            // Swal.fire('Berhasil Menghapus Data', '', 'success')
                        }
                    })
                });
            </script>
            @if (session('successcreate'))
                <script>
                    const Toast = Swal.mixin({
                        toast: true,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 2000,
                        timerProgressBar: true,
                        didOpen: (toast) => {
                            toast.addEventListener('mouseenter', Swal.stopTimer)
                            toast.addEventListener('mouseleave', Swal.resumeTimer)
                        }
                    })

                    Toast.fire({
                        icon: 'success',
                        title: '{{ session('successcreate') }}'
                    }).then((result) => {
                        window.location.reload();
                    })
                </script>
            @endif
            @if (session('successupdate'))
                <script>
                    const Toast = Swal.mixin({
                        toast: true,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 2000,
                        timerProgressBar: true,
                        didOpen: (toast) => {
                            toast.addEventListener('mouseenter', Swal.stopTimer)
                            toast.addEventListener('mouseleave', Swal.resumeTimer)
                        }
                    })

                    Toast.fire({
                        icon: 'success',
                        title: '{{ session('successupdate') }}'
                    }).then((result) => {
                        window.location.reload();
                    })
                </script>
            @endif
        @endsection
