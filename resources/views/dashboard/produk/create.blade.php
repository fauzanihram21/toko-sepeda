@extends('dashboard.layouts.main')

@section('container')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="toolbar" id="kt_toolbar">
            <div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
                <div data-kt-swapper="true" data-kt-swapper-mode="prepend"
                    data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}"
                    class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
                    <h1 class="d-flex align-items-center text-dark fw-bolder fs-3 my-1"> Produk</h1>
                    <span class="h-20px border-gray-300 border-start mx-4"></span>
                </div>

                <div class="d-flex align-items-center gap-2 gap-lg-3">


                </div>

            </div>

        </div>
        <div class="card card-custom card-create">
            <div class="card-header">
                <div class="card-title">
                    <h3 class="card-label">Tambah Produk</h3>
                </div>
            </div>
            <div class="card-body">
                <form action="{{ url('dashboard/produk') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-md-12">
                            <div class="mb-4 d-flex justify-content-center">
                                <div class="image-input image-input-empty" data-kt-image-input="true"
                                    style="background-image: url(/assets/media/svg/avatars/blank.svg)">
                                    <!--begin::Image preview wrapper-->
                                    <div class="image-input-wrapper w-125px h-125px"></div>
                                    <!--end::Image preview wrapper-->

                                    <!--begin::Edit button-->
                                    <label
                                        class="btn btn-icon btn-circle btn-color-muted btn-active-color-primary w-25px h-25px bg-body shadow"
                                        data-kt-image-input-action="change" data-bs-toggle="tooltip" data-bs-dismiss="click"
                                        title="Change avatar">
                                        <i class="bi bi-pencil-fill fs-7"></i>

                                        <!--begin::Inputs-->
                                        <input type="file" name="gambar" accept=".png, .jpg, .jpeg" />
                                        <input type="hidden" name="gambar_remove" />
                                        <!--end::Inputs-->
                                    </label>
                                    <!--end::Edit button-->

                                    <!--begin::Cancel button-->
                                    <span
                                        class="btn btn-icon btn-circle btn-color-muted btn-active-color-primary w-25px h-25px bg-body shadow"
                                        data-kt-image-input-action="cancel" data-bs-toggle="tooltip" data-bs-dismiss="click"
                                        title="Cancel avatar">
                                        <i class="bi bi-x fs-2"></i>
                                    </span>
                                    <!--end::Cancel button-->

                                    <!--begin::Remove button-->
                                    <span
                                        class="btn btn-icon btn-circle btn-color-muted btn-active-color-primary w-25px h-25px bg-body shadow"
                                        data-kt-image-input-action="remove" data-bs-toggle="tooltip" data-bs-dismiss="click"
                                        title="Remove avatar">
                                        <i class="bi bi-x fs-2"></i>
                                    </span>
                                    <!--end::Remove button-->
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="mb-4">
                                <label class="required form-label">Produk :</label>
                                <input type="text" placeholder="Produk" name="nm_produk" required="required"
                                    @error('nm_produk')is-invalid @enderror class="form-control">
                                @error('nm_produk')
                                    <div class="invalid-feedback d-block">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-4">
                                <label class="required form-label">Sub Kategori :</label>
                                <select class="form-select" data-control="select2" data-placeholder="Pilih Sub Kategori"
                                    name="sub_kategori_id">
                                    <option></option>
                                    @foreach ($subkategori as $value)
                                        <option value="{{ $value->id }}">{{ $value->nm_sub_kategori }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="mb-4">
                                <label class="required form-label">Merk :</label>
                                <select class="form-select" data-control="select2" data-placeholder="Pilih Merk"
                                    name="merk_id">
                                    <option></option>
                                    @foreach ($merk as $value)
                                        <option value="{{ $value->id }}">{{ $value->nm_merk }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label class="required form-label">Berat :</label>
                            <div class="input-group mb-4">
                                <input type="text" name="berat" required="required" class="form-control"
                                    placeholder="Berat" aria-label="Berat" aria-describedby="basic-addon2" />
                                <span class="input-group-text" id="basic-addon2">/Kg</span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="required form-label">Diskon :</label>
                            <div class="input-group mb-4">
                                <input type="number" name="diskon" class="form-control" placeholder="Diskon"
                                    aria-label="Diskon" aria-describedby="basic-addon2" />
                                <span class="input-group-text" id="basic-addon2">%</span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="required form-label">Harga :</label>
                            <div class="input-group mb-4">

                                <span class="input-group-text">Rp.</span>
                                <input type="text" placeholder="Harga" name="harga" required="required"
                                    oninput="this.value = this.value.rupiah()" @error('harga')is-invalid @enderror
                                    class="form-control">
                                @error('harga')
                                    <div class="invalid-feedback d-block">
                                        {{ $message }}
                                    </div>
                                @enderror
                                <span class="input-group-text">/</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="mb-4">

                                <label class="required form-label">Deskripsi :</label>
                                <textarea name="deskripsi" placeholder="Deskripsi" class="form-control" id="deskripsi">
                                 </textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-md-12">
                            <!--begin::Repeater-->
                            <div id="produkvariasi">
                                <!--begin::Form group-->
                                <div class="form-group">
                                    <div data-repeater-list="produkvariasi">
                                        <div data-repeater-item>
                                            <div class="form-group row">
                                                <div class="col-8">
                                                    <div class="row">
                                                        <div class="col-md-3 mb-3">
                                                            <div class="image-input image-input-empty"
                                                                data-kt-image-input="true"
                                                                style="background-image: url(/assets/media/svg/avatars/blank.svg)">
                                                                <!--begin::Image preview wrapper-->
                                                                <div class="image-input-wrapper w-125px h-125px"></div>
                                                                <!--end::Image preview wrapper-->

                                                                <!--begin::Edit button-->
                                                                <label
                                                                    class="btn btn-icon btn-circle btn-color-muted btn-active-color-primary w-25px h-25px bg-body shadow"
                                                                    data-kt-image-input-action="change"
                                                                    data-bs-toggle="tooltip" data-bs-dismiss="click"
                                                                    title="Change avatar">
                                                                    <i class="bi bi-pencil-fill fs-7"></i>

                                                                    <!--begin::Inputs-->
                                                                    <input type="file" name="gambar"
                                                                        accept=".png, .jpg, .jpeg" />
                                                                    <input type="hidden" name="gambar_remove" />
                                                                    <!--end::Inputs-->
                                                                </label>
                                                                <!--end::Edit button-->

                                                                <!--begin::Cancel button-->
                                                                <span
                                                                    class="btn btn-icon btn-circle btn-color-muted btn-active-color-primary w-25px h-25px bg-body shadow"
                                                                    data-kt-image-input-action="cancel"
                                                                    data-bs-toggle="tooltip" data-bs-dismiss="click"
                                                                    title="Cancel avatar">
                                                                    <i class="bi bi-x fs-2"></i>
                                                                </span>
                                                                <!--end::Cancel button-->

                                                                <!--begin::Remove button-->
                                                                <span
                                                                    class="btn btn-icon btn-circle btn-color-muted btn-active-color-primary w-25px h-25px bg-body shadow"
                                                                    data-kt-image-input-action="remove"
                                                                    data-bs-toggle="tooltip" data-bs-dismiss="click"
                                                                    title="Remove avatar">
                                                                    <i class="bi bi-x fs-2"></i>
                                                                </span>
                                                                <!--end::Remove button-->
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 mb-3">
                                                            <label class="form-label">Stock :</label>
                                                            <input type="number" name="stock"
                                                                class="form-control mb-2 mb-md-0" placeholder="Stock" />
                                                        </div>
                                                        <div class="col-md-3 mb-3">
                                                            <label class="form-label">Warna :</label>
                                                            <input type="text" name="warna"
                                                                class="form-control mb-2 mb-md-0" placeholder="Warna" />
                                                        </div>
                                                        <div class="col-md-3">
                                                            <a href="javascript:;" data-repeater-delete
                                                                class="btn btn-sm btn-light-danger mt-3 mt-md-8">
                                                                <i class="la la-trash-o"></i>Delete
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--end::Form group-->

                                <!--begin::Form group-->
                                <div class="form-group mt-5">
                                    <a href="javascript:;" data-repeater-create class="btn btn-light-primary">
                                        <i class="la la-plus"></i>Add
                                    </a>
                                </div>
                                <!--end::Form group-->
                            </div>

                            <div class="row mt-4">
                                <div class="col-md-12">
                                    <!--begin::Repeater-->
                                    <div id="spesifikasi">
                                        <!--begin::Form group-->
                                        <div class="form-group">
                                            <div data-repeater-list="spesifikasi">
                                                <div data-repeater-item>
                                                    <div class="form-group row">
                                                        <div class="col-8">
                                                            <div class="row">
                                                                <div class="col-md-5 mb-3">
                                                                    <label class="form-label">Spesifikasi :</label>
                                                                    <input type="text" name="nm_spesifikasi"
                                                                        class="form-control mb-2 mb-md-0"
                                                                        placeholder="Spesifikasi" />
                                                                </div>
                                                                <div class="col-md-5 mb-3">
                                                                    <label class="form-label">Deskripsi :</label>
                                                                    <textarea type="text" name="deskripsi" class="form-control mb-2 mb-md-0" placeholder="Deskripsi">
                                                    </textarea>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <a href="javascript:;" data-repeater-delete
                                                                        class="btn btn-sm btn-light-danger mt-3 mt-md-8">
                                                                        <i class="la la-trash-o"></i>Delete
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--end::Form group-->

                                        <!--begin::Form group-->
                                        <div class="form-group mt-5">
                                            <a href="javascript:;" data-repeater-create class="btn btn-light-primary">
                                                <i class="la la-plus"></i>Add
                                            </a>
                                        </div>
                                        <!--end::Form group-->
                                    </div>
                                    <!--end::Repeater-->
                                </div>

                            </div>
                            <div class="kt-form__actions mt-5">
                                <button type="submit" class="btn btn-primary btn-sm mb-2 me-2">
                                    <i class="las la-save fs-2 me-2"></i> SAVE</button>
                                <a href="/dashboard/produk/" type="button" class="btn btn-danger btn-sm mb-2 me-2">
                                    <i class="las la-ban fs-2 me-2"> </i> CANCEL
                                </a>
                            </div>
                        </div>
                </form>
            </div>
        </div>

    </div>
@endsection

@section('script')
    <script src="{{ asset('assets/plugins/custom/formrepeater/formrepeater.bundle.js') }}"></script>
    <script>
        $('#spesifikasi').repeater({
            initEmpty: false,

            show: function() {
                $(this).slideDown();
            },

            hide: function(deleteElement) {
                $(this).slideUp(deleteElement);
            }
        });

        $('#produkvariasi').repeater({
            initEmpty: false,

            show: function() {
                $(this).slideDown();
                const image = $(this).find('[data-kt-image-input="true"]')[0]
                new KTImageInput(image)
            },

            hide: function(deleteElement) {
                $(this).slideUp(deleteElement);
            }
        });

        ClassicEditor
            .create(document.querySelector('#deskripsi'))
    </script>
@endsection
