<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Laporan Produk</title>
    <style>
        body {
            font-family: 'Arial', sans-serif;
            font-size: 14px;
            color: #333;
            line-height: 1.4;
        }

        table {
            width: 100%;
        }

        table.main td {
            padding: 0.25rem 0.5rem;
            vertical-align: start;
        }

        .text-end {
            text-align: right;
        }
    </style>
</head>

<body>
    <table cellspacing="0" border="0" cellpadding="0">
        <tr>
            <td>
                <h1 style="justify-content: center">Laporan Produk</h1>
            </td>
        </tr>
    </table>

    {{-- <div>{{ $setting->nm_perusahaan }}</div> --}}






    <div>
        <div style="margin-bottom: 1rem">Daftar Produk:</div>
        <div>
            <table class="main" border="1" cellspacing="0">
                <thead>
                    <tr>
                        <th style="min-width: 100px">Tanggal</th>
                        <th style="min-width: 100px">Produk</th>
                        <th>Harga</th>
                        <th>Merk</th>
                        <th style="min-width: 100px">Warna</th>
                        <th style="min-width: 100px">Stock</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($produk as $item)
                        <tr>
                            <td>
                                <div>
                                    <span> {{ Carbon\Carbon::parse($item->created_at)->format('d-M-Y') }}</span>
                                </div>
                            </td>
                            <td class="text-end">
                                <span> {{ $item->nm_produk }} </span>
                            </td>
                            <td class="text-end">
                                @if ($item->diskon > 0)
                                    <span>Rp. {{ $item->harga_diskon }}</span>
                                @else
                                    <span>Rp. {{ $item->harga }}</span>
                                @endif
                            </td>
                            <td class="text-end">
                                <span>{{ $item->merk->nm_merk }}</span>
                            </td>
                            <td class="text-end">
                                @foreach ($item->ProdukVariasi as $var)
                                    <span>{{ $var->warna }},</span>
                                @endforeach
                            </td>
                            <td class="text-end">
                                @foreach ($item->ProdukVariasi as $var)
                                    <span>{{ $var->stock }},</span>
                                @endforeach
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</body>

</html>
