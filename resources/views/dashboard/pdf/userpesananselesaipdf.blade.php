<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Pesanan Selesai</title>
    <style>
        body {
            font-family: 'Arial', sans-serif;
            font-size: 14px;
            color: #333;
            line-height: 1.4;
        }

        table {
            width: 100%;
        }

        table.main td {
            padding: 0.25rem 0.5rem;
            vertical-align: start;
        }

        .text-end {
            text-align: right;
        }
    </style>
</head>

<body>
    <table cellspacing="0" border="0" cellpadding="0">
        <tr>
            <td>
                <h1 style="margin: 0 auto 3rem 0">{{ $pembayaran->user->name }}</h1>
            </td>
            <td>
                <h3 class="text-end"># {{ $pembayaran->no_invoice }}</h3>
            </td>
        </tr>
    </table>

    {{-- <div>{{ $setting->nm_perusahaan }}</div> --}}

    <div style="margin: 2rem 0">
        <div>Detail Pesanan</div>
        <div>Nomor: <strong>{{ $pembayaran->no_pesanan }}</strong></div>
    </div>

    <div style="margin-bottom: 2rem">
        <div>Dipesan Kepada:</div>
        <h2 style="margin: 0">Nama Penerima : {{ $pembayaran->Pengiriman->nama_penerima }}</h2>
        <h4 style="margin-top: 0; margin-bottom: 0.5rem">Nomer Telpon : {{ $pembayaran->Pengiriman->no_telp }}</h4>
        <p style="margin: 0">Alamat : {{ $pembayaran->Pengiriman->alamat }}</p>
        <p style="margin: 0"> {{ $pembayaran->Pengiriman->kota->nm_kota }},
            {{ $pembayaran->Pengiriman->provinsi->nm_provinsi }}</p>
        <p style="margin: 0">Estimasi : {{ $pembayaran->Pengiriman->estimasi }}</p>

    </div>
    <div style="margin-bottom: 3rem">
        <div>Total Tagihan:</div>
        <h2 style="margin-top: 0"> Rp. {{ number_format($pembayaran->total_harga, 0, ',', '.') }}
        </h2>

        {{-- <div>Metode Pembayaran: <strong>{{ $checkout->metode_pembayaran_text }}</strong></div> --}}
    </div>

    <div>
        <div style="margin-bottom: 1rem">Daftar Barang:</div>
        <div>
            <table class="main" border="1" cellspacing="0">
                <thead>
                    <tr>
                        <th style="min-width: 300px">Produk</th>
                        <th>Harga</th>
                        <th>Kuantitas</th>
                        <th>Subtotal</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($pembayaran->Pesanan as $item)
                        <tr>
                            <td>
                                <div>
                                    <strong>{{ $item->Produk->nm_produk }}</strong>
                                </div>
                                <div style="font-size: 0.75rem">{{ $item->Produk->SubKategori->kategori->nm_kategori }}
                                </div>
                            </td>
                            <td class="text-end">
                                <span> Rp. {{ number_format($item->sub_total / $item->kuantitas, 0, ',', '.') }}
                                    <span class="fw-normal text-gray-600 fs-6">/
                                        {{ $item->Produk->berat }} /kg</span></span>
                            </td>
                            <td class="text-end">
                                <span>{{ $item->kuantitas }} item</span>
                            </td>
                            <td>
                                <span>Rp. {{ number_format($item->sub_total, 0, ',', '.') }}</span>
                            </td>
                        </tr>
                    @endforeach
                    <tr>
                        <td colspan="3" class="text-end">Ongkir</td>
                        <td class="text-end">
                            <h3 style="margin: 0"> Rp.
                                {{ number_format($item->pembayaran->Pengiriman->ongkir, 0, ',', '.') }}
                            </h3>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" class="text-end">Total Belanja</td>
                        <td class="text-end">
                            <h3 style="margin: 0"> Rp. {{ number_format($item->pembayaran->total_harga, 0, ',', '.') }}
                            </h3>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</body>

</html>
