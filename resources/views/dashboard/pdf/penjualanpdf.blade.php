<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Laporan Penjualan</title>
    <style>
        body {
            font-family: 'Arial', sans-serif;
            font-size: 14px;
            color: #333;
            line-height: 1.4;
        }

        table {
            width: 100%;
        }

        table.main td {
            padding: 0.25rem 0.5rem;
            vertical-align: start;
        }

        .text-end {
            text-align: right;
        }

        .text-center {
            text-align: center;
        }
    </style>
</head>

<body>
    <table cellspacing="0" border="0" cellpadding="0">
        <tr>
            <td>
                <h1 class="text-center">Laporan Penjualan</h1>
            </td>
        </tr>
    </table>

    {{-- <div>{{ $setting->nm_perusahaan }}</div> --}}






    <div>
        <div style="margin-bottom: 1rem">Daftar Produk yang Terjual:</div>
        <div>
            <table class="main" border="1" cellspacing="0">
                <thead>
                    <tr>
                        <th style="min-width: 100px" rowspan="2">Tanggal</th>
                        <th style="min-width: 100px" rowspan="2">No. Pesanan</th>
                        <th style="min-width: 100px" colspan="4">Belanja</th>
                    <tr>
                        <th>Produk</th>
                        <th>Kuantitas</th>
                        <th>Harga</th>
                        <th>Subtotal</th>
                    </tr>
                    </tr>
                </thead>


                <tbody>
                    @foreach ($pembayaran as $item)
                        <tr>
                            <td>
                                <div>
                                    <span>{{ Carbon\Carbon::parse($item->created_at)->format('d-M-Y') }} </span>
                                </div>
                            </td>
                            <td class="text-end">
                                <span> {{ $item->no_pesanan }} </span>
                            </td>
                            <td>
                                @foreach ($item->Pesanan as $value)
                                    <div>
                                        <span>{{ $value->Produk->nm_produk }}</span>
                                    </div>
                                @endforeach
                            </td>
                            <td>
                                @foreach ($item->Pesanan as $value)
                                    <div>
                                        <span>{{ $value->kuantitas }}</span>
                                    </div>
                                @endforeach
                            </td>
                            <td>
                                @foreach ($item->Pesanan as $value)
                                    <div>
                                        @if ($value->Produk->diskon > 0)
                                            <span>Rp.

                                                {{ number_format($value->Produk->harga_diskon, 0, ',', '.') }}
                                            </span>
                                        @else
                                            <span>Rp.
                                                {{ number_format($value->Produk->harga, 0, ',', '.') }}
                                            </span>
                                        @endif
                                    </div>
                                @endforeach
                            </td>
                            <td>
                                @foreach ($item->Pesanan as $value)
                                    <div>
                                        <span>
                                            Rp.
                                            {{ number_format($value->sub_total, 0, ',', '.') }}
                                        </span>
                                    </div>
                                @endforeach
                            </td>
                        </tr>
                        <td></td>
                        <td></td>
                        <td colspan="4">

                            <div>

                                <span>Total Belanja
                                    Rp.
                                    {{ number_format($item->total_psn, 0, ',', '.') }}
                                </span>
                            </div>
                        </td>
                    @endforeach
                    <tr>
                        <td class="text-center" colspan="6">
                            <div>
                                <span>Total Pendapatan
                                    Rp.
                                    {{ number_format($totalPendapatan, 0, ',', '.') }}
                                </span>
                            </div>
                        </td>
                    </tr>

                    {{-- <tr>
                        <td colspan="3" class="text-end">Total Belanja</td>
                        <td class="text-end">
                            <h3 style="margin: 0">{{ $item->pembayaran->total_harga }}</h3>
                        </td>
                    </tr> --}}
                </tbody>
            </table>
        </div>
    </div>
</body>

</html>
