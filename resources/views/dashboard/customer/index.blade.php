@extends('dashboard.layouts.main')


@section('container')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="toolbar" id="kt_toolbar">
            <div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
                <div data-kt-swapper="true" data-kt-swapper-mode="prepend"
                    data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}"
                    class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
                    <h1 class="d-flex align-items-center text-dark fw-bolder fs-3 my-1">Customer</h1>
                    <span class="h-20px border-gray-300 border-start mx-4"></span>
                </div>

                <div class="d-flex align-items-center gap-2 gap-lg-3">


                </div>
            </div>
        </div>

        <div class="app-main flex-column flex-row-fluid" id="kt_app_main">
            <!--begin::Content wrapper-->
            <div class="d-flex flex-column flex-column-fluid">
                <!--begin::Toolbar-->

                <!--end::Toolbar-->
                <!--begin::Content-->
                <div id="kt_app_content" class="app-content flex-column-fluid">
                    <!--begin::Content container-->
                    <div id="kt_app_content_container" class="app-container container-xxl">
                        <!--begin::Card-->
                        <div class="card">
                            <!--begin::Card header-->
                            <div class="card-header border-0 pt-6">
                                <!--begin::Card title-->
                                <div class="card-title">
                                    <!--begin::Search-->
                                    <form method="GET" action="{{ url('dashboard/customer/') }}">
                                        <div class="d-flex align-items-center position-relative my-1">
                                            <span class="svg-icon svg-icon-1 position-absolute ms-4">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                    viewBox="0 0 24 24" fill="none">
                                                    <rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546"
                                                        height="2" rx="1" transform="rotate(45 17.0365 15.1223)"
                                                        fill="black"></rect>
                                                    <path
                                                        d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                                                        fill="black"></path>
                                                </svg>
                                            </span>
                                            <!--end::Svg Icon-->
                                            <input type="text" name="keyword" data-kt-ecommerce-category-filter="search"
                                                class="form-control form-control-solid w-250px ps-14"
                                                placeholder="Search Customer" value="{{ $keyword }}">
                                        </div>
                                    </form>
                                    <!--end::Search-->
                                </div>
                                <!--begin::Card title-->
                                <!--begin::Card toolbar-->
                                <div class="card-toolbar">
                                    <!--begin::Toolbar-->
                                    <div class="d-flex justify-content-end" data-kt-customer-table-toolbar="base">
                                        <!--begin::Filter-->

                                        <!--end::Filter-->
                                        <!--begin::Export-->

                                        <!--end::Export-->
                                        <!--begin::Add customer-->

                                        <!--end::Add customer-->
                                    </div>
                                    <!--end::Toolbar-->
                                    <!--begin::Group actions-->
                                    <div class="d-flex justify-content-end align-items-center d-none"
                                        data-kt-customer-table-toolbar="selected">
                                        <div class="fw-bold me-5">
                                            <span class="me-2"
                                                data-kt-customer-table-select="selected_count"></span>Selected
                                        </div>
                                        <button type="button" class="btn btn-danger"
                                            data-kt-customer-table-select="delete_selected">Delete Selected</button>
                                    </div>
                                    <!--end::Group actions-->
                                </div>
                                <!--end::Card toolbar-->
                            </div>
                            <!--end::Card header-->
                            <!--begin::Card body-->
                            <div class="card-body pt-0">
                                <!--begin::Table-->
                                <div id="kt_customers_table_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                                    <div class="table-responsive">
                                        <table class="table align-middle table-row-dashed fs-6 gy-5 dataTable no-footer"
                                            id="kt_customers_table">
                                            <!--begin::Table head-->
                                            <thead>
                                                <!--begin::Table row-->
                                                <tr class="text-start text-gray-400 fw-bold fs-7 text-uppercase gs-0">
                                                    <th class="w-10px pe-2 sorting_disabled" rowspan="1" colspan="1"
                                                        aria-label="
                                            
                                                
                                            
                                        "
                                                        style="width: 29.8906px;">
                                                        <div
                                                            class="form-check form-check-sm form-check-custom form-check-solid me-3">
                                                            NO
                                                        </div>
                                                    </th>
                                                    <th class="min-w-125px sorting text-center" tabindex="0"
                                                        aria-controls="kt_customers_table" rowspan="1" colspan="1"
                                                        aria-label="Customer Name: activate to sort column ascending"
                                                        style="width: 137.234px;">Name</th>
                                                    <th class="min-w-125px sorting text-center" tabindex="0"
                                                        aria-controls="kt_customers_table" rowspan="1" colspan="1"
                                                        aria-label="Email: activate to sort column ascending"
                                                        style="width: 170.25px;">USERNAME</th>
                                                    <th class="min-w-125px sorting text-center" tabindex="0"
                                                        aria-controls="kt_customers_table" rowspan="1" colspan="1"
                                                        aria-label="Email: activate to sort column ascending"
                                                        style="width: 170.25px;">Email</th>
                                                    <th class="min-w-125px sorting text-center" tabindex="0"
                                                        aria-controls="kt_customers_table" rowspan="1" colspan="1"
                                                        aria-label="Created Date: activate to sort column ascending"
                                                        style="width: 182.906px;">TANGGAL</th>
                                                    <th class="text-end min-w-70px sorting_disabled text-center"
                                                        rowspan="1" colspan="1" aria-label="Actions"
                                                        style="width: 105.75px;">
                                                        Actions</th>
                                                </tr>
                                                <!--end::Table row-->
                                            </thead>
                                            <!--end::Table head-->
                                            <!--begin::Table body-->
                                            <tbody class="fw-semibold text-gray-600">

                                                @foreach ($customer as $key => $value)
                                                    <tr class="odd">
                                                        <!--begin::Checkbox-->
                                                        <td>
                                                            <div
                                                                class="form-check form-check-sm form-check-custom form-check-solid">
                                                                {{ $customer->firstItem() + $key }}
                                                            </div>
                                                        </td>
                                                        <!--end::Checkbox-->
                                                        <!--begin::Name=-->
                                                        <td class="text-center">
                                                            <div class="text-gray-800 text-hover-primary mb-1">
                                                                {{ $value->name }}</div>
                                                        </td>
                                                        <!--end::Name=-->
                                                        <!--begin::Email=-->
                                                        <td class="text-center">{{ $value->username }}</td>
                                                        <td class="text-center">
                                                            <a href="#"
                                                                class="text-gray-600 text-hover-primary mb-1">{{ $value->email }}</a>
                                                        </td>
                                                        <!--end::Email=-->
                                                        <!--begin::Status=-->

                                                        <!--end::Status=-->
                                                        <!--begin::IP Address=-->

                                                        <!--end::IP Address=-->
                                                        <!--begin::Date=-->
                                                        <td class="text-center">
                                                            {{ Carbon\Carbon::parse($value->created_at)->format('d-M-Y') }}
                                                        </td>
                                                        <!--end::Date=-->
                                                        <!--begin::Action=-->
                                                        <td class="text-end text-center">
                                                            <a href="#"
                                                                class="btn btn-sm btn-light btn-active-light-primary"
                                                                data-kt-menu-trigger="click"
                                                                data-kt-menu-placement="bottom-end">Actions
                                                                <!--begin::Svg Icon | path: icons/duotune/arrows/arr072.svg-->
                                                                <span class="svg-icon svg-icon-5 m-0">
                                                                    <svg width="24" height="24"
                                                                        viewBox="0 0 24 24" fill="none"
                                                                        xmlns="http://www.w3.org/2000/svg">
                                                                        <path
                                                                            d="M11.4343 12.7344L7.25 8.55005C6.83579 8.13583 6.16421 8.13584 5.75 8.55005C5.33579 8.96426 5.33579 9.63583 5.75 10.05L11.2929 15.5929C11.6834 15.9835 12.3166 15.9835 12.7071 15.5929L18.25 10.05C18.6642 9.63584 18.6642 8.96426 18.25 8.55005C17.8358 8.13584 17.1642 8.13584 16.75 8.55005L12.5657 12.7344C12.2533 13.0468 11.7467 13.0468 11.4343 12.7344Z"
                                                                            fill="currentColor"></path>
                                                                    </svg>
                                                                </span>
                                                                <!--end::Svg Icon-->
                                                            </a>
                                                            <!--begin::Menu-->
                                                            <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-semibold fs-7 w-125px py-4"
                                                                data-kt-menu="true">
                                                                <!--begin::Menu item-->
                                                                <div class="menu-item px-3">
                                                                    <a href="{{ url('dashboard/customer/' . $value->id . '/detail') }}"
                                                                        class="btn btn-primary menu-link px-3 text-white w-100 justify-content-center">
                                                                        <i class="fas fa-eye"></i>
                                                                        Detail</a>
                                                                </div>
                                                                <!--end::Menu item-->
                                                                <!--begin::Menu item-->
                                                                <div class="menu-item px-3">
                                                                    <button type="submit"
                                                                        class="btn btn-danger menu-link px-3 text-white hapus w-100 justify-content-center"
                                                                        data-id="{{ $value->id }}"><i
                                                                            class="fas fa-trash"></i> Delete</button>
                                                                </div>
                                                                <!--end::Menu item-->
                                                            </div>
                                                            <!--end::Menu-->
                                                        </td>
                                                        <!--end::Action=-->
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                            <!--end::Table body-->
                                        </table>
                                        <p class="small text-muted">
                                            {!! __('Showing') !!}
                                            <span class="fw-semibold">{{ $customer->firstItem() }}</span>
                                            {!! __('to') !!}
                                            <span class="fw-semibold">{{ $customer->lastItem() }}</span>
                                            {!! __('of') !!}
                                            <span class="fw-semibold">{{ $customer->total() }}</span>
                                            {!! __('results') !!}
                                        </p>

                                        <div class="pull-right">

                                            {{ $customer->links() }}
                                        </div>

                                        <!--end::Card body-->
                                    </div>
                                    <!--end::Card-->
                                    <!--begin::Modals-->
                                    <!--begin::Modal - Customers - Add-->

                                    <!--end::Modal - Customers - Add-->
                                    <!--begin::Modal - Adjust Balance-->

                                    <!--end::Modal - New Card-->
                                    <!--end::Modals-->
                                </div>
                                <!--end::Content container-->
                            </div>
                            <!--end::Content-->
                        </div>
                        <!--end::Content wrapper-->
                        <!--begin::Footer-->

                        <!--end::Footer-->
                    </div>
                @endsection

                @section('script')
                    <script src="{{ asset('assets/plugins/global/plugins.bundle.js') }}"></script>
                    <script>
                        $("#kt_daterangepicker_1").daterangepicker();
                    </script>
                    <script>
                        $('table').on('click', '.hapus', function() {
                            var id = $(this).data('id')
                            Swal.fire({
                                title: 'Apakah Anda yakin ingin menghapus?',
                                showCancelButton: true,
                                confirmButtonText: 'Ya, Hapus',
                                showLoaderOnConfirm: true,
                                preConfirm: () => {
                                    return $.ajax({
                                        url: '{{ url('dashboard/customer') }}/' + id,
                                        method: 'POST',
                                        headers: {
                                            'X-CSRF-TOKEN': '{{ @csrf_token() }}'
                                        },
                                        data: {
                                            _method: 'DELETE'
                                        },
                                        error: function() {
                                            Swal.showValidationMessage('Gagal Menghapus Data')
                                        }
                                    });
                                }
                            }).then(result => {
                                if (result.isConfirmed) {
                                    const Toast = Swal.mixin({
                                        toast: true,
                                        position: 'top-end',
                                        showConfirmButton: false,
                                        timer: 1000,
                                        timerProgressBar: true,
                                        didOpen: (toast) => {
                                            toast.addEventListener('mouseenter', Swal.stopTimer)
                                            toast.addEventListener('mouseleave', Swal.resumeTimer)
                                        }
                                    })

                                    Toast.fire({
                                        icon: 'success',
                                        title: 'Hapus Berhasil'
                                    }).then(() => {
                                        window.location.reload()
                                    })
                                    // Swal.fire('Berhasil Menghapus Data', '', 'success')
                                }
                            })
                        });
                    </script>
                @endsection
