@extends('dashboard.layouts.main')


@section('container')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="toolbar" id="kt_toolbar">
            <div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
                <div data-kt-swapper="true" data-kt-swapper-mode="prepend"
                    data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}"
                    class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
                    <h1 class="d-flex align-items-center text-dark fw-bolder fs-3 my-1">Customer</h1>
                    <span class="h-20px border-gray-300 border-start mx-4"></span>
                </div>

                <div class="d-flex align-items-center gap-2 gap-lg-3">


                </div>
            </div>
        </div>
        <div id="kt_app_content" class="app-content flex-column-fluid">
            <!--begin::Content container-->
            <div id="kt_app_content_container" class="app-container container-xxl">
                <!--begin::Layout-->
                <div class="d-flex flex-column flex-xl-row">
                    <!--begin::Sidebar-->
                    <div class="flex-column flex-lg-row-auto w-100 w-xl-350px mb-10">
                        <!--begin::Card-->
                        <div class="card mb-5 mb-xl-8">
                            <!--begin::Card body-->
                            <div class="card-body pt-15">
                                <!--begin::Summary-->
                                <div class="d-flex flex-center flex-column mb-5">
                                    <!--begin::Avatar-->
                                    <div class="symbol symbol-150px symbol-circle mb-7">
                                        <img src="{{ isset($customer->avatar) ? asset($customer->avatar) : asset('/assets/media/svg/avatars/blank.svg') }}"
                                            alt="image">
                                    </div>
                                    <!--end::Avatar-->
                                    <!--begin::Name-->
                                    <div class="fs-3 text-gray-800 text-hover-primary fw-bold mb-1">{{ $customer->name }}
                                    </div>
                                    <!--end::Name-->
                                    <!--begin::Email-->
                                    <div class="fs-5 fw-semibold text-muted text-hover-primary mb-6">{{ $customer->email }}
                                    </div>
                                    <!--end::Email-->
                                </div>
                                <!--end::Summary-->
                                <!--begin::Details toggle-->
                                <div class="d-flex flex-stack fs-4 py-3">
                                    <div class="fw-bold">Details</div>
                                    <!--begin::Badge-->
                                    <div class="badge badge-primary d-inline">User</div>
                                    <!--begin::Badge-->
                                </div>
                                <!--end::Details toggle-->
                                <div class="separator separator-dashed my-3"></div>
                                <!--begin::Details content-->
                                <div class="pb-5 fs-6">
                                    <!--begin::Details item-->
                                    <div class="fw-bold mt-5">Account ID</div>
                                    <div class="text-gray-600">{{ $customer->id }}</div>
                                    <!--begin::Details item-->
                                    <!--begin::Details item-->
                                    <div class="fw-bold mt-5">Billing Email</div>
                                    <div class="text-gray-600">
                                        <a href="#"
                                            class="text-gray-600 text-hover-primary">{{ $customer->email }}</a>
                                    </div>
                                    <!--begin::Details item-->
                                    <!--begin::Details item-->
                                    <div class="fw-bold mt-5">Delivery Address</div>
                                    @foreach ($alamat as $value)
                                        <div class="text-gray-600">{{ $value->detail_alamat }}</div>
                                    @endforeach
                                    <!--begin::Details item-->
                                    <!--begin::Details item-->

                                    <!--begin::Details item-->
                                    <!--begin::Details item-->
                                    <!--begin::Details item-->
                                </div>
                                <!--end::Details content-->
                            </div>
                            <!--end::Card body-->
                        </div>
                        <!--end::Card-->
                    </div>
                    <!--end::Sidebar-->
                    <!--begin::Content-->
                    <div class="flex-lg-row-fluid ms-lg-15">
                        <!--begin:::Tabs-->
                        <ul class="nav nav-custom nav-tabs nav-line-tabs nav-line-tabs-2x border-0 fs-4 fw-semibold mb-8"
                            role="tablist">
                            <!--begin:::Tab item-->
                            <li class="nav-item" role="presentation">
                                <a class="nav-link text-active-primary pb-4 active" data-bs-toggle="tab"
                                    href="#kt_ecommerce_customer_overview" aria-selected="true" role="tab">Overview</a>
                            </li>
                            <!--end:::Tab item-->
                            <!--begin:::Tab item-->

                            <!--end:::Tab item-->
                        </ul>
                        <!--end:::Tabs-->
                        <!--begin:::Tab content-->
                        <div class="tab-content" id="myTabContent">
                            <!--begin:::Tab pane-->
                            <div class="tab-pane fade active show" id="kt_ecommerce_customer_overview" role="tabpanel">
                                <div class="card pt-4 mb-6 mb-xl-9">
                                    <!--begin::Card header-->

                                    <!--begin::Card-->
                                    <div class="card pt-4 mb-6 mb-xl-9">
                                        <!--begin::Card header-->
                                        <div class="card-header border-0">
                                            <!--begin::Card title-->
                                            <div class="card-title">
                                                <h2>Transaction History</h2>
                                            </div>
                                            <!--end::Card title-->
                                        </div>
                                        <!--end::Card header-->
                                        <!--begin::Card body-->
                                        <div class="card-body pt-0 pb-5">
                                            <!--begin::Table-->
                                            <div id="kt_table_customers_payment_wrapper"
                                                class="dataTables_wrapper dt-bootstrap4 no-footer">
                                                <div class="table-responsive">
                                                    <table
                                                        class="table align-middle table-row-dashed gy-5 dataTable no-footer"
                                                        id="kt_table_customers_payment">
                                                        <!--begin::Table head-->
                                                        <thead>
                                                            <!--begin::Table row-->
                                                            <tr
                                                                class="text-start text-gray-400 fw-bold fs-7 text-uppercase gs-0">
                                                                <th class="w-10px pe-2 sorting_disabled" rowspan="1"
                                                                    colspan="1"
                                                                    aria-label="
                                                            
                                                                
                                                            
                                                        "
                                                                    style="width: 29.8906px;">
                                                                    <div
                                                                        class="form-check form-check-sm form-check-custom form-check-solid me-3">
                                                                        No
                                                                    </div>
                                                                </th>
                                                                <th class="min-w-200px sorting text-center" tabindex="0"
                                                                    aria-controls="kt_ecommerce_products_table"
                                                                    rowspan="1" colspan="1"
                                                                    aria-label="NO. PESANAN: activate to sort column ascending"
                                                                    style="width: 218.359px;">NO. PESANAN</th>
                                                                <th class="text-end min-w-100px sorting text-center"
                                                                    tabindex="0"
                                                                    aria-controls="kt_ecommerce_products_table"
                                                                    rowspan="1" colspan="1"
                                                                    aria-label="PENERIMA: activate to sort column ascending"
                                                                    style="width: 109.609px;">PENERIMA</th>
                                                                <th class="text-end min-w-70px sorting text-center"
                                                                    tabindex="0"
                                                                    aria-controls="kt_ecommerce_products_table"
                                                                    rowspan="1" colspan="1"
                                                                    aria-label="ALAMAT: activate to sort column ascending"
                                                                    style="width: 98.6719px;">ALAMAT</th>
                                                                <th class="text-end min-w-100px sorting text-center"
                                                                    tabindex="0"
                                                                    aria-controls="kt_ecommerce_products_table"
                                                                    rowspan="1" colspan="1"
                                                                    aria-label="NO. INVOICE: activate to sort column ascending"
                                                                    style="width: 109.609px;">NO. INVOICE</th>
                                                                <th class="text-end min-w-100px sorting text-center"
                                                                    tabindex="0"
                                                                    aria-controls="kt_ecommerce_products_table"
                                                                    rowspan="1" colspan="1"
                                                                    aria-label="TOTAL HARGA: activate to sort column ascending"
                                                                    style="width: 109.609px;">TOTAL HARGA</th>

                                                            </tr>
                                                            <!--end::Table row-->
                                                        </thead>
                                                        <!--end::Table head-->
                                                        <!--begin::Table body-->
                                                        <tbody class="fw-semibold text-gray-600">

                                                            @foreach ($pembayaran as $key => $value)
                                                                <!--end::Table row-->
                                                                <tr class="odd">
                                                                    <!--begin::Checkbox-->
                                                                    <td>
                                                                        <div
                                                                            class="form-check form-check-sm form-check-custom form-check-solid">
                                                                            {{ $pembayaran->firstItem() + $key }}
                                                                        </div>
                                                                    </td>
                                                                    <!--end::Checkbox-->
                                                                    <!--begin::Category=-->
                                                                    <td>
                                                                        <div class="d-flex align-items-center">
                                                                            <!--begin::Thumbnail-->

                                                                            <!--end::Thumbnail-->
                                                                            <div class="text-center mx-auto">
                                                                                <!--begin::Title-->
                                                                                <div class="text-gray-800 text-hover-primary fs-5 fw-bold"
                                                                                    data-kt-ecommerce-product-filter="product_name">
                                                                                    {{ $value->no_pesanan }}</div>
                                                                                <!--end::Title-->
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                    <!--end::Category=-->
                                                                    <!--begin::SKU=-->
                                                                    <td class="text-end text-center">
                                                                        <span
                                                                            class="fw-bold">{{ $value->Pengiriman->nama_penerima }}</span>
                                                                    </td>
                                                                    <!--end::SKU=-->
                                                                    <!--begin::Qty=-->
                                                                    <td class="text-end text-center" data-order="10">
                                                                        <span
                                                                            class="fw-bold ms-3">{{ $value->Pengiriman->alamat }}</span>
                                                                    </td>
                                                                    <!--end::Qty=-->
                                                                    <!--begin::Price=-->
                                                                    <td class="text-end text-center">
                                                                        {{ $value->no_invoice }}</td>
                                                                    <!--end::Price=-->
                                                                    <!--begin::Rating-->
                                                                    <td class="text-end text-center">Rp.
                                                                        {{ number_format($value->total_harga, 0, ',', '.') }}
                                                                    </td>
                                                                    <!--begin::Status=-->

                                                                    <!--end::Status=-->
                                                                    <!--begin::Action=-->

                                                                    <!--end::Action=-->
                                                                </tr>
                                                            @endforeach

                                                        </tbody>
                                                        <!--end::Table body-->
                                                    </table>
                                                </div>
                                                <p class="small text-muted">
                                                    {!! __('Showing') !!}
                                                    <span class="fw-semibold">{{ $pembayaran->firstItem() }}</span>
                                                    {!! __('to') !!}
                                                    <span class="fw-semibold">{{ $pembayaran->lastItem() }}</span>
                                                    {!! __('of') !!}
                                                    <span class="fw-semibold">{{ $pembayaran->total() }}</span>
                                                    {!! __('results') !!}
                                                </p>
                                                <div class="pull-right">

                                                    {{ $pembayaran->links() }}
                                                </div>
                                            </div>
                                            <!--end::Table-->
                                        </div>
                                        <!--end::Card body-->
                                    </div>
                                    <!--end::Card-->
                                </div>
                                <!--end:::Tab pane-->
                                <!--begin:::Tab pane-->

                                <!--end:::Tab pane-->
                                <!--begin:::Tab pane-->

                                <!--end:::Tab pane-->
                            </div>
                            <!--end:::Tab content-->
                        </div>
                        <!--end::Content-->
                    </div>
                    <!--end::Layout-->
                    <!--begin::Modals-->
                    <!--begin::Modal - New Address-->


                </div>
                <!--end::Content container-->
            </div>
        @endsection

        @section('script')
            <script src="{{ asset('assets/plugins/global/plugins.bundle.js') }}"></script>
            <script>
                $("#kt_daterangepicker_1").daterangepicker();
            </script>
        @endsection
