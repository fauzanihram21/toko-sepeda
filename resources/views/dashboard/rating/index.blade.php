@extends('dashboard.layouts.main')


@section('container')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="toolbar" id="kt_toolbar">
            <div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
                <div data-kt-swapper="true" data-kt-swapper-mode="prepend"
                    data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}"
                    class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
                    <h1 class="d-flex align-items-center text-dark fw-bolder fs-3 my-1">Rating</h1>
                    <span class="h-20px border-gray-300 border-start mx-4"></span>
                </div>

                <div class="d-flex align-items-center gap-2 gap-lg-3">


                </div>
            </div>
        </div>

        <div class="post d-flex flex-column-fluid" id="kt_post">


            <div id="kt_content_container" class="container-xxl">
                {{-- @if (session()->has('successcreate'))
                    <div class="alert alert-dismissible bg-light-success d-flex flex-column flex-sm-row p-5 mb-10">
                        <div class="d-flex flex-column pe-0 pe-sm-10">
                            <h4 class="fw-semibold"> {{ session('successcreate') }} </h4>
                            <span>Berhasil Menambahkan Data!!</span>
                        </div>

                        <button type="button"
                            class="position-absolute position-sm-relative m-2 m-sm-0 top-0 end-0 btn btn-icon ms-sm-auto"
                            data-bs-dismiss="alert">
                            <span class="svg-icon svg-icon-1 svg-icon-primary">x</span>
                        </button>
                    </div>
                @endif

                @if (session()->has('successupdate'))
                    <div class="alert alert-dismissible bg-light-success d-flex flex-column flex-sm-row p-5 mb-10">
                        <div class="d-flex flex-column pe-0 pe-sm-10">
                            <h4 class="fw-semibold"> {{ session('successupdate') }} </h4>
                            <span>Berhasil Mengubah Data!!</span>
                        </div>

                        <button type="button"
                            class="position-absolute position-sm-relative m-2 m-sm-0 top-0 end-0 btn btn-icon ms-sm-auto"
                            data-bs-dismiss="alert">
                            <span class="svg-icon svg-icon-1 svg-icon-primary">x</span>
                        </button>
                    </div>
                @endif --}}




                <!--begin::Category-->
                <div class="card card-flush">
                    <!--begin::Card header-->
                    <div class="card-header align-items-center py-5 gap-2 gap-md-5">
                        <!--begin::Card title-->
                        <div class="card-title">
                            <!--begin::Search-->
                            <form method="GET" action="{{ url('rating/') }}">
                                <div class="d-flex align-items-center position-relative my-1">
                                    <span class="svg-icon svg-icon-1 position-absolute ms-4">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                            viewBox="0 0 24 24" fill="none">
                                            <rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546"
                                                height="2" rx="1" transform="rotate(45 17.0365 15.1223)"
                                                fill="black"></rect>
                                            <path
                                                d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                                                fill="black"></path>
                                        </svg>
                                    </span>
                                    <!--end::Svg Icon-->
                                    <input type="text" name="keyword" data-kt-ecommerce-category-filter="search"
                                        class="form-control form-control-solid w-250px ps-14" placeholder="Search Rating"
                                        value="{{ $keyword }}">
                                </div>
                            </form>

                            <!--end::Search-->
                        </div>

                        <!--end::Card title-->
                        <!--begin::Card toolbar-->
                        <div class="card-toolbar">
                            <!--begin::Add customer-->
                            {{-- <a href="{{ url('dashboard/kategori/create') }}" class="btn btn-primary"><i
                                    class="fa fa-plus"></i> Add Kategori</a> --}}
                            <!--end::Add customer-->
                        </div>
                        <!--end::Card toolbar-->
                    </div>
                    <!--end::Card header-->
                    <!--begin::Card body-->
                    <div class="card-body pt-0">
                        <!--begin::Table-->
                        <div id="kt_ecommerce_category_table_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                            <div class="table-responsive">
                                <table class="table align-middle table-row-dashed fs-6 gy-5 dataTable no-footer"
                                    id="kt_ecommerce_category_table">
                                    <!--begin::Table head-->
                                    <thead>
                                        <!--begin::Table row-->
                                        <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
                                            <th class="w-10px pe-2 sorting_disabled" rowspan="1" colspan="1"
                                                aria-label="
                                    
                                        
                                    
                                "
                                                style="width: 29.25px;">
                                                <div
                                                    class="form-check form-check-sm form-check-custom form-check-solid me-3">
                                                    NO
                                                </div>
                                            </th>
                                            <th class="min-w-250px sorting" tabindex="0"
                                                aria-controls="kt_ecommerce_category_table" rowspan="1" colspan="1"
                                                aria-label="Category: activate to sort column ascending"
                                                style="width: 629.453px;">Gambar</th>
                                            <th class="min-w-150px sorting text-center" tabindex="0"
                                                aria-controls="kt_ecommerce_category_table" rowspan="1" colspan="1"
                                                aria-label="Category Type: activate to sort column ascending"
                                                style="width: 167.594px;">Produk</th>
                                            <th class="min-w-150px sorting text-center" tabindex="0"
                                                aria-controls="kt_ecommerce_category_table" rowspan="1" colspan="1"
                                                aria-label="Category Type: activate to sort column ascending"
                                                style="width: 167.594px;">Komentar</th>
                                            <th class="min-w-150px sorting text-center" tabindex="0"
                                                aria-controls="kt_ecommerce_category_table" rowspan="1" colspan="1"
                                                aria-label="Category Type: activate to sort column ascending"
                                                style="width: 167.594px;">Total Rating</th>
                                            <th class="min-w-150px sorting text-center" tabindex="0"
                                                aria-controls="kt_ecommerce_category_table" rowspan="1" colspan="1"
                                                aria-label="Category Type: activate to sort column ascending"
                                                style="width: 167.594px;">Customer</th>
                                            <th class="text-end min-w-70px sorting_disabled text-center" rowspan="1"
                                                colspan="1" aria-label="Actions" style="width: 103.453px;">Actions</th>
                                        </tr>
                                        <!--end::Table row-->
                                    </thead>
                                    <!--end::Table head-->
                                    <!--begin::Table body-->
                                    <tbody class="fw-bold text-gray-600">
                                        @foreach ($rating as $key => $value)
                                            <tr class="odd">
                                                <!--begin::Checkbox-->
                                                <td>
                                                    <div
                                                        class="form-check form-check-sm form-check-custom form-check-solid">
                                                        {{ $rating->firstItem() + $key }}
                                                    </div>
                                                </td>
                                                <!--end::Checkbox-->
                                                <!--begin::Category=-->
                                                <td>
                                                    <div class="d-flex">
                                                        <!--begin::Thumbnail-->
                                                        <div class="symbol symbol-50px">
                                                        </div>
                                                        <!--end::Thumbnail-->
                                                        <div class="ms-5">
                                                            <!--begin::Title-->
                                                            <div class="text-gray-800 text-hover-primary fs-5 fw-bolder mb-1"
                                                                data-kt-ecommerce-category-filter="Kategori_name">
                                                                <img width="120px"
                                                                    src="{{ asset('storage/produk_rating/' . $value->gambar) }}"
                                                                    class="symbol-label">
                                                            </div>
                                                            <!--end::Title-->
                                                            <!--begin::Description-->

                                                            <!--end::Description-->
                                                        </div>
                                                    </div>
                                                </td>
                                                <!--end::Category=-->
                                                <!--begin::Type=-->
                                                <td class="text-center">{{ $value->produk->nm_produk }}</td>
                                                <td class="text-center">{{ $value->komentar }}</td>
                                                <td class="text-center">
                                                    @for ($i = 0; $i < $value->total_rating; $i++)
                                                        <svg style="color: yellow" width="24" height="24"
                                                            viewBox="0 0 24 24" fill="none"
                                                            xmlns="http://www.w3.org/2000/svg">
                                                            <path
                                                                d="M11.1359 4.48359C11.5216 3.82132 12.4784 3.82132 12.8641 4.48359L15.011 8.16962C15.1523 8.41222 15.3891 8.58425 15.6635 8.64367L19.8326 9.54646C20.5816 9.70867 20.8773 10.6186 20.3666 11.1901L17.5244 14.371C17.3374 14.5803 17.2469 14.8587 17.2752 15.138L17.7049 19.382C17.7821 20.1445 17.0081 20.7069 16.3067 20.3978L12.4032 18.6777C12.1463 18.5645 11.8537 18.5645 11.5968 18.6777L7.69326 20.3978C6.99192 20.7069 6.21789 20.1445 6.2951 19.382L6.7248 15.138C6.75308 14.8587 6.66264 14.5803 6.47558 14.371L3.63339 11.1901C3.12273 10.6186 3.41838 9.70867 4.16744 9.54646L8.3365 8.64367C8.61089 8.58425 8.84767 8.41222 8.98897 8.16962L11.1359 4.48359Z"
                                                                fill="currentColor"></path>
                                                        </svg>
                                                    @endfor
                                                </td>
                                                <td class="text-center">{{ $value->user->name }}</td>
                                                <!--end::Type=-->
                                                <!--begin::Action=-->
                                                <td class="text-end text-center">
                                                    <a href="#"
                                                        class="btn btn-sm btn-light btn-active-light-primary"
                                                        data-kt-menu-trigger="click"
                                                        data-kt-menu-placement="bottom-end">Actions
                                                        <!--begin::Svg Icon | path: icons/duotune/arrows/arr072.svg-->
                                                        <span class="svg-icon svg-icon-5 m-0">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24"
                                                                height="24" viewBox="0 0 24 24" fill="none">
                                                                <path
                                                                    d="M11.4343 12.7344L7.25 8.55005C6.83579 8.13583 6.16421 8.13584 5.75 8.55005C5.33579 8.96426 5.33579 9.63583 5.75 10.05L11.2929 15.5929C11.6834 15.9835 12.3166 15.9835 12.7071 15.5929L18.25 10.05C18.6642 9.63584 18.6642 8.96426 18.25 8.55005C17.8358 8.13584 17.1642 8.13584 16.75 8.55005L12.5657 12.7344C12.2533 13.0468 11.7467 13.0468 11.4343 12.7344Z"
                                                                    fill="black"></path>
                                                            </svg>
                                                        </span>
                                                        <!--end::Svg Icon-->
                                                    </a>
                                                    <!--begin::Menu-->
                                                    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-125px py-4"
                                                        data-kt-menu="true">
                                                        <!--begin::Menu item-->
                                                        <!--end::Menu item-->
                                                        <!--begin::Menu item-->
                                                        <div class="menu-item px-3">
                                                            <button type="submit"
                                                                class="btn btn-danger menu-link px-3 text-white hapus w-100 justify-content-center"
                                                                data-id="{{ $value->id }}"><i class="fas fa-trash"></i>
                                                                Delete</button>
                                                        </div>
                                                        <!--end::Menu item-->
                                                    </div>
                                                    <!--end::Menu-->
                                                </td>
                                        @endforeach
                                        <!--end::Action=-->
                                    </tbody>
                                    <!--end::Table body-->

                                    <!--begin::Table row-->

                                    <!--end::Table row-->
                                    <!--begin::Table row-->

                                    <!--end::Table row-->
                                    <!--begin::Table row-->

                                    <!--end::Table row-->
                                    <!--begin::Table row-->

                                    <!--end::Table row-->
                                    <!--begin::Table row-->

                                    <!--end::Table row-->
                                    <!--begin::Table row-->

                                    <!--end::Table row-->
                                    <!--begin::Table row-->

                                    <!--end::Table row-->
                                    <!--begin::Table row-->

                                    <!--end::Table row-->
                                    <!--begin::Table row-->

                                    <!--end::Table row-->
                                    <!--begin::Table row-->

                                    <!--end::Table row-->
                                    <!--begin::Table row-->

                                    <!--end::Table row-->
                                </table>
                                <p class="small text-muted">
                                    {!! __('Showing') !!}
                                    <span class="fw-semibold">{{ $rating->firstItem() }}</span>
                                    {!! __('to') !!}
                                    <span class="fw-semibold">{{ $rating->lastItem() }}</span>
                                    {!! __('of') !!}
                                    <span class="fw-semibold">{{ $rating->total() }}</span>
                                    {!! __('results') !!}
                                </p>
                                <div class="pull-right">

                                    {{ $rating->links() }}
                                </div>

                            </div>
                            <!--end::Card body-->
                        </div>
                        <!--end::Category-->
                    </div>
                    <!--end::Container-->
                </div>

            </div>
        @endsection

        @section('script')
            <script>
                $('table').on('click', '.hapus', function() {
                    var id = $(this).data('id')
                    Swal.fire({
                        title: 'Apakah Anda yakin ingin menghapus?',
                        showCancelButton: true,
                        confirmButtonText: 'Ya, Hapus',
                        showLoaderOnConfirm: true,
                        preConfirm: () => {
                            return $.ajax({
                                url: '{{ url('/ratingadmin/') }}/' + id,
                                method: 'POST',
                                headers: {
                                    'X-CSRF-TOKEN': '{{ @csrf_token() }}'
                                },
                                data: {
                                    _method: 'DELETE'
                                },
                                error: function() {
                                    Swal.showValidationMessage('Gagal Menghapus Data')
                                }
                            });
                        }
                    }).then(result => {
                        if (result.isConfirmed) {
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 1000,
                                timerProgressBar: true,
                                didOpen: (toast) => {
                                    toast.addEventListener('mouseenter', Swal.stopTimer)
                                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                                }
                            })

                            Toast.fire({
                                icon: 'success',
                                title: 'Hapus Berhasil'
                            }).then(() => {
                                window.location.href = "/rating"
                            })
                            // Swal.fire('Berhasil Menghapus Data', '', 'success')
                        }
                    })
                });
            </script>
            @if (session('successcreate'))
                <script>
                    const Toast = Swal.mixin({
                        toast: true,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 2000,
                        timerProgressBar: true,
                        didOpen: (toast) => {
                            toast.addEventListener('mouseenter', Swal.stopTimer)
                            toast.addEventListener('mouseleave', Swal.resumeTimer)
                        }
                    })

                    Toast.fire({
                        icon: 'success',
                        title: '{{ session('successcreate') }}'
                    }).then((result) => {
                        window.location.reload();
                    })
                </script>
            @endif
            @if (session('successupdate'))
                <script>
                    const Toast = Swal.mixin({
                        toast: true,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 2000,
                        timerProgressBar: true,
                        didOpen: (toast) => {
                            toast.addEventListener('mouseenter', Swal.stopTimer)
                            toast.addEventListener('mouseleave', Swal.resumeTimer)
                        }
                    })

                    Toast.fire({
                        icon: 'success',
                        title: '{{ session('successupdate') }}'
                    }).then((result) => {
                        window.location.reload();
                    })
                </script>
            @endif
        @endsection
