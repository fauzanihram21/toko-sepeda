@extends('dashboard.layouts.main')


@section('container')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="toolbar" id="kt_toolbar">
            <div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
                <div data-kt-swapper="true" data-kt-swapper-mode="prepend"
                    data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}"
                    class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
                    <h1 class="d-flex align-items-center text-dark fw-bolder fs-3 my-1">Pesanan Batal</h1>
                    <span class="h-20px border-gray-300 border-start mx-4"></span>
                </div>

                <div class="d-flex align-items-center gap-2 gap-lg-3">


                </div>
            </div>
        </div>

        <div id="kt_app_content" class="app-content flex-column-fluid">
            <!--begin::Content container-->
            <div id="kt_app_content_container" class="app-container container-xxl">

                <!--begin::Products-->
                <div class="card card-flush">
                    <!--begin::Card header-->
                    <div class="card-header align-items-center py-5 gap-2 gap-md-5">
                        <!--begin::Card title-->
                        <div class="card-title">
                            <form method="GET" action="{{ url('dashboard/pesananbatal/') }}">
                                <!--begin::Search-->
                                <div class="d-flex align-items-center position-relative my-1">
                                    <!--begin::Svg Icon | path: icons/duotune/general/gen021.svg-->
                                    <span class="svg-icon svg-icon-1 position-absolute ms-4">
                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                            xmlns="http://www.w3.org/2000/svg">
                                            <rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546"
                                                height="2" rx="1" transform="rotate(45 17.0365 15.1223)"
                                                fill="currentColor">
                                            </rect>
                                            <path
                                                d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                                                fill="currentColor"></path>
                                        </svg>
                                    </span>
                                    <!--end::Svg Icon-->
                                    <input type="text" name="keyword" data-kt-ecommerce-category-filter="search"
                                        class="form-control form-control-solid w-250px ps-14"
                                        placeholder="Search Pesanan Batal" value="{{ $keyword }}">
                                </div>
                            </form>
                        </div>
                        <!--end::Card title-->
                        <!--begin::Card toolbar-->
                        <div class="card-toolbar flex-row-fluid justify-content-end gap-5">

                            <!--begin::Add product-->

                            <!--end::Add product-->
                        </div>
                        <!--end::Card toolbar-->
                    </div>
                    <!--end::Card header-->
                    <!--begin::Card body-->
                    <div class="card-body pt-0">
                        <!--begin::Table-->
                        <div id="kt_ecommerce_products_table_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                            <div class="table-responsive">
                                <table class="table align-middle table-row-dashed fs-6 gy-5 dataTable no-footer"
                                    id="kt_ecommerce_products_table">
                                    <!--begin::Table head-->
                                    <thead>
                                        <!--begin::Table row-->
                                        <tr class="text-start text-gray-400 fw-bold fs-7 text-uppercase gs-0">
                                            <th class="w-10px pe-2 sorting_disabled" rowspan="1" colspan="1"
                                                aria-label="
                                        
                                            
                                        
                                    "
                                                style="width: 29.8906px;">
                                                <div
                                                    class="form-check form-check-sm form-check-custom form-check-solid me-3">
                                                    No
                                                </div>
                                            </th>
                                            <th class="min-w-200px sorting text-center" tabindex="0"
                                                aria-controls="kt_ecommerce_products_table" rowspan="1" colspan="1"
                                                aria-label="NO. PESANAN: activate to sort column ascending"
                                                style="width: 218.359px;">NO. PESANAN</th>
                                            <th class="text-end min-w-100px sorting text-center" tabindex="0"
                                                aria-controls="kt_ecommerce_products_table" rowspan="1" colspan="1"
                                                aria-label="PENERIMA: activate to sort column ascending"
                                                style="width: 109.609px;">PENERIMA</th>
                                            <th class="text-end min-w-70px sorting text-center" tabindex="0"
                                                aria-controls="kt_ecommerce_products_table" rowspan="1" colspan="1"
                                                aria-label="ALAMAT: activate to sort column ascending"
                                                style="width: 98.6719px;">ALAMAT</th>
                                            <th class="text-end min-w-100px sorting text-center" tabindex="0"
                                                aria-controls="kt_ecommerce_products_table" rowspan="1" colspan="1"
                                                aria-label="NO. INVOICE: activate to sort column ascending"
                                                style="width: 109.609px;">NO. INVOICE</th>
                                            <th class="text-end min-w-100px sorting text-center" tabindex="0"
                                                aria-controls="kt_ecommerce_products_table" rowspan="1" colspan="1"
                                                aria-label="TOTAL HARGA: activate to sort column ascending"
                                                style="width: 109.609px;">TOTAL HARGA</th>
                                            <th class="text-end min-w-70px sorting_disabled text-center" rowspan="1"
                                                colspan="1" aria-label="Actions" style="width: 105.391px;">Actions
                                            </th>
                                        </tr>
                                        <!--end::Table row-->
                                    </thead>
                                    <!--end::Table head-->
                                    <!--begin::Table body-->
                                    <tbody class="fw-semibold text-gray-600">

                                        @foreach ($pembayaran as $key => $value)
                                            <!--end::Table row-->
                                            <tr class="odd">
                                                <!--begin::Checkbox-->
                                                <td>
                                                    <div
                                                        class="form-check form-check-sm form-check-custom form-check-solid">
                                                        {{ $pembayaran->firstItem() + $key }}
                                                    </div>
                                                </td>
                                                <!--end::Checkbox-->
                                                <!--begin::Category=-->
                                                <td>
                                                    <div class="d-flex align-items-center">
                                                        <!--begin::Thumbnail-->

                                                        <!--end::Thumbnail-->
                                                        <div class="text-center mx-auto">
                                                            <!--begin::Title-->
                                                            <div class="text-gray-800 text-hover-primary fs-5 fw-bold"
                                                                data-kt-ecommerce-product-filter="product_name">
                                                                {{ $value->no_pesanan }}</div>
                                                            <!--end::Title-->
                                                        </div>
                                                    </div>
                                                </td>
                                                <!--end::Category=-->
                                                <!--begin::SKU=-->
                                                <td class="text-end text-center">
                                                    <span class="fw-bold">{{ $value->Pengiriman->nama_penerima }}</span>
                                                </td>
                                                <!--end::SKU=-->
                                                <!--begin::Qty=-->
                                                <td class="text-end text-center" data-order="10">
                                                    <span class="fw-bold ms-3">{{ $value->Pengiriman->alamat }}</span>
                                                </td>
                                                <!--end::Qty=-->
                                                <!--begin::Price=-->
                                                <td class="text-end text-center">{{ $value->no_invoice }}</td>
                                                <!--end::Price=-->
                                                <!--begin::Rating-->
                                                <td class="text-end text-center"> Rp.
                                                    {{ number_format($value->total_harga, 0, ',', '.') }}
                                                </td>
                                                <!--begin::Status=-->

                                                <!--end::Status=-->
                                                <!--begin::Action=-->
                                                <td class="text-end text-center">
                                                    <a href="#"
                                                        class="btn btn-sm btn-light btn-active-light-primary"
                                                        data-kt-menu-trigger="click"
                                                        data-kt-menu-placement="bottom-end">Actions
                                                        <!--begin::Svg Icon | path: icons/duotune/arrows/arr072.svg-->
                                                        <span class="svg-icon svg-icon-5 m-0">
                                                            <svg width="24" height="24" viewBox="0 0 24 24"
                                                                fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                    d="M11.4343 12.7344L7.25 8.55005C6.83579 8.13583 6.16421 8.13584 5.75 8.55005C5.33579 8.96426 5.33579 9.63583 5.75 10.05L11.2929 15.5929C11.6834 15.9835 12.3166 15.9835 12.7071 15.5929L18.25 10.05C18.6642 9.63584 18.6642 8.96426 18.25 8.55005C17.8358 8.13584 17.1642 8.13584 16.75 8.55005L12.5657 12.7344C12.2533 13.0468 11.7467 13.0468 11.4343 12.7344Z"
                                                                    fill="currentColor"></path>
                                                            </svg>
                                                        </span>
                                                        <!--end::Svg Icon-->
                                                    </a>
                                                    <!--begin::Menu-->
                                                    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-semibold fs-7 w-125px py-4"
                                                        data-kt-menu="true">
                                                        <!--begin::Menu item-->
                                                        <div class="menu-item px-3">
                                                            <a href="{{ url('dashboard/pesananbatal/' . $value->id . '/detail') }}"
                                                                type="button" class="btn-sm btn btn-primary w-100"
                                                                data-bs-toggle="modal"
                                                                data-bs-target="#modal-detail-{{ $value->id }}">
                                                                <i class="fas fa-eye"></i>
                                                                Detail
                                                            </a>
                                                        </div>

                                                    </div>
                                                    <!--end::Menu-->
                                                </td>
                                                <!--end::Action=-->
                                            </tr>
                                        @endforeach

                                    </tbody>
                                    <!--end::Table body-->
                                </table>

                            </div>
                            <p class="small text-muted">
                                {!! __('Showing') !!}
                                <span class="fw-semibold">{{ $pembayaran->firstItem() }}</span>
                                {!! __('to') !!}
                                <span class="fw-semibold">{{ $pembayaran->lastItem() }}</span>
                                {!! __('of') !!}
                                <span class="fw-semibold">{{ $pembayaran->total() }}</span>
                                {!! __('results') !!}
                            </p>
                            <div class="pull-right">

                                {{ $pembayaran->links() }}
                            </div>
                            @foreach ($pembayaran as $value)
                                <div class="modal fade" tabindex="-1" id="modal-detail-{{ $value->id }}">
                                    <div class="modal-dialog modal-xl">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h3 class="modal-title">Pesanan</h3>


                                                <!--begin::Close-->
                                                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2"
                                                    data-bs-dismiss="modal" aria-label="Close">
                                                    <span class="svg-icon svg-icon-1"></span>
                                                </div>
                                                <!--end::Close-->
                                            </div>

                                            <div class="modal-body">
                                                <div>
                                                    <div id="kt_app_content" class="app-content flex-column-fluid">
                                                        <!--begin::Content container-->
                                                        <div id="kt_app_content_container"
                                                            class="app-container container-xxl">
                                                            <!-- begin::Invoice 3-->
                                                            <div class="card">
                                                                <!-- begin::Body-->
                                                                <div class="card-body py-20">
                                                                    <!-- begin::Wrapper-->
                                                                    <div class="mw-lg-950px mx-auto w-100">
                                                                        <!-- begin::Header-->
                                                                        <div
                                                                            class="d-flex justify-content-between flex-column flex-sm-row mb-19">

                                                                            <!--end::Logo-->
                                                                            <div class="text-sm-end">
                                                                                <!--begin::Logo-->
                                                                                <a href="#"
                                                                                    class="d-block mw-150px ms-sm-auto">
                                                                                    <img src="{{ asset('storage/logo_dalam/' . $setting->logo_dalam) }}"
                                                                                        class="symbol-label w-100">
                                                                                </a>



                                                                                <!--end::Logo-->
                                                                                <!--begin::Text-->

                                                                                <!--end::Text-->
                                                                            </div>
                                                                        </div>
                                                                        <!--end::Header-->
                                                                        <!--begin::Body-->
                                                                        <div class="pb-12">
                                                                            <!--begin::Wrapper-->
                                                                            <div
                                                                                class="d-flex flex-column gap-7 gap-md-10">
                                                                                <!--begin::Message-->
                                                                                <div class="fw-bold fs-2">

                                                                                    {{ $value->user->name }}
                                                                                    <span
                                                                                        class="fs-6">({{ $value->user->email }})</span>,



                                                                                    <br>
                                                                                    <span class="text-muted fs-5">
                                                                                        <span>{{ $value->Pengiriman->no_telp }}</span>

                                                                                    </span>

                                                                                </div>
                                                                                <span class="badge badge-light-success">

                                                                                    @if ($value->status == 'menunggu_pembayaran')
                                                                                        <span
                                                                                            class="badge badge-light-dark">Menunggu
                                                                                            Pembayaran</span>
                                                                                    @elseif($value->status == 'pesanan_diterima')
                                                                                        <span
                                                                                            class="badge badge-light-primary">Pesanan
                                                                                            DiTerima</span>
                                                                                    @elseif($value->status == 'diproses')
                                                                                        <span
                                                                                            class="badge badge-light-info">Pesanan
                                                                                            DiProses</span>
                                                                                    @elseif($value->status == 'sedang_dikirim')
                                                                                        <span
                                                                                            class="badge badge-light-warning">Pesanan
                                                                                            DiKirim</span>
                                                                                    @elseif($value->status == 'selesai')
                                                                                        <span
                                                                                            class="badge badge-light-success">Pesanan
                                                                                            Selesai</span>
                                                                                    @elseif($value->status == 'cancel')
                                                                                        <span
                                                                                            class="badge badge-light-info">Pesanan
                                                                                            DiBatalkan</span>
                                                                                    @endif
                                                                                </span>
                                                                                <!--begin::Message-->
                                                                                <!--begin::Separator-->
                                                                                <div class="separator"></div>
                                                                                <!--begin::Separator-->
                                                                                <!--begin::Order details-->
                                                                                <div
                                                                                    class="d-flex flex-column flex-sm-row gap-7 gap-md-10 fw-bold">
                                                                                    <div
                                                                                        class="flex-root d-flex flex-column">
                                                                                        <span class="text-muted">Nomer
                                                                                            Pesanan</span>
                                                                                        <span class="fs-5">
                                                                                            {{ $value->no_pesanan }}
                                                                                        </span>
                                                                                    </div>
                                                                                    <div
                                                                                        class="flex-root d-flex flex-column">
                                                                                        <span class="text-muted">Tanggal
                                                                                            Pembayaran</span>
                                                                                        <span
                                                                                            class="fs-5">{{ $value->created_at }}</span>
                                                                                    </div>
                                                                                    <div
                                                                                        class="flex-root d-flex flex-column">
                                                                                        <span class="text-muted">Nomer
                                                                                            Invoice</span>
                                                                                        <span
                                                                                            class="fs-5">{{ $value->no_invoice }}</span>
                                                                                    </div>
                                                                                    <div
                                                                                        class="flex-root d-flex flex-column">
                                                                                        <span class="text-muted">Nama
                                                                                            Penerima</span>
                                                                                        <span
                                                                                            class="fs-5">{{ $value->Pengiriman->nama_penerima }}</span>
                                                                                    </div>
                                                                                    <div
                                                                                        class="flex-root d-flex flex-column">
                                                                                        <span
                                                                                            class="text-muted">Pengiriman</span>
                                                                                        <span
                                                                                            class="fs-5">{{ $value->Pengiriman->nm_ekspedisi }}
                                                                                            -
                                                                                            {{ $value->Pengiriman->paket_layanan }}</span>
                                                                                    </div>

                                                                                </div>
                                                                                <!--end::Order details-->
                                                                                <!--begin::Billing & shipping-->
                                                                                <div
                                                                                    class="d-flex flex-column flex-sm-row gap-7 gap-md-10 fw-bold">
                                                                                    <div
                                                                                        class="flex-root d-flex flex-column">
                                                                                        <span class="text-muted">Alamat
                                                                                            Penerima</span>
                                                                                        <span
                                                                                            class="fs-6">{{ $value->Pengiriman->alamat }}
                                                                                            <br>Kode Pos
                                                                                            :
                                                                                            {{ $value->Pengiriman->kode_pos }}
                                                                                            <br>Provinsi :
                                                                                            {{ $value->Pengiriman->provinsi->nm_provinsi }}
                                                                                            <br>Kota :
                                                                                            {{ $value->Pengiriman->kota->nm_kota }}</span>
                                                                                    </div>

                                                                                </div>
                                                                                <!--end::Billing & shipping-->
                                                                                <!--begin:Order summary-->
                                                                                <div
                                                                                    class="d-flex justify-content-between flex-column">
                                                                                    <!--begin::Table-->
                                                                                    <div
                                                                                        class="table-responsive border-bottom mb-9">
                                                                                        <table
                                                                                            class="table align-middle table-row-dashed fs-6 gy-5 mb-0">
                                                                                            <thead>
                                                                                                <tr
                                                                                                    class="border-bottom fs-6 fw-bold text-muted">
                                                                                                    <th
                                                                                                        class="min-w-175px pb-2">
                                                                                                        Produk</th>
                                                                                                    <th
                                                                                                        class="min-w-70px text-end pb-2">
                                                                                                        Harga</th>
                                                                                                    <th
                                                                                                        class="min-w-80px text-end pb-2">
                                                                                                        Kuantitas</th>
                                                                                                    <th
                                                                                                        class="min-w-100px text-end pb-2">
                                                                                                        Total</th>
                                                                                                </tr>
                                                                                            </thead>
                                                                                            <tbody
                                                                                                @foreach ($value->Pesanan as $item) 
                                                                                            <!--begin::Products-->
                                                                                            <tr>
                                                                                                <!--begin::Product-->
                                                                                                <td>
                                                                                                    <div
                                                                                                        class="d-flex align-items-center">
                                                                                                        <!--begin::Thumbnail-->
                                                                                                        <a href="/metronic8/demo1/../demo1/apps/ecommerce/catalog/edit-product.html"
                                                                                                            class="symbol symbol-50px">
                                                                                                            @foreach ($item->Produk->ProdukVariasi as $g)
                                                                                                                
                                                                                                            <img src="{{ asset('storage/produk_variasi/' . $g->gambar) }}"
                                                                                                            class="symbol-label"> @endforeach
                                                                                                </a>
                                                                                                <!--end::Thumbnail-->
                                                                                                <!--begin::Title-->
                                                                                                <div class="ms-5">
                                                                                                    <div class="fw-bold">
                                                                                                        {{ $item->Produk->nm_produk }}
                                                                                                    </div>
                                                                                                    <div
                                                                                                        class="fs-7 text-muted">

                                                                                                    </div>
                                                                                                </div>
                                                                                                <!--end::Title-->
                                                                                    </div>
                                                                                    </td>
                                                                                    <!--end::Product-->
                                                                                    <!--begin::SKU-->
                                                                                    <td class="text-end">
                                                                                        Rp.
                                                                                        {{ number_format($item->sub_total / $item->kuantitas, 0, ',', '.') }}


                                                                                    </td>
                                                                                    <!--end::SKU-->
                                                                                    <!--begin::Quantity-->
                                                                                    <td class="text-end">
                                                                                        {{ $item->kuantitas }}
                                                                                    </td>
                                                                                    <!--end::Quantity-->
                                                                                    <!--begin::Total-->
                                                                                    <td class="text-end">
                                                                                        Rp.
                                                                                        {{ number_format($item->sub_total, 0, ',', '.') }}


                                                                                    </td>
                                                                                    <!--end::Total-->
                                                                                    </tr>

                                                                                    <!--end::Products-->
                                                                                    <!--begin::Subtotal-->

                                                                                    <!--end::Grand total-->
                            @endforeach
                            <tr>
                                <td colspan="3" class="text-end">
                                    Ongkir
                                </td>
                                <td class="text-end">
                                    Rp.
                                    {{ number_format($value->Pengiriman->ongkir, 0, ',', '.') }}


                                </td>
                            </tr>
                            <tr>
                                <!--end::Shipping-->
                                <!--begin::Grand total-->
                            <tr>
                                <td colspan="3" class="fs-3 text-dark fw-bold text-end">
                                    Total Harga</td>
                                <td class="text-dark fs-3 fw-bolder text-end">
                                    Rp.
                                    {{ number_format($value->total_harga, 0, ',', '.') }}
                                </td>
                            </tr>
                            </tbody>
                            </table>
                        </div>
                        <!--end::Table-->
                    </div>
                    <!--end:Order summary-->
                </div>
            </div>
            <!--end::Wrapper-->
        </div>
        <!--end::Body-->
        <!-- begin::Footer-->

        <!-- end::Footer-->
    </div>
    <!-- end::Wrapper-->
    </div>
    <!-- end::Body-->
    </div>
    <!-- end::Invoice 1-->
    </div>
    <!--end::Content container-->
    </div>
    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
    </div>
    </div>
    </div>
    </div>
    @endforeach

    </div>
    <!--end::Table-->
    </div>
    <!--end::Card body-->
    </div>
    <!--end::Products-->
    </div>
    <!--end::Content container-->
    </div>
@endsection
