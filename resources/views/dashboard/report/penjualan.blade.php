@extends('dashboard.layouts.main')


@section('container')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="toolbar" id="kt_toolbar">
            <div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
                <div data-kt-swapper="true" data-kt-swapper-mode="prepend"
                    data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}"
                    class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
                    <h1 class="d-flex align-items-center text-dark fw-bolder fs-3 my-1">Laporan Penjualan</h1>
                    <span class="h-20px border-gray-300 border-start mx-4"></span>
                </div>

                <div class="d-flex align-items-center gap-2 gap-lg-3">


                </div>
            </div>
        </div>

        <div class="post d-flex flex-column-fluid" id="kt_post">


            <div id="kt_app_content" class="app-content flex-column-fluid">
                <!--begin::Content container-->
                <div id="kt_app_content_container" class="app-container container-xxl">
                    <!--begin::Products-->
                    <div class="card card-flush">
                        <!--begin::Card header-->
                        <div class="card-header align-items-center py-5 gap-2 gap-md-5"
                            data-select2-id="select2-data-124-i3li">
                            <!--begin::Card title-->
                            <div class="card-title">
                                <!--begin::Search-->
                                <form method="GET" action="{{ url('dashboard/report/penjualan/') }}">
                                    <div class="d-flex align-items-center position-relative my-1">
                                        <!--begin::Svg Icon | path: icons/duotune/general/gen021.svg-->
                                        <span class="svg-icon svg-icon-1 position-absolute ms-4">
                                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                xmlns="http://www.w3.org/2000/svg">
                                                <rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546"
                                                    height="2" rx="1" transform="rotate(45 17.0365 15.1223)"
                                                    fill="currentColor"></rect>
                                                <path
                                                    d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                                                    fill="currentColor"></path>
                                            </svg>
                                        </span>
                                        <!--end::Svg Icon-->
                                        <input type="text" name="keyword" data-kt-ecommerce-category-filter="search"
                                            class="form-control form-control-solid w-250px ps-14"
                                            placeholder="Search Laporan Penjualan" value="{{ $keyword }}">
                                    </div>
                                </form>
                                <!--end::Search-->
                                <!--begin::Export buttons-->
                                <div id="kt_ecommerce_report_views_export" class="d-none">
                                    <div class="dt-buttons btn-group flex-wrap"> <button
                                            class="btn btn-secondary buttons-copy buttons-html5" tabindex="0"
                                            aria-controls="kt_ecommerce_report_views_table"
                                            type="button"><span>Copy</span></button> <button
                                            class="btn btn-secondary buttons-excel buttons-html5" tabindex="0"
                                            aria-controls="kt_ecommerce_report_views_table"
                                            type="button"><span>Excel</span></button> <button
                                            class="btn btn-secondary buttons-csv buttons-html5" tabindex="0"
                                            aria-controls="kt_ecommerce_report_views_table"
                                            type="button"><span>CSV</span></button> <button
                                            class="btn btn-secondary buttons-pdf buttons-html5" tabindex="0"
                                            aria-controls="kt_ecommerce_report_views_table"
                                            type="button"><span>PDF</span></button> </div>
                                </div>
                                <!--end::Export buttons-->
                            </div>
                            <!--end::Card title-->
                            <!--begin::Card toolbar-->
                            <div class="card-toolbar flex-row-fluid justify-content-end gap-5"
                                data-select2-id="select2-data-123-k3cj">
                                <!--begin::Daterangepicker-->
                                <input class="form-control form-control-solid w-100 mw-250px" placeholder="Pick a date"
                                    id="kt_datepicker_1" name="date" />
                                <!--end::Daterangepicker-->
                                <!--begin::Filter-->

                                <!--end::Filter-->
                                <!--begin::Export dropdown-->
                                <button type="button" class="btn btn-light-primary" data-kt-menu-trigger="click"
                                    data-kt-menu-placement="bottom-end">
                                    <!--begin::Svg Icon | path: icons/duotune/arrows/arr078.svg-->
                                    <span class="svg-icon svg-icon-2">
                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                            xmlns="http://www.w3.org/2000/svg">
                                            <rect opacity="0.3" x="12.75" y="4.25" width="12" height="2"
                                                rx="1" transform="rotate(90 12.75 4.25)" fill="currentColor"></rect>
                                            <path
                                                d="M12.0573 6.11875L13.5203 7.87435C13.9121 8.34457 14.6232 8.37683 15.056 7.94401C15.4457 7.5543 15.4641 6.92836 15.0979 6.51643L12.4974 3.59084C12.0996 3.14332 11.4004 3.14332 11.0026 3.59084L8.40206 6.51643C8.0359 6.92836 8.0543 7.5543 8.44401 7.94401C8.87683 8.37683 9.58785 8.34458 9.9797 7.87435L11.4427 6.11875C11.6026 5.92684 11.8974 5.92684 12.0573 6.11875Z"
                                                fill="currentColor"></path>
                                            <path opacity="0.3"
                                                d="M18.75 8.25H17.75C17.1977 8.25 16.75 8.69772 16.75 9.25C16.75 9.80228 17.1977 10.25 17.75 10.25C18.3023 10.25 18.75 10.6977 18.75 11.25V18.25C18.75 18.8023 18.3023 19.25 17.75 19.25H5.75C5.19772 19.25 4.75 18.8023 4.75 18.25V11.25C4.75 10.6977 5.19771 10.25 5.75 10.25C6.30229 10.25 6.75 9.80228 6.75 9.25C6.75 8.69772 6.30229 8.25 5.75 8.25H4.75C3.64543 8.25 2.75 9.14543 2.75 10.25V19.25C2.75 20.3546 3.64543 21.25 4.75 21.25H18.75C19.8546 21.25 20.75 20.3546 20.75 19.25V10.25C20.75 9.14543 19.8546 8.25 18.75 8.25Z"
                                                fill="currentColor"></path>
                                        </svg>
                                    </span>
                                    <!--end::Svg Icon-->Export Report
                                </button>
                                <!--begin::Menu-->
                                <div id="kt_ecommerce_report_views_export_menu"
                                    class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-semibold fs-7 w-200px py-4"
                                    data-kt-menu="true" style="">
                                    <!--begin::Menu item-->

                                    <!--end::Menu item-->
                                    <!--begin::Menu item-->
                                    <div class="menu-item px-3">
                                        <button type="button" class="btn btn-success mb-2 w-100" data-bs-toggle="modal"
                                            data-bs-target="#excel">
                                            Download Excel
                                        </button>
                                    </div>


                                    <!--end::Menu item-->
                                    <!--begin::Menu item-->

                                    <!--end::Menu item-->
                                    <!--begin::Menu item-->
                                    <div class="menu-item px-3">
                                        <button type="button" class="btn btn-danger w-100" data-bs-toggle="modal"
                                            data-bs-target="#pdf">
                                            Download Pdf
                                        </button>
                                    </div>
                                    <!--end::Menu item-->
                                </div>
                                <!--end::Menu-->
                                <!--end::Export dropdown-->
                            </div>
                            <!--end::Card toolbar-->
                        </div>
                        <!--end::Card header-->
                        <!--begin::Card body-->
                        <div class="card-body pt-0">
                            <!--begin::Table-->
                            <div id="kt_ecommerce_report_views_table_wrapper"
                                class="dataTables_wrapper dt-bootstrap4 no-footer">
                                <div class="table-responsive">
                                    <table class="table align-middle table-row-dashed fs-6 gy-5 dataTable no-footer"
                                        id="kt_ecommerce_report_views_table">
                                        <!--begin::Table head-->
                                        <thead>
                                            <!--begin::Table row-->
                                            <tr class="text-start text-gray-400 fw-bold fs-7 text-uppercase gs-0">
                                                <th class="w-10px pe-2 sorting_disabled" rowspan="1" colspan="1"
                                                    aria-label="" style="width: 29.8906px;">
                                                    <div
                                                        class="form-check form-check-sm form-check-custom form-check-solid me-3">
                                                        No
                                                    </div>
                                                </th>
                                                <th class="min-w-200px  sorting" tabindex="0"
                                                    aria-controls="kt_ecommerce_report_views_table" rowspan="1"
                                                    colspan="1" aria-label="TANGGAL: activate to sort column ascending"
                                                    style="width: 165.047px;">TANGGAL</th>
                                                <th class="text-end min-w-100px sorting text-center" tabindex="0"
                                                    aria-controls="kt_ecommerce_report_views_table" rowspan="1"
                                                    colspan="1"
                                                    aria-label="NO. PESANAN: activate to sort column ascending"
                                                    style="width: 147.109px;">NO. PESANAN</th>

                                                <th class="text-end min-w-100px sorting text-center" tabindex="0"
                                                    aria-controls="kt_ecommerce_report_views_table" rowspan="1"
                                                    colspan="1" aria-label="TOTAL: activate to sort column ascending"
                                                    style="width: 147.109px;">TOTAL</th>
                                            </tr>
                                            <!--end::Table row-->
                                        </thead>
                                        <!--end::Table head-->
                                        <!--begin::Table body-->
                                        <tbody class="fw-semibold text-gray-600">
                                            @foreach ($penjualan as $key => $value)
                                                <tr class="odd">
                                                    <td>
                                                        <div
                                                            class="form-check form-check-sm form-check-custom form-check-solid">
                                                            {{ $penjualan->firstItem() + $key }}
                                                        </div>
                                                    </td>
                                                    <!--begin::Product=-->
                                                    <td>
                                                        <div class="d-flex align-items-center">
                                                            <!--begin::Thumbnail-->
                                                            <a href="/metronic8/demo1/../demo1/apps/ecommerce/catalog/edit-product.html"
                                                                class="symbol symbol-50px">
                                                                {{-- <img src="{{ asset('foto_sepeda/' . $value->produkvariasi->gambar) }}"
                                                                    class="symbol-label"> --}}
                                                            </a>
                                                            <!--end::Thumbnail-->
                                                            <div class="ms-5">
                                                                <!--begin::Title-->
                                                                <div href="/metronic8/demo1/../demo1/apps/ecommerce/catalog/edit-product.html"
                                                                    class="text-gray-800 text-hover-primary fs-5 fw-bold"
                                                                    data-kt-ecommerce-product-filter="product_name">
                                                                    {{ Carbon\Carbon::parse($value->created_at)->format('d-M-Y') }}
                                                                </div>
                                                                <!--end::Title-->
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <!--end::Product=-->
                                                    <!--begin::SKU=-->
                                                    <td class="text-end pe-0 text-center">
                                                        <span class="fw-bold">{{ $value->no_pesanan }}</span>
                                                    </td>
                                                    <!--end::SKU=-->
                                                    <!--begin::Rating-->

                                                    <!--end::Rating-->
                                                    <!--begin::Price=-->
                                                    <td
                                                        class="text-end
                                                        pe-0 text-center">
                                                        @php
                                                            $subtotal = 0;
                                                        @endphp
                                                        @foreach ($value->Pesanan as $psn)
                                                            @php
                                                                $subtotal += $psn->sub_total;
                                                            @endphp
                                                        @endforeach

                                                        <span class="fw-bold">
                                                            Rp. {{ number_format($subtotal, 0, ',', '.') }}

                                                        </span>
                                                    </td>
                                                    <!--end::Price=-->
                                                    <!--begin::Percent=-->

                                                    <!--end::Percent=-->
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <p class="small text-muted">
                                    {!! __('Showing') !!}
                                    <span class="fw-semibold">{{ $penjualan->firstItem() }}</span>
                                    {!! __('to') !!}
                                    <span class="fw-semibold">{{ $penjualan->lastItem() }}</span>
                                    {!! __('of') !!}
                                    <span class="fw-semibold">{{ $penjualan->total() }}</span>
                                    {!! __('results') !!}
                                </p>
                                <div class="pull-right">

                                    {{ $penjualan->links() }}
                                </div>
                            </div>
                            <!--end::Table-->
                        </div>
                        <!--end::Card body-->
                    </div>
                    <!--end::Products-->
                </div>
                <!--end::Content container-->
            </div>
            <div class="modal fade" tabindex="-1" id="excel">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h3 class="modal-title">Excel Laporan Penjualan</h3>

                            <!--begin::Close-->
                            <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal"
                                aria-label="Close">
                                <span class="svg-icon svg-icon-1"></span>
                            </div>
                            <!--end::Close-->
                        </div>

                        <div class="modal-body">
                            <form action="{{ url('/dashboard/report/penjualan/excelhari') }}" method="POST"
                                class="row">
                                @csrf
                                <div class="col-8 mb-10">
                                    <label for="" class="form-label">Laporan Pertanggal</label>
                                    <input class="form-control" name="date" placeholder="Pick date"
                                        id="kt_datepicker_10" />
                                </div>
                                <div class="col-4 mb-4">
                                    <button type="submit" class="btn btn-primary mt-9">
                                        Download
                                    </button>
                                </div>
                            </form>

                            <form action="{{ url('/dashboard/report/penjualan/excelbulantahun') }}" method="POST"
                                class="row mt-3">
                                @csrf
                                <div class="col-4 mb-4">
                                    <div class="form-group">
                                        <label>Laporan Bulan</label>
                                        <select class="form-select" data-placeholder="Pilih Kategori" name="month">
                                            <option></option>
                                            <option value="1">Januari</option>
                                            <option value="2">Februari</option>
                                            <option value="3">Maret</option>
                                            <option value="4">April</option>
                                            <option value="5">Mei</option>
                                            <option value="6">Juni</option>
                                            <option value="7">Juli</option>
                                            <option value="8">Agustus</option>
                                            <option value="9">September</option>
                                            <option value="10">Oktober</option>
                                            <option value="11">November</option>
                                            <option value="12">Desember</option>
                                        </select>
                                        <!-- <input type="text" class="form-control"> -->
                                    </div>
                                </div>
                                <div class="col-4 mb-4">
                                    <div class="form-group">
                                        <label>Tahun</label>
                                        <select class="form-select" data-placeholder="Pilih Kategori" name="year">
                                            {{ $last = date('Y') - 3 }}
                                            {{ $now = date('Y') + 1 }}
                                            <option></option>
                                            @for ($i = $now; $i >= $last; $i--)
                                                <option value="{{ $i }}">{{ $i }}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                                <div class="col-4 mb-4">
                                    <button type="submit" class="btn btn-primary mt-6">
                                        Download
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>



            <div class="modal fade" tabindex="-1" id="pdf">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h3 class="modal-title">Pdf Laporan Penjualan</h3>

                            <!--begin::Close-->
                            <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal"
                                aria-label="Close">
                                <span class="svg-icon svg-icon-1"></span>
                            </div>
                            <!--end::Close-->
                        </div>

                        <div class="modal-body">
                            <form action="{{ url('/dashboard/report/penjualan/pdfhari') }}" method="POST"
                                class="row">
                                @csrf
                                <div class="col-8 mb-10">
                                    <label for="" class="form-label">Laporan Pertanggal</label>
                                    <input class="form-control" name="date" placeholder="Pick date"
                                        id="kt_datepicker_11" />
                                </div>
                                <div class="col-4 mb-4">
                                    <button type="submit" class="btn btn-primary mt-9">
                                        Download
                                    </button>
                                </div>
                            </form>

                            <form action="{{ url('/dashboard/report/penjualan/pdfbulantahun') }}" method="POST"
                                class="row mt-3">
                                @csrf
                                <div class="col-4 mb-4">
                                    <div class="form-group">
                                        <label>Laporan Bulan</label>
                                        <select class="form-select" data-placeholder="Pilih Kategori" name="month">
                                            <option></option>
                                            <option value="1">Januari</option>
                                            <option value="2">Februari</option>
                                            <option value="3">Maret</option>
                                            <option value="4">April</option>
                                            <option value="5">Mei</option>
                                            <option value="6">Juni</option>
                                            <option value="7">Juli</option>
                                            <option value="8">Agustus</option>
                                            <option value="9">September</option>
                                            <option value="10">Oktober</option>
                                            <option value="11">November</option>
                                            <option value="12">Desember</option>
                                        </select>
                                        <!-- <input type="text" class="form-control"> -->
                                    </div>
                                </div>
                                <div class="col-4 mb-4">
                                    <div class="form-group">
                                        <label>Tahun</label>
                                        <select class="form-select" data-placeholder="Pilih Kategori" name="year">
                                            {{ $last = date('Y') - 3 }}
                                            {{ $now = date('Y') + 1 }}
                                            <option></option>
                                            @for ($i = $now; $i >= $last; $i--)
                                                <option value="{{ $i }}">{{ $i }}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                                <div class="col-4 mb-4">
                                    <button type="submit" class="btn btn-primary mt-6">
                                        Download
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        @endsection

        @section('script')
            <script>
                $("#kt_datepicker_1").flatpickr({
                    defaultDate: "{{ request()->date }}",
                    onChange: function(date, dateStr) {
                        window.location.href = "{{ url('/dashboard/report/penjualan') }}/?date=" + dateStr;
                    }
                });
                $("#kt_datepicker_10").flatpickr();
                $("#kt_datepicker_11").flatpickr();
                $("#kt_daterangepicker_pdf").daterangepicker();
            </script>
        @endsection
